# Introdução

Os casos do modelo Bid Based Dispatch foram montados a partir de bases SDDP. Dessa forma, há vários parâmetros abaixo que não são considerados pelo modelo. Alguns destes estão com a descrição em branco. 

# Sistema

Arquivo ASCII localizado no diretório de dados do estudo. Este arquivo denominado sistem.DAT contém a identificação dos sistemas do caso.

O primeiro registro indica a versão do arquivo. Neste momento, ele deverá vir com o texto "$version 1". O segundo registro é um header. Os demais têm formato CSV (separados por vírgula):

| Column | Type | Description |
| ----------- | ----------- | ----------- |
| Num | inteiro | Número do sistema |
| Nome | string | Nome do sistema |
| ID | string | ID do sistema, aparece como "XX" no nome de arquivos que dependem do sistema. |
| UnM | string |  |

# Estação hidrológica

Arquivo ASCII localizado no diretório de dados do estudo. Este arquivo denominado htopol_w.DAT contém a identificação das estações hidrológicas do caso.

O primeiro registro indica a versão do arquivo. Neste momento, ele deverá vir com o texto "$version 1". O segundo registro é um header. Os demais têm formato CSV (separados por vírgula):

| Column | Type | Description |
| ----------- | ----------- | ----------- |
| Contador | inteiro |  |
| Num | inteiro | Código da estação |
| Nome | string | Nome da estação |
| NumJus | inteiro | Código das estações a jusante |

# Usina hidrelétrica

Arquivo ASCII localizado no diretório de dados do estudo. Este arquivo denominado chidroXX.DAT contém os parâmetros das usinas hidrelétricas de cada sistema XX.

O primeiro registro indica a versão do arquivo. Neste momento, ele deverá vir com o texto "$version 1". O segundo registro é um header. Os demais têm formato CSV (separados por vírgula):

| Column | Type | Description |
| ----------- | ----------- | ----------- |
| Num | inteiro | Código da usina |
| Nome | string | Nome da usina |
| PV | inteiro |  |
| VAA | inteiro |  |
| TAA | inteiro |  |
| Unidades | inteiro | Número de unidades geradoras |
| Existing | inteiro | Estado |
| PotInst | real | Capacidade Instalada |
| FPMed | real | Coeficiente médio de produção |
| Qmin | real | Representa a mínima vazão turbinável da usina, que pode ser necessária para evitar problemas nas turbinas ou outras restrições operativas. |
| Qmax | real | Representa a máxima vazão turbinável da usina |
| Vmin | real | Capacidade de armazenamento mínimo do reservatório |
| Vmax | real | Capacidade de armazenamento máximo do reservatório |
| Vinic | real | Quantidade de água armazenada no reservatório da usina hidro ao início do estudo |
| DfTotMin | real |  |
| VNC | inteiro | Se selecionado, o vertimento é uma variável de controle e permite que a usina verta em qualquer nível de armazenamento do reservatório. Se não selecionada, o vertimento não é uma variável de controle, e só é permitido quando o reservatório está em seu volume máximo. |
| ICP | real | Representa o efeito das falhas aleatórias do equipamento em sua capacidade de produção |
| IH | real | Representa o efeito conjunto da manutenção e da saída forçada do equipamento na capacidade de produção da usina |
| FP(1) | real |  |
| FP.VOL(1) | real |  |
| FP(2) | real |  |
| FP.VOL(2) | real |  |
| FP(3) | real |  |
| FP.VOL(3) | real |  |
| FP(4) | real |  |
| FP.VOL(4) | real |  |
| FP(5) | real |  |
| FP.VOL(5) | real |  |
| SxA_Area(1) | real |  |
| SxA_Storage(1) | real |  |
| SxA_Area(2) | real |  |
| SxA_Storage(2) | real |  |
| SxA_Area(3) | real |  |
| SxA_Storage(3) | real |  |
| SxA_Area(4) | real |  |
| SxA_Storage(4) | real |  |
| SxA_Area(5) | real |  |
| SxA_Storage(5) | real |  |
| SxF_Filtration(1) | real |  |
| SxF_Storage(1) | real |  |
| SxF_Filtration(2) | real |  |
| SxF_Storage(2) | real |  |
| SxF_Filtration(3) | real |  |
| SxF_Storage(3) | real |  |
| SxF_Filtration(4) | real |  |
| SxF_Storage(4) | real |  |
| SxF_Filtration(5) | real |  |
| SxF_Storage(5) | real |  |
| SxH_Head(1) | real |  |
| SxH_Storage(1) | real |  |
| SxH_Head(2) | real |  |
| SxH_Storage(2) | real |  |
| SxH_Head(3) | real |  |
| SxH_Storage(3) | real |  |
| SxH_Head(4) | real |  |
| SxH_Storage(4) | real |  |
| SxH_Head(5) | real |  |
| SxH_Storage(5) | real |  |
| Evap(1) | real |  |
| Evap(2) | real |  |
| Evap(3) | real |  |
| Evap(4) | real |  |
| Evap(5) | real |  |
| Evap(6) | real |  |
| Evap(7) | real |  |
| Evap(8) | real |  |
| Evap(9) | real |  |
| Evap(10) | real |  |
| Evap(11) | real |  |
| Evap(12) | real |  |
| VinicType | inteiro | Tipo de condição inicial0 = volume (pu)1 = cota (m) |
| FAAType | inteiro |  |
| FAA | inteiro |  |
| AreaType | inteiro |  |
| AreaConst | real | Valor constante de área para a recursão backward ou para usinas fio d'água |
| FiltType | inteiro | 0= infiltra para a central a jusante para turbinamento1=infiltra para a central a jusante para infiltração |
| FiltConst | real | Código da central a jusante para infiltração |
| RegFactor | real | Mede a capacidade de modulação da usina fio d'agua entre patamares. 0= capacidade total 1 = não possui capacidade de regulação |
| ProdTypePol | inteiro | Chave que representa o coeficiente de produção utilizado no cálculo da política0: valor médio 1= função do armazenamento (usa a tabela fator de produção vs. armazenamento) |
| SpillCostType | inteiro | Chave para custo de vertimento explícito0 = custo de vertimento (do modelo)1= custo de vertimento explícito |
| SpillCost | real | Custo de vertimento |
| OutSampling | inteiro | Chave da representação do sorteio de falhas de geradores por Monte Carlo0 = não considera sorteio de falhas1 = considera sorteio de falhas |
| O&MCost | real | Custo variavel de operação e manutenção |
| ProdTypeSim | inteiro | Coeficiente de produção na simulação final= -1 constante= 0 em função do volume inicial (utiliza tabela produção vs. volume)= 1 em função da diferença entre a altura do nível inicial do reservatório e a altura média do canal de fuga (utiliza a tabela cota vs. volume)= 2  em função da diferença entre a altura do nível inicial do reservatório e a altura variável do canal de fuga (utiliza tabela cota vs. volume e tabela de altura do canal de fuga vs. caudal defluente) |
| TurbGenEffic | real | Eficiência do grupo turbina/gerador |
| AssocReservoir | inteiro |  |
| MeanTailLevel | real | Altura média do canal de fuga |
| DownSEType | inteiro |  |
| DownSEPlant | inteiro |  |
| Indicador | inteiro | =1 os seguintes dados serão utilizados=0 os seguintes dados serão ignorados |
| TurIn(1) | real |  |
| TotIn(1) | real |  |
| TurIn(2) | real |  |
| TotIn(2) | real |  |
| TurIn(3) | real |  |
| TotIn(3) | real |  |
| TurIn(4) | real |  |
| TotIn(4) | real |  |
| TurIn(5) | real |  |
| TotIn(5) | real |  |
| TailwaterFlag | inteiro |  |
| OxT_Tail(1) | real |  |
| OxT_Outflow(1) | real |  |
| OxT_Tail(2) | real |  |
| OxT_Outflow(2) | real |  |
| OxT_Tail(3) | real |  |
| OxT_Outflow(3) | real |  |
| OxT_Tail(4) | real |  |
| OxT_Outflow(4) | real |  |
| OxT_Tail(5) | real |  |
| OxT_Outflow(5) | real |  |
| Eaflu | inteiro | 0: Indica que a usina está sendo considerada para cálculo de energia afluente e armazenada (default), 1= não participa do cálculo |
| TurbLossFactor | real | Fator de perdas hidráulicas nas defluências de turbinamento |
| SpillLossFactor | real | Fator de perdas hidráulicas nas defluências de vertimento |
| SysSpillingId | string |  |
| SysTurbiningId | string |  |
| RunRiverType | inteiro |  |

# Reservatório virtual

Conjunto de arquivos onde se define a estrutura de configuração dos reservatórios virtuais.

## Parâmetros estáticos

Arquivo ASCII localizado no diretório de dados do estudo. Este arquivo denominado bidbaseddispatch_virtualreservoir.DAT contém a identificação dos reservatórios virtuais do caso.

O primeiro registro indica a versão do arquivo. Neste momento, ele deverá vir com o texto "$version 1". O segundo registro é um header. Os demais têm formato CSV (separados por vírgula):


| Column | Type | Description |
| ----------- | ----------- | ----------- |
| ReservoirID | inteiro | Valor único que identifica o reservatório virtual. |
| Agente | referencia | Referência ao agente associado ao reservatório virtual. |
| Sistema | inteiro | Referência ao sistema associado ao reservatório virtual. |

## Parâmetros dinâmicos

Arquivo ASCII localizado no diretório de dados do estudo. Este arquivo denominado bidbaseddispatch_virtualreservoirdatabidbaseddispatch.DAT contém os parâmetros dos reservatórios virtuais que podem variar no tempo. Na implementação atual o modelo utiliza apenas o valor inicial.

O primeiro registro indica a versão do arquivo. Neste momento, ele deverá vir com o texto "$version 1". O segundo registro é um header. Os demais têm formato CSV (separados por vírgula):

| Column | Type | Description |
| ----------- | ----------- | ----------- |
| BidBasedDispatch_VirtualReservoir | referencia | Referência ao reservatório virtual. |
| DataBidBasedDispatch | data | Data referência para os parâmetros que variam no tempo. |
| CotasMRE | real | Valor utilizado para o rateio da afluência do sistema. |
| CotasMRE2 | real | Valor utilizado para o rateio dos volumes inicias do sistema. |

# Agente

Conjunto de arquivos onde se define a estrutura de configuração dos agentes.

## Parâmetros escalares

Arquivo ASCII localizado no diretório de dados do estudo. Este arquivo denominado bidbaseddispatch_agente.DAT contém a identificação dos agentes do caso, além dos parâmetros escalares.

O primeiro registro indica a versão do arquivo. Neste momento, ele deverá vir com o texto "$version 1". O segundo registro é um header. Os demais têm formato CSV (separados por vírgula):

| Column | Type | Description |
| ----------- | ----------- | ----------- |
| AgentID | inteiro | Valor único que identifica o agente. |
| Name | string | Nome do agente. |
| RiskFactorHyd | real | Fator de risco utilizado pelo wizard para o cálculo das ofertas hidrelétricas do agente. |
| RiskFactorTer | real | Fator de risco utilizado pelo wizard para o cálculo das ofertas termelétricas do agente. |
| WizardID | inteiro | Valor que determina o tipo de wizard a ser utilizado pelo agente. 0 = Ler do arquivo. 1 = Markup simples: Oferta = Custo x (1+RiskFactor). |

## Parâmetros vetoriais

Arquivo ASCII localizado no diretório de dados do estudo. Este arquivo denominado bidbaseddispatch_agentelist.DAT contém as unidades geradoras associadas a cada agente do caso.

O primeiro registro indica a versão do arquivo. Neste momento, ele deverá vir com o texto "$version 1". O segundo registro é um header. Os demais têm formato CSV (separados por vírgula):

| Column | Type | Description |
| ----------- | ----------- | ----------- |
| BidBasedDispatch_Agente | referencia | Referência ao agente. |
| HydroPlants | vetor de referencias | Lista de referências às usinas hidrelétricas associadas ao agente. |
| ThermalPlants | vetor de referencias | Lista de referências às usinas termelétricas associadas ao agente. |
| GndPlants | vetor de referencias | Lista de referências às usinas renováveis associadas ao agente. |

# Usina termelétrica

Arquivo ASCII localizado no diretório de dados do estudo. Este arquivo denominado ctermiXX.DAT contém os parâmetros das usinas termelétricas de cada sistema XX.

O primeiro registro indica a versão do arquivo. Neste momento, ele deverá vir com o texto "$version 1". O segundo registro é um header. Os demais têm formato CSV (separados por vírgula):

| Column | Type | Description |
| ----------- | ----------- | ----------- |
| Num | inteiro | Código da usina |
| Nome | string | Nome da usina |
| Unidades | inteiro | Numero de unidades de geradores |
| Existing | inteiro | Estado |
| PotInst | real | Potência instalada total |
| GerMin | real | Capacidade mínima de geração |
| GerMax | real | Capacidade máxima de geração |
| ICP | real | Taxa de indisponibilidade forçada (sorteio de falhas por Monte Carlo) |
| IH | real | Taxa de indisponibilidade histórica |
| O&MCost | real | Custo de O&M variável |
| MR | inteiro |  |
| Comb | inteiro |  |
| G(1) | real |  |
| CEsp(1,1) | real |  |
| G(2) | real |  |
| CEsp(2,1) | real |  |
| G(3) | real |  |
| CEsp(3,1) | real |  |
| NAdF | inteiro | Número de combustíveis alternativos em caso de usinas térmicas multi-combustíveis |
| NAF(1) | inteiro |  |
| NAF(2) | inteiro |  |
| NAF(3) | inteiro |  |
| ComT | inteiro | Seleção de "unit-commitment" |
| CTransp | real | Custo de transporte de combustível |
| StartUp | real | Custo de partida para térmica com "unit-commitment" |
| sfal | inteiro | Chave para representação de sorteio de falhas por Monte Carlo0: não representa sorteio de falhas1: representa sorteio de falhas |
| CEsp(1,2) | real |  |
| CEsp(2,2) | real |  |
| CEsp(3,2) | real |  |
| CEsp(1,3) | real |  |
| CEsp(2,3) | real |  |
| CEsp(3,3) | real |  |
| CEsp(1,4) | real |  |
| CEsp(2,4) | real |  |
| CEsp(3,4) | real |  |
| CEsp(1,5) | real |  |
| CEsp(2,5) | real |  |
| CEsp(3,5) | real |  |
| NGas | inteiro | Aponta para o sistema de gás na qual está a usina térmica. Para térmicas associadas a um nó da rede de gás, o fator de comsumo deve ser informado em kUV/MWh (milhões de unidades de volume/MWh) |
| NuCC | inteiro |  |
| NameCC | string |  |
| CoefE | real | Coeficiente de emissão de CO21= todo CO2 produzido é emitidoValor entre 0 e 1 = existe um filtro que captura parte das emissões de CO2 |

# Estação renovável

Arquivo ASCII localizado no diretório de dados do estudo. Este arquivo denominado gndsiteXX.DAT contém a identificação das estações renováveis de cada sistema XX.

O primeiro registro indica a versão do arquivo. Neste momento, ele deverá vir com o texto "$version 1". O segundo registro é um header. Os demais têm formato CSV (separados por vírgula):

| Column | Type | Description |
| ----------- | ----------- | ----------- |
| Code | inteiro | Código da estação |
| Name | string | Nome da estação |
| TechType | inteiro | Tipo de tecnologia. 0 = Geral. 1 = Eólica. 2 = Solar. 3 = Biomassa. 4 = PCH. 5 = Geotérmica. 6 = CSP. |
| SourceType | inteiro |  |

# Usina renovável

Arquivo ASCII localizado no diretório de dados do estudo. Este arquivo denominado cgndXX.DAT contém os parâmetros das usinas renováveis de cada sistema XX.

O primeiro registro indica a versão do arquivo. Neste momento, ele deverá vir com o texto "$version 1". O segundo registro é um header. Os demais têm formato CSV (separados por vírgula):

| Column | Type | Description |
| ----------- | ----------- | ----------- |
| Num | inteiro | Código da usina |
| Nome | string | Nome da usina |
| CodBus | inteiro |  |
| Existing | inteiro | Estado |
| Unidades | inteiro | Número de unidades geradoras |
| PotInst | real | Capacidade Instalada |
| FatOpe | real | O fator de operação representa uma restrição na geração de potência instalada máxima devido a restrições nos elementos de suporte. |
| ICP | real | Probabilidade de falha do gerador renovável |
| sfal | inteiro |  |
| Site | inteiro |  |

# Interconexão

Arquivo ASCII localizado no diretório de dados do estudo. Este arquivo denominado interc.DAT contém os parâmetros das interconexões do caso.

O primeiro registro indica a versão do arquivo. Neste momento, ele deverá vir com o texto "$version 1". O segundo registro é um header. Os demais têm formato CSV (separados por vírgula):

| Column | Type | Description |
| ----------- | ----------- | ----------- |
| FROM | inteiro | Sistema de origem |
| TO | inteiro | Sistema de destino |
| LossFactor-> | real | Fator de perdas DE->PARA |
| LossFactor<- | real | Fator de perdas PARA->DE |
| Num | inteiro | Código da interconexão |
| Nome | string | Nome da interconexão |
| Capacity-> | real | Capacidade DE->PARA |
| Capacity<- | real | Capacidade PARA->DE |
| Existing | inteiro | Estado |
| Currency | string |  |

# Barra

Arquivo ASCII localizado no diretório de dados do estudo. Este arquivo denominado dbus.DAT contém a identificação das barras do caso.

O primeiro registro indica a versão do arquivo. Neste momento, ele deverá vir com o texto "$version 1". O segundo registro é um header. Os demais têm formato CSV (separados por vírgula):

| Column | Type | Description |
| ----------- | ----------- | ----------- |
| Bus | inteiro | Código da barra |
| Tipo | inteiro |  |
| Nome | string | Nome da barra |
| IdSistema | string | ID do sistema |
| NdxBarra | inteiro |  |
| TpUsina | inteiro |  |
| CodUsina | inteiro |  |
| NomeUsina | string |  |
| CodArea | inteiro |  |
| Pload1 | real |  |
| Pind1 | real |  |
| PerF1 | real |  |
| Pload2 | real |  |
| Pind2 | real |  |
| PerF2 | real |  |
| Pload3 | real |  |
| Pind3 | real |  |
| PerF3 | real |  |
| Pload4 | real |  |
| Pind4 | real |  |
| PerF4 | real |  |
| Pload5 | real |  |
| Pind5 | real |  |
| PerF5 | real |  |
| Icca | inteiro |  |

# Carga por barra

Arquivo ASCII localizado no diretório de dados do estudo. Este arquivo denominado dbf005XX.DAT contém as cargas por barra de cada sistema XX.

O primeiro registro indica a versão do arquivo. Neste momento, ele deverá vir com o texto "$version 1". O segundo registro é um header. Os demais têm formato CSV (separados por vírgula):

| Column | Type | Description |
| ----------- | ----------- | ----------- |
| Bus | inteiro | Código da barra |
| NomeBus | string | Nome da barra |
| CodCarga | inteiro | Código da carga |
| DataMod |  |  |
| Llev | inteiro |  |
| Load | real | Demanda |

# Linha de transmissão

Arquivo ASCII localizado no diretório de dados do estudo. Este arquivo denominado dcirc.DAT contém os parâmetros dos circuitos do caso.

O primeiro registro indica a versão do arquivo. Neste momento, ele deverá vir com o texto "$version 1". O segundo registro é um header. Os demais têm formato CSV (separados por vírgula):

| Column | Type | Description |
| ----------- | ----------- | ----------- |
| Bor | inteiro | Barra de origem |
| Bde | inteiro | Barra de destino |
| R | real | Resistência |
| X | real | Reatância |
| Nome | string | Nome do circuito |
| MVAr | real | Susceptância total |
| Tmn | real | Tap mínimo do transformador |
| Tmx | real | Tap máximo do transformador |
| Rn | real | Limite de fluxo - situação normal |
| Re | real | Limite de fluxo - emergência |
| Id | inteiro | Código do circuito |
| I | inteiro | Estado |
| C | inteiro |  |
| Iflh | inteiro | Indica se é ou não considerada falha nos circuitos |
| Prob | real | Probabilidade de falha |
| Cmon | inteiro |  |
| Status | inteiro | Indica se é ou não considerado na operação |
| FlagDC | inteiro |  |
| Pmn | real | Potência ativa mínima |
| Pmx | real | Potência ativa máxima |
| SysFromId | string |  |
| SysToId | string |  |

# Elo CC

Arquivo ASCII localizado no diretório de dados do estudo. Este arquivo denominado dclink.DAT contém os parâmetros dos elos CC do caso.

O primeiro registro indica a versão do arquivo. Neste momento, ele deverá vir com o texto "$version 1". O segundo registro é um header. Os demais têm formato CSV (separados por vírgula):

| Column | Type | Description |
| ----------- | ----------- | ----------- |
| Bfr | inteiro | Barra de origem |
| Bto | inteiro | Barra de destino |
| Capacity-> | real | De-Para Limite de Transferência |
| LossFactor-> | real | De-Para Fator de Perda |
| Capacity<- | real | Para-De Limite de Transferência |
| LossFactor<- | real | Para-De Fator de Perda |
| Num | inteiro | Código do elo CC |
| Name | string | Nome do elo CC |
| Existing | inteiro | Estado |
| CapacityEmergency-> | real |  |
| CapacityEmergency<- | real |  |
| R | real |  |
| FlagQuadraticLoss | inteiro |  |

