is_tmp_file(filepath::String) = occursin(r".tmp", filepath)

function clean_tmp_files(path::String)
    @assert isdir(path)
    files = readdir(path)
    tmp_files = filter!(is_tmp_file, files)
    rm.(joinpath.(path, tmp_files), force = true)
    return nothing
end

function set_simulation_mode(path::String, simulation_mode::BidBasedDispatch.GameType)
    @assert isdir(path)
    dispatch_config_path = joinpath(path, "dispatch_config.dat")
    (tmppath, tmpio) = mktemp()
    open(dispatch_config_path) do io
        for line in eachline(io, keep=true) # keep so the new line isn't chomped
            if occursin("gametype", line)
                line = "gametype = $(Int(simulation_mode)) # 1 - Human, 2 - Computer\n"
            end
            write(tmpio, line)
        end
    end
    close(tmpio)
    mv(tmppath, dispatch_config_path, force=true)
    return true
end