# BidBased
## Commits relevantes

| Commit                                 | Pessoa          | Caso teste      | Documentação     | Comentário
| :------------------------------------- | :-------------- | :-------------- | :--------------  | :-------------- 
| dia, semana e ano atualizada           | Thiago          |         ❌       |       ✔️          | falta adicionar ano no manual
| validação dos arquivos graf            | Vidigal         |         ✔️       |       ❌          |
| resultados concatenados                | Thiago          |         ✔️       |       ✔️          | falta manual
| add O&M gnd wizard                     | Vidigal         |         ❌       |       ✔️          | ✔️
| segundo butão da interface             | Thiago          |         ❌       |       ✔️          | ✔️
| sempre escreve lp                      | Vidigal         |         ❌       |       ❌          | ✔️
| compra reservatorios virtuais          | Thiago          |         ✔️       |       ✔️          | Falta manual
| offset in year/stage                   | Thiago          |         ❌       |       ❌          | ✔️
| awizardgnd added                       | Vidigal         |         ✔️       |       ✔️          | ✔️
| relative gap                           | Thiago          |         ❌       |       ✔️          | opções de solver, 6.1
| vertimento linearizado                 | Vidigal         |         ✔️       |       ✔️          | ✔️
| big M                                  | Vidigal         |         ✔️       |       ✔️          | ✔️
| add x=0 case on spill control          | Vidigal         |         ✔️       |       ❌          | ✔️
| demand offer                           | Thiago          |         ✔️       |       ✔️          | arquivo de demand e associar com agente
| regioes de armazenamento               | Vidigal         |         ✔️       |       ✔️          | ✔️
| output files                           | Thiago          |         ✔️       |       ✔️          | revisar
| reference offer files with wizard      | Vidigal         |         ❌       |       ✔️          | revisar 