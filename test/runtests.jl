using Test
using BidBasedDispatch
using PSRIO

psrio = PSRIO.create()

include("utils.jl")

@testset "All Cases" begin

    @testset "Case01" begin
        path_Case01 = joinpath(@__DIR__(), "data", "cases", "Case01")
        path_Case01_new = joinpath(@__DIR__(), "data", "cases", "Case01", "outputs")
        path_Case01_reference = joinpath(@__DIR__(), "data", "outputs", "Case01")

        BidBasedDispatch.main(path_Case01*"\\")
        p = PSRIO.run(psrio, [path_Case01_new, path_Case01_reference], verbose=3, recipes=["compare.lua"], model="none", return_errors=true)
        @test p.exitcode == 0
    end

    @testset "Feature Tests" begin

        @testset "CaseForcedSpillage" begin
            path_Case01 = joinpath(@__DIR__(), "data", "cases", "FeatureTests", "CaseForcedSpillage")
            path_Case01_new = joinpath(@__DIR__(), "data", "cases", "FeatureTests", "CaseForcedSpillage", "outputs")
            path_Case01_reference = joinpath(@__DIR__(), "data", "outputs", "CaseForcedSpillage")

            BidBasedDispatch.main(path_Case01*"\\")
            p = PSRIO.run(psrio, [path_Case01_new, path_Case01_reference], verbose=3, recipes=["compare.lua"], model="none", return_errors=true)
            @test p.exitcode == 0
        end

        @testset "CaseLinearForcedSpillage" begin
            path_Case01 = joinpath(@__DIR__(), "data", "cases", "FeatureTests", "CaseLinearForcedSpillage")
            path_Case01_new = joinpath(@__DIR__(), "data", "cases", "FeatureTests", "CaseLinearForcedSpillage", "outputs")
            path_Case01_reference = joinpath(@__DIR__(), "data", "outputs", "CaseLinearForcedSpillage")

            BidBasedDispatch.main(path_Case01*"\\")
            p = PSRIO.run(psrio, [path_Case01_new, path_Case01_reference], verbose=3, recipes=["compare.lua"], model="none", return_errors=true)
            @test p.exitcode == 0
        end

        @testset "CaseStorageRegions" begin
            path_Case01 = joinpath(@__DIR__(), "data", "cases", "FeatureTests", "CaseStorageRegions")
            path_Case01_new = joinpath(@__DIR__(), "data", "cases", "FeatureTests", "CaseStorageRegions", "outputs")
            path_Case01_reference = joinpath(@__DIR__(), "data", "outputs", "CaseStorageRegions")

            BidBasedDispatch.main(path_Case01*"\\")
            p = PSRIO.run(psrio, [path_Case01_new, path_Case01_reference], verbose=3, recipes=["compare.lua"], model="none", return_errors=true)
            @test p.exitcode == 0
        end

        @testset "CaseDemandOffer" begin
            path_Case01 = joinpath(@__DIR__(), "data", "cases", "FeatureTests", "CaseDemandOffer")
            path_Case01_new = joinpath(@__DIR__(), "data", "cases", "FeatureTests", "CaseDemandOffer", "outputs")
            path_Case01_reference = joinpath(@__DIR__(), "data", "outputs", "CaseDemandOffer")

            BidBasedDispatch.main(path_Case01*"\\")
            p = PSRIO.run(psrio, [path_Case01_new, path_Case01_reference], verbose=3, recipes=["compare.lua"], model="none", return_errors=true)
            @test p.exitcode == 0
        end

        @testset "Commit" begin
            path_Case01 = joinpath(@__DIR__(), "data", "cases", "FeatureTests", "Commit")
            path_Case01_new = joinpath(@__DIR__(), "data", "cases", "FeatureTests", "Commit", "outputs")
            path_Case01_reference = joinpath(@__DIR__(), "data", "outputs", "Commit")

            BidBasedDispatch.main(path_Case01*"\\")
            p = PSRIO.run(psrio, [path_Case01_new, path_Case01_reference], verbose=3, recipes=["compare.lua"], model="none", return_errors=true)
            @test p.exitcode == 0
        end

        @testset "MaxUpDown" begin
            path_Case01 = joinpath(@__DIR__(), "data", "cases", "FeatureTests", "MaxUpDown")
            path_Case01_new = joinpath(@__DIR__(), "data", "cases", "FeatureTests", "MaxUpDown", "outputs")
            path_Case01_reference = joinpath(@__DIR__(), "data", "outputs", "MaxUpDown")

            BidBasedDispatch.main(path_Case01*"\\")
            p = PSRIO.run(psrio, [path_Case01_new, path_Case01_reference], verbose=3, recipes=["compare.lua"], model="none", return_errors=true)
            @test p.exitcode == 0
        end

        @testset "MinTime" begin
            path_Case01 = joinpath(@__DIR__(), "data", "cases", "FeatureTests", "MinTime")
            path_Case01_new = joinpath(@__DIR__(), "data", "cases", "FeatureTests", "MinTime", "outputs")
            path_Case01_reference = joinpath(@__DIR__(), "data", "outputs", "MinTime")

            BidBasedDispatch.main(path_Case01*"\\")
            p = PSRIO.run(psrio, [path_Case01_new, path_Case01_reference], verbose=3, recipes=["compare.lua"], model="none", return_errors=true)
            @test p.exitcode == 0
        end

        @testset "Ramp" begin
            path_Case01 = joinpath(@__DIR__(), "data", "cases", "FeatureTests", "Ramp")
            path_Case01_new = joinpath(@__DIR__(), "data", "cases", "FeatureTests", "Ramp", "outputs")
            path_Case01_reference = joinpath(@__DIR__(), "data", "outputs", "Ramp")

            BidBasedDispatch.main(path_Case01*"\\")
            p = PSRIO.run(psrio, [path_Case01_new, path_Case01_reference], verbose=3, recipes=["compare.lua"], model="none", return_errors=true)
            @test p.exitcode == 0
        end

        @testset "Buffer" begin
            path_Case01 = joinpath(@__DIR__(), "data", "cases", "FeatureTests", "Buffer")
            path_Case01_new = joinpath(@__DIR__(), "data", "cases", "FeatureTests", "Buffer", "outputs")
            path_Case01_reference = joinpath(@__DIR__(), "data", "outputs", "Buffer")

            BidBasedDispatch.main(path_Case01*"\\")
            p = PSRIO.run(psrio, [path_Case01_new, path_Case01_reference], verbose=3, recipes=["compare.lua"], model="none", return_errors=true)
            @test p.exitcode == 0
        end

    end

    @testset "Example Cases" begin

        @testset "1.1 Termicas" begin
            path_Case01 = joinpath(@__DIR__(), "..", "examples", "1.1 Termicas")
            path_Case01_new = joinpath(@__DIR__(), "..", "examples", "1.1 Termicas", "outputs")
            path_Case01_reference = joinpath(@__DIR__(), "data", "outputs", "1.1 Termicas")

            BidBasedDispatch.main(path_Case01*"\\")
            p = PSRIO.run(psrio, [path_Case01_new, path_Case01_reference], verbose=3, recipes=["compare.lua"], model="none", return_errors=true)
            @test p.exitcode == 0
        end

        @testset "1.2 Hidro" begin
            path_Case01 = joinpath(@__DIR__(), "..", "examples", "1.2 Hidro")
            path_Case01_new = joinpath(@__DIR__(), "..", "examples", "1.2 Hidro", "outputs")
            path_Case01_reference = joinpath(@__DIR__(), "data", "outputs", "1.2 Hidro")

            BidBasedDispatch.main(path_Case01*"\\")
            p = PSRIO.run(psrio, [path_Case01_new, path_Case01_reference], verbose=3, recipes=["compare.lua"], model="none", return_errors=true)
            @test p.exitcode == 0
        end

        @testset "1.3 Conjunto" begin
            path_Case01 = joinpath(@__DIR__(), "..", "examples", "1.3 Conjunto")
            path_Case01_new = joinpath(@__DIR__(),  "..", "examples", "1.3 Conjunto", "outputs")
            path_Case01_reference = joinpath(@__DIR__(), "data", "outputs", "1.3 Conjunto")

            BidBasedDispatch.main(path_Case01*"\\")
            p = PSRIO.run(psrio, [path_Case01_new, path_Case01_reference], verbose=3, recipes=["compare.lua"], model="none", return_errors=true)
            @test p.exitcode == 0
        end

        @testset "1.4 Hidro" begin
            path_Case01 = joinpath(@__DIR__(), "..", "examples", "1.4 Hidro")
            path_Case01_new = joinpath(@__DIR__(), "..", "examples", "1.4 Hidro", "outputs")
            path_Case01_reference = joinpath(@__DIR__(), "data", "outputs", "1.4 Hidro")

            BidBasedDispatch.main(path_Case01*"\\")
            p = PSRIO.run(psrio, [path_Case01_new, path_Case01_reference], verbose=3, recipes=["compare.lua"], model="none", return_errors=true)
            @test p.exitcode == 0
        end

        @testset "2.1 Termico" begin
            path_Case01 = joinpath(@__DIR__(), "..", "examples", "2.1 Termico")
            path_Case01_new = joinpath(@__DIR__(), "..", "examples", "2.1 Termico", "outputs")
            path_Case01_reference = joinpath(@__DIR__(), "data", "outputs", "2.1 Termico")

            BidBasedDispatch.main(path_Case01*"\\")
            p = PSRIO.run(psrio, [path_Case01_new, path_Case01_reference], verbose=3, recipes=["compare.lua"], model="none", return_errors=true)
            @test p.exitcode == 0
        end

    end

    @testset "GRAF Validation Tests" begin

        @testset "Missing File" begin
            path_case = joinpath(@__DIR__(), "data", "cases", "GRAFValidationTests", "1_MissingFile")
            @test_throws ErrorException("Arquivo demand não tem as versões .bin e .csv na pasta $(path_case*"\\").") BidBasedDispatch.main(path_case)
        end

        @testset "Both Files" begin
            path_case = joinpath(@__DIR__(), "data", "cases", "GRAFValidationTests", "2_BothFiles")
            @test_throws ErrorException("Arquivo demand tem as versões .bin e .csv na pasta $(path_case*"\\"). Só pode ter uma versão.") BidBasedDispatch.main(path_case)
        end

        @testset "Wrong Date" begin
            path_case = joinpath(@__DIR__(), "data", "cases", "GRAFValidationTests", "3_WrongDate")
            @test_throws ErrorException("Erro ao tentar ler etapa 53 do arquivo demand.") BidBasedDispatch.main(path_case)
        end

        @testset "Missing Agent" begin
            path_case = joinpath(@__DIR__(), "data", "cases", "GRAFValidationTests", "4_MissingAgent")
            @test_throws ErrorException("agent Demanda 2 not found in file") BidBasedDispatch.main(path_case)
        end

        @testset "Missing Scenario" begin
            path_case = joinpath(@__DIR__(), "data", "cases", "GRAFValidationTests", "5_MissingScenario")
            @test_throws ErrorException("O arquivo demand possui 4 cenarios, o caso possui 5") BidBasedDispatch.main(path_case)
        end

    end

    @testset "Offer Validation Tests" begin
        
        @testset "Mismatched Scenarios" begin
            path_case = joinpath(@__DIR__(), "data", "cases", "OfferValidationTests", "1_MismatchedScenarios")
            @test_logs (:warn, "Arquivos agn_1_sale_offer_hyd_qtd.csv e agn_1_sale_offer_hyd_price.csv possuem números de cenários diferentes.") match_mode=:any BidBasedDispatch.main(path_case)
        end

        @testset "Mismatched Blocks" begin
            path_case = joinpath(@__DIR__(), "data", "cases", "OfferValidationTests", "2_MismatchedBlocks")
            @test_logs (:warn, "Arquivos agn_4_sale_offer_ter_qtd.csv e agn_4_sale_offer_ter_price.csv possuem números de blocos diferentes.") match_mode=:any BidBasedDispatch.main(path_case)
        end
        
        @testset "Mismatched Header Size" begin
            path_case = joinpath(@__DIR__(), "data", "cases", "OfferValidationTests", "3_MismatchedHeaderSize")
            @test_logs (:warn, "Arquivos agn_1_sale_offer_hyd_qtd.csv e agn_1_sale_offer_hyd_price.csv possuem números de ofertas diferentes.") match_mode=:any BidBasedDispatch.main(path_case)
        end
        
        @testset "Mismatched Headers" begin
            path_case = joinpath(@__DIR__(), "data", "cases", "OfferValidationTests", "4_MismatchedHeaders")
            @test_logs (:warn, "Arquivos agn_1_sale_offer_hyd_qtd.csv e agn_1_sale_offer_hyd_price.csv possuem cabeçalhos diferentes.") match_mode=:any BidBasedDispatch.main(path_case)
        end
        
        @testset "Missing Plant" begin
            path_case = joinpath(@__DIR__(), "data", "cases", "OfferValidationTests", "5_MissingPlant")
            @test_logs (:warn, "Reservatório virtual 4 nos arquivos agn_1_sale_offer_hyd_qtd.csv e agn_1_sale_offer_hyd_price.csv não existe.") match_mode=:any BidBasedDispatch.main(path_case)
        end
        
        @testset "Missassigned Plant" begin
            path_case = joinpath(@__DIR__(), "data", "cases", "OfferValidationTests", "6_MisassignedPlant")
            @test_logs (:warn, "Usina 2 nos arquivos agn_4_sale_offer_ter_qtd.csv e agn_4_sale_offer_ter_price.csv não pertence ao agente 4.") match_mode=:any BidBasedDispatch.main(path_case)
        end

    end

end
