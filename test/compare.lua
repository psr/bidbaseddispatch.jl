-- psrio.exe --model none -r script.lua caso1 caso2

local test_label = "test";
local benchmark_label = "benchmark";

local test = Generic(1); -- caso1
local benchmark = Generic(2); -- caso2

local report = Report("Test vs Benchmark");

local function compare_outputs(dashboard, filename, title, blocks_aggregation,epsilon)
    local output_test = test:load(filename);
    local output_benchmark = benchmark:load(filename);

    if output_benchmark:loaded() or output_test:loaded() then
        local compare = Compare(filename,epsilon or 1e-4);
        compare:add(output_test);
        compare:add(output_benchmark);
        report:push(compare);

        local chart = Chart(title);
        chart:add_line(output_test:aggregate_blocks(blocks_aggregation):aggregate_scenarios(BY_AVERAGE()):aggregate_agents(BY_SUM(), test_label));
        chart:add_line(output_benchmark:aggregate_blocks(blocks_aggregation):aggregate_scenarios(BY_AVERAGE()):aggregate_agents(BY_SUM(), benchmark_label));
        dashboard:push(chart);
    end
end

-- local gen = Dashboard("Costs");
-- compare_outputs(costs, "HGenRT", "Hidro Generation RT", BY_AVERAGE());
-- compare_outputs(costs, "cmgbus", "Bus Marginal Cost", BY_AVERAGE());
-- compare_outputs(costs, "cmgcir", "Circuit Marginal Cost", BY_AVERAGE());
-- compare_outputs(costs, "defcos", "Deficit Cost", BY_AVERAGE());

local generationDA = Dashboard("Generation DA");
compare_outputs(generationDA, "HGenDA", "Hydro Generation", BY_SUM());
compare_outputs(generationDA, "RGenDA", "Renewable Generation", BY_SUM());
compare_outputs(generationDA, "TGenDA", "Thermal Generation", BY_SUM());


local generationRT = Dashboard("Generation RT");
compare_outputs(generationRT, "HGenRT", "Hydro Generation", BY_SUM());
compare_outputs(generationRT, "RGenRT", "Renewable Generation", BY_SUM());
compare_outputs(generationRT, "TGenRT", "Thermal Generation", BY_SUM());

local physicalDA = Dashboard("Physical Operation DA");
compare_outputs(physicalDA, "VolDA", "Volum", BY_SUM());
compare_outputs(physicalDA, "TurbDA", "Turbining", BY_SUM());
compare_outputs(physicalDA, "VertDA", "Spillage", BY_SUM());

local physicalRT = Dashboard("Physical Operation RT");
compare_outputs(physicalRT, "VolRT", "Volum", BY_SUM());
compare_outputs(physicalRT, "TurbRT", "Turbining", BY_SUM());
compare_outputs(physicalRT, "VertRT", "Spillage", BY_SUM());

local hydrooffer = Dashboard("Hydro Offer");
compare_outputs(hydrooffer, "HydSaleOfferPrice", "Offer Price", BY_SUM());
compare_outputs(hydrooffer, "HydSaleOfferQtd", "Quantity Offer", BY_SUM());
compare_outputs(hydrooffer, "HydSaleOfferAccDA", "Accepted Offer DA", BY_SUM());
compare_outputs(hydrooffer, "HydSaleOfferAccRT", "Accepted Offer RT", BY_SUM());

compare_outputs(hydrooffer, "HydPurchaseOfferPrice", "Offer Price", BY_SUM());
compare_outputs(hydrooffer, "HydPurchaseOfferQtd", "Quantity Offer", BY_SUM());
compare_outputs(hydrooffer, "HydPurchaseOfferAccDA", "Accepted Offer DA", BY_SUM());
compare_outputs(hydrooffer, "HydPurchaseOfferAccRT", "Accepted Offer RT", BY_SUM());

local termoffer = Dashboard("Thermo Offer");
compare_outputs(termoffer, "TerOfferPrice", "Offer Price", BY_SUM());
compare_outputs(termoffer, "TerOfferQtd", "Quantity Offer", BY_SUM());
compare_outputs(termoffer, "TerOfferAccDA", "Accepted Offer DA", BY_SUM());
compare_outputs(termoffer, "TerOfferAccRT", "Accepted Offer RT", BY_SUM());

local gndoffer = Dashboard("Renewable Offer");
compare_outputs(gndoffer, "GndOfferPrice", "Offer Price", BY_SUM());
compare_outputs(gndoffer, "GndOfferQtd", "Quantity Offer", BY_SUM());
compare_outputs(gndoffer, "GndOfferAccDA", "Accepted Offer DA", BY_SUM());
compare_outputs(gndoffer, "GndOfferAccRT", "Accepted Offer RT", BY_SUM());

report:save("compare");
(generationDA + generationRT + physicalDA + physicalRT + hydrooffer + termoffer + gndoffer ):save("compare");