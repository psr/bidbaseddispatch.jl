# BidBasedDispatch

Este repositório contém os modelos computacionais do projeto de P&D de formação de preços por ofertas.

## Instalação

Para rodar os modelos é necessário ter a versão 1.6 ou superior da linguagem de programação Julia instalado no computador. O link para download pode ser encontrado em https://julialang.org/downloads/. Depois que o Julia for instalado é necessário colocar editar as variáveis de ambiente do usuário para poder rodar o julia a partir de um command prompt.

Para editar as variáveis de ambiente o usuário deve ir em "edit system environment variables"

![environment_variables](./figs/environment_variables.PNG)

clicar em "Environment Variables" e editar as variáveis de ambiente do usuário.

![edit_path](./figs/edit_path.PNG)

e adicionar o caminho até a pasta com o binário do Julia na variável de ambiente `path`. **É importante que o path não contenha espaços**. Caso o caminho padrão do instalador contenha espaços como por exemplo:
```
C:\Users\guilhermebodin\AppData\Local\Programs\Julia 1.6.1\bin
``` 
renomeie a pasta para que ele não tenha mais espaços, como por exemplo
```
C:\Users\guilhermebodin\AppData\Local\Programs\Julia-1.6.1\bin
```

![add_julia](./figs/add_julia.PNG)

Uma vez que o Julia esteja instalado existem duas possibilidades para fazer o download do código fonte do software:

* A primeira e mais simples para usuários é fazer o download do código fonte através do botão "Downloads" no menu da barra esquerda do bitbucket e posteriormente no botão "Download". Mostramos o passo a passo nas imagens a seguir

![add_julia](./figs/download_repo.PNG)
![add_julia](./figs/download_1.PNG)
![add_julia](./figs/download.PNG)

* A segunda, mais recomendada para desenvolvedores é usar o software "git" para clonar o repositório para a sua máquina.

Uma vez que todos os arquivos estejam no computador basta clicar no arquivo `installpkg.bat` ou rodar através do cmd o comando `.\installpkg.bat` a partir da pasta `bidbaseddispatch.jl`

Durante a instalação do pacote, 2 dependênias da microsoft tentaram ser instaladas. Caso ainda não possua essa dependência, a seguinte janela abrirá: 

![add_julia](./figs/depsc++install.png)

Neste caso, basta aceitar os termos e instalar.

A maioria dos computadores já possuem essas dependências, neste caso a janela será assim:

![add_julia](./figs/depsc++.png)

Basta apenas fechar a janela para prosseguir com a instalação.


## Rodando estudos pela linha de comando

Para rodar estudos é necessário ter uma pasta com os dados de entrada de um caso do SDDP e os arquivos adicionais: `BidBasedDispatch.dat`, `bidbaseddispatch_agente.dat`, `bidbaseddispatch_agentelist.dat`, `bidbaseddispatch_configuracaoestudo.dat`, `bidbaseddispatch_virtualreservoir.dat`, `bidbaseddispatch_virtualreservoirdatabidbaseddispatch.dat`.

Uma vez que os arquivos estejam disponibilzados no formato correto a execução é feita através do comando

```
.\run.bat "path_para_a_pasta_do_caso"
```

**Recomendamos que os usuários sempre coloquem os casos em paths que não contanham espaços**. Os resultados serão escritos em uma pasta chamada `outputs` dentro da pasta do caso do estudo.

## Rodando estudos pela interface

Para abrir a interface basta rodar o arquivo `interface.bat`.

## Manuais

O modelo possui três manuais sendo um manual de usuário para a interface e dois manuais para os arquivos. Os manuais podem ser encontrados o seguinte caminho: 

```
bidbaseddispatch.jl\docs
```

O manual do usuário para a interface e o manual de arquivos tipo GRAF estão no formato pdf. O outro manual de dados está em markdown, um jeito fácil de lê-lo é pelo próprio bitbucket. 