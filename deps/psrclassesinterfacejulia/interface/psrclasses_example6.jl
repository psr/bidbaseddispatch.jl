using Libdl

# ----------------------------------
# Parte 1 - Inicializacao PSRCLASSES
# ----------------------------------

# Seta PATH para PSRCLASSES
PATH_PSRCLASSES = normpath(joinpath(@__DIR__, ".."))

if get(ENV, "PSRC_JL_TESTING", "0") != "1"
#       Inclui as definicoes do modulo PSRClasses
#       -----------------------------------------
    include(string(PATH_PSRCLASSES, "psrclasses.jl"))
    include(joinpath(PATH_PSRCLASSES, "psrclassesoo.jl"))

#       Inicializa a DLL do PSRClasses
#       ------------------------------
    PSRClasses_init(PATH_PSRCLASSES)
end

# -----------------------------------
# Parte 2 - Configuracao do resultado
# -----------------------------------

# Cria objeto para manipulacao de saida GRAF
iograf = PSRIOGrafResultBinary()
# para CSV use:
# iograf = PSRIOGrafResult_create(0)

# Configura parametros da saida
# -----------------------------
# Variavel por bloco = 1
# Variavel por serie = 1
# Unidade = MW
# Tipo de Estagio = mensal
# Estagio inicial = Janeiro de 2016
# Total de blocos = 3
# Total de Series = 10
iograf.setNameLength(24)

iograf.setVariableByBlock(1)
iograf.setVariableBySerie(1)
iograf.setUnit("MW")

iograf.setStageType(PSR_STAGETYPE_MONTHLY)
# iograf.setStageType(PSR_STAGETYPE_WEEKLY)

iograf.setInitialStage(1)
iograf.setInitialYear(2016)

iograf.setTotalBlocks(3)
iograf.setTotalSeries(10)
iograf.setSequencialModel(true)

# case seja horário:
# iograf.setVariableByHour(1)
# if studyptr != C_NULL
#     iograf.useHourlyDurationFrom(studyptr)
# else
#     error(not suppeorted)
# end

# Define os agentes (3 agentes, X, Y e Z)
iograf.addAgent("X")
iograf.addAgent("Y")
iograf.addAgent("Z")

# Inicia gravacao do resultado
FILE_PATH = normpath(joinpath(PATH_PSRCLASSES, "psrclasses_example6"))
iograf.initSave(FILE_PATH * ".hdr", FILE_PATH * ".bin")
# versão CSV
# PSRIOGrafResult_initSave("./output/psrclasses_example6.csv", PSRIO_GRAF_FORMAT_DEFAULT)

# ---------------------------------------------
# Parte 3 - Gravacao dos registros do resultado
# ---------------------------------------------

# Loop de gravacao
for estagio = 1:12
    for serie = 1:10
        for bloco = 1:3

            iograf.setCurrentStage(estagio)
            iograf.setCurrentSerie(serie)
            iograf.setCurrentBlock(bloco)

            #       Seta valor dos agentes para o registro
            agentX_value = estagio + serie + 0.
            agentY_value = serie - estagio + 0.
            agentZ_value = estagio + serie + bloco * 100.

            iograf.setData(0, agentX_value)
            iograf.setData(1, agentY_value)
            iograf.setData(2, agentZ_value)

            #       Grava registro
            iograf.writeRegistry()
        end
    end
end
# Finaliza gravacao
iograf.closeSave()
iograf.deleteInstance()
# Em CSV
# PSRIOGrafResult_closeSave()
# PSRIOGrafResult_deleteInstance()

# ------------------------------------------------------------------
# Leitura de arquivo formato GRAF
# ------------------------------------------------------------------

FILE_PATH = normpath(joinpath(PATH_PSRCLASSES, "psrclasses_example6"))

iograf = PSRIOGrafResultBinary()
iograf.initLoad(FILE_PATH * ".hdr", FILE_PATH * ".bin")
# versão CSV
# iograf = PSRIOGrafResult_create(0)
# PSRIOGrafResult_initLoad(FILEPATH, PSRIO_GRAF_FORMAT_DEFAULT)

@assert iograf.getTotalStages() == 12
@assert iograf.getTotalSeries() == 10
@assert iograf.getTotalBlocks() == 3
@assert iograf.getNameLength() == 24
@assert iograf.getVariableByBlock() == 1
@assert iograf.getVariableBySerie() == 1
@assert iograf.getVariableBySerie() == 1
@assert iograf.getVariableByHour() == 0
@assert iograf.getStageType() == PSR_STAGETYPE_MONTHLY
@assert iograf.getInitialStage() == 1
@assert iograf.getInitialYear() == 2016
@assert strip(iograf.getUnit()) == "MW"


# em arquivos horário numero de blocos pode varia por etapa:
# iograf.getTotalBlocks2( ptr_pointer,stage )

# obtem número de colunas
@assert iograf.maxAgent() == 3
names = ["X", "Y", "Z"]
for i in 1:3
    @assert strip(iograf.getAgent(i - 1)) == names[i]
end

for estagio = 1:12
    for serie = 1:10
        for bloco = 1:3

            # posiciona o ponteiro na entrada correta
            iograf.seekStage2(estagio, serie, bloco)
            @assert iograf.getCurrentStage() == estagio
            @assert iograf.getCurrentSerie() == serie
            @assert iograf.getCurrentBlock() == bloco
            # obtem valor dos agentes para o registro
            agentX_value = estagio + serie
            agentY_value = serie - estagio
            agentZ_value = estagio + serie + bloco * 100

            @assert iograf.getData(0) == agentX_value
            @assert iograf.getData(1) == agentY_value
            @assert iograf.getData(2) == agentZ_value
        end
    end
end
# ------------------------------------------------------------------
# Segundo metodo de iterção - mais eficiente
# ------------------------------------------------------------------

# Vai para posicao especifica do arquivo
# nesse caso a primiera entrada
iograf.seekStage2(1, 1, 1)
for estagio = 1:12
    for serie = 1:10
        for bloco = 1:3

            @assert iograf.getCurrentStage() == estagio
            @assert iograf.getCurrentSerie() == serie
            @assert iograf.getCurrentBlock() == bloco

            agentX_value = estagio + serie
            agentY_value = serie - estagio
            agentZ_value = estagio + serie + bloco * 100

            @assert iograf.getData(0) == agentX_value
            @assert iograf.getData(1) == agentY_value
            @assert iograf.getData(2) == agentZ_value

            iograf.nextRegistry(false)
        end
    end
end
# é possivel também mover entre etapas fora de ordem...
for estagio = [6,5,11,2]
    iograf.seekStage2(estagio, 1, 1)
    for serie = 1:10
        for bloco = 1:3

            @assert iograf.getCurrentStage() == estagio
            @assert iograf.getCurrentSerie() == serie
            @assert iograf.getCurrentBlock() == bloco

            agentX_value = estagio + serie
            agentY_value = serie - estagio
            agentZ_value = estagio + serie + bloco * 100

            @assert iograf.getData(0) == agentX_value
            @assert iograf.getData(1) == agentY_value
            @assert iograf.getData(2) == agentZ_value

            # avança para o próximo registro
            iograf.nextRegistry(false)
        end
    end
end

iograf.closeLoad()
iograf.deleteInstance()

# ------------------------------------------------------------------
# Conversão de arquivo formato GRAF
# ------------------------------------------------------------------

FILEPATH_HEADER = FILE_PATH * ".hdr"
FILEPATH_BIN = FILE_PATH * ".bin"
FILEPATH_CSV = FILE_PATH * ".csv"
# ------------------------------------------------------------------
# Binarios -> CSV
# ------------------------------------------------------------------

# ATENÇÃO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# ATENÇÃO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# ATENÇÃO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# ESSA CONVERSÃO TRUNCA NOMES EM 12 CARACTERES

iograf = PSRIOGrafResultBinary()
ret = iograf.toCSV(FILEPATH_HEADER, FILEPATH_BIN, FILEPATH_CSV)
iograf.deleteInstance()

# ------------------------------------------------------------------
# CSV -> Binarios
# ------------------------------------------------------------------

iograf = PSRIOGrafResult()
iograf.toBinary(FILEPATH_CSV, FILEPATH_HEADER, FILEPATH_BIN)
iograf.deleteInstance()

# ------------------------------------------------------------------
# Segunda leitura para testar conversão
# ------------------------------------------------------------------

iograf = PSRIOGrafResultBinary()
iograf.initLoad(FILE_PATH * ".hdr", FILE_PATH * ".bin")

# versão CSV
# iograf = PSRIOGrafResult_create(0)
# PSRIOGrafResult_initLoad(FILEPATH, PSRIO_GRAF_FORMAT_DEFAULT)

@assert iograf.getTotalStages() == 12
@assert iograf.getTotalSeries() == 10
@assert iograf.getTotalBlocks() == 3
# A CONVERSÃO TRUNCA NOMES EM 12 CARACTERES
@assert iograf.getNameLength() == 12
@assert iograf.getVariableByBlock() == 1
@assert iograf.getVariableBySerie() == 1
@assert iograf.getVariableBySerie() == 1
@assert iograf.getVariableByHour() == 0
@assert iograf.getStageType() == PSR_STAGETYPE_MONTHLY
@assert iograf.getInitialStage() == 1
@assert iograf.getInitialYear() == 2016
@assert strip(iograf.getUnit()) == "MW"
# obtem número de colunas
# A CONVERSÃO TRUNCA NOMES EM 12 CARACTERES
@assert iograf.maxAgent() == 3
names = ["X", "Y", "Z"]
for i in 1:3
    @assert strip(iograf.getAgent(i - 1)) == names[i]
end

for estagio = 1:12
    for serie = 1:10
        for bloco = 1:3
            # posiciona o ponteiro na entrada correta
            iograf.seekStage2(estagio, serie, bloco)
            @assert iograf.getCurrentStage() == estagio
            @assert iograf.getCurrentSerie() == serie
            @assert iograf.getCurrentBlock() == bloco

            # obtem valor dos agentes para o registro
            agentX_value = estagio + serie
            agentY_value = serie - estagio
            agentZ_value = estagio + serie + bloco * 100

            @assert iograf.getData(0) == agentX_value
            @assert iograf.getData(1) == agentY_value
            @assert iograf.getData(2) == agentZ_value
        end
    end
end

iograf.closeLoad()
iograf.deleteInstance()
