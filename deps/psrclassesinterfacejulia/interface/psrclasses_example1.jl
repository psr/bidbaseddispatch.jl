#       ----------------------------------
#       Parte 1 - Inicializacao PSRCLASSES
#       ----------------------------------

#       Seta PATH para PSRCLASSES
#       -------------------------
PATH_PSRCLASSES = normpath(joinpath(@__DIR__, ".."))
PATH_CASE = joinpath(PATH_PSRCLASSES, "examples", "data", "bo-caso4/")

if get(ENV, "PSRC_JL_TESTING", "0") != "1"
#       Inclui as definicoes do modulo PSRClasses
#       -----------------------------------------
    include(joinpath(PATH_PSRCLASSES, "psrclasses.jl"))
    include(joinpath(PATH_PSRCLASSES, "psrclassesoo.jl"))

#       Inicializa a DLL do PSRClasses
#       ------------------------------
    PSRClasses_init(PATH_PSRCLASSES)
end

#       Configura Gerenciador de Log
#       ----------------------------
ilog = PSRManagerLog()

#       Inicializa idioma portugues para log
#       ------------------------------------
ilog.initPortuguese()

#       Acrescenta Log do Tipo Console Simples ao Gerenciador (log na tela)
#       -------------------------------------------------------------------
ilog.addLog(PSRLogSimpleConsole())

#       Configura Gerenciador de Mascaras e carrega Mascaras
#       ----------------------------------------------------
igmsk  = PSRManagerIOMask()
iret = igmsk.importFile(string(PATH_PSRCLASSES, "Masks_SDDP_V10.2.pmk"))
iret = igmsk.importFile(string(PATH_PSRCLASSES, "Masks_SDDP_V10.3.pmk"))
iret = igmsk.importFile(string(PATH_PSRCLASSES, "Masks_SDDP_Blocks.pmk"))

#       Configura Gerenciador de Modelos e carrega Modelos
#       --------------------------------------------------
igmdl = PSRManagerModels()
iret = igmdl.importFile(string(PATH_PSRCLASSES, "Models_SDDP_V10.2.pmd"))
iret = igmdl.importFile(string(PATH_PSRCLASSES, "Models_SDDP_V10.3.pmd"))


#       -------------------------------------
#       Parte 2 - Carrega estudo da base SDDP
#       -------------------------------------

#       Cria Estudo
#       -----------
istdy = PSRStudy()

#       Cria Objeto Leitor de Estudo SDDP e carrega dados
#       ----------------------------------------------------
iosddp = PSRIOSDDP()
iret = iosddp.load(istdy, PATH_CASE, PATH_CASE, PSR_SDDP_VERSION_14)


#       --------------------------------------------
#       Parte 3 - Obtem lista de entidades desejadas
#       --------------------------------------------

#       Obtem lista de usinas do estudo
#       ---------------------------------
lstsys = istdy.getCollectionSystems()
lsthyd = istdy.getCollectionPlants(PSR_PLANTTYPE_HYDRO)
lstter = istdy.getCollectionPlants(PSR_PLANTTYPE_THERMAL)
listgnd = istdy.getCollectionPlants(PSR_PLANTTYPE_GND)

#       Obtem total de sistemas e usinas associadas
#       -------------------------------------------
nsys = lstsys.maxElements()
nhydro = lsthyd.maxElements()
nthermal = lstter.maxElements()
ngnd = listgnd.maxElements()


#       -----------------------------------------
#       Parte 4 - Obtem informacoes do PSRCLASSES
#       -----------------------------------------

#       Prealoca estruturas de dados para sistemas
#       ---------------------------------------
systemNameAux = allocstring(nsys, 12)
systemCode = Array{Int32}(undef, nsys)

#       Prealoca estruturas de dados para as termicas
#       ------------------------------------------
thermNameAux = allocstring(nthermal, 12)
thermCode = Array{Int32}(undef, nthermal)
thermFut = Array{Int32}(undef, nthermal)
thermCap = Array{Float64}(undef, nthermal)
thermCost = Array{Float64}(undef, nthermal)
thermCVaria = Array{Float64}(undef, nthermal)
thermCTransp = Array{Float64}(undef, nthermal)

#       Obtem ponteiro de usinas termicas para os sistemas associados
#       -------------------------------------------------------
ipthermsys = Array{Int32}(undef, nthermal)
lstter.mapRelationShip(lstsys, ipthermsys, PSR_RELATIONSHIP_1TO1, false)

#       Obtem ponteiro de usinas hydros para os sistemas associados
#       -----------------------------------------------------
iphydrosys = Array{Int32}(undef, nhydro)
lsthyd.mapRelationShip(lstsys, iphydrosys, PSR_RELATIONSHIP_1TO1, false)

#       Cria mapeador de dados para os sistemas
#       ---------------------------------------
imapsys = PSRMapData()
imapsys.addElements(lstsys)
imapsys.mapParm("name", systemNameAux, 12)  # TODO
imapsys.mapParm("code", systemCode, 0)

#       Cria mapeador de dados para as usinas termicas
#       ----------------------------------------------
imapter = PSRMapData()
imapter.addElements(lstter)
imapter.mapParm("name", thermNameAux, 12)
imapter.mapParm("code", thermCode, 0)
imapter.mapVector("Existing", thermFut, 0)
imapter.mapVector("PotInst", thermCap, 0)
imapter.mapVector("O&MCost", thermCVaria, 0)
imapter.mapVector("CTransp", thermCTransp, 0)
imapter.mapDimensionedVector("CEsp", thermCost, 0, "segmemt", "block")



#       Cria um controlador de tempo e associa todo o estudo
#       ----------------------------------------------------------
ictrl = PSRTimeController()
ictrl.addElement(istdy, true)
ictrl.configureFrom(istdy)


#       Posiciona controlador de tempo no primeiro estagio do estudo
#       ------------------------------------------------------------
ictrl.gotoStage(1)

#       Posiciona os vetores com as dimens�es informadas
#       Vetores que foram mapeados com a dimens�o "segment" ser�o posicionados em segment=1
#       Vetores que foram mapeados com a dimens�o "block" ser�o posicionados em block=1
#       -----------------------------------------------------------------------------------
ictrl.setDimension("segmemt", 1)
ictrl.setDimension("block", 1)

#       Popula estruturas de dados mapeados
#       -----------------------------------
imapsys.pullToMemory()
imapter.pullToMemory()

#       Passo adicional para obter arrays de strings
#       --------------------------------------------
systemName = splitstring(systemNameAux, 12)
thermName = splitstring(thermNameAux, 12)

#       --------------------------------------------------
#       Parte 5 - Exibe um sumario das informacoes do caso
#       --------------------------------------------------
println(string("Descricao do caso: ", istdy.description()))

println(string("Total de estagios: ", istdy.getNumberStages()))
println(string("Total de cenarios: ", istdy.getNumberSimulations()))
println(string("Total de blocos: ", istdy.getNumberBlocks()))

println(string("Total de sistemas do caso: ", nsys))
println(string("Total de hydros do caso: ", nhydro))
println(string("Total de termicas do caso: ", nthermal))
println(string("Total de gnds do caso: ", ngnd))

println("Overview das Termicas:")


#       Obtem par�metros de interesse do estudo
#       ---------------------------------------
number_stages = istdy.getNumberStages()
number_blocks = istdy.getNumberBlocks()

@static if get(ENV, "PSRC_JL_TESTING", "0") == "1"
    @test number_stages == 160
    @test number_blocks == 3
end

#       Loops de configura��es (percorrer todos os est�gios e blocos)
#       -------------------------------------------------------------
for stage = 1:5 # number_stages
    for block = 1:number_blocks

        println(string("Configuracao: ", stage, " bloco: ", block))

#           Seta o estagio
#           --------------
        ictrl.gotoStage(stage)

#           Seta o bloco atual pelo time controller
#           ---------------------------------------------------
        ictrl.setDimension("block", block)

#           Refaz o pull para a memoria dos atributos
#           atualizando os vetores JULIA para a fotografia atual
#           ---------------------------------------------------
        imapsys.pullToMemory()
        imapter.pullToMemory()

#           Mostra na tela as informacoes mapeadas das termicas
#           ---------------------------------------------------
        println(string("Exibindo atributos para estagio: ", stage, " bloco: ", block))
        for iterm = 1:nthermal
            println(string(thermCode[iterm], " ",
                              thermName[iterm], " ",
                              "SISTEMA: ",systemCode[ipthermsys[iterm]], " ", systemName[ipthermsys[iterm]], " ",
                              thermFut[iterm], " ",
                              thermCap[iterm], " ",
                              thermCost[iterm], " ",
                              thermCVaria[iterm], " ",
                              thermCTransp[iterm]))
        end
    end
end

#=

=#