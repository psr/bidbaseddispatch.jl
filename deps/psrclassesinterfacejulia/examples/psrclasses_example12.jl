#       ----------------------------------
#       Parte 1 - Inicializacao PSRCLASSES
#       ----------------------------------

#       Seta PATH para PSRCLASSES
#       -------------------------
PATH_PSRCLASSES = normpath(joinpath(@__DIR__, ".."))

if get(ENV, "PSRC_JL_TESTING", "0") != "1"
#       Inclui as definicoes do modulo PSRClasses
#       -----------------------------------------
    include(string(PATH_PSRCLASSES, "psrclasses.jl"))

#       Inicializa a DLL do PSRClasses
#       ------------------------------
    PSRClasses_init(PATH_PSRCLASSES)
end

#       Configura Gerenciador de Log
#       ----------------------------
ilog = PSRManagerLog_getInstance(0)

#       Inicializa idioma portugues para log
#       ------------------------------------
PSRManagerLog_initPortuguese(ilog)

#       Acrescenta Log do Tipo Console Simples ao Gerenciador (log na tela)
#       -------------------------------------------------------------------
ilogcons = PSRLogSimpleConsole_create(0)
PSRManagerLog_addLog(ilog, ilogcons)

#       Configura Gerenciador de Mascaras e carrega Mascaras
#       ----------------------------------------------------
igmsk  = PSRManagerIOMask_getInstance(0)
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_V10.2.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_V10.3.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_Blocks.pmk"))

#       Configura Gerenciador de Modelos e carrega Modelos
#       --------------------------------------------------
igmdl = PSRManagerModels_getInstance(0)
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_V10.2.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_V10.3.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_Keywords.pmd"))

#       -------------------------------------
#       Parte 2 - Carrega estudo da base SDDP
#       -------------------------------------

#       Cria Estudo
#       -----------
istdy = PSRStudy_create(0)

#       Cria Objeto Leitor de Estudo SDDP e carrega dados
#       ----------------------------------------------------
PATH_CASE = joinpath(PATH_PSRCLASSES, "examples\\data\\exemplo-renovavel\\")
iosddp = PSRIOSDDP_create(0)
iret = PSRIOSDDP_load(iosddp, istdy, PATH_CASE,
                                             PATH_CASE, PSR_SDDP_VERSION_14)

number_stages = PSRStudy_getNumberStages(istdy)
number_simulations = PSRStudy_getNumberSimulations(istdy)

#       Obtem ponteiro para controlador de cenarios de gnd gerados no load
#       Se nao houver cenario horario, ictrlgnd sera C_NULL
#       ------------------------------------------------------------------
ictrlgnd = PSRIOSDDP_getGndHourlyScenarios(iosddp)

#       Obtem lista de usinas do estudo
#       ---------------------------------
listgnd = PSRStudy_getCollectionPlants(istdy, PSR_PLANTTYPE_GND)

#       Obtem total de sistemas e usinas associadas
#       -------------------------------------------
ngnd = PSRCollectionElement_maxElements(listgnd)

#       Prealoca estruturas de dados para as gnds
#       ------------------------------------------
gndGeneration = Array{Float64}(undef, ngnd)

#       Cria mapeador de dados para as usinas gnd
#       ----------------------------------------------
imapgnd = PSRMapData_create(0)
PSRMapData_addElements(imapgnd, listgnd)
PSRMapData_mapVector(imapgnd, "HourGeneration", gndGeneration, 0)

#       Cria um controlador de tempo e associa todo o estudo
#       ----------------------------------------------------------
ictrl = PSRTimeController_create(0)
PSRTimeController_addElement(ictrl, istdy, true)
PSRTimeController_configureFrom(ictrl, istdy)

for stage = 1:min(2, number_stages)
    for scen = 1:min(3, number_simulations)

#         Posiciona gnds no estudo em: (estagio/hora/cenario)
#         ---------------------------------------------------
        if ( ictrlgnd != C_NULL)
            PSRIOElementHourlyScenarios_gotoStageHour(ictrlgnd, stage, scen, 1)
        end

#         Popula estruturas de dados mapeados
#         -----------------------------------
        PSRMapData_pullToMemory(imapgnd)

        for ignd = 1:min(4, ngnd)
            println(gndGeneration)
        end
    end
end


