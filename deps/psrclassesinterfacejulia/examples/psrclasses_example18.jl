#       ----------------------------------
#       Parte 1 - Inicializacao PSRCLASSES
#       ----------------------------------

#       Seta PATH para PSRCLASSES
#       -------------------------
PATH_PSRCLASSES = normpath(joinpath(@__DIR__, ".."))

if get(ENV, "PSRC_JL_TESTING", "0") != "1"
#       Inclui as definicoes do modulo PSRClasses
#       -----------------------------------------
    include(string(PATH_PSRCLASSES, "psrclasses.jl"))

#       Inicializa a DLL do PSRClasses
#       ------------------------------
    PSRClasses_init(PATH_PSRCLASSES)
end
#       Configura Gerenciador de Log
#       ----------------------------
ilog = PSRManagerLog_getInstance(0)

#       Inicializa idioma portugues para log
#       ------------------------------------
PSRManagerLog_initPortuguese(ilog)

#       Acrescenta Log do Tipo Console Simples ao Gerenciador (log na tela)
#       -------------------------------------------------------------------
ilogcons = PSRLogSimpleConsole_create(0)
PSRManagerLog_addLog(ilog, ilogcons)

#       Configura Gerenciador de Mascaras e carrega Mascaras
#       ----------------------------------------------------
igmsk  = PSRManagerIOMask_getInstance(0)
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_V10.2.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_V10.3.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_Blocks.pmk"))

#       Configura Gerenciador de Modelos e carrega Modelos
#       --------------------------------------------------
igmdl = PSRManagerModels_getInstance(0)
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_V10.2.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_V10.3.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_Keywords.pmd"))

#       -------------------------------------
#       Parte 2 - Carrega estudo da base SDDP
#       -------------------------------------

#       Cria Estudo
#       -----------
istdy = PSRStudy_create(0)


#       Cria Objeto Leitor de Estudo SDDP e carrega dados
#       ----------------------------------------------------
PATH_CASE = joinpath(PATH_PSRCLASSES, "examples\\data\\bo-caso4\\")
iosddp = PSRIOSDDP_create(0)
iret = PSRIOSDDP_load(iosddp, istdy, PATH_CASE,
                                             PATH_CASE, PSR_SDDP_VERSION_14)

#       Obtem lista de usinas hydro do estudo
#       -------------------------------------
lsthyd = PSRStudy_getCollectionPlants(istdy, PSR_PLANTTYPE_HYDRO)
nhydro = PSRCollectionElement_maxElements(lsthyd)

#       Prealoca estruturas de dados para as hydro
#       ------------------------------------------
hydroNameAux = allocstring(nhydro, 12)
hydroCode = Array{Int32}(undef, nhydro)
hydroInfoRieg = Array{Int32}(undef, nhydro)

#       Cria mapeador de dados para as usinas hydro
#       ----------------------------------------------
imaphyd = PSRMapData_create(0)
PSRMapData_addElements(imaphyd, lsthyd)
PSRMapData_mapParm(imaphyd, "name", hydroNameAux, 12)
PSRMapData_mapParm(imaphyd, "code", hydroCode, 0)
PSRMapData_mapVector(imaphyd, "@info(DataRieg)", hydroInfoRieg, 0)

ictrl = PSRTimeController_create(0)
PSRTimeController_addElement(ictrl, istdy, true)
PSRTimeController_configureFrom(ictrl, istdy)

#       Posiciona controlador de tempo no primeiro estagio do estudo
#       ------------------------------------------------------------
PSRTimeController_gotoStage(ictrl, 1)

#       Popula estruturas de dados mapeados
#       -----------------------------------
PSRMapData_pullToMemory(imaphyd)

#       Passo adicional para obter arrays de strings
#       --------------------------------------------
hydroName = splitstring(hydroNameAux, 12)

#       Mostra usinas downstream para cada usina
#       ----------------------------------------
for ihyd = 1:min(2, nhydro)

    if ( hydroInfoRieg[ihyd] == MAP_INFOTYPE_NULL)
        println(string(hydroCode[ihyd], " - ", hydroName[ihyd], " nao possui informacao de irrigacao"))
    elseif ( hydroInfoRieg[ihyd] == MAP_INFOTYPE_BEFOREDATA)
        println(string(hydroCode[ihyd], " - ", hydroName[ihyd], " possui informacao de irrigacao, mas em data posterior a data atual"))
    elseif ( hydroInfoRieg[ihyd] == MAP_INFOTYPE_AFTERYEAR)
        println(string(hydroCode[ihyd], " - ", hydroName[ihyd], " possui informacao de irrigacao, mas o ultimo ano de dados eh anterior ao ano atual"))
    end
end
