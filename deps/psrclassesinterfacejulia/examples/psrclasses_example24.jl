import Libdl

PATH_PSRCLASSES = normpath(joinpath(@__DIR__, ".."))

if get(ENV, "PSRC_JL_TESTING", "0") != "1"
#       Inclui as definicoes do modulo PSRClasses
#       -----------------------------------------
    include(string(PATH_PSRCLASSES, "psrclasses.jl"))

#       Inicializa a DLL do PSRClasses
#       ------------------------------
    PSRClasses_init(PATH_PSRCLASSES)
end

# Cria handle para funcao de custo futuro
fcost = PSRIOSDDPFutureCost_create(0)

# Define configuracao da funcao de custo futuro
PSRIOSDDPFutureCost_setInitialStage(fcost, 1, 2016)
PSRIOSDDPFutureCost_setTotalStages(fcost, 12)
# you cant store any data in the last stage

# Variaveis de volume
PSRIOSDDPFutureCost_addVolume(fcost, 1)
PSRIOSDDPFutureCost_addVolume(fcost, 2)

# Variaveis de vazao
PSRIOSDDPFutureCost_setOrd(fcost, 6)
PSRIOSDDPFutureCost_addInflow(fcost, 1)
PSRIOSDDPFutureCost_addInflow(fcost, 2)
# PSRIOSDDPFutureCost_addInflow(fcost, 3)


@show PSRIOSDDPFutureCost_maxVolume(fcost)
@show PSRIOSDDPFutureCost_maxInflow(fcost)
@show PSRIOSDDPFutureCost_maxOrd(fcost)

println("Init FCF in SAVE mode")
FCF_PATH = normpath(joinpath(PATH_PSRCLASSES, "psrclasses_example11.psr"))
PSRIOSDDPFutureCost_initSave(fcost, FCF_PATH)
# println("write 5 cuts")

# Adiciona 5 cortes para os estagios
for stage = 1:12# 2
    for icut = 1:1
        for ord = 1:6
            for inflow = 1:2
                PSRIOSDDPFutureCost_setInflow(fcost, Int32(inflow - 1), Int32(ord - 1), inflow * 10. + ord)
            end
        end
        for vol = 1:2
            PSRIOSDDPFutureCost_setVolume(fcost, vol - 1, vol * 100. + icut * 1000)
        end
        PSRIOSDDPFutureCost_setRHS(fcost, stage * 100. + icut)
        #   Define header do corte [definido pela tripla (simulacao, iteracao, cluster)]
        PSRIOSDDPFutureCost_setHeaderInformation(fcost, 1, 1, 0)
        #   Adiciona o corte definido a FCF
        PSRIOSDDPFutureCost_addCut(fcost, stage)
    end
end

# Fecha funcao
# ------------
PSRIOSDDPFutureCost_close(fcost)
PSRIOSDDPFutureCost_deleteInstance(fcost)

# abre a fcf no modo append
# -----------------------------------------

# Cria handle para funcao de custo futuro
# -----------------------------------------
fcost = PSRIOSDDPFutureCost_create(0)

println("Init FCF in APPEND mode")
PSRIOSDDPFutureCost_initAppend(fcost, FCF_PATH)

@show PSRIOSDDPFutureCost_maxVolume(fcost)
@show PSRIOSDDPFutureCost_maxInflow(fcost)
@show PSRIOSDDPFutureCost_maxOrd(fcost)

# Adiciona 5 cortes para os estagios
for stage = 1:12# 2
    for icut = 1:1
        for ord = 1:6
            for inflow = 1:2
                PSRIOSDDPFutureCost_setInflow(fcost, Int32(inflow - 1), Int32(ord - 1), inflow * 10. + ord)
            end
        end
        for vol = 1:2
            PSRIOSDDPFutureCost_setVolume(fcost, vol - 1, vol * 100. + icut * 1000)
        end
        PSRIOSDDPFutureCost_setRHS(fcost, stage * 100. + icut)
        #   Define header do corte [definido pela tripla (simulacao, iteracao, cluster)]
        PSRIOSDDPFutureCost_setHeaderInformation(fcost, 1, 1, 0)
        #   Adiciona o corte definido a FCF
        PSRIOSDDPFutureCost_addCut(fcost, stage)
    end
end

# Fecha funcao
# ------------
PSRIOSDDPFutureCost_close(fcost)
PSRIOSDDPFutureCost_deleteInstance(fcost)

# Cria handle para funcao de custo futuro
# -----------------------------------------
fcost = PSRIOSDDPFutureCost_create(0)

println("Init FCF in READ mode")
PSRIOSDDPFutureCost_load(fcost, FCF_PATH)
@show PSRIOSDDPFutureCost_maxVolume(fcost)
@show PSRIOSDDPFutureCost_maxInflow(fcost)
@show PSRIOSDDPFutureCost_maxOrd(fcost)

# Fecha funcao
PSRIOSDDPFutureCost_close(fcost)
PSRIOSDDPFutureCost_deleteInstance(fcost)