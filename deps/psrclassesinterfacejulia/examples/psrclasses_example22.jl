#       ----------------------------------
#       Parte 1 - Inicializacao PSRCLASSES
#       ----------------------------------

#       Seta PATH para PSRCLASSES
#       -------------------------
PATH_PSRCLASSES = normpath(joinpath(@__DIR__, ".."))

if get(ENV, "PSRC_JL_TESTING", "0") != "1"
#       Inclui as definicoes do modulo PSRClasses
#       -----------------------------------------
    include(string(PATH_PSRCLASSES, "psrclasses.jl"))

#       Inicializa a DLL do PSRClasses
#       ------------------------------
    PSRClasses_init(PATH_PSRCLASSES)
end
#       Configura Gerenciador de Log
#       ----------------------------
ilog = PSRManagerLog_getInstance(0)

#       Inicializa idioma portugues para log
#       ------------------------------------
PSRManagerLog_initPortuguese(ilog)

#       Acrescenta Log do Tipo Console Simples ao Gerenciador (log na tela)
#       -------------------------------------------------------------------
ilogcons = PSRLogSimpleConsole_create(0)
PSRManagerLog_addLog(ilog, ilogcons)

#       Configura Gerenciador de Mascaras e carrega Mascaras
#       ----------------------------------------------------
igmsk  = PSRManagerIOMask_getInstance(0)
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_NetPlan_V2.6.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_NetPlan_V3.0.pmk"))

#       Configura Gerenciador de Modelos e carrega Modelos
#       --------------------------------------------------
igmdl = PSRManagerModels_getInstance(0)
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_NETPLAN_V2.6.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_NETPLAN_V3.0.pmd"))

#       ----------------------------------------
#       Parte 2 - Carrega estudo da base NETPLAN
#       ----------------------------------------

#       Cria Estudo
#       -----------
istdy = PSRStudy_create(0)

#       Cria Objeto Leitor de Estudo NETPLAN e carrega dados
#       ----------------------------------------------------
PATH_CASE = joinpath(PATH_PSRCLASSES, "examples\\data\\netplan-expansao-24barras\\")
ionetp = PSRIONETPLAN_create(0)
PSRIONETPLAN_useSerieCandidates(ionetp, true)
iret = PSRIONETPLAN_load(ionetp, istdy, PATH_CASE)

#       ---------------------------------------------
#       Parte 3 - Obtem lista das entidades do estudo
#       ---------------------------------------------

#       Obtem lista de capacitores do estudo
#       ------------------------------------
listcapc = PSRStudy_getCollectionShunts(istdy, PSR_DEVICETYPE_CAPACITOR)

#       Obtem total de capacitores
#       --------------------------
ncapc = PSRCollectionElement_maxElements(listcapc)

#       Obtem ponteiro da estrutura de expansao do estudo
#       -------------------------------------------------
expansion = PSRStudy_expansionData(istdy)

#       Cria projeto e decisao de expansao para todos os capacitores
#       ------------------------------------------------------------
for icapc = 1:ncapc
    capacitor = PSRCollectionElement_element(listcapc, icapc - 1)

#         Cria projeto
#         ------------
    project = PSRExpansionProject_create(0)
    PSRExpansionProject_setUnitElement(project, capacitor)
    PSRExpansionData_addProject(expansion, project)

#         Cria decisao de expansao
#         ------------------------
    decision = PSRExpansionDecision_create(0)
    PSRExpansionData_addDecision(expansion, decision)

#         Adiciona 3 unidades pra cada capacitor em janeiro de 2020
#         ---------------------------------------------------------
    PSRExpansionDecision_setProject(decision, project, 1, 2020, 1, 2020, 3, PSR_DECISIONTYPE_EXACTLY)
end

#       Atualiza base NETPLAN
#       ---------------------
ionetplan = PSRIONETPLAN_create(0)
PSRIONETPLAN_updateFromExpansionData(ionetplan, istdy, PATH_CASE, false)
