#       ----------------------------------
#       Parte 1 - Inicializacao PSRCLASSES
#       ----------------------------------

#       Seta PATH para PSRCLASSES
#       -------------------------
PATH_PSRCLASSES = normpath(joinpath(@__DIR__, ".."))


if get(ENV, "PSRC_JL_TESTING", "0") != "1"
#       Inclui as definicoes do modulo PSRClasses
#       -----------------------------------------
    include(string(PATH_PSRCLASSES, "psrclasses.jl"))

#       Inicializa a DLL do PSRClasses
#       ------------------------------
    PSRClasses_init(PATH_PSRCLASSES)
end
#       Inclui as definicoes do modulo PSRClasses
#       -----------------------------------------
        # include(string(PATH_PSRCLASSES,"psrclasses.jl"))

#       Inicializa a DLL do PSRClasses
#       ------------------------------
        # PSRClasses_init(PATH_PSRCLASSES)

#       Configura Gerenciador de Log
#       ----------------------------
ilog = PSRManagerLog_getInstance(0)

#       Inicializa idioma portugues para log
#       ------------------------------------
PSRManagerLog_initPortuguese(ilog)

#       Acrescenta Log do Tipo Console Simples ao Gerenciador (log na tela)
#       -------------------------------------------------------------------
ilogcons = PSRLogSimpleConsole_create(0)
PSRManagerLog_addLog(ilog, ilogcons)

#       Configura Gerenciador de Mascaras e carrega Mascaras
#       ----------------------------------------------------
# erro não pode ser lido 15/12/2020
igmsk  = PSRManagerIOMask_getInstance(0)
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_V10.2.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_V10.3.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_Blocks.pmk"))

#       Configura Gerenciador de Modelos e carrega Modelos
#       --------------------------------------------------
# erro não pode ser lido 15/12/2020
igmdl = PSRManagerModels_getInstance(0)
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_V10.2.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_V10.3.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_Keywords.pmd"))

#       -------------------------------------
#       Parte 2 - Carrega estudo da base SDDP
#       -------------------------------------

#       Cria Estudo
#       -----------
istdy = PSRStudy_create(0)


#       Cria Objeto Leitor de Estudo SDDP e carrega dados
#       ----------------------------------------------------
PATH_CASE = joinpath(PATH_PSRCLASSES, "examples\\data\\bo-caso4\\")
iosddp = PSRIOSDDP_create(0)
iret = PSRIOSDDP_load(iosddp, istdy, PATH_CASE,
                                             PATH_CASE, PSR_SDDP_VERSION_14)

parser = PSRParsers_create(0)
parser = PSRParsers_getInstance(parser)

data = PSRStudy_getDateFromStage(istdy, 1)

mes = PSRParsers_getMonthFromDate(parser, data)
ano = PSRParsers_getYearFromDate(parser, data)

println(string("Estagio inicial: ", mes, "/", ano))

#       -------------------------------------
#       Parte 3 - Obtem lista de Usinas Hidro
#       -------------------------------------

#       Obtem lista de usinas hydro do estudo
#       -------------------------------------
lsthyd = PSRStudy_getCollectionPlants(istdy, PSR_PLANTTYPE_HYDRO)
nhydro = PSRCollectionElement_maxElements(lsthyd)

#       Prealoca estruturas de dados para as hydro
#       ------------------------------------------
hydroNameAux = allocstring(nhydro, 12)
hydroCode = Array{Int32}(undef, nhydro) # Array(Int32, nhydro)


#       Cria mapeador de dados para as usinas hydro
#       ----------------------------------------------
imaphyd = PSRMapData_create(0)
PSRMapData_addElements(imaphyd, lsthyd)
PSRMapData_mapParm(imaphyd, "name", hydroNameAux, 12)
PSRMapData_mapParm(imaphyd, "code", hydroCode, 0)

#       Popula estruturas de dados mapeados
#       -----------------------------------
PSRMapData_pullToMemory(imaphyd)

#       Passo adicional para obter arrays de strings
#       --------------------------------------------
hydroName = splitstring(hydroNameAux, 12)

#       Obtem ponteiro de usinas hydro para usinas downstream
#       -----------------------------------------------------
iphydTurTo = Array{Int32}(undef, nhydro) # Array(Int32, nhydro)
iphydSpillTo = Array{Int32}(undef, nhydro) # Array(Int32, nhydro)

PSRCollectionElement_mapRelationShip(lsthyd, lsthyd, iphydTurTo, PSR_RELATIONSHIP_TURBINED_TO, false)
PSRCollectionElement_mapRelationShip(lsthyd, lsthyd, iphydSpillTo, PSR_RELATIONSHIP_SPILLED_TO, false)

#       Mostra usinas downstream para cada usina
#       ----------------------------------------
for ihyd = 1:min(3, nhydro)

#           Preenche identifica de usinas downstream (se existentes)
#           --------------------------------------------------------
    str_usina_turb = "NONE"
    str_usina_vert = "NONE"

    index_plant_turb = iphydTurTo[ihyd]
    index_plant_vert = iphydSpillTo[ihyd]

    if ( index_plant_turb > 0 )
        str_usina_turb = string(hydroCode[index_plant_turb], " - ", hydroName[index_plant_turb])
    end
    if ( index_plant_vert > 0 )
        str_usina_vert = string(hydroCode[index_plant_vert], " - ", hydroName[index_plant_vert])
    end

    println(string(hydroCode[ihyd], " - ", hydroName[ihyd], " Turb: ", str_usina_turb, " Vert: ", str_usina_vert))
end

#       Faz mapeamento inverso para obter usinas upstream (turbinamento)
#       ----------------------------------------------------------------
ipTurb = Array{Int32}(undef, nhydro) # Array(Int32, nhydro)
lnkTurb = Array{Int32}(undef, nhydro * 4) # Array(Int32, nhydro*4)
numTurb = Array{Int32}(undef, nhydro * 4) # Array(Int32, nhydro*4)

#       Mapeamento cria automaticamente uma estrutura de lista encadeada
#       ----------------------------------------------------------------
PSRCollectionElement_mapInverseRelationShip(lsthyd, lsthyd, ipTurb, lnkTurb, numTurb, PSR_RELATIONSHIP_TURBINED_TO, false)

#       Mostra usinas que estao upstream para cada usina (turbinamento)
#       ---------------------------------------------------------------
for ihyd = 1:min(3, nhydro)
    println(string("Mostrando Usinas Downstream (turbinamento) de ", hydroCode[ihyd], " - ", hydroName[ihyd]))

    current_index = ipTurb[ihyd]
    while ( current_index > 0 )
        index_plant_turb = numTurb[current_index]

        println(string(" - ", hydroCode[index_plant_turb], " - ", hydroName[index_plant_turb]))
        current_index = lnkTurb[current_index]
    end
end

