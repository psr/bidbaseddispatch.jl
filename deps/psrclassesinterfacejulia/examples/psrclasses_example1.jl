#       ----------------------------------
#       Parte 1 - Inicializacao PSRCLASSES
#       ----------------------------------

#       Seta PATH para PSRCLASSES
#       -------------------------
PATH_PSRCLASSES = normpath(joinpath(@__DIR__, ".."))

if get(ENV, "PSRC_JL_TESTING", "0") != "1"
#       Inclui as definicoes do modulo PSRClasses
#       -----------------------------------------
    include(string(PATH_PSRCLASSES, "psrclasses.jl"))

#       Inicializa a DLL do PSRClasses
#       ------------------------------
    PSRClasses_init(PATH_PSRCLASSES)
end

#       Configura Gerenciador de Log
#       ----------------------------
ilog = PSRManagerLog_getInstance(0)

#       Inicializa idioma portugues para log
#       ------------------------------------
PSRManagerLog_initPortuguese(ilog)

#       Acrescenta Log do Tipo Console Simples ao Gerenciador (log na tela)
#       -------------------------------------------------------------------
ilogcons = PSRLogSimpleConsole_create(0)
PSRManagerLog_addLog(ilog, ilogcons)

#       Configura Gerenciador de Mascaras e carrega Mascaras
#       ----------------------------------------------------
igmsk  = PSRManagerIOMask_getInstance(0)
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_V10.2.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_V10.3.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_Blocks.pmk"))

#       Configura Gerenciador de Modelos e carrega Modelos
#       --------------------------------------------------
igmdl = PSRManagerModels_getInstance(0)
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_V10.2.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_V10.3.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_Keywords.pmd"))

#       -------------------------------------
#       Parte 2 - Carrega estudo da base SDDP
#       -------------------------------------

#       Cria Estudo
#       -----------
istdy = PSRStudy_create(0)

#       Cria Objeto Leitor de Estudo SDDP e carrega dados
#       ----------------------------------------------------
PATH_CASE = joinpath(PATH_PSRCLASSES, "examples\\data\\bo-caso4\\")
iosddp = PSRIOSDDP_create(0)
iret = PSRIOSDDP_load(iosddp, istdy, PATH_CASE,
                                             PATH_CASE, PSR_SDDP_VERSION_14)
#       --------------------------------------------
#       Parte 3 - Obtem lista de entidades desejadas
#       --------------------------------------------

#       Obtem lista de usinas do estudo
#       ---------------------------------
lstsys = PSRStudy_getCollectionSystems(istdy)
lsthyd = PSRStudy_getCollectionPlants(istdy, PSR_PLANTTYPE_HYDRO)
lstter = PSRStudy_getCollectionPlants(istdy, PSR_PLANTTYPE_THERMAL)
listgnd = PSRStudy_getCollectionPlants(istdy, PSR_PLANTTYPE_GND)

#       Obtem total de sistemas e usinas associadas
#       -------------------------------------------
nsys = PSRCollectionElement_maxElements(lstsys)
nhydro = PSRCollectionElement_maxElements(lsthyd)
nthermal = PSRCollectionElement_maxElements(lstter)
ngnd = PSRCollectionElement_maxElements(listgnd)

#       -----------------------------------------
#       Parte 4 - Obtem informacoes do PSRCLASSES
#       -----------------------------------------

#       Prealoca estruturas de dados para sistemas
#       ---------------------------------------
systemNameAux = allocstring(nsys, 12)
systemCode = Array{Int32}(undef, nsys)

#       Prealoca estruturas de dados para as termicas
#       ------------------------------------------
thermNameAux = allocstring(nthermal, 12)
thermCode = Array{Int32}(undef, nthermal)
thermFut = Array{Int32}(undef, nthermal)
thermCap = Array{Float64}(undef, nthermal)
thermCost = Array{Float64}(undef, nthermal)
thermCVaria = Array{Float64}(undef, nthermal)
thermCTransp = Array{Float64}(undef, nthermal)

#       Obtem ponteiro de usinas termicas para os sistemas associados
#       -------------------------------------------------------
ipthermsys = Array{Int32}(undef, nthermal)
PSRCollectionElement_mapRelationShip(lstter, lstsys, ipthermsys, PSR_RELATIONSHIP_1TO1, false)

#       Obtem ponteiro de usinas hydros para os sistemas associados
#       -----------------------------------------------------
iphydrosys = Array{Int32}(undef, nhydro)
PSRCollectionElement_mapRelationShip(lsthyd, lstsys, iphydrosys, PSR_RELATIONSHIP_1TO1, false)

#       Cria mapeador de dados para os sistemas
#       ---------------------------------------
imapsys = PSRMapData_create(0)
PSRMapData_addElements(imapsys, lstsys)
PSRMapData_mapParm(imapsys, "name", systemNameAux, 12)
PSRMapData_mapParm(imapsys, "code", systemCode, 0)

#       Cria mapeador de dados para as usinas termicas
#       ----------------------------------------------
imapter = PSRMapData_create(0)
PSRMapData_addElements(imapter, lstter)
PSRMapData_mapParm(imapter, "name", thermNameAux, 12)
PSRMapData_mapParm(imapter, "code", thermCode, 0)
PSRMapData_mapVector(imapter, "Existing", thermFut, 0)
PSRMapData_mapVector(imapter, "PotInst", thermCap, 0)
PSRMapData_mapVector(imapter, "O&MCost", thermCVaria, 0)
PSRMapData_mapVector(imapter, "CTransp", thermCTransp, 0)
PSRMapData_mapDimensionedVector(imapter, "CEsp", thermCost, 0, "segmemt", "block")

#       Cria um controlador de tempo e associa todo o estudo
#       ----------------------------------------------------------
ictrl = PSRTimeController_create(0)
PSRTimeController_addElement(ictrl, istdy, true)
PSRTimeController_configureFrom(ictrl, istdy)


#       Posiciona controlador de tempo no primeiro estagio do estudo
#       ------------------------------------------------------------
PSRTimeController_gotoStage(ictrl, 1)

#       Posiciona os vetores com as dimens�es informadas
#       Vetores que foram mapeados com a dimens�o "segment" ser�o posicionados em segment=1
#       Vetores que foram mapeados com a dimens�o "block" ser�o posicionados em block=1
#       -----------------------------------------------------------------------------------
PSRTimeController_setDimension(ictrl, "segmemt", 1)
PSRTimeController_setDimension(ictrl, "block", 1)

#       Popula estruturas de dados mapeados
#       -----------------------------------
PSRMapData_pullToMemory(imapsys)
PSRMapData_pullToMemory(imapter)

#       Passo adicional para obter arrays de strings
#       --------------------------------------------
systemName = splitstring(systemNameAux, 12)
thermName = splitstring(thermNameAux, 12)

#       --------------------------------------------------
#       Parte 5 - Exibe um sumario das informacoes do caso
#       --------------------------------------------------
println(string("Descricao do caso: ", PSRStudy_description(istdy)))

println(string("Total de estagios: ", PSRStudy_getNumberStages(istdy)))
println(string("Total de cenarios: ", PSRStudy_getNumberSimulations(istdy)))
println(string("Total de blocos: ", PSRStudy_getNumberBlocks(istdy)))

println(string("Total de sistemas do caso: ", nsys))
println(string("Total de hydros do caso: ", nhydro))
println(string("Total de termicas do caso: ", nthermal))
println(string("Total de gnds do caso: ", ngnd))

println("Overview das Termicas:")


#       Obtem par�metros de interesse do estudo
#       ---------------------------------------
number_stages = PSRStudy_getNumberStages(istdy)
number_blocks = PSRStudy_getNumberBlocks(istdy)

if get(ENV, "PSRC_JL_TESTING", "0") == "1"
    @test number_stages == 160
    @test number_blocks == 3
end

#       Loops de configura��es (percorrer todos os est�gios e blocos)
#       -------------------------------------------------------------
for stage = 1:5 # number_stages
    for block = 1:number_blocks

        println(string("Configuracao: ", stage, " bloco: ", block))

#           Seta o estagio
#           --------------
        PSRTimeController_gotoStage(ictrl, stage)

#           Seta o bloco atual pelo time controller
#           ---------------------------------------------------
        PSRTimeController_setDimension(ictrl, "block", block)

#           Refaz o pull para a memoria dos atributos
#           atualizando os vetores JULIA para a fotografia atual
#           ---------------------------------------------------
        PSRMapData_pullToMemory(imapsys)
        PSRMapData_pullToMemory(imapter)

#           Mostra na tela as informacoes mapeadas das termicas
#           ---------------------------------------------------
        println(string("Exibindo atributos para estagio: ", stage, " bloco: ", block))
        for iterm = 1:nthermal
            println(string(thermCode[iterm], " ",
                              thermName[iterm], " ",
                              "SISTEMA: ",systemCode[ipthermsys[iterm]], " ", systemName[ipthermsys[iterm]], " ",
                              thermFut[iterm], " ",
                              thermCap[iterm], " ",
                              thermCost[iterm], " ",
                              thermCVaria[iterm], " ",
                              thermCTransp[iterm]))
        end
    end
end

