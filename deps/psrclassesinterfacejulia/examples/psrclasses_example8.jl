# Seta PATH para PSRCLASSES
PATH_PSRCLASSES = normpath(joinpath(@__DIR__, ".."))

@static if get(ENV, "PSRC_JL_TESTING", "0") != "1"
    import Libdl
#       Inclui as definicoes do modulo PSRClasses
#       -----------------------------------------
    include(string(PATH_PSRCLASSES, "psrclasses.jl"))

#       Inicializa a DLL do PSRClasses
#       ------------------------------
    PSRClasses_init(PATH_PSRCLASSES)
end
#     Cria handle para funcao de custo futuro
#     -----------------------------------------
fcost = PSRIOSDDPFutureCost_create(0)

#     Carrega funcao de custo futuro
#     ------------------------------
      # FILE_PATH = normpath(joinpath(PATH_PSRCLASSES, "examples/data/bo-caso1/costmebo.psr"))
FILE_PATH = "D:\\sddp-dev\\sddptestdata\\sddp2\\ex_deterministic\\caso49-sim\\costmes1.psr"

PSRIOSDDPFutureCost_load(fcost, FILE_PATH)

#     Numero de variaveis de volume na funcao de custo futuro
#     -------------------------------------------------------
maxvol = PSRIOSDDPFutureCost_maxVolume(fcost)

#     Numero de variaveis de inflows na funcao de custo futuro e ordem usada
#     ----------------------------------------------------------------------
maxinflow = PSRIOSDDPFutureCost_maxInflow(fcost)
maxord = PSRIOSDDPFutureCost_maxOrd(fcost)

#     Obtem o total de estagios representados
#     ---------------------------------------
totalStages = PSRIOSDDPFutureCost_maxStage(fcost)

#     Identifica codigo das entidades as quais se referem cada variavel
#     -----------------------------------------------------------------
for idvol = 1:min(3, maxvol)
    println(string("Variavel vol: ", idvol, " codigo = ", PSRIOSDDPFutureCost_idVolume(fcost, idvol - 1)))
end
for idinflow = 1:min(2, maxinflow)
    println(string("Variavel inflow: ", idinflow, " codigo = ", PSRIOSDDPFutureCost_idInflow(fcost, idinflow - 1)))
end

#     Inicializa variaveis de coeficientes
#     ------------------------------------
coefsVol = Array{Float64}(undef, maxvol)
coefsInflow = Array{Float64}(undef, maxinflow, maxord)

#     Percorre funcao de custo futuro para cada estagio
#     -------------------------------------------------
for stage = 1:totalStages

#       Vai para o estagio corrente
#       ---------------------------
    PSRIOSDDPFutureCost_gotoStage(fcost, stage)

#       Obtem o total de restricoes associadas ao estagio
#       -------------------------------------------------
    totalConst = PSRIOSDDPFutureCost_numberCuts(fcost)

    for constr = 1:totalConst
#         Le proxima restricao
#         --------------------
        PSRIOSDDPFutureCost_getCut(fcost)

#         Obtem os coeficientes relacionados aos volumes
#         ----------------------------------------------
        for idvol = 1:maxvol
            coefsVol[idvol] = PSRIOSDDPFutureCost_getVolume(fcost, idvol - 1)
        end

#         Obtem os coeficientes relacionados aos inflows de cada ordem
#         ------------------------------------------------------------
        for idord = 1:maxord
            for idinflow = 1:maxinflow
                coefsInflow[idinflow, idord] = PSRIOSDDPFutureCost_getInflow(fcost, idinflow - 1, idord - 1)
              # println(coefsInflow)
            end
        end

#         Obtem RHS
#         ---------
        RHS = PSRIOSDDPFutureCost_getRHS(fcost)

    end

end

#     Fecha funcao
#     ------------
PSRIOSDDPFutureCost_close(fcost)


