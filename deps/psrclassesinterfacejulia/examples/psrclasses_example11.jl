
PATH_PSRCLASSES = normpath(joinpath(@__DIR__, ".."))

@static if get(ENV, "PSRC_JL_TESTING", "0") != "1"
    import Libdl
#       Inclui as definicoes do modulo PSRClasses
#       -----------------------------------------
    include(string(PATH_PSRCLASSES, "psrclasses.jl"))

#       Inicializa a DLL do PSRClasses
#       ------------------------------
    PSRClasses_init(PATH_PSRCLASSES)
end

# Cria handle para funcao de custo futuro
fcost = PSRIOSDDPFutureCost_create(0)

# Define configuracao da funcao de custo futuro
PSRIOSDDPFutureCost_setInitialStage(fcost, 1, 2016)
PSRIOSDDPFutureCost_setTotalStages(fcost, 12)
# you cant store any data in the last stage

# Variaveis de volume
PSRIOSDDPFutureCost_addVolume(fcost, 1)
PSRIOSDDPFutureCost_addVolume(fcost, 2)

# Variaveis de vazao
PSRIOSDDPFutureCost_setOrd(fcost, 6)
PSRIOSDDPFutureCost_addInflow(fcost, 1)
PSRIOSDDPFutureCost_addInflow(fcost, 2)
# PSRIOSDDPFutureCost_addInflow(fcost, 3)


# Variaveis genericas
PSRIOSDDPFutureCost_addStateVar(fcost, PSR_STATEVAR_FUEL_RESERVOIR, 1)
PSRIOSDDPFutureCost_addStateVar(fcost, PSR_STATEVAR_FUEL_RESERVOIR, 2)

PSRIOSDDPFutureCost_addStateVar(fcost, 8, 2)
PSRIOSDDPFutureCost_addStateVar(fcost, 8, 3)

# PSRIOSDDPFutureCost_addStateVar(fcost, 9, 1)
PSRIOSDDPFutureCost_addStateVar(fcost, 9, 2)
PSRIOSDDPFutureCost_addStateVar(fcost, 9, 3)

@show PSRIOSDDPFutureCost_maxVolume(fcost)
@show PSRIOSDDPFutureCost_maxInflow(fcost)
@show PSRIOSDDPFutureCost_maxOrd(fcost)
@show PSRIOSDDPFutureCost_maxStateVarType(fcost, PSR_STATEVAR_FUEL_RESERVOIR)
@show PSRIOSDDPFutureCost_maxStateVarType(fcost, 8)
@show PSRIOSDDPFutureCost_maxStateVarType(fcost, 8+1)

@show PSRIOSDDPFutureCost_idStateVarType(fcost, PSR_STATEVAR_FUEL_RESERVOIR, 0) 
@show PSRIOSDDPFutureCost_idStateVarType(fcost, PSR_STATEVAR_FUEL_RESERVOIR, 1)

@show PSRIOSDDPFutureCost_idStateVarType(fcost, 8, 0) 
@show PSRIOSDDPFutureCost_idStateVarType(fcost, 8, 1)

@show PSRIOSDDPFutureCost_idStateVarType(fcost, 8+1, 0) 
@show PSRIOSDDPFutureCost_idStateVarType(fcost, 8+1, 1) 
# @show PSRIOSDDPFutureCost_idStateVarType(fcost, 8+1, 2) 

println("Init FCF in SAVE mode")
FCF_PATH = normpath(joinpath(PATH_PSRCLASSES,"psrclasses_example11.psr"))
PSRIOSDDPFutureCost_initSave(fcost, FCF_PATH)
#println("write 5 cuts")

# Adiciona 5 cortes para os estagios
for stage = 1:12#2
    for icut = 1:1
        for ord = 1:6
            for inflow = 1:2
                PSRIOSDDPFutureCost_setInflow(fcost, Int32(inflow - 1), Int32(ord - 1), inflow * 10. + ord)
            end
        end
        for vol = 1:2
            PSRIOSDDPFutureCost_setVolume(fcost, vol - 1, vol * 100. + icut * 1000)
        end
        for other = 1:2
            PSRIOSDDPFutureCost_setStateVar(fcost, PSR_STATEVAR_FUEL_RESERVOIR, other-1, 1.1)
        end
        for other = 1:2
            PSRIOSDDPFutureCost_setStateVar(fcost, 8, other-1, 1.1)
        end
        for other = 1:2
            PSRIOSDDPFutureCost_setStateVar(fcost, 9, other-1, 1.1)
        end
        PSRIOSDDPFutureCost_setRHS(fcost, stage * 100. + icut)
        #   Define header do corte [definido pela tripla (simulacao, iteracao, cluster)]
        PSRIOSDDPFutureCost_setHeaderInformation(fcost, 1, 1, 0)
        #   Adiciona o corte definido a FCF
        PSRIOSDDPFutureCost_addCut(fcost, stage)
    end
end

T = PSRIOSDDPFutureCost_maxStage(fcost)
@assert T == 12
for t in 1:T
    PSRIOSDDPFutureCost_gotoStage(fcost, t)
    cuts = PSRIOSDDPFutureCost_numberCuts(fcost)
    for cut = 1:cuts
        PSRIOSDDPFutureCost_getCut(fcost)
        #println(string("Corte atual: ", cut))
        for inflow = 1:2
            #println(string("inflow(1) = ", PSRIOSDDPFutureCost_getInflow(fcost, inflow - 1, 0)))
        end
        for vol = 1:2
            #println(string("volume = ", PSRIOSDDPFutureCost_getVolume(fcost, vol - 1)))
        end
        for other = 1:2
            #println(string("other = ", PSRIOSDDPFutureCost_getStateVar(fcost, PSR_STATEVAR_FUEL_RESERVOIR, other - 1)))
        end
        for other = 1:2
            #println(string("8 = ", PSRIOSDDPFutureCost_getStateVar(fcost, 8, other-1)))
        end
        for other = 1:2
            #println(string("9 = ", PSRIOSDDPFutureCost_getStateVar(fcost, 9, other-1)))
        end
        #println(string(" RHS = ", PSRIOSDDPFutureCost_getRHS(fcost)))
    end
end

# Fecha funcao
# ------------
PSRIOSDDPFutureCost_close(fcost)
PSRIOSDDPFutureCost_deleteInstance(fcost)

# abre a fcf no modo read

# Cria handle para funcao de custo futuro
# -----------------------------------------
fcost = PSRIOSDDPFutureCost_create(0)
# # Variaveis de vazao
# PSRIOSDDPFutureCost_addInflow(fcost, 1)
# PSRIOSDDPFutureCost_addInflow(fcost, 2)
# PSRIOSDDPFutureCost_addInflow(fcost, 3)
# # Variaveis de volume
# PSRIOSDDPFutureCost_addVolume(fcost, 1)
# PSRIOSDDPFutureCost_addVolume(fcost, 2)
# # Variaveis genericas
PSRIOSDDPFutureCost_addStateVar(fcost, PSR_STATEVAR_FUEL_RESERVOIR, 1)
PSRIOSDDPFutureCost_addStateVar(fcost, PSR_STATEVAR_FUEL_RESERVOIR, 2)

PSRIOSDDPFutureCost_addStateVar(fcost, 8, 2)
PSRIOSDDPFutureCost_addStateVar(fcost, 8, 3)

# PSRIOSDDPFutureCost_addStateVar(fcost, 8+1, 1)
PSRIOSDDPFutureCost_addStateVar(fcost, 8+1, 2)
PSRIOSDDPFutureCost_addStateVar(fcost, 8+1, 3)

println("Init FCF in READ mode")
PSRIOSDDPFutureCost_load(fcost, FCF_PATH)
@show PSRIOSDDPFutureCost_maxVolume(fcost)
@show PSRIOSDDPFutureCost_maxInflow(fcost)
@show PSRIOSDDPFutureCost_maxOrd(fcost)

@show PSRIOSDDPFutureCost_maxStateVarType(fcost, PSR_STATEVAR_FUEL_RESERVOIR)
@show PSRIOSDDPFutureCost_maxStateVarType(fcost, 8)
@show PSRIOSDDPFutureCost_maxStateVarType(fcost, 8+1)

@show PSRIOSDDPFutureCost_idStateVarType(fcost, PSR_STATEVAR_FUEL_RESERVOIR, 0) 
@show PSRIOSDDPFutureCost_idStateVarType(fcost, PSR_STATEVAR_FUEL_RESERVOIR, 1)

@show PSRIOSDDPFutureCost_idStateVarType(fcost, 8, 0) 
@show PSRIOSDDPFutureCost_idStateVarType(fcost, 8, 1)

@show PSRIOSDDPFutureCost_idStateVarType(fcost, 8+1, 0) 
@show PSRIOSDDPFutureCost_idStateVarType(fcost, 8+1, 1) 
# @show PSRIOSDDPFutureCost_idStateVarType(fcost, 8+1, 2) 

@show T = PSRIOSDDPFutureCost_maxStage(fcost)
for t in 1:T
    PSRIOSDDPFutureCost_gotoStage(fcost, t)
    cuts = PSRIOSDDPFutureCost_numberCuts(fcost)
    for cut = 1:cuts
        PSRIOSDDPFutureCost_getCut(fcost)
        #println(string("Corte atual: ", cut))
        for inflow = 1:2
            #println(string("inflow(1) = ", PSRIOSDDPFutureCost_getInflow(fcost, inflow - 1, 0)))
        end
        for vol = 1:2
            #println(string("volume = ", PSRIOSDDPFutureCost_getVolume(fcost, vol - 1)))
        end
        for other = 1:2
            #println(string("other = ", PSRIOSDDPFutureCost_getStateVar(fcost, PSR_STATEVAR_FUEL_RESERVOIR, other - 1)))
        end
        for other = 1:2
            #println(string("8 = ", PSRIOSDDPFutureCost_getStateVar(fcost, 8, other-1)))
        end
        for other = 1:2
            #println(string("9 = ", PSRIOSDDPFutureCost_getStateVar(fcost, 9, other-1)))
        end
        #println(string(" RHS = ", PSRIOSDDPFutureCost_getRHS(fcost)))
    end
end

# Fecha funcao
PSRIOSDDPFutureCost_close(fcost)
PSRIOSDDPFutureCost_deleteInstance(fcost)

# abre a fcf no modo append
# -----------------------------------------

# Cria handle para funcao de custo futuro
# -----------------------------------------
fcost = PSRIOSDDPFutureCost_create(0)
# # Variaveis de vazao
# # ------------------
# PSRIOSDDPFutureCost_addInflow(fcost, 1)
# PSRIOSDDPFutureCost_addInflow(fcost, 2)
# PSRIOSDDPFutureCost_addInflow(fcost, 3)
# # Variaveis de volume
# # -------------------
# PSRIOSDDPFutureCost_addVolume(fcost, 1)
# PSRIOSDDPFutureCost_addVolume(fcost, 2)
# # Variaveis genericas
# # -------------------
PSRIOSDDPFutureCost_addStateVar(fcost, PSR_STATEVAR_FUEL_RESERVOIR, 1)
PSRIOSDDPFutureCost_addStateVar(fcost, PSR_STATEVAR_FUEL_RESERVOIR, 2)

PSRIOSDDPFutureCost_addStateVar(fcost, 8, 2)
PSRIOSDDPFutureCost_addStateVar(fcost, 8, 3)

# PSRIOSDDPFutureCost_addStateVar(fcost, 8+1, 1)
PSRIOSDDPFutureCost_addStateVar(fcost, 8+1, 2)
PSRIOSDDPFutureCost_addStateVar(fcost, 8+1, 3)

println("Init FCF in APPEND mode")
PSRIOSDDPFutureCost_initAppend(fcost, FCF_PATH)

@show PSRIOSDDPFutureCost_maxVolume(fcost)
@show PSRIOSDDPFutureCost_maxInflow(fcost)
@show PSRIOSDDPFutureCost_maxOrd(fcost)

@show PSRIOSDDPFutureCost_maxStateVarType(fcost, PSR_STATEVAR_FUEL_RESERVOIR)
@show PSRIOSDDPFutureCost_maxStateVarType(fcost, 8)
@show PSRIOSDDPFutureCost_maxStateVarType(fcost, 8+1)

@show PSRIOSDDPFutureCost_idStateVarType(fcost, PSR_STATEVAR_FUEL_RESERVOIR, 0) 
@show PSRIOSDDPFutureCost_idStateVarType(fcost, PSR_STATEVAR_FUEL_RESERVOIR, 1)

@show PSRIOSDDPFutureCost_idStateVarType(fcost, 8, 0) 
@show PSRIOSDDPFutureCost_idStateVarType(fcost, 8, 1)

@show PSRIOSDDPFutureCost_idStateVarType(fcost, 8+1, 0) 
@show PSRIOSDDPFutureCost_idStateVarType(fcost, 8+1, 1) 

# Adiciona 5 cortes para os estagios
for stage = 1:12#2
    for icut = 1:1
        for ord = 1:6
            for inflow = 1:2
                PSRIOSDDPFutureCost_setInflow(fcost, Int32(inflow - 1), Int32(ord - 1), inflow * 10. + ord)
            end
        end
        for vol = 1:2
            PSRIOSDDPFutureCost_setVolume(fcost, vol - 1, vol * 100. + icut * 1000)
        end
        for other = 1:2
            PSRIOSDDPFutureCost_setStateVar(fcost, PSR_STATEVAR_FUEL_RESERVOIR, other-1, 1.1)
        end
        for other = 1:2
            PSRIOSDDPFutureCost_setStateVar(fcost, 8, other-1, 1.1)
        end
        for other = 1:2
            PSRIOSDDPFutureCost_setStateVar(fcost, 9, other-1, 1.1)
        end
        PSRIOSDDPFutureCost_setRHS(fcost, stage * 100. + icut)
        #   Define header do corte [definido pela tripla (simulacao, iteracao, cluster)]
        PSRIOSDDPFutureCost_setHeaderInformation(fcost, 1, 1, 0)
        #   Adiciona o corte definido a FCF
        PSRIOSDDPFutureCost_addCut(fcost, stage)
    end
end

T = PSRIOSDDPFutureCost_maxStage(fcost)
@assert T == 12
for t in 1:T
    PSRIOSDDPFutureCost_gotoStage(fcost, t)
    cuts = PSRIOSDDPFutureCost_numberCuts(fcost)
    for cut = 1:cuts
        PSRIOSDDPFutureCost_getCut(fcost)
        #println(string("Corte atual: ", cut))
        for inflow = 1:2
            #println(string("inflow(1) = ", PSRIOSDDPFutureCost_getInflow(fcost, inflow - 1, 0)))
        end
        for vol = 1:2
            #println(string("volume = ", PSRIOSDDPFutureCost_getVolume(fcost, vol - 1)))
        end
        for other = 1:2
            #println(string("other = ", PSRIOSDDPFutureCost_getStateVar(fcost, PSR_STATEVAR_FUEL_RESERVOIR, other - 1)))
        end
        for other = 1:2
            #println(string("8 = ", PSRIOSDDPFutureCost_getStateVar(fcost, 8, other-1)))
        end
        for other = 1:2
            #println(string("9 = ", PSRIOSDDPFutureCost_getStateVar(fcost, 9, other-1)))
        end
        #println(string(" RHS = ", PSRIOSDDPFutureCost_getRHS(fcost)))
    end
end

# Fecha funcao
# ------------
PSRIOSDDPFutureCost_close(fcost)
PSRIOSDDPFutureCost_deleteInstance(fcost)

# Cria handle para funcao de custo futuro
# -----------------------------------------
fcost = PSRIOSDDPFutureCost_create(0)
# # Variaveis de vazao
# PSRIOSDDPFutureCost_addInflow(fcost, 1)
# PSRIOSDDPFutureCost_addInflow(fcost, 2)
# PSRIOSDDPFutureCost_addInflow(fcost, 3)
# # Variaveis de volume
# PSRIOSDDPFutureCost_addVolume(fcost, 1)
# PSRIOSDDPFutureCost_addVolume(fcost, 2)
# # Variaveis genericas
PSRIOSDDPFutureCost_addStateVar(fcost, PSR_STATEVAR_FUEL_RESERVOIR, 1)
PSRIOSDDPFutureCost_addStateVar(fcost, PSR_STATEVAR_FUEL_RESERVOIR, 2)

PSRIOSDDPFutureCost_addStateVar(fcost, 8, 2)
PSRIOSDDPFutureCost_addStateVar(fcost, 8, 3)

# PSRIOSDDPFutureCost_addStateVar(fcost, 8+1, 1)
PSRIOSDDPFutureCost_addStateVar(fcost, 8+1, 2)
PSRIOSDDPFutureCost_addStateVar(fcost, 8+1, 3)

println("Init FCF in READ mode")
PSRIOSDDPFutureCost_load(fcost, FCF_PATH)
@show PSRIOSDDPFutureCost_maxVolume(fcost)
@show PSRIOSDDPFutureCost_maxInflow(fcost)
@show PSRIOSDDPFutureCost_maxOrd(fcost)

@show PSRIOSDDPFutureCost_maxStateVarType(fcost, PSR_STATEVAR_FUEL_RESERVOIR)
@show PSRIOSDDPFutureCost_maxStateVarType(fcost, 8)
@show PSRIOSDDPFutureCost_maxStateVarType(fcost, 8+1)

@show PSRIOSDDPFutureCost_idStateVarType(fcost, PSR_STATEVAR_FUEL_RESERVOIR, 0) 
@show PSRIOSDDPFutureCost_idStateVarType(fcost, PSR_STATEVAR_FUEL_RESERVOIR, 1)

@show PSRIOSDDPFutureCost_idStateVarType(fcost, 8, 0) 
@show PSRIOSDDPFutureCost_idStateVarType(fcost, 8, 1)

@show PSRIOSDDPFutureCost_idStateVarType(fcost, 8+1, 0) 
@show PSRIOSDDPFutureCost_idStateVarType(fcost, 8+1, 1) 
# @show PSRIOSDDPFutureCost_idStateVarType(fcost, 8+1, 2) 

T = PSRIOSDDPFutureCost_maxStage(fcost)
@assert T == 12
for t in 1:T
    PSRIOSDDPFutureCost_gotoStage(fcost, t)
    cuts = PSRIOSDDPFutureCost_numberCuts(fcost)
    for cut = 1:cuts
        PSRIOSDDPFutureCost_getCut(fcost)
        #println(string("Corte atual: ", cut))
        for inflow = 1:2
            #println(string("inflow(1) = ", PSRIOSDDPFutureCost_getInflow(fcost, inflow - 1, 0)))
        end
        for vol = 1:2
            #println(string("volume = ", PSRIOSDDPFutureCost_getVolume(fcost, vol - 1)))
        end
        for other = 1:2
            #println(string("other = ", PSRIOSDDPFutureCost_getStateVar(fcost, PSR_STATEVAR_FUEL_RESERVOIR, other - 1)))
        end
        for other = 1:2
            #println(string("8 = ", PSRIOSDDPFutureCost_getStateVar(fcost, 8, other-1)))
        end
        for other = 1:2
            #println(string("9 = ", PSRIOSDDPFutureCost_getStateVar(fcost, 9, other-1)))
        end
        #println(string(" RHS = ", PSRIOSDDPFutureCost_getRHS(fcost)))
    end
end

# Fecha funcao
PSRIOSDDPFutureCost_close(fcost)
PSRIOSDDPFutureCost_deleteInstance(fcost)