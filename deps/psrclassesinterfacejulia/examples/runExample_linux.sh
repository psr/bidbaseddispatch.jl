#!/bin/bash

#-- baixa e descomprime os binarios para o Julia 0.6.4 x86
# wget https://julialang-s3.julialang.org/bin/linux/x64/0.6/julia-0.6.4-linux-x86_64.tar.gz
# tar -xzvf julia-0.6.4-linux-x86_64.tar.gz

#baixa os repos da PSR psrclassesinterfacejulia e awsintegration
# git clone https://bitbucket.org/psr/psrclassesinterfacejulia.git
# git clone https://bitbucket.org/psr/awsintegration.git


#daqui para baixo, o script cuida de botar temporiaramente na path as dependencias necessarias
# para a execucao da psrclassesinterfacejulia numa maquina linux, considerando os caminhos como estao
#na maquina sydney.

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

export LD_LIBRARY_PATH=/opt/java/jdk1.7.0_79/jre/lib/amd64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/opt/java/jdk1.7.0_79/jre/lib/amd64/server:$LD_LIBRARY_PATH

export LD_LIBRARY_PATH=$DIR/../../awsintegration/lib/lin64:$LD_LIBRARY_PATH
export AWS_DEPS=$DIR/../../awsintegration/AWSJava/aws/
export LD_LIBRARY_PATH=$DIR/../:$LD_LIBRARY_PATH

#caminho para o executavel Julia 0.6.4
juliaexe="/opt/home/andrekrauss/julia-0.6.4/bin/julia"
#caminho para o script julia que queremos rodar
example="/opt/home/andrekrauss/psrclassesinterfacejulia/examples/psrclasses_example1_alt.jl"

$juliaexe $example
#ldd /opt/home/joaquimgarcia/psrclassesinterfacejulia/PSRClasses.so
