#     Seta PATH para PSRCLASSES
#     -------------------------
PATH_PSRCLASSES = normpath(joinpath(@__DIR__, ".."))

if get(ENV, "PSRC_JL_TESTING", "0") != "1"
#       Inclui as definicoes do modulo PSRClasses
#       -----------------------------------------
    include(string(PATH_PSRCLASSES, "psrclasses.jl"))

#       Inicializa a DLL do PSRClasses
#       ------------------------------
    PSRClasses_init(PATH_PSRCLASSES)
end
#     ----------------------------------------------
#     Parte 1 - Gravacao de Funcao de Custo Imediato
#     ----------------------------------------------

#     Define vetor de tipo e codigo das variaveis
#     ------------------------------------------------------
typevar = Array{Int32}(undef, 3)
codevar = Array{Int32}(undef, 3)
coefs = Array{Float64}(undef, 3)
rhs = Array{Float64}(undef, 1)
for ivar = 1:3
    typevar[ivar] = 1
    codevar[ivar] = ivar
end

#     Cria handle para funcao de custo imediato
#     -----------------------------------------
ime = PSRIOSDDPImmediateCost_create(0)

#     Configura (estagios, series)
#     ----------------------------
PSRIOSDDPImmediateCost_setTotalStages(ime, 12)
PSRIOSDDPImmediateCost_setTotalScenarios(ime, 100)

#     Define variaveis (tipo e codigo da variavel)
#     definicao se da pelo par: (tipo,codigo)
#     --------------------------------------------
for ivar = 1:3
    PSRIOSDDPImmediateCost_addVariable(ime, 1, ivar)
end

#     Inicia gravacao do arquivo de custo imediato
#     --------------------------------------------
PATH_CASE = joinpath(PATH_PSRCLASSES, "examples\\psrclasses_example7.psr")
iret = PSRIOSDDPImmediateCost_initSave(ime, PATH_CASE)

#     Loop de gravacao
#     ----------------
for stage = 1:12
    for scen = 1:100
#         Define equacao do corte
#         -----------------------
        coefs[1] = stage
        coefs[2] = scen
        coefs[3] = stage * 10
        rhs[1] = scen * 100
#         Adiciona corte para estagio/serie
#         ---------------------------------
        PSRIOSDDPImmediateCost_addCut(ime, stage, scen, coefs, rhs)
    end
end

#     Finaliza gravacao
#     -----------------
PSRIOSDDPImmediateCost_close(ime)

#     ---------------------------------------------
#     Parte 2 - Leitura de Funcao de Custo Imediato
#     ---------------------------------------------

#     Cria handle para funcao de custo imediato
#     -----------------------------------------
ime = PSRIOSDDPImmediateCost_create(0)

#     Inicia leitura do arquivo de custo imediato
#     -------------------------------------------
iret = PSRIOSDDPImmediateCost_initLoad(ime, PATH_CASE)

#     Ordena coeficientes da funcao de custo imediato de acordo com vetor codevar
#     ---------------------------------------------------------------------------
PSRIOSDDPImmediateCost_sortVariables(ime, typevar, codevar)

#     Loop de leitura
#     ---------------
for ist = 1:12
    for isc = 1:100
#         Vai para estagio/cenario
#         -------------------------
        PSRIOSDDPImmediateCost_gotoStage(ime, ist, isc)

#         Loop de cortes associados a estagio/cenario
#         -------------------------------------------
        itotcut = PSRIOSDDPImmediateCost_totalCuts(ime)
        for icut = 1:itotcut
#           Obtem proximo corte de estagio/cenario
#           --------------------------------------
            PSRIOSDDPImmediateCost_getCut(ime, coefs, rhs)
            println(string(coefs, " - ", rhs))
        end
    end
end

#     Finaliza leitura
#     -----------------
PSRIOSDDPImmediateCost_close(ime)
