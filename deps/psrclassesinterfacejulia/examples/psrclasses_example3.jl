#       ----------------------------------
#       Parte 1 - Inicializacao PSRCLASSES
#       ----------------------------------

#       Seta PATH para PSRCLASSES
#       -------------------------
PATH_PSRCLASSES = normpath(joinpath(@__DIR__, ".."))

if get(ENV, "PSRC_JL_TESTING", "0") != "1"
#       Inclui as definicoes do modulo PSRClasses
#       -----------------------------------------
    include(string(PATH_PSRCLASSES, "psrclasses.jl"))

#       Inicializa a DLL do PSRClasses
#       ------------------------------
    PSRClasses_init(PATH_PSRCLASSES)
end

#       Configura Gerenciador de Log
#       ----------------------------
ilog = PSRManagerLog_getInstance(0)

#       Inicializa idioma portugues para log
#       ------------------------------------
PSRManagerLog_initPortuguese(ilog)

#       Acrescenta Log do Tipo Console Simples ao Gerenciador (log na tela)
#       -------------------------------------------------------------------
ilogcons = PSRLogSimpleConsole_create(0)
PSRManagerLog_addLog(ilog, ilogcons)

#       Configura Gerenciador de Mascaras e carrega Mascaras
#       ----------------------------------------------------
igmsk  = PSRManagerIOMask_getInstance(0)
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_V10.2.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_V10.3.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_Blocks.pmk"))

#       Configura Gerenciador de Modelos e carrega Modelos
#       --------------------------------------------------
igmdl = PSRManagerModels_getInstance(0)
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_V10.2.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_V10.3.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_Keywords.pmd"))

#       -------------------------------------
#       Parte 2 - Carrega estudo da base SDDP
#       -------------------------------------

#       Cria Estudo
#       -----------
istdy = PSRStudy_create(0)

#       Cria Objeto Leitor de Estudo SDDP e carrega dados
#       ----------------------------------------------------
PATH_CASE = joinpath(PATH_PSRCLASSES, "examples\\data\\bo-caso4\\")
iosddp = PSRIOSDDP_create(0)
iret = PSRIOSDDP_load(iosddp, istdy, PATH_CASE,
                                                PATH_CASE, PSR_SDDP_VERSION_14)
#       -------------------------------------
#       Parte 3 - Obtem lista de Usinas Hidro
#       -------------------------------------

#       Obtem lista de usinas hydro do estudo
#       -------------------------------------
lsthyd = PSRStudy_getCollectionPlants(istdy, PSR_PLANTTYPE_HYDRO)
nhydro = PSRCollectionElement_maxElements(lsthyd)

#       Prealoca estruturas de dados para as hydro
#       ------------------------------------------
hydroNameAux = allocstring(nhydro, 12)
hydroCode = Array{Int32}(undef, nhydro)

#       Obtem lista de postos hidrologicos do estudo
#       --------------------------------------------
lststations = PSRStudy_getCollectionGaugingStations(istdy)
nstations = PSRCollectionElement_maxElements(lststations)

#       Prealoca vetor para receber vazoes
#       ----------------------------------
stationInflow =  Array{Float64}(undef, nstations)

#       Cria mapeador de dados para as usinas hydro
#       ----------------------------------------------
imaphyd = PSRMapData_create(0)
PSRMapData_addElements(imaphyd, lsthyd)
PSRMapData_mapParm(imaphyd, "name", hydroNameAux, 12)
PSRMapData_mapParm(imaphyd, "code", hydroCode, 0)

#       Popula estruturas de dados mapeados
#       -----------------------------------
PSRMapData_pullToMemory(imaphyd)

#       Passo adicional para obter arrays de strings
#       --------------------------------------------
hydroName = splitstring(hydroNameAux, 12)

#       Obtem ponteiro de usinas hydro para postos hidrologicos
#       -------------------------------------------------------
iphydStation =  Array{Int32}(undef, nhydro)
PSRCollectionElement_mapRelationShip(lsthyd, lststations, iphydStation, PSR_RELATIONSHIP_1TO1, false)

#       Cria objeto leitor das vazies foward e backward
#       -----------------------------------------------
PATH_CASE = joinpath(PATH_PSRCLASSES, "examples\\data\\bo-caso4\\forw.psr")
	iinflow = PSRIOSDDPHydroForwardBackward_create(0)
	PSRIOSDDPHydroForwardBackward_load(iinflow, istdy , PATH_CASE,
	                                                    PATH_CASE)
	PSRIOSDDPHydroForwardBackward_mapTo(iinflow, stationInflow)

#       Obtem parametros do estudo
#       --------------------------
numberStages = PSRStudy_getNumberStages(istdy)
numberSimulations = PSRStudy_getNumberSimulations(istdy)
numberOpenings = PSRStudy_getNumberOpenings(istdy)

#       Exibe as vazoes forwards para todos os estagios
#       -----------------------------------------------
for stage = 1:min(2, numberStages)
    for sim = 1:min(3, numberSimulations)
        println(string("Estagio: ", stage, " Simulation: ", sim))

#           Preenche o vetor de vazoes com as vazoes correspondentes ao estagio/cenario
#           ---------------------------------------------------------------------------
        PSRIOSDDPHydroForwardBackward_setForward(iinflow, stage, sim)

#           Vazoes para todas as hydros (dos postos associados)
#           --------------------------------------------------
        for ihyd = 1:nhydro
            if ( iphydStation[ihyd] > 0 )
                inflow = stationInflow[iphydStation[ihyd]]
            else
                inflow = 0
            end

            println(string(" - ", hydroCode[ihyd], " - ", hydroName[ihyd], ": ", inflow))
        end
    end
end

#       Exibe as todas as vazoes backward para o ultimo estagio
#       -------------------------------------------------------
for sim = 1:min(3, numberSimulations)
    for open = 1:min(2, numberOpenings)
        println(string("Simulation: ", sim, " Opening: ", open))

#           Preenche o vetor de vazoes com as vazoes correspondentes ao estagio/cenario
#           ---------------------------------------------------------------------------
        PSRIOSDDPHydroForwardBackward_setBackward(iinflow, numberStages, sim, open)

#           Vazoes para todas as hydros (dos postos associados)
#           --------------------------------------------------
        for ihyd = 1:nhydro
            if ( iphydStation[ihyd] > 0 )
                inflow = stationInflow[iphydStation[ihyd]]
            else
                inflow = 0
            end

            println(string(" - ", hydroCode[ihyd], " - ", hydroName[ihyd], ": ", inflow))
        end
    end
end


#       Fecha IO com a forward e backward
#       ---------------------------------
	PSRIOSDDPHydroForwardBackward_close(iinflow)
