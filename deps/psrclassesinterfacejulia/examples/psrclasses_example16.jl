#       Seta PATH para PSRCLASSES
#       -------------------------
PATH_PSRCLASSES = normpath(joinpath(@__DIR__, ".."))

if get(ENV, "PSRC_JL_TESTING", "0") != "1"
#       Inclui as definicoes do modulo PSRClasses
#       -----------------------------------------
    include(string(PATH_PSRCLASSES, "psrclasses.jl"))

#       Inicializa a DLL do PSRClasses
#       ------------------------------
    PSRClasses_init(PATH_PSRCLASSES)
end
#       Configura Gerenciador de Log
#       ----------------------------
ilog = PSRManagerLog_getInstance(0)

#       Inicializa idioma portugues para log
#       ------------------------------------
PSRManagerLog_initPortuguese(ilog)

#       Acrescenta Log do Tipo Console Simples ao Gerenciador (log na tela)
#       -------------------------------------------------------------------
ilogcons = PSRLogSimpleConsole_create(0)
PSRManagerLog_addLog(ilog, ilogcons)

#       Configura Gerenciador de Mascaras e carrega Mascaras
#       ----------------------------------------------------
igmsk  = PSRManagerIOMask_getInstance(0)
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_V10.2.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_V10.3.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_Blocks.pmk"))

#       Configura Gerenciador de Modelos e carrega Modelos
#       --------------------------------------------------
igmdl = PSRManagerModels_getInstance(0)
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_V10.2.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_V10.3.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_Keywords.pmd"))

#       -------------------------------------
#       Parte 2 - Carrega estudo da base SDDP
#       -------------------------------------

#       Cria Estudo
#       -----------
istdy = PSRStudy_create(0)

#       Cria Objeto Leitor de Estudo SDDP e carrega dados
#       ----------------------------------------------------
PATH_CASE = joinpath(PATH_PSRCLASSES, "examples\\data\\exemplo-manutencao\\")
iosddp = PSRIOSDDP_create(0)
iret = PSRIOSDDP_load(iosddp, istdy, PATH_CASE,
                                             PATH_CASE, PSR_SDDP_VERSION_14)

#       Cria planilha e objeto leitor de planilha
#       -----------------------------------------
sheet = PSRSpreadsheet_create(0)
loadsheet = PSRIOSpreadsheetCSV_create(0)
PSRIO_CSVDATA_useMask(loadsheet, PSRManagerIOMask_getMask(igmsk, "SDDP_v10.2_csvpmhime"))

#       Carrega planilha de CSV
#       -----------------------
filename = joinpath(PATH_CASE, "cmter01.csv")
iret = PSRIOSpreadsheetCSV_load(loadsheet, sheet, filename)

#       Importa informaao da planilha para as usinas termicas do sistema
#       ----------------------------------------------------------------
system = PSRStudy_system(istdy, 0)

maintenance = PSRIOSpreadsheetPlantMaintenance_create(0)
PSRIO_CSVDATA_useMask(maintenance, PSRManagerIOMask_getMask(igmsk, "SDDP_v10.2_csvpmhime"))
PSRIOSpreadsheetPlantMaintenance_load( maintenance, system, sheet, PSR_PLANTTYPE_THERMAL,
                                               "MODL:SDDP_V10.2_Manutencao",
                                               PSR_MAINTENANCE_ACTIONTYPE_ABSOLUTE,
                                               PSR_MAINTENANCETYPE_POWER )

#       Salva arquivo de manutencoes de sistema
#       ---------------------------------------
filename = joinpath(PATH_PSRCLASSES, "examples\\output\\psrclasses_example16.dat")
iomaint = PSRIOSDDPPlantMaintenance_create(0)
PSRIO_MIXROWDATA_useMask(iomaint, PSRManagerIOMask_getMask(igmsk, "SDDP_v10.2_pmhime"))
PSRIOSDDPPlantMaintenance_save(iomaint, system, filename, PSR_PLANTTYPE_THERMAL)
