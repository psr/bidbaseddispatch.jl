#       ----------------------------------
#       Parte 1 - Inicializacao PSRCLASSES
#       ----------------------------------

#       Seta PATH para PSRCLASSES
#       -------------------------
PATH_PSRCLASSES = normpath(joinpath(@__DIR__, ".."))

if get(ENV, "PSRC_JL_TESTING", "0") != "1"
#       Inclui as definicoes do modulo PSRClasses
#       -----------------------------------------
    include(string(PATH_PSRCLASSES, "psrclasses.jl"))

#       Inicializa a DLL do PSRClasses
#       ------------------------------
    PSRClasses_init(PATH_PSRCLASSES)
end

#       Configura Gerenciador de Log
#       ----------------------------
ilog = PSRManagerLog_getInstance(0)

#       Inicializa idioma portugues para log
#       ------------------------------------
PSRManagerLog_initPortuguese(ilog)

#       Acrescenta Log do Tipo Console Simples ao Gerenciador (log na tela)
#       -------------------------------------------------------------------
ilogcons = PSRLogSimpleConsole_create(0)
PSRManagerLog_addLog(ilog, ilogcons)

#       Configura Gerenciador de Mascaras e carrega Mascaras
#       ----------------------------------------------------
igmsk  = PSRManagerIOMask_getInstance(0)
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_NetPlan_V2.6.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_NetPlan_V3.0.pmk"))

#       Configura Gerenciador de Modelos e carrega Modelos
#       --------------------------------------------------
igmdl = PSRManagerModels_getInstance(0)
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_NETPLAN_V2.6.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_NETPLAN_V3.0.pmd"))

#       ----------------------------------------
#       Parte 2 - Carrega estudo da base NETPLAN
#       ----------------------------------------

#       Cria Estudo
#       -----------
istdy = PSRStudy_create(0)

#       Cria Objeto Leitor de Estudo NETPLAN e carrega dados
#       ----------------------------------------------------
ionetp = PSRIONETPLAN_create(0)

PSRIONETPLAN_useSerieCandidates(ionetp, false)
PATH_CASE = joinpath(PATH_PSRCLASSES, "examples\\data\\bo-netplan\\")

iret = PSRIONETPLAN_load(ionetp, istdy, PATH_CASE)

#       ---------------------------------------------
#       Parte 3 - Obtem lista das entidades do estudo
#       ---------------------------------------------

#       Obtem lista de barras do estudo
#       -------------------------------
listbus = PSRStudy_getCollectionBuses(istdy)

#       Obtem lista de areas do estudo
#       -------------------------------
listarea  = PSRStudy_getCollectionAreas(istdy)

#       Obtem lista de linhas de transmissao do estudo
#       ----------------------------------------------
listlt = PSRStudy_getCollectionSeries(istdy)

#       Obtem lista de transformadores do estudo
#       ----------------------------------------
listtraf = PSRStudy_getCollectionTransformers(istdy)

#       Obtem lista de cargas do estudo
#       -------------------------------
listload = PSRStudy_getCollectionShunts(istdy, PSR_DEVICETYPE_LOAD)

#       Obtem lista de geradores do estudo
#       ----------------------------------
listger = PSRStudy_getCollectionShunts(istdy, PSR_DEVICETYPE_GENERATOR)

#       --------------------------------------------------
#       Parte 4 - Exibe um sumario das informacoes do caso
#       --------------------------------------------------

#       Obtem total de barras da lista de barras
#       -----------------------------------------------
nbus = PSRCollectionElement_maxElements(listbus)

#       Obtem total de areas da lista de areas
#       -----------------------------------------------
narea = PSRCollectionElement_maxElements(listarea)

#       Obtem total de linhas da lista de lt
#       ------------------------------------
nlt = PSRCollectionElement_maxElements(listlt)

#       Obtem total de transformadores da lista de trafos
#       -------------------------------------------------
ntraf = PSRCollectionElement_maxElements(listtraf)

#       Obtem total de cargas da lista de cargas
#       -------------------------------------------------
nload = PSRCollectionElement_maxElements(listload)

#       Obtem total de geradores da lista de geradores
#       -------------------------------------------------
nger = PSRCollectionElement_maxElements(listger)

#       -------------------------------------
#       Parte 5 - Acessa dados do PSR Classes
#       -------------------------------------

#       Cria mapeador de dados para as barras
#       -------------------------------------
imapbus = PSRMapData_create(0)

#       Inicializa o mapeador com a lista de barras
#       ---------------------------------------------
PSRMapData_addElements(imapbus, listbus)

#       Mapeia os dados de barras desejados (codigo e nome)
#       ---------------------------------------------------
codbus =  Array{Int32}(undef, nbus)
namebusAux = allocstring(nbus, 12)
volt_psr =  Array{Float64}(undef, nbus)
vmin_psr = Array{Float64}(undef, nbus)
vmax_psr = Array{Float64}(undef, nbus)
angle = Array{Float64}(undef, nbus)

itot = PSRMapData_mapParm(imapbus, "code", codbus, 0)
itot = PSRMapData_mapParm(imapbus, "name", namebusAux, 12)
itot = PSRMapData_mapDimensionedVector(imapbus, "Volt", volt_psr, 0, "block", "")
itot = PSRMapData_mapDimensionedVector(imapbus, "Vmin", vmin_psr, 0, "block", "")
itot = PSRMapData_mapDimensionedVector(imapbus, "Vmax", vmax_psr, 0, "block", "")
itot = PSRMapData_mapDimensionedVector(imapbus, "Angle", angle, 0, "scen", "block")

#       Cria mapeador de dados para as cargas
#       -------------------------------------
imapload = PSRMapData_create(0)

#       Inicializa o mapeador com a lista de cargas
#       ----------------------------------------------
PSRMapData_addElements(imapload, listload)

#       Mapeia os dados de carga desejados
#       ----------------------------------
codload = Array{Int32}(undef, nload)
nameloadAux = allocstring(nload, 12)
P = Array{Float64}(undef, nload)
Q = Array{Float64}(undef, nload)
PSRMapData_mapParm(imapload, "code", codload, 0)
PSRMapData_mapParm(imapload, "name", nameloadAux, 12)
PSRMapData_mapDimensionedVector(imapload, "Pp", P, 0, "scen", "block")
PSRMapData_mapDimensionedVector(imapload, "Qp", Q, 0, "scen", "block")

#       Cria mapeador de dados para os geradores
#       ----------------------------------------
imapger = PSRMapData_create(0)

#       Inicializa o mapeador com a lista de geradores
#       ----------------------------------------------
PSRMapData_addElements(imapger, listger)

#       Mapeia os dados de geradores desejados
#       ------------------------------------------------------
codger = Array{Int32}(undef, nger)
namegerAux = allocstring(nger, 12)
opergerAux = allocstring(nger, 1)
Pmn = Array{Float64}(undef, nger)
Qmn = Array{Float64}(undef, nger)
Pmx = Array{Float64}(undef, nger)
Qmx = Array{Float64}(undef, nger)
Pg = Array{Float64}(undef, nger)

PSRMapData_mapParm(imapger, "code", codger, 0)
PSRMapData_mapParm(imapger, "name", namegerAux, 12)
PSRMapData_mapVector(imapger, "O", opergerAux, 1)
PSRMapData_mapVector(imapger, "Pmn", Pmn, 0)
PSRMapData_mapVector(imapger, "Pmx", Pmx, 0)
PSRMapData_mapVector(imapger, "Qmn", Qmn, 0)
PSRMapData_mapVector(imapger, "Qmx", Qmx, 0)
PSRMapData_mapDimensionedVector(imapger, "P", Pg, 0,  "scen", "block")

#       Faz o pull para a memoria - Tranfere os dados do PSR Classes para os vetores Julia
#       ----------------------------------------------------------------------------------
PSRMapData_pullToMemory(imapbus)
PSRMapData_pullToMemory(imapload)
PSRMapData_pullToMemory(imapger)

#       Passo adicional para obter arrays de strings
#       --------------------------------------------
namebus = splitstring(namebusAux, 12)
nameload = splitstring(nameloadAux, 12)
nameger = splitstring(namegerAux, 12)
operger = splitstring(opergerAux, 12)

#       ---------------------------------
#       Parte 6 - Exibe os dados do caso
#       ---------------------------------

#       Apresenta o sumario na tela
#       -------------------------------------------------
println(string("Total de areas do estudo: ", narea))
println(string("Total de barras do estudo: ", nbus))
println(string("Total de linhas do estudo: ", nlt))
println(string("Total de trafos do estudo: ", ntraf))
println(string("Total de cargas do estudo: ", nload))
println(string("Total de gerads do estudo: ", nger))
