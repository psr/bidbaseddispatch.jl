#       ----------------------------------
#       Parte 1 - Inicializacao PSRCLASSES
#       ----------------------------------

#       Seta PATH para PSRCLASSES
#       -------------------------
        PATH_PSRCLASSES = "../"
        
#       Inclui as definicoes do modulo PSRClasses        
#       -----------------------------------------
        include(string(PATH_PSRCLASSES,"psrclasses.jl"))
        
#       Inicializa a DLL do PSRClasses
#       ------------------------------
        PSRClasses_init(PATH_PSRCLASSES)        

#       Configura Gerenciador de Log 
#       ----------------------------
        ilog = PSRManagerLog_getInstance(0)    

#       Inicializa idioma portugues para log
#       ------------------------------------
        PSRManagerLog_initPortuguese(ilog)
        
#       Acrescenta Log do Tipo Console Simples ao Gerenciador (log na tela)
#       -------------------------------------------------------------------
        ilogcons = PSRLogSimpleConsole_create(0)    
        PSRManagerLog_addLog(ilog, ilogcons)  
        
#       Configura Gerenciador de Mascaras e carrega Mascaras
#       ----------------------------------------------------
        igmsk  = PSRManagerIOMask_getInstance(0)
        iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES,"Masks_SDDP_V10.2.pmk"))
        iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES,"Masks_SDDP_V10.3.pmk"))
        iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES,"Masks_SDDP_Blocks.pmk"))
                                                                       
#       Configura Gerenciador de Modelos e carrega Modelos
#       --------------------------------------------------
        igmdl = PSRManagerModels_getInstance(0)
        iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES,"Models_SDDP_V10.2.pmd"))
        iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES,"Models_SDDP_V10.3.pmd"))      
        iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES,"Models_SDDP_Keywords.pmd"))      
        
#       -------------------------------------
#       Parte 2 - Carrega estudo da base SDDP
#       -------------------------------------
        
#       Cria Estudo
#       -----------
        istdy = PSRStudy_create(0)
        
#       Cria Objeto Leitor de Estudo SDDP e carrega dados
#       ----------------------------------------------------
        iosddp = PSRIOSDDP_create(0)        
        iret = PSRIOSDDP_load(iosddp, istdy, "./data/bo-caso4/", 
                                             "./data/bo-caso4/", PSR_SDDP_VERSION_14)    


		#cria interface para o extension
		extension = SDDPExtension_create(0)
		
		#seta estudo no extension
		SDDPExtension_setStudy( extension,istdy )
		
		#uma call gen�rica qualquer de uma fun�ao definida no extensions
		SDDPExtension_call(extension, "preprocess")
		
		#uma call in�til que d� um print
		SDDPExtension_call(extension, "uselessPrint")
                          
