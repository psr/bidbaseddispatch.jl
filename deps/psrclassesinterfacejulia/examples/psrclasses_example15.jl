#       ----------------------------------
#       Parte 1 - Inicializacao PSRCLASSES
#       ----------------------------------

#       Seta PATH para PSRCLASSES
#       -------------------------
PATH_PSRCLASSES = normpath(joinpath(@__DIR__, ".."))

if get(ENV, "PSRC_JL_TESTING", "0") != "1"
#       Inclui as definicoes do modulo PSRClasses
#       -----------------------------------------
    include(string(PATH_PSRCLASSES, "psrclasses.jl"))

#       Inicializa a DLL do PSRClasses
#       ------------------------------
    PSRClasses_init(PATH_PSRCLASSES)
end

#       Configura Gerenciador de Log
#       ----------------------------
ilog = PSRManagerLog_getInstance(0)

#       Inicializa idioma portugues para log
#       ------------------------------------
PSRManagerLog_initPortuguese(ilog)

#       Acrescenta Log do Tipo Console Simples ao Gerenciador (log na tela)
#       -------------------------------------------------------------------
ilogcons = PSRLogSimpleConsole_create(0)
PSRManagerLog_addLog(ilog, ilogcons)

#       Configura Gerenciador de Mascaras e carrega Mascaras
#       ----------------------------------------------------
igmsk  = PSRManagerIOMask_getInstance(0)
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_V10.2.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_V10.3.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_Blocks.pmk"))

#       Configura Gerenciador de Modelos e carrega Modelos
#       --------------------------------------------------
igmdl = PSRManagerModels_getInstance(0)
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_V10.2.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_V10.3.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_Keywords.pmd"))

#       -------------------------------------
#       Parte 2 - Carrega estudo da base SDDP
#       -------------------------------------

#       Cria Estudo
#       -----------
istdy = PSRStudy_create(0)

#       Cria Objeto Leitor de Estudo SDDP e carrega dados
#       ----------------------------------------------------
PATH_CASE = joinpath(PATH_PSRCLASSES, "examples\\data\\bo-caso4\\")
iosddp = PSRIOSDDP_create(0)
iret = PSRIOSDDP_load(iosddp, istdy, PATH_CASE,
                        PATH_CASE, PSR_SDDP_VERSION_14)

println(PSRStudy_getDateFromStage(istdy, 1))
#       --------------------------------------------
#       Parte 3 - Obtem lista de entidades desejadas
#       --------------------------------------------

#       Obtem lista de usinas do estudo
#       ---------------------------------
lstsys = PSRStudy_getCollectionSystems(istdy)
lstter = PSRStudy_getCollectionPlants(istdy, PSR_PLANTTYPE_THERMAL)

nthermal = PSRCollectionElement_maxElements(lstter)

#       Pega um elemento qualquer
#       -------------------------
itherm = PSRCollectionElement_element(lstter, 0)

#       Acessa o modelo do elemento
#       ---------------------------
imodel = PSRElement_model(itherm)

#       Percorre todos os vetores do modelo
#       -----------------------------------
totalvec = PSRModel_maxVector(imodel)

for ivec = 1:min(3, totalvec)

#         Obtem atributos do vetor
#         ------------------------
    vetor = PSRModel_vector(imodel, ivec - 1)

    id = PSRVector_id(vetor)
    tipo = PSRVector_getDataType(vetor)

    print(string("id: ", id))

    if ( tipo == PSR_PARM_STRING || tipo == PSR_PARM_STRING_POINTER )
        print(" tipo: string")
    elseif ( tipo == PSR_PARM_INTEGER || tipo == PSR_PARM_INTEGER_POINTER)
        print(" tipo: inteiro")
    elseif ( tipo == PSR_PARM_REAL || tipo == PSR_PARM_REAL_POINTER)
        print(" tipo: real")
    elseif ( tipo == PSR_PARM_DATE || tipo == PSR_PARM_DATE_POINTER)
        print(" tipo: data")
    end

#         Verifica se eh dimensionado
#         ---------------------------
    dimInfo = PSRVector_getDimensionInformation(vetor)
    if ( dimInfo == C_NULL )
        println("")
    else
        totaldims = PSRVectorDimensionInformation_getNumberDimensions(dimInfo)
        println(string(" dimensionado com ", totaldims, " dimensoes"))

    end

end

