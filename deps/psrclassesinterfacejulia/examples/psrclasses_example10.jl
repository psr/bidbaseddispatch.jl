#       Seta PATH para PSRCLASSES
#       -------------------------
PATH_PSRCLASSES = normpath(joinpath(@__DIR__, ".."))

if get(ENV, "PSRC_JL_TESTING", "0") != "1"
#       Inclui as definicoes do modulo PSRClasses
#       -----------------------------------------
    include(string(PATH_PSRCLASSES, "psrclasses.jl"))

#       Inicializa a DLL do PSRClasses
#       ------------------------------
    PSRClasses_init(PATH_PSRCLASSES)
end

#       Configura Gerenciador de Log
#       ----------------------------
ilog = PSRManagerLog_getInstance(0)

#       Inicializa idioma portugues para log
#       ------------------------------------
PSRManagerLog_initPortuguese(ilog)

#       Acrescenta Log do Tipo Console Simples ao Gerenciador (log na tela)
#       -------------------------------------------------------------------
ilogcons = PSRLogSimpleConsole_create(0)
PSRManagerLog_addLog(ilog, ilogcons)

#       Configura Gerenciador de Mascaras e carrega Mascaras
#       ----------------------------------------------------
igmsk  = PSRManagerIOMask_getInstance(0)
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_V10.2.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_V10.3.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_Blocks.pmk"))

#       Configura Gerenciador de Modelos e carrega Modelos
#       --------------------------------------------------
igmdl = PSRManagerModels_getInstance(0)
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_V10.2.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_V10.3.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_Keywords.pmd"))

#       Cria Estudo
#       -----------
istdy = PSRStudy_create(0)

#       Cria Objeto Leitor de Estudo SDDP e carrega dados
#       ----------------------------------------------------
PATH_CASE = joinpath(PATH_PSRCLASSES, "examples\\data\\typical data\\")
iosddp = PSRIOSDDP_create(0)
iret = PSRIOSDDP_load(iosddp, istdy, PATH_CASE,
                                             PATH_CASE, PSR_SDDP_VERSION_14)

#       Carrega dados extras de informa��o de dados tipicos
#       ---------------------------------------------------
iotypical = PSRIOTypicalProfiles_create(0)
PSRIOTypicalProfiles_load(iotypical, istdy, PATH_CASE)

#       Obtem handle para representacao de dados tipicos
#       ------------------------------------------------
itypical = PSRStudy_typicalData(istdy)

#       Obtem total de registro de dados tipicos
#       ----------------------------------------
totalTypical = PSRTypicalData_getTotalTypical(itypical)
println(string("Total de valores tipicos: ", totalTypical))

#       Obtem lista de usinas gnd do estudo
#       -------------------------------------
lstgnd = PSRStudy_getCollectionPlants(istdy, PSR_PLANTTYPE_GND)
ngnd = PSRCollectionElement_maxElements(lstgnd)

#       Prealoca estruturas de dados para as hydro
#       ------------------------------------------
gndNameAux = allocstring(ngnd, 12)
gndCode = Array{Int32}(undef, ngnd)
gndValue = Array{Float64}(undef, ngnd)

#       Cria mapeador de dados para as usinas gnd
#       -----------------------------------------
imapgnd = PSRMapData_create(0)
PSRMapData_addElements(imapgnd, lstgnd)
PSRMapData_mapParm(imapgnd, "name", gndNameAux, 12)
PSRMapData_mapParm(imapgnd, "code", gndCode, 0)

#       Valores tipicos sao mapeados para um vetor chamado "typicalSerie"
#       -----------------------------------------------------------------
PSRMapData_mapVector(imapgnd, "typicalSerie", gndValue, 0)

#       Popula estruturas de dados mapeados
#       -----------------------------------
PSRMapData_pullToMemory(imapgnd)

#       Passo adicional para obter arrays de strings
#       --------------------------------------------
gndName = splitstring(gndNameAux, 12)

#       Primeiro exemplo de utilizacao: percorrendo registro a registro de informacao de dados tipicos
#       ----------------------------------------------------------------------------------------------
for ityp = 1:min(3, totalTypical)

    println(string("Exibindo registro tipico: ", ityp))

#         Seta valor tipico para o registro corrent (0-based)
#         ---------------------------------------------------
    PSRTypicalData_gotoTypical2(itypical, ityp - 1)

#         Atualiza variaveis JULIA
#         ------------------------
    PSRMapData_pullToMemory(imapgnd)

#         Exibe valores de todas as gnds
#         ------------------------
    for ignd = 1:ngnd
        println(string(gndCode[ignd], " ",
                           gndName[ignd], " ",
                           gndValue[ignd]))
    end
end

#       Segundo exemplo de utilizacao: acessando um registro pela tripla: (season, day, block)
#       --------------------------------------------------------------------------------------
println("Exibindo dados tipicos para (3,2,24)")

#       Obtem registro para season = 3, dia tipico = 2, bloco = 24
#       ----------------------------------------------------------
PSRTypicalData_gotoTypical(itypical, 3, 2, 24)

#       Atualiza variaveis JULIA
#       ------------------------
PSRMapData_pullToMemory(imapgnd)

#       Exibe valores de todas as gnds
#       ------------------------
for ignd = 1:ngnd
    println(string(gndCode[ignd], " ",
                         gndName[ignd], " ",
                         gndValue[ignd]))

end

