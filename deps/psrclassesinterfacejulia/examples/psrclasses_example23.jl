#       Seta PATH para PSRCLASSES
#       -------------------------
PATH_PSRCLASSES = normpath(joinpath(@__DIR__, ".."))

if get(ENV, "PSRC_JL_TESTING", "0") != "1"
#       Inclui as definicoes do modulo PSRClasses
#       -----------------------------------------
    include(string(PATH_PSRCLASSES, "psrclasses.jl"))

#       Inicializa a DLL do PSRClasses
#       ------------------------------
    PSRClasses_init(PATH_PSRCLASSES)
end

#       Configura Gerenciador de Log
#       ----------------------------
ilog = PSRManagerLog_getInstance(0)

#       Inicializa idioma portugues para log
#       ------------------------------------
PSRManagerLog_initPortuguese(ilog)

#       Acrescenta Log do Tipo Console Simples ao Gerenciador (log na tela)
#       -------------------------------------------------------------------
ilogcons = PSRLogSimpleConsole_create(0)
PSRManagerLog_addLog(ilog, ilogcons)

#       Configura Gerenciador de Mascaras e carrega Mascaras
#       ----------------------------------------------------
igmsk  = PSRManagerIOMask_getInstance(0)
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_V10.2.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_V10.3.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_Blocks.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_Optgen.pmk"))


#       Configura Gerenciador de Modelos e carrega Modelos
#       --------------------------------------------------
igmdl = PSRManagerModels_getInstance(0)
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_V10.2.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_V10.3.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_Optgen.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_Optgen_Keywords.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_Keywords.pmd"))

#       Cria Estudo
#       -----------
istdy = PSRStudy_create(0)

#       Cria Objeto Leitor de Estudo SDDP e carrega dados
#       ----------------------------------------------------
PATH_CASE = joinpath(PATH_PSRCLASSES, "examples\\data\\caso_dias_tipicos\\")
iosddp = PSRIOSDDP_create(0)
iret = PSRIOSDDP_load(iosddp, istdy, PATH_CASE,
                                             PATH_CASE, PSR_SDDP_VERSION_14)

#       Carrega dados adicionais do optgen para o estudo
#       ------------------------------------------------
iooptgen = PSRIOOptgen_create(0)
iret = PSRIOOptgen_load(iooptgen, istdy, joinpath(PATH_PSRCLASSES, PATH_CASE))
iret = PSRIOOptgen_loadTypicalDays(iooptgen, istdy, joinpath(PATH_PSRCLASSES, PATH_CASE))

#       Cria TimerController da expansao (com dias tipicos)
#       -------------------------------------------------
ictrl = PSRExpansionTimeController_create(0)
PSRTimeController_addElement(ictrl, istdy, true)
PSRTimeController_configureFrom(ictrl, istdy)

#       Obtem numero de seasons e dias tipicos do estudo
#       ------------------------------------------------
nseasons = PSRExpansionTimeController_getNumberSeasons(ictrl)
nypical_days = PSRExpansionTimeController_getNumberTypicalDays(ictrl)

println(string("Total de seasons: ", nseasons))
println(string("Total de typical days: ", nypical_days))

#       Obtem os estagios associados a cada estacao
#       -------------------------------------------
for season = 1:min(2, nseasons)

#         Obtem os numero de estagios representado pelo ano-season
#         ----------------------------------------------------
    number_stages = PSRExpansionTimeController_getNumberAssociatedStages(ictrl, 2030, season)

#         Obtem os estagios representados pela season
#         -------------------------------------------
    stagesFromSeason = Array{Int32}(undef, number_stages)
    PSRExpansionTimeController_getAssociatedStages(ictrl, 2030, season, stagesFromSeason)
    println(string("Estagios associados a season: ", season, " = ", stagesFromSeason))
end

#       Varre todos os estagios do estudo
#       ---------------------------------
for stage = 1:2# 12

    PSRTimeController_gotoStage(ictrl, stage)

#         Obtem qual eh a seaso atual, associada ao estagio atual
#         -------------------------------------------------------
    season = PSRExpansionTimeController_getCurrentSeason(ictrl)

#         Varre todos os dias tipicos
#         ---------------------------
    for typical_day = 1:min(3, nypical_days)

#           Obtem os numero de dias do mes representado pelo dia tipico atual
#           -----------------------------------------------------------------
        number_days = PSRExpansionTimeController_getNumberAssociatedDays(ictrl, typical_day)

#           Obtem os estagios representados pela season
#           -------------------------------------------
        daysFromTypicalDay = Array{Int32}(undef, number_days)
        PSRExpansionTimeController_getAssociatedDays(ictrl, typical_day, daysFromTypicalDay)
        println(string("Dias do mes, stage = ", stage, " associados ao dia tipico: ", typical_day, " = ", daysFromTypicalDay))

#           Percorre as 24 horas do dia tipico, obtendo expansionBlock (bloco do optgen) e commitBlock
#           ------------------------------------------------------------------------------------------
        for hour = 1:min(4, 24)
#             Avanca para hora especifica do dia tipico corrente
#             --------------------------------------------------
            PSRExpansionTimeController_gotoTypicalDayHour(ictrl, typical_day, hour)

#             Obtem os blocks especificos
#             ---------------------------
            optgen_block =  PSRExpansionTimeController_expansionBlock(ictrl)
            commit_block =  PSRExpansionTimeController_commitBlock(ictrl)

            println(string("stage = ", stage, " season = ", season, " dia tipico = ", typical_day, " hora = ", hour, " expansion block = ", optgen_block, " commit block = ", commit_block))
        end

    end


end





