#       Seta PATH para PSRCLASSES
#       -------------------------
PATH_PSRCLASSES = normpath(joinpath(@__DIR__, ".."))

if get(ENV, "PSRC_JL_TESTING", "0") != "1"
#       Inclui as definicoes do modulo PSRClasses
#       -----------------------------------------
    include(string(PATH_PSRCLASSES, "psrclasses.jl"))

#       Inicializa a DLL do PSRClasses
#       ------------------------------
    PSRClasses_init(PATH_PSRCLASSES)
end

#       Configura Gerenciador de Log
#       ----------------------------
ilog = PSRManagerLog_getInstance(0)

#       Inicializa idioma portugues para log
#       ------------------------------------
PSRManagerLog_initPortuguese(ilog)

#       Acrescenta Log do Tipo Console Simples ao Gerenciador (log na tela)
#       -------------------------------------------------------------------
ilogcons = PSRLogSimpleConsole_create(0)
PSRManagerLog_addLog(ilog, ilogcons)

#       Configura Gerenciador de Mascaras e carrega Mascaras
#       ----------------------------------------------------
igmsk  = PSRManagerIOMask_getInstance(0)
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_V10.2.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_V10.3.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_Blocks.pmk"))

#       Configura Gerenciador de Modelos e carrega Modelos
#       --------------------------------------------------
igmdl = PSRManagerModels_getInstance(0)
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_V10.2.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_V10.3.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_Keywords.pmd"))

#       Cria Estudo
#       -----------
istdy = PSRStudy_create(0)

#       Cria Objeto Leitor de Estudo SDDP e carrega dados
#       ----------------------------------------------------
PATH_CASE = joinpath(PATH_PSRCLASSES, "examples\\data\\bo-caso4\\")
iosddp = PSRIOSDDP_create(0)
iret = PSRIOSDDP_load(iosddp, istdy, PATH_CASE, PATH_CASE, PSR_SDDP_VERSION_14)

#       Acessa sistema do estudo
#       ------------------------
system = PSRStudy_system(istdy, 0)

#       Acessa lista de reserva de geracao do sistema
#       ---------------------------------------------
listgenres = PSRSystem_getReserveGenerationConstraintList(system)

#       Obtem a lista de reserva como uma colecao (para poder utilizar os metodos especificos de colecoes)
#       --------------------------------------------------------------------------------------------------
colgenres = PSRConstraintList_asCollectionElements(listgenres)
numgenres = PSRCollectionElement_maxElements(colgenres)

#       Cria mapeador de dados para as restricoes de reserva de geracao
#       ---------------------------------------------------------------
Type = Array{Int32}(undef, numgenres)
Penalty = Array{Float64}(undef, numgenres)
Restricao = Array{Float64}(undef, numgenres)
CompensateSel = Array{Int32}(undef, numgenres)
CompensateNoSel = Array{Int32}(undef, numgenres)

imapresger = PSRMapData_create(0)
PSRMapData_addElements(imapresger, colgenres)
PSRMapData_mapParm(imapresger, "Penalty", Penalty, 0)
PSRMapData_mapParm(imapresger, "Type", Type, 0)
PSRMapData_mapParm(imapresger, "CompensateSel", CompensateSel, 0)
PSRMapData_mapParm(imapresger, "CompensateNoSel", CompensateNoSel, 0)
PSRMapData_mapDimensionedVector(imapresger, "Restricao", Restricao, 0, "block", "")

PSRMapData_pullToMemory(imapresger)

#       Obtem colecao de usinas hydros
#       ------------------------------
colhyd = PSRStudy_getCollectionPlants(istdy, PSR_PLANTTYPE_HYDRO)
numhyd = PSRCollectionElement_maxElements(colhyd)

#       Cria mapeador de dados para as restricoes de reserva de geracao
#       ---------------------------------------------------------------
ipfirst = Array{Int32}(undef, numgenres)
ipnext = Array{Int32}(undef, numhyd * numgenres)
ippointer = Array{Int32}(undef, numhyd * numgenres)

#       Verifica todas as usinas hydros que pertencem a cada restricao de reserva
#       -------------------------------------------------------------------------
PSRCollectionElement_mapComplexRelationShip(colgenres, colhyd, ipfirst, ipnext, ippointer, PSR_RELATIONSHIP_1TON, false)
for ires = 1:min(2, numgenres)
    println(PSRConstraintData_name(PSRCollectionElement_element(colgenres, ires - 1)))
    next = ipfirst[ires]
    while ( next > 0 )
        println(string("name = ", PSRPlant_name(PSRCollectionElement_element(colhyd, ippointer[next] - 1))))
        next = ipnext[next]
    end
end

#       Analise inversa
#       Verifica todas as restricoes que cada usina hydro compoe
#       --------------------------------------------------------
ipfirst = Array{Int32}(undef, numhyd)
PSRCollectionElement_mapComplexRelationShip(colhyd, colgenres, ipfirst, ipnext, ippointer, PSR_RELATIONSHIP_1TON, false)
for ihyd = 1:min(3, numhyd)
    println(PSRPlant_name(PSRCollectionElement_element(colhyd, ihyd - 1)))
    next = ipfirst[ihyd]
    while ( next > 0 )
        println(string("name = ", PSRConstraintData_name(PSRCollectionElement_element(colgenres, ippointer[next] - 1))))
        next = ipnext[next]
    end
end



