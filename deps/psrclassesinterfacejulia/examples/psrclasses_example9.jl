#       Seta PATH para PSRCLASSES
#       -------------------------
PATH_PSRCLASSES = normpath(joinpath(@__DIR__, ".."))

if get(ENV, "PSRC_JL_TESTING", "0") != "1"
#       Inclui as definicoes do modulo PSRClasses
#       -----------------------------------------
    include(string(PATH_PSRCLASSES, "psrclasses.jl"))

#       Inicializa a DLL do PSRClasses
#       ------------------------------
    PSRClasses_init(PATH_PSRCLASSES)
end

#       Configura Gerenciador de Log
#       ----------------------------
ilog = PSRManagerLog_getInstance(0)

#       Inicializa idioma portugues para log
#       ------------------------------------
PSRManagerLog_initPortuguese(ilog)

#       Acrescenta Log do Tipo Console Simples ao Gerenciador (log na tela)
#       -------------------------------------------------------------------
ilogcons = PSRLogSimpleConsole_create(0)
PSRManagerLog_addLog(ilog, ilogcons)

#       Configura Gerenciador de Mascaras e carrega Mascaras
#       ----------------------------------------------------
igmsk  = PSRManagerIOMask_getInstance(0)
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_V10.2.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_V10.3.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_Blocks.pmk"))
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_Optgen.pmk"))


#       Configura Gerenciador de Modelos e carrega Modelos
#       --------------------------------------------------
igmdl = PSRManagerModels_getInstance(0)
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_V10.2.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_V10.3.pmd"))
        # iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES,"Models_Optgen_Keywords.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_Optgen.pmd"))
iret = PSRManagerModels_importFile(igmdl, string(PATH_PSRCLASSES, "Models_SDDP_Keywords.pmd"))

#       Cria Estudo
#       -----------
istdy = PSRStudy_create(0)

#       Cria Objeto Leitor de Estudo SDDP e carrega dados
#       ----------------------------------------------------
iosddp = PSRIOSDDP_create(0)
PATH_CASE = joinpath(PATH_PSRCLASSES, "examples\\data\\ExpB3eor0tol\\")
iret = PSRIOSDDP_load(iosddp, istdy, PATH_CASE,
                                             PATH_CASE, PSR_SDDP_VERSION_12)


#       Carrega dados adicionais do optgen para o estudo
#       ------------------------------------------------
iooptgen = PSRIOOptgen_create(0)
iret = PSRIOOptgen_load(iooptgen, istdy,  PATH_CASE)

#       Obtem ponteiro para as informacoes de expansao
#       ----------------------------------------------
iexpd = PSRStudy_expansionData(istdy)

#       Remove decisoes previas do caso
#       -------------------------------
PSRExpansionData_removeDecisions(iexpd)

#       Obtem lista de usinas hydro do estudo
#       -------------------------------------
lsthyd = PSRStudy_getCollectionPlants(istdy, PSR_PLANTTYPE_HYDRO)
nhydro = PSRCollectionElement_maxElements(lsthyd)

#       Prealoca estruturas de dados para as hydro
#       ------------------------------------------
hydroNameAux = allocstring(nhydro, 12)

#       Cria mapeador de dados para as usinas hydro
#       ----------------------------------------------
imaphyd = PSRMapData_create(0)
PSRMapData_addElements(imaphyd, lsthyd)
PSRMapData_mapParm(imaphyd, "name", hydroNameAux, 12)

#       Popula estruturas de dados mapeados
#       -----------------------------------
PSRMapData_pullToMemory(imaphyd)

#       Passo adicional para obter arrays de strings
#       --------------------------------------------
hydroName = splitstring(hydroNameAux, 12)

#       Mostra o custo de investimento para todos os projetos de hidrel�tricas
#       --------------------------------------------------------------------
for ihyd = 1:min(2, nhydro)
#           Obtem ponteiro para a hydro
#           ---------------------------
    hydro = PSRCollectionElement_element(lsthyd, ihyd - 1)

#           Obtem ponteiro para projeto da hydro (se nao existir, retorna C_NULL)
#           ---------------------------------------------------------------------
    iproj = PSRExpansionData_getProject5(iexpd, hydro)
    if ( iproj != C_NULL )
#             Acessa modelo de dados do projeto
#             ---------------------------------
        imodproj = PSRElement_model(iproj)

#             Obtem custo de investimento
#             ---------------------------
        investCost = PSRParm_getReal(PSRModel_parm2(imodproj, "InvestCost"))
        println(string(hydroName[ihyd], " = ", investCost))
    end
end

#       Seleciona o primeiro projeto do caso
#       ------------------------------------
ifirst_proj = PSRExpansionData_project(iexpd, 0)

#       Cria uma nova decisao e adiciona a representacao de expansao do estudo
#       ----------------------------------------------------------------------
idecision = PSRExpansionDecision_create(0)

#       Associa modelo de decisao
#       -------------------------
PSRManagerModels_buildModel(igmdl, PSRElement_model(idecision), "MODL:Optgen_DecisaoExpansao")

#       Associa o primeiro projeto a decisao e define seus parametros
#       -------------------------------------------------------------
#       Argumentos: decision, project, min period, min year, max period, max year, capacity
PSRExpansionDecision_setProject(idecision, ifirst_proj, 8, 2016, 8, 2016, 100, PSR_DECISIONTYPE_EXACTLY)

#       Adiciona decisao a representacao de expansao do estudo
#       ------------------------------------------------------
PSRExpansionData_addDecision(iexpd, idecision)

#       Atualiza as informacoes de decisoes na base
#       -------------------------------------------
iodecision = PSRIOOptgenExpansionDecision_create(0)
PSRIOOptgenExpansionDecision_save(iodecision, istdy, "examples\\output\\psrclasses_example9.csv")

#       Atualiza as informacoes de decisoes na base
#       -------------------------------------------
PSRModel_print(PSRElement_model(idecision), "examples\\output\\psrclasses_example9_decisions.out")





