PSRClasses.initialize()

PATH_PSRCLASSES = dirname(PSRClasses._PSRCLASSES_BASE_PATH)

#= 
    Initialize PSRClasses =#

ilog = PSRManagerLog()
initPortuguese(ilog)
ilogcons = PSRLogSimpleConsole()
addLog(ilog, ilogcons)

igmsk  = PSRManagerIOMask()
iret = importFile(igmsk, joinpath(PATH_PSRCLASSES, "Masks_SDDP_V10.2.pmk"))
iret = importFile(igmsk, joinpath(PATH_PSRCLASSES, "Masks_SDDP_V10.3.pmk"))
iret = importFile(igmsk, joinpath(PATH_PSRCLASSES, "Masks_SDDP_Blocks.pmk"))

igmdl = PSRManagerModels()
iret = importFile(igmdl, joinpath(PATH_PSRCLASSES, "Models_SDDP_V10.2.pmd"))
iret = importFile(igmdl, joinpath(PATH_PSRCLASSES, "Models_SDDP_V10.3.pmd"))

#= 
    Load data with PSRClasses =#

# PATH_CASE = joinpath(dirname(@__DIR__), "data", "example", "00/")
PATH_CASE = joinpath(@__DIR__, "data", "bo-caso1/")
@assert isdir(PATH_CASE)
PATH_SDAT = joinpath(PATH_CASE, "sddp.dat")
@assert isfile(PATH_SDAT)

istdy = PSRStudy()
iosddp = PSRIOSDDP()
PSRIOSDDP_useNetwork(iosddp, false)
useIndexedHydroParameters(iosddp, true)
useOnlySelectedSystems(iosddp, true)
load_ret = load(iosddp, istdy, PATH_CASE, PATH_CASE, PSR_SDDP_VERSION_14)
@assert load_ret == 1

iupdater = PSRUpdaterSDDP()
applyExpansionDecisions(iupdater, istdy)
applyRulesForDefaultBlocks(iupdater, istdy)
# applyMaintenanceData(iupdater, istdy)
toFuelConsumptionRepresentation(iupdater, istdy)

PATH_FORW = joinpath(PATH_CASE, "forw.psr")
PATH_BACK = joinpath(PATH_CASE, "back.psr")
@assert isfile(PATH_FORW)
@assert isfile(PATH_BACK)
iinflow = PSRIOSDDPHydroForwardBackward()
inflow_ret = load(iinflow, istdy, PATH_FORW, PATH_BACK)
@assert inflow_ret == 1
ihydro = PSRIOSDDPHydroParameters()
generateIndexedHydroParameters2(ihydro, istdy)

number_stages = getNumberStages(istdy)
number_blocks = getNumberBlocks(istdy)
number_scenarios = getNumberSimulations(istdy)

listsys = getCollectionSystems(istdy)
listhyd = getCollectionPlants(istdy, PSR_PLANTTYPE_HYDRO)
query(listhyd, "REMOVEDATA WHERE Existing>=2")
listter = getCollectionPlants(istdy, PSR_PLANTTYPE_THERMAL)
query(listter, "REMOVEDATA WHERE Existing>=2")
listgnd = getCollectionPlants(istdy, PSR_PLANTTYPE_GND)
query(listgnd, "REMOVEDATA WHERE Existing>=2")
liststa = getCollectionGaugingStations(istdy)

listfls = getCollectionFuels(istdy)
listint = getCollectionInterconnections(istdy)
listfcs = getCollectionFuelConsumptions(istdy)
listbat = getCollectionShunts(istdy, PSR_DEVICETYPE_BATTERY)

listdem = getCollectionDemands(istdy)
listdsg = getCollectionDemandSegments(istdy)

# psrc.int_lst = PSRStudy_getCollectionInterconnections(psrc.study_ptr)
# psrc.bus_lst = PSRStudy_getCollectionBuses(psrc.study_ptr)
# psrc.gen_lst = PSRStudy_getCollectionShunts(psrc.study_ptr, PSR_DEVICETYPE_GENERATOR)
# psrc.lod_lst = PSRStudy_getCollectionShunts(psrc.study_ptr, PSR_DEVICETYPE_LOAD)
# psrc.inj_lst = PSRStudy_getCollectionShunts(psrc.study_ptr, PSR_DEVICETYPE_POWERINJECTION)
# psrc.dcl_lst = PSRStudy_getCollectionSeries2(psrc.study_ptr, PSR_DEVICETYPE_LINKDC)
# PSRCollectionElement_query(psrc.dcl_lst, "REMOVEDATA WHERE Existing>=2")
# psrc.cir_lst = PSRStudy_getCollectionSeries(psrc.study_ptr)
# # add trafos to regular circuits
# trf_lst = PSRStudy_getCollectionTransformers(psrc.study_ptr)
# PSRCollectionElement_addCollection(psrc.cir_lst, trf_lst)

# # must be sorted to match psrnetwork
# PSRCollectionElement_sortOn(psrc.cir_lst, "code")
# PSRCollectionElement_sortOn(psrc.bus_lst, "code")
# PSRCollectionElement_sortOn(psrc.dcl_lst, "code")
# PSRCollectionElement_sortOn(psrc.isc_lst, "code")

nsys     = maxElements(listsys)
# @assert nsys == 1
nthermal = maxElements(listter)
nfuel    = maxElements(listfls)
nfcs     = maxElements(listfcs)
@assert nthermal == nfcs
nstation = maxElements(liststa) # !
nhydro   = maxElements(listhyd)
ndemand  = maxElements(listdem)
ndemseg  = maxElements(listdsg)
@assert ndemand == ndemseg
ngnd     = maxElements(listgnd)
nbat     = maxElements(listbat)

#= 
    Thermal =#

# Main thermal data

ipthermsys = Array{Int32}(undef, nthermal)
mapRelationShip(listter, listsys, ipthermsys, PSR_RELATIONSHIP_1TO1, false)

thermCode    = Array{Int32}(undef, nthermal)
thermFut     = Array{Int32}(undef, nthermal)
thermCap     = Array{Float64}(undef, nthermal)
# thermCost    = Array{Float64}(undef, nthermal)
# thermCVaria  = Array{Float64}(undef, nthermal)
# thermCTransp = Array{Float64}(undef, nthermal)

map_ter = PSRMapData()
addElements(map_ter, listter)
mapParm(map_ter, "code", thermCode, 0)
mapVector(map_ter, "Existing", thermFut, 0)
# mapVector(map_ter, "PotInst", thermCap, 0)
mapVector(map_ter, "GerMax", thermCap, 0)
# mapVector(map_ter, "O&MCost", thermCVaria, 0)
# mapVector(map_ter, "CTransp", thermCTransp, 0)
# mapDimensionedVector(map_ter, "CEsp", thermCost, 0, "segment", "block")

t_exist(j) = thermFut[j] <= 0

# Fuel data

fuelCost = Array{Float64}(undef, nfuel)
map_fls = PSRMapData()
addElements(map_fls, listfls)
mapVector(map_fls, "Custo", fuelCost, 0)

# Fuel consumtion data

ipfcsfls = Array{Int32}(undef, nfcs)
mapRelationShip(listfcs, listfls, ipfcsfls, PSR_RELATIONSHIP_1TO1, false)

ipfcster = Array{Int32}(undef, nfcs)
mapRelationShip(listfcs, listter, ipfcster, PSR_RELATIONSHIP_1TO1, false)

fcsCEsp    = Array{Float64}(undef, nfcs)
fcsCVaria  = Array{Float64}(undef, nfcs)
fcsCTransp = Array{Float64}(undef, nfcs)

map_fcs = PSRMapData()
addElements(map_fcs, listfcs)

mapVector(map_fcs, "O&MCost", fcsCVaria, 0)
mapVector(map_fcs, "CTransp", fcsCTransp, 0)
mapDimensionedVector(map_fcs, "CEsp", fcsCEsp, 0, "segment", "block")

# final thermal cost

fcs_cost(j) = fcsCEsp[j] * (fuelCost[ipfcsfls[j]] + fcsCTransp[j]) + fcsCVaria[j]

# only valid if the mapping it 1 to 1
for i in 1:nfcs
    @assert ipfcster[i] == i
end
thermal_cost(j) = fcs_cost(j)

#= 
    Hydro =#

# Hydro main data

iphydrosys = Array{Int32}(undef, nhydro)
mapRelationShip(listhyd, listsys, iphydrosys, PSR_RELATIONSHIP_1TO1, false)

iphydrosta = Array{Int32}(undef, nhydro)
mapRelationShip(listhyd, liststa, iphydrosta, PSR_RELATIONSHIP_1TO1, false)

hydroCode    = Array{Int32}(undef, nhydro)
hydroFut     = Array{Int32}(undef, nhydro)
hydroCap     = Array{Float64}(undef, nhydro)
hydroCVaria  = Array{Float64}(undef, nhydro)
hydroFPMed   = Array{Float64}(undef, nhydro)
hydroQmax    = Array{Float64}(undef, nhydro)
hydroVmax    = Array{Float64}(undef, nhydro)
hydroVmin    = Array{Float64}(undef, nhydro)
hydroVinic   = Array{Float64}(undef, nhydro)

map_hyd = PSRMapData()
addElements(map_hyd, listhyd)
mapParm(map_hyd, "code", hydroCode, 0)
mapVector(map_hyd, "Existing", hydroFut, 0)
mapParm(map_hyd, "Vinic", hydroVinic, 0)
# mapVector(map_hyd, "PotInst", hydroCap, 0)
mapVector(map_hyd, "PotInst", hydroCap, 0)
mapVector(map_hyd, "O&MCost", hydroCVaria, 0)
mapVector(map_hyd, "FPMed", hydroFPMed, 0)
mapVector(map_hyd, "Qmax", hydroQmax, 0)
mapVector(map_hyd, "Vmax", hydroVmax, 0)
mapVector(map_hyd, "Vmin", hydroVmin, 0)

h_exist(i) = hydroFut[i] <= 0
initial_volume(i) = hydroVmin[i] + hydroVinic[i] * (hydroVmax[i] - hydroVmin[i])

# Map hydro topology
#   upstream turbining
first   = Array{Int32}(undef, nhydro)
next    = Array{Int32}(undef, nhydro * nhydro)
pointer = Array{Int32}(undef, nhydro * nhydro)
mapComplexRelationShip(listhyd, listhyd, first, next, pointer, PSR_RELATIONSHIP_TURBINED_TO, false)
MT = [zeros(Int32, 0) for i in 1:nhydro]
for r in 1:nhydro
    current_index = first[r]
    while current_index > 0
        push!(MT[r], pointer[current_index])
        current_index = next[current_index]
    end
end
#   upstream spillage
first   = Array{Int32}(undef, nhydro)
next    = Array{Int32}(undef, nhydro * nhydro)
pointer = Array{Int32}(undef, nhydro * nhydro)
mapComplexRelationShip(listhyd, listhyd, first, next, pointer, PSR_RELATIONSHIP_SPILLED_TO, false)
MS = [zeros(Int32, 0) for i in 1:nhydro]
for r in 1:nhydro
    current_index = first[r]
    while current_index > 0
        push!(MS[r], pointer[current_index])
        current_index = next[current_index]
    end
end

# Hydro stations data

staOrder     = Array{Int32}(undef, nstation)
staInflow     = Array{Float64}(undef, nstation)

map_sta = PSRMapData()
addElements(map_sta, liststa)
mapVector(map_sta, "Ordem", staOrder, 0)

PSRIOSDDPHydroForwardBackward_mapTo(iinflow, staInflow)


#= 
    Demand =#

# Main demand data

demandCode    = Array{Int32}(undef, ndemand)
demandElastic = Array{Int32}(undef, ndemand)

map_dem = PSRMapData()
addElements(map_dem, listdem)

mapParm(map_dem, "code",     demandCode, 0)
mapParm(map_dem, "Elastico", demandElastic, 0)

# Demand segment data

demandDemandTemp = Array{Float64}(undef, ndemseg)
demandDemand     = Array{Float64}(undef, ndemseg)
# demandCost  = Array{Float64}(undef, ndemand)

map_dsg = PSRMapData()
addElements(map_dsg, listdsg)

mapDimensionedVector(map_dsg, "Demanda", demandDemandTemp, 0, "block", "")

#= 
    Time controller =#

ictrl = PSRTimeController()
addElement(ictrl, istdy, true)
configureFrom(ictrl, istdy)
setDimension(ictrl, "segment", 1)
setDimension(ictrl, "block", 1)

gotoStage(ictrl, 1)
pullToMemory(map_ter)
pullToMemory(map_fls)
pullToMemory(map_fcs)

pullToMemory(map_hyd)
pullToMemory(map_sta)
setForward(iinflow, 1, 1) # (t, s)

pullToMemory(map_dem)
pullToMemory(map_dsg)

@assert all(0 .<= hydroVinic .<= 1)
@assert sum(demandElastic) == 0






println(string("Descricao do caso: ", description(istdy)))

println(string("Total de estagios: ", number_stages))
println(string("Total de cenarios: ", number_scenarios))
println(string("Total de blocos: ", number_blocks))

println(string("Total de sistemas do caso: ", nsys))
println(string("Total de hydros do caso: ", nhydro))
println(string("Total de termicas do caso: ", nthermal))
println(string("Total de gnds do caso: ", ngnd))
println(string("Total de demandas do caso: ", ndemseg))
println(string("Total de demseg do caso: ", ndemand))