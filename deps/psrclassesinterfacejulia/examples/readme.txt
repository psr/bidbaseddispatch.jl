Exemplo				Descrição dos exemplos
psrclasses_example1.jl		Ciclo padrão para uma aplicação com PSRClasses (modelo SDDP)
psrclasses_example2.jl		Exibe a topologia da rede hidrologia de um estudo 
psrclasses_example3.jl		Processamento de dados de vazões geradas (arquivos forward e backward)
psrclasses_example4.jl		Carregamento de dados Netplan
psrclasses_example5.jl		Geração de resultado GRAF CSV
psrclasses_example6.jl		Geração e leitura de resultado GRAF Binário
psrclasses_example7.jl		Geração e leitura de função de custo imediato
psrclasses_example8.jl		Leitura de função de custo futuro
psrclasses_example9.jl		Carregamento de dados de expansão (Optgen) e geração de dados de decisão de expansão
psrclasses_example10.jl		Carregamento de dados típicos para um caso SDDP
psrclasses_example11.jl		Geração e leitura simultânea de função de custo futuro
psrclasses_example12.jl		Carregamento de cenários horários de GND para um caso SDDP (a partir de CSV) 
psrclasses_example13.jl		Cria configuração de resultados de um estudo a partir de um indexdef.fmt
psrclasses_example14.jl		Exibe os dados de reserva de geração de um sistema
psrclasses_example15.jl		Exibe propriedades de um elemento do estudo de forma genérica
psrclasses_example16.jl		Importa manutenções de usinas a partir de .CSV e salva arquivo de manutenções do SDDP
psrclasses_example17.jl		Importa imagem binária de caso gerado a partir do PSRCore
psrclasses_example18.jl		Exibe informações adicionais de mapeamento, a respeito dos dados de irrigação
psrclasses_example22.jl		Gera dados de expansao no formato netplan
psrclasses_example23.jl		Carregamento de dados de expansão (Optgen) com dias tipicos
psrclasses_example24.jl		Geração e leitura e append alternados da função de custo futuro
