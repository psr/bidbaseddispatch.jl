#       Seta PATH para PSRCLASSES
#       -------------------------
PATH_PSRCLASSES = normpath(joinpath(@__DIR__, ".."))

if get(ENV, "PSRC_JL_TESTING", "0") != "1"
#       Inclui as definicoes do modulo PSRClasses
#       -----------------------------------------
    include(string(PATH_PSRCLASSES, "psrclasses.jl"))

#       Inicializa a DLL do PSRClasses
#       ------------------------------
    PSRClasses_init(PATH_PSRCLASSES)
end

#       Configura Gerenciador de Mascaras e carrega Mascaras
#       ----------------------------------------------------
igmsk  = PSRManagerIOMask_getInstance(0)
iret = PSRManagerIOMask_importFile(igmsk, string(PATH_PSRCLASSES, "Masks_SDDP_V10.2.pmk"))

istdy = PSRStudy_create(0)
#       Cria Objeto Leitor de Estudo SDDP e carrega dados
#       ----------------------------------------------------
iosddp = PSRIOSDDP_create(0)

#       Carrega configuracao de GRAF a partir de uma pasta de definicoes (local do indexdef.fmt)
#       ---------------------------------------------------------------------------------------
PATH_CASE = joinpath(PATH_PSRCLASSES, "examples\\data\\sddp files")
PSRIOSDDP_loadGrafConfiguration(iosddp, istdy,  PATH_CASE);

#       Acessa objeto de graf configuration do estudo
#       ---------------------------------------------
grafConf = PSRStudy_getGrafConfiguration(istdy)

#       Seleciona um GRAF pelo codigo da saida (2 = potencia hydro diponivel) e um GRAF pelo nome
#       -----------------------------------------------------------------------------------------
PSRGrafConfiguration_selectConfiguration(grafConf, 2)
PSRGrafConfiguration_selectConfiguration2(grafConf, "demand")

#       Gera dadger.grf e indice.grf associado ao GrafConfiguration do estudo no diretorio corrente atual
#       ------------------------------------------------------------------------------------
PSRIOSDDP_saveIndexGrf(iosddp, istdy, ".\\output\\")
