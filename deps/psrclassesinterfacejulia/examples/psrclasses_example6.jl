using Libdl

# ----------------------------------
# Parte 1 - Inicializacao PSRCLASSES
# ----------------------------------

# Seta PATH para PSRCLASSES
PATH_PSRCLASSES = normpath(joinpath(@__DIR__, ".."))

if get(ENV, "PSRC_JL_TESTING", "0") != "1"
#       Inclui as definicoes do modulo PSRClasses
#       -----------------------------------------
    include(string(PATH_PSRCLASSES, "psrclasses.jl"))

#       Inicializa a DLL do PSRClasses
#       ------------------------------
    PSRClasses_init(PATH_PSRCLASSES)
end

# Configura Gerenciador de Log
ilog = PSRManagerLog_getInstance(0)

# Inicializa idioma portugues para log
PSRManagerLog_initPortuguese(ilog)

# -----------------------------------
# Parte 2 - Configuracao do resultado
# -----------------------------------

# Cria objeto para manipulacao de saida GRAF
iograf = PSRIOGrafResultBinary_create(0)
# para CSV use:
# iograf = PSRIOGrafResult_create(0)

# Configura parametros da saida
# -----------------------------
# Variavel por bloco = 1
# Variavel por serie = 1
# Unidade = MW
# Tipo de Estagio = mensal
# Estagio inicial = Janeiro de 2016
# Total de blocos = 3
# Total de Series = 10
PSRIOGrafResultBase_setNameLength(iograf, 24)

PSRIOGrafResultBase_setVariableByBlock(iograf, 1)
PSRIOGrafResultBase_setVariableBySerie(iograf, 1)
PSRIOGrafResultBase_setUnit(iograf, "MW")

PSRIOGrafResultBase_setStageType(iograf, PSR_STAGETYPE_MONTHLY)
# PSRIOGrafResultBase_setStageType(iograf, PSR_STAGETYPE_WEEKLY)

PSRIOGrafResultBase_setInitialStage(iograf, 1)
PSRIOGrafResultBase_setInitialYear(iograf, 2016)

PSRIOGrafResultBase_setTotalBlocks(iograf, 3)
PSRIOGrafResultBase_setTotalSeries(iograf, 10)
PSRIOGrafResultBase_setSequencialModel(iograf, true)

# case seja horário:
# PSRIOGrafResultBase_setVariableByHour(iograf, 1)
# if studyptr != C_NULL
#     PSRIOGrafResultBase_useHourlyDurationFrom(iograf, studyptr)
# else
#     error(not suppeorted)
# end

# Define os agentes (3 agentes, X, Y e Z)
PSRIOGrafResultBase_addAgent(iograf, "X")
PSRIOGrafResultBase_addAgent(iograf, "Y")
PSRIOGrafResultBase_addAgent(iograf, "Z")

# Inicia gravacao do resultado
FILE_PATH = normpath(joinpath(PATH_PSRCLASSES, "psrclasses_example6"))
PSRIOGrafResultBinary_initSave(iograf, FILE_PATH * ".hdr", FILE_PATH * ".bin")
# versão CSV
# PSRIOGrafResult_initSave(iograf, "./output/psrclasses_example6.csv", PSRIO_GRAF_FORMAT_DEFAULT)

# ---------------------------------------------
# Parte 3 - Gravacao dos registros do resultado
# ---------------------------------------------

# Loop de gravacao
for estagio = 1:12
    for serie = 1:10
        for bloco = 1:3

            PSRIOGrafResultBase_setCurrentStage(iograf, estagio)
            PSRIOGrafResultBase_setCurrentSerie(iograf, serie)
            PSRIOGrafResultBase_setCurrentBlock(iograf, bloco)

            #       Seta valor dos agentes para o registro
            agentX_value = estagio + serie
            agentY_value = serie - estagio
            agentZ_value = estagio + serie + bloco * 100

            PSRIOGrafResultBase_setData(iograf, 0, agentX_value)
            PSRIOGrafResultBase_setData(iograf, 1, agentY_value)
            PSRIOGrafResultBase_setData(iograf, 2, agentZ_value)

            #       Grava registro
            PSRIOGrafResultBase_writeRegistry(iograf)
        end
    end
end
# Finaliza gravacao
PSRIOGrafResultBinary_closeSave(iograf)
PSRIOGrafResultBinary_deleteInstance(iograf)
# Em CSV
# PSRIOGrafResult_closeSave(iograf)
# PSRIOGrafResult_deleteInstance(iograf)

# ------------------------------------------------------------------
# Leitura de arquivo formato GRAF
# ------------------------------------------------------------------

FILE_PATH = normpath(joinpath(PATH_PSRCLASSES, "psrclasses_example6"))

iograf = PSRIOGrafResultBinary_create(0)
PSRIOGrafResultBinary_initLoad(iograf, FILE_PATH * ".hdr", FILE_PATH * ".bin")
# versão CSV
# iograf = PSRIOGrafResult_create(0)
# PSRIOGrafResult_initLoad(iograf, FILEPATH, PSRIO_GRAF_FORMAT_DEFAULT)

@assert PSRIOGrafResultBase_getTotalStages(iograf) == 12
@assert PSRIOGrafResultBase_getTotalSeries(iograf) == 10
@assert PSRIOGrafResultBase_getTotalBlocks(iograf) == 3
@assert PSRIOGrafResultBase_getNameLength(iograf) == 24
@assert PSRIOGrafResultBase_getVariableByBlock(iograf) == 1
@assert PSRIOGrafResultBase_getVariableBySerie(iograf) == 1
@assert PSRIOGrafResultBase_getVariableBySerie(iograf) == 1
@assert PSRIOGrafResultBase_getVariableByHour(iograf) == 0
@assert PSRIOGrafResultBase_getStageType(iograf) == PSR_STAGETYPE_MONTHLY
@assert PSRIOGrafResultBase_getInitialStage(iograf) == 1
@assert PSRIOGrafResultBase_getInitialYear(iograf) == 2016
@assert strip(PSRIOGrafResultBase_getUnit(iograf)) == "MW"


# em arquivos horário numero de blocos pode varia por etapa:
# PSRIOGrafResultBase_getTotalBlocks2( ptr_pointer,stage )

# obtem número de colunas
@assert PSRIOGrafResultBase_maxAgent(iograf) == 3
names = ["X", "Y", "Z"]
for i in 1:3
    @assert strip(PSRIOGrafResultBase_getAgent(iograf, i - 1)) == names[i]
end

for estagio = 1:12
    for serie = 1:10
        for bloco = 1:3

            # posiciona o ponteiro na entrada correta
            PSRIOGrafResultBase_seekStage2(iograf, estagio, serie, bloco)
            @assert PSRIOGrafResultBase_getCurrentStage(iograf) == estagio
            @assert PSRIOGrafResultBase_getCurrentSerie(iograf) == serie
            @assert PSRIOGrafResultBase_getCurrentBlock(iograf) == bloco
            # obtem valor dos agentes para o registro
            agentX_value = estagio + serie
            agentY_value = serie - estagio
            agentZ_value = estagio + serie + bloco * 100

            @assert PSRIOGrafResultBase_getData(iograf, 0) == agentX_value
            @assert PSRIOGrafResultBase_getData(iograf, 1) == agentY_value
            @assert PSRIOGrafResultBase_getData(iograf, 2) == agentZ_value
        end
    end
end
# ------------------------------------------------------------------
# Segundo metodo de iterção - mais eficiente
# ------------------------------------------------------------------

# Vai para posicao especifica do arquivo
# nesse caso a primiera entrada
PSRIOGrafResultBase_seekStage2(iograf, 1, 1, 1)
for estagio = 1:12
    for serie = 1:10
        for bloco = 1:3

            @assert PSRIOGrafResultBase_getCurrentStage(iograf) == estagio
            @assert PSRIOGrafResultBase_getCurrentSerie(iograf) == serie
            @assert PSRIOGrafResultBase_getCurrentBlock(iograf) == bloco

            agentX_value = estagio + serie
            agentY_value = serie - estagio
            agentZ_value = estagio + serie + bloco * 100

            @assert PSRIOGrafResultBase_getData(iograf, 0) == agentX_value
            @assert PSRIOGrafResultBase_getData(iograf, 1) == agentY_value
            @assert PSRIOGrafResultBase_getData(iograf, 2) == agentZ_value

            PSRIOGrafResultBase_nextRegistry(iograf, false)
        end
    end
end
# é possivel também mover entre etapas fora de ordem...
for estagio = [6,5,11,2]
    PSRIOGrafResultBase_seekStage2(iograf, estagio, 1, 1)
    for serie = 1:10
        for bloco = 1:3

            @assert PSRIOGrafResultBase_getCurrentStage(iograf) == estagio
            @assert PSRIOGrafResultBase_getCurrentSerie(iograf) == serie
            @assert PSRIOGrafResultBase_getCurrentBlock(iograf) == bloco

            agentX_value = estagio + serie
            agentY_value = serie - estagio
            agentZ_value = estagio + serie + bloco * 100

            @assert PSRIOGrafResultBase_getData(iograf, 0) == agentX_value
            @assert PSRIOGrafResultBase_getData(iograf, 1) == agentY_value
            @assert PSRIOGrafResultBase_getData(iograf, 2) == agentZ_value

            # avança para o próximo registro
            PSRIOGrafResultBase_nextRegistry(iograf, false)
        end
    end
end

PSRIOGrafResultBase_closeLoad(iograf)
PSRIOGrafResultBase_deleteInstance(iograf)

# ------------------------------------------------------------------
# Conversão de arquivo formato GRAF
# ------------------------------------------------------------------

FILEPATH_HEADER = FILE_PATH * ".hdr"
FILEPATH_BIN = FILE_PATH * ".bin"
FILEPATH_CSV = FILE_PATH * ".csv"
# ------------------------------------------------------------------
# Binarios -> CSV
# ------------------------------------------------------------------

# ATENÇÃO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# ATENÇÃO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# ATENÇÃO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# ESSA CONVERSÃO TRUNCA NOMES EM 12 CARACTERES

iograf = PSRIOGrafResultBinary_create(0)
ret = PSRIOGrafResultBinary_toCSV(iograf, FILEPATH_HEADER, FILEPATH_BIN, FILEPATH_CSV)
PSRIOGrafResultBase_deleteInstance(iograf)

# ------------------------------------------------------------------
# CSV -> Binarios
# ------------------------------------------------------------------

iograf = PSRIOGrafResult_create(0)
PSRIOGrafResult_toBinary(iograf, FILEPATH_CSV, FILEPATH_HEADER, FILEPATH_BIN)
PSRIOGrafResultBase_deleteInstance(iograf)

# ------------------------------------------------------------------
# Segunda leitura para testar conversão
# ------------------------------------------------------------------

iograf = PSRIOGrafResultBinary_create(0)
PSRIOGrafResultBinary_initLoad(iograf, FILE_PATH * ".hdr", FILE_PATH * ".bin")

# versão CSV
# iograf = PSRIOGrafResult_create(0)
# PSRIOGrafResult_initLoad(iograf, FILEPATH, PSRIO_GRAF_FORMAT_DEFAULT)

@assert PSRIOGrafResultBase_getTotalStages(iograf) == 12
@assert PSRIOGrafResultBase_getTotalSeries(iograf) == 10
@assert PSRIOGrafResultBase_getTotalBlocks(iograf) == 3
# A CONVERSÃO TRUNCA NOMES EM 12 CARACTERES
@assert PSRIOGrafResultBase_getNameLength(iograf) == 12
@assert PSRIOGrafResultBase_getVariableByBlock(iograf) == 1
@assert PSRIOGrafResultBase_getVariableBySerie(iograf) == 1
@assert PSRIOGrafResultBase_getVariableBySerie(iograf) == 1
@assert PSRIOGrafResultBase_getVariableByHour(iograf) == 0
@assert PSRIOGrafResultBase_getStageType(iograf) == PSR_STAGETYPE_MONTHLY
@assert PSRIOGrafResultBase_getInitialStage(iograf) == 1
@assert PSRIOGrafResultBase_getInitialYear(iograf) == 2016
@assert strip(PSRIOGrafResultBase_getUnit(iograf)) == "MW"
# obtem número de colunas
# A CONVERSÃO TRUNCA NOMES EM 12 CARACTERES
@assert PSRIOGrafResultBase_maxAgent(iograf) == 3
names = ["X", "Y", "Z"]
for i in 1:3
    @assert strip(PSRIOGrafResultBase_getAgent(iograf, i - 1)) == names[i]
end

for estagio = 1:12
    for serie = 1:10
        for bloco = 1:3
            # posiciona o ponteiro na entrada correta
            PSRIOGrafResultBase_seekStage2(iograf, estagio, serie, bloco)
            @assert PSRIOGrafResultBase_getCurrentStage(iograf) == estagio
            @assert PSRIOGrafResultBase_getCurrentSerie(iograf) == serie
            @assert PSRIOGrafResultBase_getCurrentBlock(iograf) == bloco

            # obtem valor dos agentes para o registro
            agentX_value = estagio + serie
            agentY_value = serie - estagio
            agentZ_value = estagio + serie + bloco * 100

            @assert PSRIOGrafResultBase_getData(iograf, 0) == agentX_value
            @assert PSRIOGrafResultBase_getData(iograf, 1) == agentY_value
            @assert PSRIOGrafResultBase_getData(iograf, 2) == agentZ_value
        end
    end
end

PSRIOGrafResultBase_closeLoad(iograf)
PSRIOGrafResultBase_deleteInstance(iograf)
# exit(1)
