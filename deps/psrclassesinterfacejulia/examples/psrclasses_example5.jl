#       ----------------------------------
#       Parte 1 - Inicializacao PSRCLASSES
#       ----------------------------------

#       Seta PATH para PSRCLASSES
#       -------------------------
PATH_PSRCLASSES = normpath(joinpath(@__DIR__, ".."))

if get(ENV, "PSRC_JL_TESTING", "0") != "1"
#       Inclui as definicoes do modulo PSRClasses
#       -----------------------------------------
    include(string(PATH_PSRCLASSES, "psrclasses.jl"))

#       Inicializa a DLL do PSRClasses
#       ------------------------------
    PSRClasses_init(PATH_PSRCLASSES)
end

#       Configura Gerenciador de Log
#       ----------------------------
ilog = PSRManagerLog_getInstance(0)

#       Inicializa idioma portugues para log
#       ------------------------------------
PSRManagerLog_initPortuguese(ilog)

#       -----------------------------------
#       Parte 2 - Configuracao do resultado
#       -----------------------------------

#       Cria objeto para manipulacao de saida GRAF
#       ------------------------------------
iograf = PSRIOGrafResult_create(0)

#       Configura parametros da saida
#       -----------------------------
#       Variavel por bloco = 1
#       Variavel por serie = 1
#       Unidade = MW
#       Tipo de Estagio = mensal
#       Estagio inicial = Janeiro de 2016
#       Total de blocos = 3
#       Total de Series = 10

PSRIOGrafResultBase_setVariableByBlock(iograf, 1)
PSRIOGrafResultBase_setVariableBySerie(iograf, 1)
PSRIOGrafResultBase_setUnit(iograf, "MW")
PSRIOGrafResultBase_setStageType(iograf, PSR_STAGETYPE_MONTHLY)
PSRIOGrafResultBase_setInitialStage(iograf, 1)
PSRIOGrafResultBase_setInitialYear(iograf, 2016)
PSRIOGrafResultBase_setTotalBlocks(iograf, 3)
PSRIOGrafResultBase_setTotalSeries(iograf, 10)

PSRIOGrafResultBase_setSequencialModel(iograf, true)


#       Define os agentes (3 agentes, X, Y e Z)
#       ----------------------------------------
PSRIOGrafResultBase_addAgent(iograf, "X")
PSRIOGrafResultBase_addAgent(iograf, "Y")
PSRIOGrafResultBase_addAgent(iograf, "Z")

#       Inicia gravacao do resultado
#       ----------------------------
PATH_CASE = joinpath(PATH_PSRCLASSES, "examples\\psrclasses_example5.csv")
PSRIOGrafResult_initSave(iograf, PATH_CASE, PSRIO_GRAF_FORMAT_DEFAULT)

#       ---------------------------------------------
#       Parte 3 - Gravacao dos registros do resultado
#       ---------------------------------------------

#       Loop de gravacao
#       ----------------
for estagio = 1:12
    for serie = 1:10
        for bloco = 1:3

#             Seta valor dos agentes para o registro
#             --------------------------------------
            agentX_value = estagio + serie
            agentY_value = serie - estagio
            agentZ_value = estagio + serie + bloco * 100

            PSRIOGrafResultBase_setData(iograf, 0, agentX_value)
            PSRIOGrafResultBase_setData(iograf, 1, agentY_value)
            PSRIOGrafResultBase_setData(iograf, 2, agentZ_value)

#             Grava registro
#             --------------
            PSRIOGrafResultBase_writeRegistry(iograf)
        end
    end
end

#       Finaliza gravacao
#       -----------------
PSRIOGrafResult_closeSave(iograf)

