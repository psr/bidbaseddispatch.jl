#       Seta PATH para PSRCLASSES
#       -------------------------
        PATH_PSRCLASSES = "../"
        
#       Seta PATH dos dados
#       -------------------
        PATH_BIN = "./"
        
#       Inclui as definicoes do modulo PSRClasses        
#       -----------------------------------------
        include(string(PATH_PSRCLASSES,"psrclasses.jl"))
        
#       Inicializa a DLL do PSRClasses
#       ------------------------------
        PSRClasses_init(PATH_PSRCLASSES)        

#       Configura Gerenciador de Log 
#       ----------------------------
        ilog = PSRManagerLog_getInstance(0)    

#       Inicializa idioma portugues para log
#       ------------------------------------
        PSRManagerLog_initPortuguese(ilog)
        
#       Acrescenta Log do Tipo Console Simples ao Gerenciador (log na tela)
#       -------------------------------------------------------------------
        ilogcons = PSRLogSimpleConsole_create(0)    
        PSRManagerLog_addLog(ilog, ilogcons)          
        
        datanode = PSRMessageDataNode_create(0)
        PSRMessageDataNode_readFromFile(datanode, string(PATH_BIN,"psrclasses.bin"))
        
        processor = PSRMessageProcessor_create(0)
        PSRMessageProcessor_process(processor, datanode)
        istdy = PSRMessageProcessor_currentStudy(processor)
        
#       Obtem lista de postos hidrologicos do estudo
#       --------------------------------------------
        lststations = PSRStudy_getCollectionGaugingStations(istdy)
        nstations = PSRCollectionElement_maxElements(lststations)               
        
#       Prealoca vetor para receber vazoes
#       ----------------------------------
        stationInflow = Array(Float64, nstations)                  

#       Cria objeto leitor das vazies foward e backward
#       -----------------------------------------------
	iinflow = PSRIOSDDPHydroForwardBackward_create(0)
	ret = PSRIOSDDPHydroForwardBackward_load(iinflow, istdy , string(PATH_BIN,"forw.psr"),
	                                                    string(PATH_BIN,"back.psr"))
	PSRIOSDDPHydroForwardBackward_mapTo(iinflow, stationInflow)     
	
	total_stages = PSRStudy_getNumberStages(istdy)
	total_simulations = PSRStudy_getNumberSimulations(istdy)
	total_opening = PSRStudy_getNumberOpenings(istdy)
        
        println(string("Total de estagios: ",total_stages))
        println(string("Total de cenarios: ",total_simulations))
        println(string("Total de aberturas: ",total_opening))
        println(string("Total de estacoes: ",nstations))
        
        file = open(string(PATH_BIN,"inflow_back.csv"), "w")
        write(file, "Stage, Scenario,Opening")
        for istation in 1:nstations
           station = PSRCollectionElement_element(lststations, istation-1)
           code = PSRGaugingStation_code(station)
           write(file, ", $code")
        end
        write(file,"\n")
        
        for  stage in 1:total_stages
          for scenario in 1:total_simulations
            for open in 1:total_opening
              write(file, "$stage, $scenario, $open")
              PSRIOSDDPHydroForwardBackward_setBackward(iinflow, stage, scenario, open)
              for istation in 1:nstations
                inflow = round(stationInflow[istation],5)
                write(file, ", $inflow")
              end            
              write(file,"\n")
            end
          end    
        end  

        close(file)
