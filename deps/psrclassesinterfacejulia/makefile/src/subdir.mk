################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables
CPP_SRCS += \
../src/PSRClasses.cpp \
$(PSRCLASSES_HOME)/parallel/PSRGrafResultCassandra.cpp \
$(PSRCLASSES_HOME)/parallel/CassandraInterface.cpp \
$(PSRCLASSES_HOME)/parallel/PSRGrafResultS3.cpp \
$(PSRCLASSES_HOME)/parallel/PSRGrafResultParallel.cpp \
$(EXTENSIONS_HOME)/SDDPExtension.cpp \
$(EXTENSIONS_HOME)/ModelExtension.cpp \
$(EXTENSIONS_HOME)/ExtensionsSystems.cpp \
$(EXTENSIONS_HOME)/ExtensionsPlants.cpp \
$(EXTENSIONS_HOME)/ExtensionsNetwork.cpp \
$(EXTENSIONS_HOME)/PSRCColElements.cpp \
$(EXTENSIONS_HOME)/PSRCConfig.cpp \
$(EXTENSIONS_HOME)/PSRCMap.cpp \
$(EXTENSIONS_HOME)/PSRCPreProcess.cpp \
$(EXTENSIONS_HOME)/SddpCoordinated.cpp \
$(EXTENSIONS_HOME)/ExtensionsGeneral.cpp


O_SRCS += \
../src/PSRClasses.o \
$(PSRCLASSES_HOME)/parallel/PSRGrafResultCassandra.o \
$(PSRCLASSES_HOME)/parallel/CassandraInterface.o \
$(PSRCLASSES_HOME)/parallel/PSRGrafResultS3.o \
$(PSRCLASSES_HOME)/parallel/PSRGrafResultParallel.o \
$(EXTENSIONS_HOME)/SDDPExtension.o \
$(EXTENSIONS_HOME)/ModelExtension.o \
$(EXTENSIONS_HOME)/ExtensionsSystems.o \
$(EXTENSIONS_HOME)/ExtensionsPlants.o \
$(EXTENSIONS_HOME)/ExtensionsNetwork.o \
$(EXTENSIONS_HOME)/PSRCColElements.o \
$(EXTENSIONS_HOME)/PSRCConfig.o \
$(EXTENSIONS_HOME)/PSRCMap.o \
$(EXTENSIONS_HOME)/PSRCPreProcess.o \
$(EXTENSIONS_HOME)/SddpCoordinated.o \
$(EXTENSIONS_HOME)/ExtensionsGeneral.o

OBJS += \
./src/PSRClasses.o \
$(PSRCLASSES_HOME)/parallel/PSRGrafResultCassandra.o \
$(PSRCLASSES_HOME)/parallel/CassandraInterface.o \
$(PSRCLASSES_HOME)/parallel/PSRGrafResultS3.o \
$(PSRCLASSES_HOME)/parallel/PSRGrafResultParallel.o \
$(EXTENSIONS_HOME)/SDDPExtension.o \
$(EXTENSIONS_HOME)/ModelExtension.o \
$(EXTENSIONS_HOME)/ExtensionsSystems.o \
$(EXTENSIONS_HOME)/ExtensionsPlants.o \
$(EXTENSIONS_HOME)/ExtensionsNetwork.o \
$(EXTENSIONS_HOME)/PSRCColElements.o \
$(EXTENSIONS_HOME)/PSRCConfig.o \
$(EXTENSIONS_HOME)/PSRCMap.o \
$(EXTENSIONS_HOME)/PSRCPreProcess.o \
$(EXTENSIONS_HOME)/SddpCoordinated.o \
$(EXTENSIONS_HOME)/ExtensionsGeneral.o

CPP_DEPS += \
./src/PSRClasses_mosel.d \
$(PSRCLASSES_HOME)/parallel/PSRGrafResultCassandra.d \
$(PSRCLASSES_HOME)/parallel/CassandraInterface.d \
$(PSRCLASSES_HOME)/parallel/PSRGrafResultS3.d \
$(PSRCLASSES_HOME)/parallel/PSRGrafResultParallel.d \
$(EXTENSIONS_HOME)/SDDPExtension.d \
$(EXTENSIONS_HOME)/ModelExtension.d \
$(EXTENSIONS_HOME)/ExtensionsSystems.d \
$(EXTENSIONS_HOME)/ExtensionsPlants.d \
$(EXTENSIONS_HOME)/ExtensionsNetwork.d \
$(EXTENSIONS_HOME)/PSRCColElements.d \
$(EXTENSIONS_HOME)/PSRCConfig.d \
$(EXTENSIONS_HOME)/PSRCMap.d \
$(EXTENSIONS_HOME)/PSRCPreProcess.d \
$(EXTENSIONS_HOME)/SddpCoordinated.d \
$(EXTENSIONS_HOME)/ExtensionsGeneral.d

# Each subdirectory must supply rules for building sources it contributes
$(PSRCLASSES_HOME)/parallel/%.o: $(PSRCLASSES_HOME)/parallel/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I /opt/mpich2/mpich2-3.2-intel16-x64/include -I /usr/include/dse/ -I"$(PSRCLASSES_HOME)" -I"$(EXTENSIONS_HOME)" -DJULIA -std=c++11 -O3 -w -c -fmessage-length=0 -fpermissive -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '
$(EXTENSIONS_HOME)/%.o: $(EXTENSIONS_HOME)/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"$(PSRCLASSES_HOME)" -I"$(EXTENSIONS_HOME)" -std=c++11 -O3 -w -c -fmessage-length=0 -DWITH_EXTENSIONS -fpermissive -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"$(PSRCLASSES_HOME)" -I"$(EXTENSIONS_HOME)" -I /usr/include/dse/ -std=c++11 -O3 -w -c -fmessage-length=0 -DWITH_EXTENSIONS -fpermissive -fPIC -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '
