# module Utilities

# using PSRClasses

function relation_map(lst_from::AbstractPSRCollectionElement,
    lst_to::AbstractPSRCollectionElement, TYPE::Integer)
    n_from = PSRCollectionElement_maxElements(lst_from)
    map = zeros(Int32, n_from)
    PSRCollectionElement_mapRelationShip(lst_from, lst_to, map, TYPE, false )
    return map
end
function complex_relation_map(lst_from::AbstractPSRCollectionElement,
    lst_to::AbstractPSRCollectionElement, TYPE::Integer)
    n_from = maxElements(lst_from)
    n_to = maxElements(lst_to)
    # elemnts of list TO which are connectes to list FROM
    # for instance:
    # 1) the (sub)set of thermals(TO) that belong to some contraint(FROM)
    # or
    # the (sub)set of constraints(TO) that contain some thermal(FROM)
    first   = Array{Int32}(undef, n_from)
    next    = Array{Int32}(undef, n_to*n_from)
    pointer = Array{Int32}(undef, n_to*n_from)
    mapComplexRelationShip(lst_from, lst_to, first, next, pointer, TYPE, false )
    # 1) array (of size constraints) of sets of thermals
    OUT = [zeros(Int32,0) for i in 1:n_from]
    for r in 1:n_from
        current_index = first[r]
        while ( current_index > 0 )
            push!(OUT[r], pointer[current_index] )
            current_index = next[current_index]
        end
    end
    return OUT
end
function revert_complex_relation_map(map::Vector{Vector{I}},
    outerdim_size::Integer) where {I<:Integer}
    OUT = Vector{Int32}[zeros(Int32,0) for i in 1:outerdim_size]
    for i in eachindex(map)
        for j in map[i]
            push!(OUT[j], Int32(i))
        end
    end
    return OUT
end
function reverse_complex_relation_map(lst_from::AbstractPSRCollectionElement,
    lst_to::AbstractPSRCollectionElement, TYPE::Integer)
    n_to = maxElements(lst_to)
    relation = complex_relation(lst_from, lst_to, TYPE)
    OUT = revert_complex_relation(map, n_to)
    return OUT
end

psr_type(::Type{Float64}) = PSR_PARM_REAL
psr_type(::Type{Int32}) = PSR_PARM_INTEGER
psr_type(::Type{String}) = PSR_PARM_STRING
function mapped_vector(mapper::AbstractPSRMapData, name::String,
    lst::AbstractPSRCollectionElement, ::Type{T},
    dim1::String="", dim2::String="";
    ignore::Bool=false)::Vector{T} where T <: Union{Int32, Float64}

    if isempty(dim1) && !isempty(dim2)
        error("It is not possible to have empty dimension one with non-empty dimension 2")
    end

    dimension = !isempty(dim1) + !isempty(dim2)

    n = maxElements(lst)
    out = zeros(T, n)

    if ignore || n == 0
        return out
    end

    # since it has more than 0 elements we can verify the type
    # by getting ther first element
    ptr = element(lst, 0)
    imodel = model(ptr)
    ivec = if dimension == 0
        vector2(imodel, name)
    elseif dimension == 1
        vector2(imodel, name*"(1)")
    elseif dimension == 2
        vector2(imodel, name*"(1,1)")
    end
    # check is attribute was found with proper dimension
    if !isnull(ivec)
        tp = getDataType(ivec)
        if tp != psr_type(T)
            error("Vector attribute $(name) is not of type $(T)")
        end
    else
        error("Failed to map Vector attribute $(name), with $(dimension) extra dimensions")
    end

    # map attribute into vector
    ret = if dimension == 0
        mapVector(mapper, name, out, 0)
    else
        mapDimensionedVector(mapper, name, out, 0, dim1, dim2)
    end
    if ret != length(out) # should be here by construction
        error("PSRClasses wrong length")
    end

    return out
end
function mapped_parm(mapper::AbstractPSRMapData, name::String,
    lst::AbstractPSRCollectionElement, ::Type{T},
    dim1::String="", dim2::String="";
    ignore::Bool=false)::Vector{T} where T <: Union{Int32, Float64}

    if isempty(dim1) && !isempty(dim2)
        error("It is not possible to have empty dimension one with non-empty dimension 2")
    end

    dimension = !isempty(dim1) + !isempty(dim2)

    n = maxElements(lst)
    out = zeros(T, n)

    if ignore || n == 0
        return out
    end

    # since it has more than 0 elements we can verify the type
    # by getting ther first element
    ptr = element(lst, 0)
    imodel = model(ptr)
    ivec = if dimension == 0
        parm2(imodel, name)
    elseif dimension == 1
        parm2(imodel, name*"(1)")
    elseif dimension == 2
        parm2(imodel, name*"(1,1)")
    end
    # check is attribute was found with proper dimension
    if !isnull(ivec)
        tp = getDataType(ivec)
        if tp != psr_type(T)
            error("Parm attribute $(name) is not of type $(T)")
        end
    elseif name != "code"
        error("Failed to map Parm attribute $(name), with $(dimension) extra dimensions")
    end

    # map attribute into Parm
    ret = if dimension == 0
        mapParm(mapper, name, out, 0)
    else
        mapDimensionedParm(mapper, name, out, 0, dim1, dim2)
    end
    if ret != length(out) # should be here by construction
        error("PSRClasses wrong length")
    end

    return out
end
function mapped_parm(mapper::AbstractPSRMapData, name::String,
    lst::AbstractPSRCollectionElement, ::Type{String}, strlen::Integer = 0;
    ignore::Bool=false)::String

    if strlen == 0
        error("Parameter strlen must be larger than zero")
    end

    n = maxElements(lst)
    pre = allocstring(n, strlen)

    if ignore || n == 0
        return pre
    end

    # since it has more than 0 elements we can verify the type
    # by getting ther first element
    ptr = element(lst, 0)
    imodel = model(ptr)
    ivec = parm2(imodel, name)
    # check is attribute was found with proper dimension
    if !isnull(ivec)
        tp = getDataType(ivec)
        if tp != psr_type(T)
            error("Parm attribute $(name) is not of type $(T)")
        end
    elseif name != "name" && name != "AVId"
        error("Failed to map Parm attribute $(name), not found in model of $(typeof(lst))")
    end

    # map attribute into vector
    ret = PSRMapData_mapParm(mapper, name, pre, strlen)
    if ret != n # should be here by construction
        error("PSRClasses wrong length")
    end

    return pre
end

function get_parm2(name::String,
    lst::AbstractPSRCollectionElement, ::Type{T},
    dim1::String="", dim2::String="";
    ignore::Bool=false) where T <: Union{Int32, Float64}

    static_map = PSRMapData()
    addElements(static_map, lst)

    out = mapped_parm(static_map, name, lst, T,
        dim1, dim2, ignore=ignore)

    pullToMemory(static_map)
    deleteInstance(static_map)
    return out
end

function get_parm2(name::String,
    lst::AbstractPSRCollectionElement, ::Type{T},
    strlen::Integer = 0;
    ignore::Bool=false) where T <: String

    static_map = PSRMapData()
    addElements(static_map, lst)

    pre = mapped_parm(static_map, name, lst, T,
        strlen, ignore=ignore)

    pullToMemory(static_map)

    out = splitstring(pre, strlen)

    deleteInstance(static_map)
    return out
end

function empty_parm(name::String, lst::AbstractPSRCollectionElement)
    n = maxElements(lst)
    out = zeros(Bool, n)
    println(name)
    for i in 1:n
        println(i)
        ptr = element(lst, i-1)
        isnull(ptr) && continue
        imodel = model(ptr)
        isnull(imodel) && continue
        parm_ptr = parm2(imodel, name)
        isnull(parm_ptr) && continue
        out[i] = !noParm(parm_ptr)
        println(i)
    end
    return out
end

function get_parms(name::String, lst::AbstractPSRCollectionElement,
    ::Type{T};
    check_type::Bool = true,
    check_parm::Bool = true,
    ignore::Bool = false,
    default::T = zero(T)
) where T <: Union{Float64, Int32}
    n = maxElements(lst)
    out = fill(default, n)
    if ignore || n == 0
        return out
    end
    if check_type
        imodel = element(lst, 0) |> model
        parm_ptr = parm2(imodel, name)
        if isnull(parm_ptr)
            if check_parm
                error("Failed to find parm attribute $(name)")
            else
                return out
            end
        else
            tp = getDataType(parm_ptr)
            if tp != psr_type(T)
                error("Parm attribute $(name) is not of type $(T)")
            end
        end
    end
    for i in 1:n
        ptr = element(lst, i-1)
        isnull(ptr) && continue
        imodel = model(ptr)
        isnull(imodel) && continue
        parm_ptr = parm2(imodel, name)
        isnull(parm_ptr) && continue
        if !noParm(parm_ptr)
            out[i] = _get_parm(parm_ptr, T)
        # else keep default
        end
    end
    return out
end
_get_parm(ptr, ::Type{Int32}) = getInteger(ptr)
_get_parm(ptr, ::Type{Float64}) = getReal(ptr)


# end