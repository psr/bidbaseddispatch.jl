__precompile__(true)

module PSRClasses

import Libdl

include(joinpath(".","..","PSRClasses.jl"))
include(joinpath(".","..","PSRClassesOO.jl"))

const _EXCLUDE_SYMBOLS = [Symbol(@__MODULE__), :eval, :include, :error]

const _PSRCLASSES_BASE_PATH = joinpath(@__DIR__, "..", "PSRClasses." * Libdl.dlext)
const PSRCLASSES_PATH_EX = joinpath(@__DIR__, "..", "examples")

function initialize(;path = _PSRCLASSES_BASE_PATH)
    Libdl.dlopen(path)
end

for sym in names(@__MODULE__, all=true)
    sym_string = string(sym)
    if sym in _EXCLUDE_SYMBOLS || startswith(sym_string, "_") ||
        startswith(sym_string, "initialize")
        continue
    end
    if !(Base.isidentifier(sym) || (startswith(sym_string, "@") &&
         Base.isidentifier(sym_string[2:end])))
       continue
    end
    @eval export $sym
end

include("Utilities/Utilities.jl")

end