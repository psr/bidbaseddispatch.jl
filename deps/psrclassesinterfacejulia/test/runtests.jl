using Test, PSRClasses

PSRClasses.initialize()

# VERBOSE = false # TODO FIX THIS

ENV["PSRC_JL_TESTING"] = 1
@test ENV["PSRC_JL_TESTING"] == "1"


# if !VERBOSE
#     orig = stdout
#     redirect_stdout()
# end
@testset "PSRClasses.jl" begin
    @testset "psrclasses_example1" begin
        include("..\\examples\\psrclasses_example1.jl")
    end
    @testset "psrclasses_example1_oo" begin
        include("..\\interface\\psrclasses_example1_oo.jl")
    end
    @testset "psrclasses_example2" begin
        include("..\\examples\\psrclasses_example2.jl")
    end
    @testset "psrclasses_example3" begin
        include("..\\examples\\psrclasses_example3.jl")
    end
    @testset "psrclasses_example4" begin
        include("..\\examples\\psrclasses_example4.jl")
    end
    @testset "psrclasses_example5" begin
        include("..\\examples\\psrclasses_example5.jl")
    end
    @testset "psrclasses_example6" begin
        include("..\\examples\\psrclasses_example6.jl")
    end
    @testset "psrclasses_example7" begin
        include("..\\examples\\psrclasses_example7.jl")
    end
    @testset "psrclasses_example8" begin
        include("..\\examples\\psrclasses_example8.jl")
    end
    #@testset "Basic" begin
    #    include("..\\examples\\psrclasses_example9.jl")
    #end
    @testset "psrclasses_example10" begin
        include("..\\examples\\psrclasses_example10.jl")
    end
    @testset "psrclasses_example11" begin
        include("..\\examples\\psrclasses_example11.jl")
    end
    @testset "psrclasses_example12" begin
        include("..\\examples\\psrclasses_example12.jl")
    end
    @testset "psrclasses_example13" begin
        include("..\\examples\\psrclasses_example13.jl")
    end
    @testset "psrclasses_example14" begin
        include("..\\examples\\psrclasses_example14.jl")
    end
    @testset "psrclasses_example15" begin
        include("..\\examples\\psrclasses_example15.jl")
    end
    @testset "psrclasses_example16" begin
        include("..\\examples\\psrclasses_example16.jl")
    end
    @testset "psrclasses_example17" begin
        include("..\\examples\\psrclasses_example17.jl")
    end
    @testset "psrclasses_example18" begin
        include("..\\examples\\psrclasses_example18.jl")
    end
    @testset "psrclasses_example22" begin
        include("..\\examples\\psrclasses_example22.jl")
    end
    #@testset "Basic" begin
    #    include("..\\examples\\psrclasses_example23.jl")
    #end
    @testset "psrclasses_example24" begin
        include("..\\examples\\psrclasses_example24.jl")
    end
    @testset "psrclasses_example25" begin
        include("..\\examples\\psrclasses_example25.jl")
    end
end
# if !VERBOSE
#     redirect_stdout(orig)
# end

ENV["PSRC_JL_TESTING"] = 0
@test ENV["PSRC_JL_TESTING"] == "0"
