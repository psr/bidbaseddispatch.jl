# PSRClasses.jl

Can be added by:

```julia
] add https://bitbucket.org/psr/psrclassesinterfacejulia
```

If you need extensions use:

```julia
] add https://bitbucket.org/psr/psrclassesinterfacejulia#extensions
```