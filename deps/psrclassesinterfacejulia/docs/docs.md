# Docs para PSRClasses

## Tutorial

https://sites.google.com/a/psr-inc.com/intranet/cursos-da-psr/2020

## Exemplos

ver pasta exemplos

## Error comuns

- no linux é preciso adicionar .so no LD_LIBRARY_PATH, incluindo java

Exemplo:

```bash

export LD_LIBRARY_PATH=/usr/lib/jvm/jre/bin/:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/usr/lib/jvm/jre/lib/amd64/server/:$LD_LIBRARY_PATH
```

ou

```bash
export LD_LIBRARY_PATH=/usr/lib/jvm/java-1.7.0-openjdk-1.7.0.261.x86_64/jre/lib/amd64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/usr/lib/jvm/java-1.7.0-openjdk-1.7.0.261.x86_64/jre/lib/amd64/server:$LD_LIBRARY_PATH
```