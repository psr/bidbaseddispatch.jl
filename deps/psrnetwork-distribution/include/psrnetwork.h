#pragma once  

#ifndef PSRNETWORK_H
#define PSRNETWORK_H

#include <cstdint>

#ifdef WIN32
#ifdef PSRNETWORK_EXPORTS  
#define PSRNETWORK_API __declspec(dllexport)   
#else  
#define PSRNETWORK_API __declspec(dllimport)   
#endif 
#else
#define PSRNETWORK_API
#endif 

#ifdef WIN32
#define FORTRAN_POINTER __int64   
#else
#define FORTRAN_POINTER int64_t
#endif 

#ifndef PSRNETWORK_VIOLATIONS_NONE
#define PSRNETWORK_VIOLATIONS_NONE -1
#endif                      
#ifndef PSRNETWORK_VIOLATIONS_ALL
#define PSRNETWORK_VIOLATIONS_ALL 0
#endif                       
#ifndef PSRNETWORK_VIOLATIONS_MONITORED
#define PSRNETWORK_VIOLATIONS_MONITORED 1
#endif                      
#ifndef PSRNETWORK_VIOLATIONS_MONITORED_AND_TIELINES
#define PSRNETWORK_VIOLATIONS_MONITORED_AND_TIELINES 2
#endif                                             
#ifndef PSRNETWORK_VIOLATIONS_TIELINES
#define PSRNETWORK_VIOLATIONS_TIELINES 3
#endif  

extern "C" {
    // INITIALIZE PSRCLASSES
    PSRNETWORK_API void PSRNetwork_initializePSRClasses(const int psrclasses_verbose, const char *masks_path, const char *model_path);
    PSRNETWORK_API void PSRNETWORK_INITIALIZEPSRCLASSES(const int *psrclasses_verbose, const char *masks_path, const char *model_path);

    // LOAD STUDY
    PSRNETWORK_API void* PSRNetworkSDDP_loadStudy(const char* path);
    PSRNETWORK_API void* PSRNETWORKSDDP_LOADSTUDY(const char* path);
    PSRNETWORK_API void* PSRNetworkNETPLAN_loadStudy(const char* path, int use_candidates);

    // CONSTRUCTORS 
    PSRNETWORK_API void* PSRNetwork_create(void *study, int database, int verbose, int solver, int buses_order_settings);
    PSRNETWORK_API void* PSRNETWORK_CREATE(FORTRAN_POINTER &study, int* database, int *verbose, int *solver, int *buses_order_settings);

    // DESTRUCTOR
    PSRNETWORK_API void PSRNetwork_delete(void* psrnetwork);

    // CONFIG
    PSRNETWORK_API void PSRNetwork_setMinReactance(void *psrnetwork, double min_reactance);
    PSRNETWORK_API void PSRNetwork_considerOptimizedCircuits(void* psrnetwork, int consider_optimized_circuits);

    /* CIRCUITS */
    // GET CIRCUITS SIZE
    PSRNETWORK_API void PSRNetwork_getCircuitsSize(void *psrnetwork, int *out_circuits_size);
    // GET CIRCUITS CODES
    PSRNETWORK_API void PSRNetwork_getCircuitsCodes(void *psrnetwork, int circuits_size, int *out_circuits_codes);
    // GET CIRCUITS NAMES
    PSRNETWORK_API void PSRNetwork_getCircuitsNames(void *psrnetwork, int circuits_size, int buffer_size, char **out_circuits_names);
    // SET SELECTED CIRCUITS
    PSRNETWORK_API void PSRNetwork_setSelectedCircuits(void *psrnetwork, int selected_size, int *selected);
    // CLEAR SELECTED CIRCUITS
    PSRNETWORK_API void PSRNetwork_clearSelectedCircuits(void *psrnetwork);
    // GET CIRCUIT MAX FLOW
    PSRNETWORK_API double PSRNetwork_getCircuitMaxFlow(void *psrnetwork, int circuit_index);
    // GET CIRCUIT FACTS FLAG
    PSRNETWORK_API int PSRNetwork_getCircuitFACTSFlag(void *psrnetwork, int circuit_index);
    // GET CIRCUIT FACTS XMIN
    PSRNETWORK_API double PSRNetwork_getCircuitFACTSXmin(void *psrnetwork, int circuit_index);
    // GET CIRCUIT FACTS XMAX
    PSRNETWORK_API double PSRNetwork_getCircuitFACTSXmax(void *psrnetwork, int circuit_index);
    // GET CIRCUIT FLOW LOSSES FLAG
    PSRNETWORK_API int PSRNetwork_getCircuitLossesFlag(void *psrnetwork, int circuit_index);
    // GET CIRCUIT RESISTANCE
    PSRNETWORK_API double PSRNetwork_getCircuitResistance(void *psrnetwork, int circuit_index);
    // GET CIRCUIT SUSCEPTANCE
    PSRNETWORK_API double PSRNetwork_getCircuitSusceptance(void *psrnetwork, int circuit_index);

    /* BUSES */
    // GET BUSES SIZE
    PSRNETWORK_API void PSRNetwork_getBusesSize(void *psrnetwork, int *out_buses_size);
    // GET BUSES CODES
    PSRNETWORK_API void PSRNetwork_getBusesCodes(void *psrnetwork, int buses_size, int *out_buses_codes);
    // GET BUSES NAMES
    PSRNETWORK_API void PSRNetwork_getBusesNames(void *psrnetwork, int buses_size, int buffer_size, char **out_buses_names);

    // RESTRICT REDUNDANT CIRCUITS
    PSRNETWORK_API void PSRNetwork_restrictRedundantCircuits(void *psrnetwork, int block, int buses_size, int *buses_with_generation_or_demand);

    /* B MATRIX */
    // UPDATE STAGE
    PSRNETWORK_API void PSRNetworkSDDP_updateStage(void *psrnetwork, int stage);
    PSRNETWORK_API void PSRNETWORKSDDP_UPDATESTAGE(FORTRAN_POINTER &psrnetwork, int *stage);
    // UPDATE STAGE
    PSRNETWORK_API void PSRNetworkNETPLAN_updateStage(void *psrnetwork, int stage, int block);
    // GET B TRIPLETS SIZE
    PSRNETWORK_API void PSRNetwork_getBTripletsSize(void *psrnetwork, int *out_size);
    // GET B TRIPLETS
    PSRNETWORK_API void PSRNetwork_getBTriplets(void *psrnetwork, int size, int *out_rows, int *out_columns, double *out_values);
    // GET CONNECTED COMPONENTS
    PSRNETWORK_API void PSRNetwork_getConnectedComponents(void *psrnetwork, int connected_components_size, int *out_connected_components);
    // GET BRIDGES
    PSRNETWORK_API void PSRNetwork_getBridges(void *psrnetwork, int bridges_size, int *out_bridges);

    /* SLACKS */
    // GET SLACKS SIZE
    PSRNETWORK_API void PSRNetwork_getSlacksSize(void *psrnetwork, int *out_slacks_size);
    // GET SLACKS
    PSRNETWORK_API void PSRNetwork_getSlacks(void *psrnetwork, int slacks_size, int *out_slacks);

    // FLOW LOSSES
    PSRNETWORK_API void PSRNetwork_setLosses(void *psrnetwork, int block, int losses_size, double *losses);
    // DC LINK FLOWS
    PSRNETWORK_API void PSRNetwork_setDCLinksFlows(void *psrnetwork, int dclinks_flows_size, double *dclinks_flows);
    // PHASE SHIFTER
    PSRNETWORK_API void PSRNetwork_setPhaseShifter(void *psrnetwork, int phase_shifter_size, double *phase_shifter);
    // SOLVE B
    PSRNETWORK_API int PSRNetwork_solveB(void *psrnetwork, int block, int P_size, double *P);
    PSRNETWORK_API int PSRNETWORK_SOLVEB(FORTRAN_POINTER &psrnetwork, int *block, int *P_size, double *P);

    // SOLVE LOSSES 
    PSRNETWORK_API void PSRNetwork_solveLosses(void *psrnetwork, int block, int losses_size, double *out_losses);

    // GET THETAS
    PSRNETWORK_API void PSRNetwork_getThetas(void *psrnetwork, int block, int thetas_size, double *out_thetas);
    // GET FLOWS
    PSRNETWORK_API void PSRNetwork_getFlows(void *psrnetwork, int block, int flows_size, double *out_flows);
    // GET BETA
    PSRNETWORK_API void PSRNetwork_getBeta(void *psrnetwork, int circuit_index, int beta_size, double *out_beta);
    // GET BETA WITHOUT SLACKS
    PSRNETWORK_API void PSRNetwork_getBetaWithoutSlacks(void *psrnetwork, int circuit_index, int beta_size, double *out_beta);

    // GET ETA
    PSRNETWORK_API void PSRNetwork_getEta(void *psrnetwork, int circuit_index, int eta_size, double *out_eta);
    // GET ETA WITHOUT SLACKS
    PSRNETWORK_API void PSRNetwork_getEtaWithoutSlacks(void *psrnetwork, int circuit_index, int eta_size, double *out_eta);

    // RESET VIOLATIONS
    PSRNETWORK_API void PSRNetwork_resetViolations(void *psrnetwork);
    // IGNORE CIRCUITS VIOLATIONS
    PSRNETWORK_API void PSRNetwork_ignoreCircuitsViolations(void *psrnetwork, int block, int size, int *circuits);
    // SET ALL SUM CIRCUITS VIOLATIONS MODE
    PSRNETWORK_API void PSRNetwork_setAllSumCircuitsViolationsMode(void *psrnetwork);

    // SET MAX VIOLATIONS
    PSRNETWORK_API void PSRNetwork_setMaxViolations(void *psrnetwork, int max_violations, int max_contingency_violations, int max_sum_circuits_violations);
    PSRNETWORK_API void PSRNETWORK_SETMAXVIOLATIONS(FORTRAN_POINTER &psrnetwork, int *max_violations, int *max_contingency_violations, int *max_sum_circuits_violations);
    // SET VIOLATIONS SETTINGS
    PSRNETWORK_API void PSRNetwork_setViolationsSettings(void *psrnetwork, int settings);
    PSRNETWORK_API void PSRNETWORK_SETVIOLATIONSSETTINGS(FORTRAN_POINTER &psrnetwork, int *settings);
    // GET VIOLATIONS SIZE
    PSRNETWORK_API int PSRNetwork_getViolationsSize(void *psrnetwork, int block);
    PSRNETWORK_API int PSRNETWORK_GETVIOLATIONSSIZE(FORTRAN_POINTER &psrnetwork, int *block);
    // GET VIOLATION BETA
    PSRNETWORK_API void PSRNetwork_getViolationBeta(void *psrnetwork, int block, int i, int beta_size, double *out_beta, int *out_sense, int *out_circuit_index, double *out_circuit_capacity);
    PSRNETWORK_API void PSRNETWORK_GETVIOLATIONBETA(FORTRAN_POINTER &psrnetwork, int *block, int *i, int *beta_size, double *out_beta, int *out_sense, int *out_circuit_index, double *out_circuit_capacity);

    // SET SUM CIRCUITS VIOLATIONS SETTINGS
    PSRNETWORK_API void PSRNetwork_setSumCircuitsViolationsSettings(void *psrnetwork, int settings);
    // GET SUM CIRCUITS VIOLATIONS SIZE
    PSRNETWORK_API void PSRNetwork_getSumCircuitsViolationsSize(void *psrnetwork, int block, int *out_violations_size);
    // GET SUM CIRCUITS VIOLATION BETA
    PSRNETWORK_API void PSRNetwork_getSumCircuitsViolationBeta(void *psrnetwork, int block, int i, int beta_size, double *out_beta, int *out_sense, int *out_constraint_index, int *out_constraint_lowerbound_noparm, double *out_constraint_lowerbound, int *out_constraint_upperbound_noparm, double *out_constraint_upperbound);
    // GET SUM CIRCUITS VIOLATION FLOW
    PSRNETWORK_API void PSRNetwork_getSumCircuitsViolationFlow(void *psrnetwork, int block, int i, double *out_flow);

    // SET CONTINGENCY VIOLATIONS SETTINGS
    PSRNETWORK_API void PSRNetwork_setContingencyViolationsSettings(void *psrnetwork, int settings, int consider_bridges);
    PSRNETWORK_API void PSRNETWORK_SETCONTINGENCYVIOLATIONSSETTINGS(FORTRAN_POINTER &psrnetwork, int *settings, int *consider_bridges);
    // GET CONTINGENCY VIOLATIONS SIZE
    PSRNETWORK_API void PSRNetwork_getContingencyViolationsSize(void *psrnetwork, int block, int *out_violations_size);
    // GET CONTINGENCY VIOLATION BETA
    PSRNETWORK_API void PSRNetwork_getContingencyViolationBeta(void *psrnetwork, int block, int i, int beta_size, double *out_beta, int *out_sense, int *out_removed_circuit_index, int *out_circuit_index, double *out_circuit_capacity);
    // GET CONTINGENCY VIOLATION FLOW
    PSRNETWORK_API void PSRNetwork_getContingencyViolationFlow(void *psrnetwork, int block, int i, double *out_removed_circuit_flow);
    // GET CONTINGENCY VIOLATION ANALYSED
    PSRNETWORK_API void PSRNetwork_getContingencyViolationAnalysed(void* psrnetwork, int circuits_size, int* out_analysed);

    // COMPUTE SUM CIRCUITS FLOWS
    PSRNETWORK_API void PSRNetwork_computeSumCircuitsFlows(void *psrnetwork, const int block, const int sumcircuits_flows_size, double *out_sumcircuits_flows);
    // COMPUTE CONTINGENCY FLOWS
    PSRNETWORK_API void PSRNetwork_computeContingencyFlows(void *psrnetwork, int block, int removed_circuit_index, int contingency_flows_size, double *out_contingency_flows);
    // COMPUTE CONTINGENCY BETA
    PSRNETWORK_API void PSRNetwork_computeContingencyBeta(void *psrnetwork, int removed_circuit_index, int circuit_index, int beta_size, double *out_beta);

    // PRINT BENCHMARK
    PSRNETWORK_API void PSRNetwork_printBenchmark();
}

#endif // PSRNETWORK_H