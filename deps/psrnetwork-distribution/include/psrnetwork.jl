module PSRNetwork
    using Libdl

    @eval macro $(Symbol("ccall"))(expr)
        expr.head == :(::) && expr.args[1].head == :call || error("Invalid use of @ccall")
        rettype = expr.args[2]
        fname = expr.args[1].args[1]
        cargs = expr.args[1].args[2:end]

        arglist = []
        typlist = []
        tupexpr = :(())
        ccexpr = :(ccall($(esc(fname)), $(esc(rettype)), $(esc(tupexpr))))
        for carg in cargs
            carg.head == :(::) ||  error("Invalid use of @ccall")
            push!(ccexpr.args, :($(esc(carg.args[1]))))
            push!(tupexpr.args, carg.args[2])
        end
        ccexpr
    end

    @static if VERSION < v"0.7"
        const Nothing = Void
    end

    export
        initialize,
        initialize_PSRClasses,
        load_study_SDDP,
        load_study_NETPLAN,
        default,
        get_circuits_size,
        get_circuits_codes!,
        get_circuits_codes,
        get_buses_size,
        get_buses_codes!,
        get_buses_codes,
        update_stage,
        get_connected_components!,
        get_bridges!,
        get_B_triplets_size,
        get_B_triplets!,
        solve_B,
        solve_losses!,
        set_selected_circuits,
        clear_selected_circuits,
        reset_violations,
        set_max_violations,
        set_violations_settings,
        set_sumcircuits_violations_settings,
        set_contingency_violations_settings,
        get_violations_size,
        get_violation_beta!,
        get_sumcircuits_violations_size,
        get_sumcircuits_violation_beta!,
        get_sumcircuits_violation_flow,
        get_contingency_violations_size,
        get_contingency_violation_beta!,
        get_contingency_violation_flow,
        get_contingency_violation_analysed!,
        get_flows!,
        get_thetas!,
        get_beta!,
        get_eta!,
        compute_sumcircuits_flows!,
        compute_contingency_flows!,
        compute_contingency_beta!,
        get_slacks_size,
        get_slacks!,
        get_maxflow,
        get_FACTS_flag,
        get_FACTS_xmin,
        get_FACTS_xmax,
        restrict_redundant_circuits,
        ignore_circuits_violations,
        get_circuit_resistance,
        get_circuit_losses_flag,
        set_losses!,
		set_min_reactance!,
        set_all_sumcircuits_violations_mode

    struct Pointer
        pointer::Ptr{UInt8}

        function Pointer(pointer::Ptr{UInt8})
            return new(pointer)
        end

        function Pointer()
            return new(C_NULL)
        end
    end

    const VIOLATIONS_NONE = -1
    const VIOLATIONS_ALL = 0
    const VIOLATIONS_MONITORED = 1
    const VIOLATIONS_MONITORED_AND_TIELINES = 2
    const VIOLATIONS_TIELINES = 3

    const LESS_THAN = -1
    const GREATER_THAN = +1

    const Database_SDDP = 0
    const Database_NETPLAN = 1

    function initialize(path::AbstractString)::Nothing
        Libdl.dlopen(joinpath(path, "PSRNetwork"))
        return nothing
    end

    function initialize_PSRClasses(verbose::Integer, masks_path::AbstractString, model_path::AbstractString)::Nothing
        @ccall (:PSRNetwork_initializePSRClasses,:PSRNetwork)(verbose::Int32, masks_path::Ptr{UInt8}, model_path::Ptr{UInt8})::Nothing
        return nothing
    end

    function load_study_SDDP(path::AbstractString)::Ptr{UInt8}
        return @ccall (:PSRNetworkSDDP_loadStudy,:PSRNetwork)(path::Ptr{UInt8})::Ptr{UInt8}
    end

    function load_study_NETPLAN(path::AbstractString, use_candidates::Bool)::Ptr{UInt8}
        return ccall((:PSRNetworkNETPLAN_loadStudy, "PSRNetwork"), Ptr{UInt8}, (Ptr{UInt8}, Int32), path, use_candidates ? 1 : 0)
    end

    function default(study::Ptr{UInt8}, database::Integer, verbose::Integer, solver::Integer, bus_order_settings::Integer=0)::Pointer
        pointer = @ccall (:PSRNetwork_create,:PSRNetwork)(study::Ptr{UInt8}, database::Int32, verbose::Int32, solver::Int32, bus_order_settings::Int32)::Ptr{UInt8}
        return Pointer(pointer)
    end

    function get_circuits_size(psrnetwork::Pointer)::Int32
        out_circuits_size = zeros(Int32, 1)
        ccall((:PSRNetwork_getCircuitsSize, "PSRNetwork"), Nothing, (Ptr{UInt8}, Ptr{UInt8}), psrnetwork.pointer, out_circuits_size)
        return out_circuits_size[1]
    end

    function get_circuits_codes!(psrnetwork::Pointer, out_circuits_codes::Vector{Int32})::Nothing
        circuits_size = length(out_circuits_codes)
        ccall((:PSRNetwork_getCircuitsCodes, "PSRNetwork"), Nothing, (Ptr{UInt8}, Int32, Ptr{UInt8}), psrnetwork.pointer, Int32(circuits_size), out_circuits_codes)
        return nothing
    end

    function get_circuits_codes(psrnetwork::Pointer)::Vector
        out_circuits_codes = zeros(Int32, PSRNetwork.get_circuits_size(psrnetwork))
        get_circuits_codes!(psrnetwork, out_circuits_codes)
        return out_circuits_codes
    end

    function get_buses_size(psrnetwork::Pointer)::Int32
        out_buses_size = zeros(Int32, 1)
        ccall((:PSRNetwork_getBusesSize, "PSRNetwork"), Nothing, (Ptr{UInt8}, Ptr{UInt8}), psrnetwork.pointer, out_buses_size)
        return out_buses_size[1]
    end

    function get_buses_codes!(psrnetwork::Pointer, out_buses_codes::Vector{Int32})::Nothing
        buses_size = length(out_buses_codes)
        ccall((:PSRNetwork_getBusesCodes, "PSRNetwork"), Nothing, (Ptr{UInt8}, Int32, Ptr{UInt8}), psrnetwork.pointer, Int32(buses_size), out_buses_codes)
        return nothing
    end

    function get_buses_codes(psrnetwork::Pointer)::Vector
        out_buses_codes = zeros(Int32, PSRNetwork.get_buses_size(psrnetwork))
        get_buses_codes!(psrnetwork, out_buses_codes)
        return out_buses_codes
    end

    function update_stage(psrnetwork::Pointer, stage::Integer)::Nothing
        @ccall (:PSRNetworkSDDP_updateStage,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, stage::Int32)::Nothing
        return nothing
    end

    function update_stage(psrnetwork::Pointer, stage::Integer, block::Integer)::Nothing
        @ccall (:PSRNetworkNETPLAN_updateStage,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, stage::Int32, block::Int32)::Nothing
        return nothing
    end

    function get_connected_components!(psrnetwork::Pointer, out_connected_components::Vector)::Nothing
        @ccall (:PSRNetwork_getConnectedComponents,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, length(out_connected_components)::Int32, out_connected_components::Ptr{UInt8})::Nothing
        return nothing
    end

    function get_connected_components(psrnetwork::Pointer)::Vector
        out_connected_components = zeros(Int32, PSRNetwork.get_buses_size(psrnetwork))
        @ccall (:PSRNetwork_getConnectedComponents,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, length(out_connected_components)::Int32, out_connected_components::Ptr{UInt8})::Nothing
        return out_connected_components
    end

    function get_bridges!(psrnetwork::Pointer, out_bridges::Vector{Int32})::Nothing
        @ccall (:PSRNetwork_getBridges,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, length(out_bridges)::Int32, out_bridges::Ptr{UInt8})::Nothing
        return nothing
    end

    function get_B_triplets_size(psrnetwork::Pointer)::Integer
        out_size = zeros(Int32, 1)
        @ccall (:PSRNetwork_getBTripletsSize,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, out_size::Ptr{UInt8})::Nothing
        return out_size[1]
    end

    function get_B_triplets!(psrnetwork::Pointer, out_row::Vector, out_column::Vector, out_value::Vector)::Nothing
        @assert length(out_value) == length(out_row)
        @assert length(out_value) == length(out_column)
        @ccall (:PSRNetwork_getBTriplets,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, length(out_value)::Int32, out_row::Ptr{UInt8}, out_column::Ptr{UInt8}, out_value::Ptr{UInt8})::Nothing
        return nothing
    end

    function set_dclinks_flows(psrnetwork::Pointer, dclinks_flows::Vector{Float64})::Nothing
        dclinks_flows_size = length(dclinks_flows)
        @ccall (:PSRNetwork_setDCLinksFlows,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, dclinks_flows_size::Int32, dclinks_flows::Ptr{UInt8})::Nothing
        return nothing
    end

    function set_phase_shifter(psrnetwork::Pointer, phase_shifter::Vector{Float64})::Nothing
        phase_shifter_size = length(phase_shifter)
        @ccall (:PSRNetwork_setPhaseShifter,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, phase_shifter_size::Int32, phase_shifter::Ptr{UInt8})::Nothing
        return nothing
    end

    function solve_B(psrnetwork::Pointer, block::Integer, P::Vector)::Int32
        P_size = length(P)
        return @ccall (:PSRNetwork_solveB,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, block::Int32, P_size::Int32, P::Ptr{UInt8})::Int32
    end

    function solve_losses!(psrnetwork::Pointer, block::Integer, out_loses::Vector)::Nothing
        @ccall (:PSRNetwork_solveLosses,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, block::Int32, length(out_loses)::Int32, out_loses::Ptr{UInt8})::Nothing
        return nothing
    end

    function set_selected_circuits(psrnetwork::Pointer, selected::Vector)::Nothing
        selected_size = length(selected)
        ccall((:PSRNetwork_setSelectedCircuits, "PSRNetwork"), Nothing, (Ptr{UInt8}, Int32, Ptr{UInt8}), psrnetwork.pointer, Int32(selected_size), selected)
        return nothing
    end

    function clear_selected_circuits(psrnetwork::Pointer)::Nothing
        ccall((:PSRNetwork_clearSelectedCircuits, "PSRNetwork"), Nothing, (Ptr{UInt8},), psrnetwork.pointer)
        return nothing
    end

    function reset_violations(psrnetwork::Pointer)::Nothing
        @ccall (:PSRNetwork_resetViolations,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8})::Nothing
        return nothing
    end

    function set_max_violations(psrnetwork::Pointer, max_violations::Integer, max_contingency_violations::Integer, max_sumcircuits_violations::Integer)::Nothing
        @ccall (:PSRNetwork_setMaxViolations,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, max_violations::Int32, max_contingency_violations::Int32, max_sumcircuits_violations::Int32)::Nothing
        return nothing
    end

    function set_violations_settings(psrnetwork::Pointer, settings::Integer)::Nothing
        @ccall (:PSRNetwork_setViolationsSettings,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, settings::Int32)::Nothing
        return nothing
    end

    function set_sumcircuits_violations_settings(psrnetwork::Pointer, settings::Integer)::Nothing    
        @ccall (:PSRNetwork_setSumCircuitsViolationsSettings,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, settings::Int32)::Nothing
        return nothing
    end

    function set_contingency_violations_settings(psrnetwork::Pointer, settings::Integer, consider_bridges::Bool)::Nothing
        @ccall (:PSRNetwork_setContingencyViolationsSettings,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, settings::Int32, (consider_bridges ? 1 : 0)::Int32)::Nothing
        return nothing
    end

    function get_violations_size(psrnetwork::Pointer, block::Integer)::Integer
        return @ccall (:PSRNetwork_getViolationsSize,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, block::Int32)::Int32
    end

    function get_violation_beta!(psrnetwork::Pointer, block::Integer, i::Integer, out_beta::Vector)
        out_sense = zeros(Int32, 1)
        out_circuit_index = zeros(Int32, 1)
        out_circuit_capacity = zeros(Float64, 1)

        @ccall (:PSRNetwork_getViolationBeta,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, block::Int32, i::Int32, length(out_beta)::Int32, out_beta::Ptr{UInt8}, out_sense::Ptr{UInt8}, out_circuit_index::Ptr{UInt8}, out_circuit_capacity::Ptr{UInt8})::Int32
        return out_sense[1], out_circuit_index[1], out_circuit_capacity[1]
    end

    function get_sumcircuits_violations_size(psrnetwork::Pointer, block::Integer)::Integer
        out_violations_size = zeros(Int32, 1)
        @ccall (:PSRNetwork_getSumCircuitsViolationsSize,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, block::Int32, out_violations_size::Ptr{UInt8})::Nothing
        return out_violations_size[1]
    end

    function get_sumcircuits_violation_beta!(psrnetwork::Pointer, block::Integer, i::Integer, out_beta::Vector)
        out_sense = zeros(Int32, 1)
        out_constraint_index = zeros(Int32, 1)
        out_constraint_lowerbound_noparm = zeros(Int32, 1)
        out_constraint_lowerbound = zeros(Float64, 1)
        out_constraint_upperbound_noparm = zeros(Int32, 1)
        out_constraint_upperbound = zeros(Float64, 1)

        @ccall (:PSRNetwork_getSumCircuitsViolationBeta,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, block::Int32, i::Int32, length(out_beta)::Int32, out_beta::Ptr{UInt8}, out_sense::Ptr{UInt8}, out_constraint_index::Ptr{UInt8}, out_constraint_lowerbound_noparm::Ptr{UInt8}, out_constraint_lowerbound::Ptr{UInt8}, out_constraint_upperbound_noparm::Ptr{UInt8}, out_constraint_upperbound::Ptr{UInt8})::Int32
        return out_sense[1], out_constraint_index[1], out_constraint_lowerbound_noparm[1] == 1, out_constraint_lowerbound[1], out_constraint_upperbound_noparm[1] == 1, out_constraint_upperbound[1]
    end

    function get_sumcircuits_violation_flow(psrnetwork::Pointer, block::Integer, i::Integer)::Float64
        out_flow = zeros(Float64, 1)
        ccall((:PSRNetwork_getSumCircuitsViolationFlow, "PSRNetwork"), Nothing, (Ptr{UInt8}, Int32, Int32, Ptr{UInt8}), psrnetwork.pointer, Int32(block), Int32(i), out_flow)
        return out_flow[1]
    end

    function get_contingency_violations_size(psrnetwork::Pointer, block::Integer)::Integer
        out_violations_size = zeros(Int32, 1)
        @ccall (:PSRNetwork_getContingencyViolationsSize,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, block::Int32, out_violations_size::Ptr{UInt8})::Nothing
        return out_violations_size[1]
    end

    function get_contingency_violation_beta!(psrnetwork::Pointer, block::Integer, i::Integer, out_beta::Vector)
        out_sense = zeros(Int32, 1)
        out_circuit_index = zeros(Int32, 1)
        out_circuit_capacity = zeros(Float64, 1)
        out_removed_circuit_index = zeros(Int32, 1)

        @ccall (:PSRNetwork_getContingencyViolationBeta,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, block::Int32, i::Int32, length(out_beta)::Int32, out_beta::Ptr{UInt8}, out_sense::Ptr{UInt8}, out_removed_circuit_index::Ptr{UInt8}, out_circuit_index::Ptr{UInt8}, out_circuit_capacity::Ptr{UInt8})::Int32
        return out_sense[1], out_removed_circuit_index[1], out_circuit_index[1], out_circuit_capacity[1]
    end

    function get_contingency_violation_flow(psrnetwork::Pointer, block::Integer, i::Integer)::Float64
        out_removed_circuit_flow = zeros(Float64, 1)

        ccall((:PSRNetwork_getContingencyViolationFlow, "PSRNetwork"), Nothing, (Ptr{UInt8}, Int32, Int32, Ptr{UInt8}), psrnetwork.pointer, Int32(block), Int32(i), out_removed_circuit_flow)
        return out_removed_circuit_flow[1]
    end

    function get_contingency_violation_analysed!(psrnetwork::Pointer, out_analysed::Vector{Int32})::Nothing
        @ccall (:PSRNetwork_getContingencyViolationAnalysed,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, length(out_analysed)::Int32, out_analysed::Ptr{UInt8})::Nothing
        return nothing
    end

    function get_flows!(psrnetwork::Pointer, block::Integer, out_flows::Vector)::Nothing
        @ccall (:PSRNetwork_getFlows,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, block::Int32, length(out_flows)::Int32, out_flows::Ptr{UInt8})::Nothing
        return nothing
    end

    function get_thetas!(psrnetwork::Pointer, block::Integer, out_thetas::Vector)::Nothing
        @ccall (:PSRNetwork_getThetas,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, block::Int32, length(out_thetas)::Int32, out_thetas::Ptr{UInt8})::Nothing
        return nothing
    end

    function get_beta!(psrnetwork::Pointer, circuit_index::Integer, out_beta::Vector, without_slacks::Bool=false)::Nothing
        if without_slacks
            @ccall (:PSRNetwork_getBetaWithoutSlacks,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, circuit_index::Int32, length(out_beta)::Int32, out_beta::Ptr{UInt8})::Nothing
        else
            @ccall (:PSRNetwork_getBeta,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, circuit_index::Int32, length(out_beta)::Int32, out_beta::Ptr{UInt8})::Nothing
        end
        return nothing
    end

    function get_eta!(psrnetwork::Pointer, circuit_index::Integer, out_eta::Vector, without_slacks::Bool=false)::Nothing
        if without_slacks
            @ccall (:PSRNetwork_getEtaWithoutSlacks,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, circuit_index::Int32, length(out_eta)::Int32, out_eta::Ptr{UInt8})::Nothing
        else
            @ccall (:PSRNetwork_getEta,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, circuit_index::Int32, length(out_eta)::Int32, out_eta::Ptr{UInt8})::Nothing
        end
        return nothing
    end

    function compute_sumcircuits_flows!(psrnetwork::Pointer, block::Integer, out_sumcircuits_flows::Vector)
        sumcircuits_flows_size = length(out_sumcircuits_flows)
        ccall((:PSRNetwork_computeSumCircuitsFlows, "PSRNetwork"), Nothing, (Ptr{UInt8}, Int32, Int32, Ptr{UInt8}), psrnetwork.pointer, Int32(block), Int32(sumcircuits_flows_size), out_sumcircuits_flows)
        return nothing
    end

    function compute_contingency_flows!(psrnetwork::Pointer, block::Integer, removed_circuit_index::Integer, out_contingency_flows::Vector)
        contingency_flows_size = length(out_contingency_flows)
        ccall((:PSRNetwork_computeContingencyFlows, "PSRNetwork"), Nothing, (Ptr{UInt8}, Int32, Int32, Int32, Ptr{UInt8}), psrnetwork.pointer, Int32(block), Int32(removed_circuit_index), Int32(contingency_flows_size), out_contingency_flows)
        return nothing
    end

    function compute_contingency_beta!(psrnetwork::Pointer, removed_circuit_index::Integer, circuit_index::Integer, out_beta::Vector)::Nothing
        beta_size = Int32(length(out_beta))
        ccall((:PSRNetwork_computeContingencyBeta, "PSRNetwork"), Nothing, (Ptr{UInt8}, Int32, Int32, Int32, Ptr{UInt8}), psrnetwork.pointer, Int32(removed_circuit_index), Int32(circuit_index), beta_size, out_beta)
        return nothing
    end

    function delete(psrnetwork::Pointer)
        ccall((:PSRNetwork_delete, "PSRNetwork"), Nothing, (Ptr{UInt8},), psrnetwork.pointer)
        return nothing
    end

    function print_benchmark()::Nothing
        @ccall (:PSRNetwork_printBenchmark,:PSRNetwork)()::Nothing
        return nothing
    end

    function get_slacks_size(psrnetwork::Pointer)::Integer
        out_slacks_size = zeros(Int32, 1)
        @ccall (:PSRNetwork_getSlacksSize,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, out_slacks_size::Ptr{UInt8})::Nothing
        return out_slacks_size[1]
    end

    function get_slacks(psrnetwork::Pointer)::Vector
        slacks_size = get_slacks_size(psrnetwork)
        out_slacks = zeros(Int32, slacks_size)
        @ccall (:PSRNetwork_getSlacks,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, slacks_size::Int32, out_slacks::Ptr{UInt8})::Nothing
        return out_slacks
    end

    function get_maxflow(psrnetwork::Pointer, circuit_index::Integer)::Float64
        return @ccall (:PSRNetwork_getCircuitMaxFlow,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, circuit_index::Int32)::Float64
    end

    function get_FACTS_flag(psrnetwork::Pointer, circuit_index::Integer)::Integer
        return @ccall (:PSRNetwork_getCircuitFACTSFlag,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, circuit_index::Int32)::Int32
    end

    function get_FACTS_xmin(psrnetwork::Pointer, circuit_index::Integer)::Float64
        return @ccall (:PSRNetwork_getCircuitFACTSXmin,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, circuit_index::Int32)::Float64
    end

    function get_FACTS_xmax(psrnetwork::Pointer, circuit_index::Integer)::Float64
        return @ccall (:PSRNetwork_getCircuitFACTSXmax,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, circuit_index::Int32)::Float64
    end

    function restrict_redundant_circuits(psrnetwork::Pointer, block::Integer, buses_with_generation_or_demand::Vector)::Nothing
        @ccall (:PSRNetwork_restrictRedundantCircuits,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, block::Int32, length(buses_with_generation_or_demand)::Int32, buses_with_generation_or_demand::Ptr{UInt8})::Nothing
        return nothing
    end    

    function ignore_circuits_violations(psrnetwork::Pointer, block::Integer, circuits::Vector)::Nothing
        @ccall (:PSRNetwork_ignoreCircuitsViolations,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, block::Int32, length(circuits)::Int32, circuits::Ptr{UInt8})::Nothing
    end

    function get_circuit_resistance(psrnetwork::Pointer, circuit_index::Integer)::Float64
        return @ccall (:PSRNetwork_getCircuitResistance,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, circuit_index::Int32)::Float64
    end

    function get_circuit_losses_flag(psrnetwork::Pointer, circuit_index::Integer)::Integer
        return @ccall (:PSRNetwork_getCircuitLossesFlag,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, circuit_index::Int32)::Int32
    end

    function get_circuit_susceptance(psrnetwork::Pointer, circuit_index::Integer)::Float64
        return @ccall (:PSRNetwork_getCircuitSusceptance,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, circuit_index::Int32)::Float64
    end

    function set_losses!(psrnetwork::Pointer, block::Integer, flows::Vector)::Integer
        return @ccall (:PSRNetwork_setLosses,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, block::Int32, length(flows)::Int32, flows::Ptr{UInt8})::Int32
    end

    function set_min_reactance!(psrnetwork::Pointer, min_reactance::Float64)::Nothing
        @ccall (:PSRNetwork_setMinReactance,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, min_reactance::Float64)::Nothing
		return nothing
    end
    
    function set_all_sumcircuits_violations_mode(psrnetwork::Pointer)::Nothing
        @ccall (:PSRNetwork_setAllSumCircuitsViolationsMode,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8})::Nothing
        return nothing
    end
end