#pragma once  

#ifndef PSRNETWORKEXTENDED_H
#define PSRNETWORKEXTENDED_H

#ifdef WIN32
#ifdef PSRNETWORK_EXPORTS  
#define PSRNETWORKEXTENDED_API __declspec(dllexport)   
#else  
#define PSRNETWORKEXTENDED_API __declspec(dllimport)   
#endif 
#else
#define PSRNETWORKEXTENDED_API
#endif

extern "C" {
    PSRNETWORKEXTENDED_API void PSRNetworkExtended_printVector(int size, double *vector);

    PSRNETWORKEXTENDED_API void PSRNetworkExtended_printB(void *psrnetwork);

    PSRNETWORKEXTENDED_API void PSRNetworkExtended_compensateSlacks(void *psrnetwork, int bus0, int bus1, int *connected_components, int connected_components_size, double *P);
}

#endif // PSRNETWORKEXTENDED_H