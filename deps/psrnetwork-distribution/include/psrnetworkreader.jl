module PSRNetworkReader
    using Libdl

    @eval macro $(Symbol("ccall"))(expr)
        expr.head == :(::) && expr.args[1].head == :call || error("Invalid use of @ccall")
        rettype = expr.args[2]
        fname = expr.args[1].args[1]
        cargs = expr.args[1].args[2:end]

        arglist = []
        typlist = []
        tupexpr = :(())
        ccexpr = :(ccall($(esc(fname)), $(esc(rettype)), $(esc(tupexpr))))
        for carg in cargs
            carg.head == :(::) ||  error("Invalid use of @ccall")
            push!(ccexpr.args, :($(esc(carg.args[1]))))
            push!(tupexpr.args, carg.args[2])
        end
        ccexpr
    end

    @static if VERSION < v"0.7"
        const Nothing = Void
    end

    export
        SDDP,
        NETPLAN,
        get_injections!,
        get_gerter!,
        get_gerhid!,
        get_excbus!,
        get_gergnd!,
        get_dclink!,
        get_dembus!,
        get_defbus!,
        get_phaseshifter!

    struct Pointer
        pointer::Ptr{UInt8}

        function Pointer(pointer::Ptr{UInt8})
            return new(pointer)
        end

        function Pointer()
            return new(C_NULL)
        end
    end

    function SDDP(study::Ptr{UInt8}, path::AbstractString, is_psrcore::Bool=false)::Pointer
        pointer = @ccall (:PSRNetworkReaderSDDP_create,:PSRNetwork)(study::Ptr{UInt8}, path::Ptr{UInt8}, (is_psrcore ? 1 : 0)::Int32)::Ptr{UInt8}
        return Pointer(pointer)
    end

    function NETPLAN(study::Ptr{UInt8}, path::AbstractString)::Pointer
        pointer = @ccall (:PSRNetworkReaderNETPLAN_create,:PSRNetwork)(study::Ptr{UInt8}, path::Ptr{UInt8})::Ptr{UInt8}
        return Pointer(pointer)
    end

    function get_injections!(psrnetwork_reader::Pointer, stage::Integer, scenario::Integer, block::Integer, out_injections::Vector)::Nothing
        @ccall (:PSRNetworkReader_getInjections,:PSRNetwork)(psrnetwork_reader.pointer::Ptr{UInt8}, stage::Int32, scenario::Int32, block::Int32, length(out_injections)::Int32, out_injections::Ptr{UInt8})::Nothing
        return nothing
    end

    function get_gerter!(psrnetwork_reader::Pointer, stage::Integer, scenario::Integer, block::Integer, output_gerter::Vector)::Nothing
        @ccall (:PSRNetworkReaderSDDP_getGerter,:PSRNetwork)(psrnetwork_reader.pointer::Ptr{UInt8}, stage::Int32, scenario::Int32, block::Int32, length(output_gerter)::Int32, output_gerter::Ptr{UInt8})::Nothing
        return nothing
    end

    function get_gerhid!(psrnetwork_reader::Pointer, stage::Integer, scenario::Integer, block::Integer, output_gerhid::Vector)::Nothing
        @ccall (:PSRNetworkReaderSDDP_getGerhid,:PSRNetwork)(psrnetwork_reader.pointer::Ptr{UInt8}, stage::Int32, scenario::Int32, block::Int32, length(output_gerhid)::Int32, output_gerhid::Ptr{UInt8})::Nothing
        return nothing
    end

    function get_excbus!(psrnetwork_reader::Pointer, stage::Integer, scenario::Integer, block::Integer, output_excbus::Vector)::Nothing
        @ccall (:PSRNetworkReaderSDDP_getExcbus,:PSRNetwork)(psrnetwork_reader.pointer::Ptr{UInt8}, stage::Int32, scenario::Int32, block::Int32, length(output_excbus)::Int32, output_excbus::Ptr{UInt8})::Nothing
        return nothing
    end

    function get_gergnd!(psrnetwork_reader::Pointer, stage::Integer, scenario::Integer, block::Integer, output_gergnd::Vector)::Nothing
        @ccall (:PSRNetworkReaderSDDP_getGergnd,:PSRNetwork)(psrnetwork_reader.pointer::Ptr{UInt8}, stage::Int32, scenario::Int32, block::Int32, length(output_gergnd)::Int32, output_gergnd::Ptr{UInt8})::Nothing
        return nothing
    end

    function get_dclink!(psrnetwork_reader::Pointer, stage::Integer, scenario::Integer, block::Integer, output_dclink::Vector)::Nothing
        @ccall (:PSRNetworkReaderSDDP_getDCLink,:PSRNetwork)(psrnetwork_reader.pointer::Ptr{UInt8}, stage::Int32, scenario::Int32, block::Int32, length(output_dclink)::Int32, output_dclink::Ptr{UInt8})::Nothing
        return nothing
    end

    function get_dembus!(psrnetwork_reader::Pointer, stage::Integer, block::Integer, output_dembus::Vector)::Nothing
        @ccall (:PSRNetworkReaderSDDP_getDembus,:PSRNetwork)(psrnetwork_reader.pointer::Ptr{UInt8}, stage::Int32, block::Int32, length(output_dembus)::Int32, output_dembus::Ptr{UInt8})::Nothing
        return nothing
    end

    function get_defbus!(psrnetwork_reader::Pointer, stage::Integer, scenario::Integer, block::Integer, output_defbus::Vector)::Nothing
        @ccall (:PSRNetworkReaderSDDP_getDefbus,:PSRNetwork)(psrnetwork_reader.pointer::Ptr{UInt8}, stage::Int32, scenario::Int32, block::Int32, length(output_defbus)::Int32, output_defbus::Ptr{UInt8})::Nothing
        return nothing
    end

    function get_phaseshifter!(psrnetwork_reader::Pointer, stage::Integer, scenario::Integer, block::Integer, output_phase_shifter::Vector)::Nothing
        @ccall (:PSRNetworkReaderSDDP_getPhaseShifter,:PSRNetwork)(psrnetwork_reader.pointer::Ptr{UInt8}, stage::Int32, scenario::Int32, block::Int32, length(output_phase_shifter)::Int32, output_phase_shifter::Ptr{UInt8})::Nothing
        return nothing
    end
    
    function get_businj!(psrnetwork_reader::Pointer, stage::Integer, scenario::Integer, block::Integer, output_businj::Vector)::Nothing
        @ccall (:PSRNetworkReaderSDDP_getBusinj,:PSRNetwork)(psrnetwork_reader.pointer::Ptr{UInt8}, stage::Int32, scenario::Int32, block::Int32, length(output_businj)::Int32, output_businj::Ptr{UInt8})::Nothing
        return nothing
    end
end