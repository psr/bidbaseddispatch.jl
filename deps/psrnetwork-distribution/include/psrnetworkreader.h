#pragma once  

#ifndef PSRNETWORKREADER_H
#define PSRNETWORKREADER_H

#ifdef WIN32
#ifdef PSRNETWORK_EXPORTS  
#define PSRNETWORKREADER_API __declspec(dllexport)   
#else  
#define PSRNETWORKREADER_API __declspec(dllimport)   
#endif 
#else
#define PSRNETWORKREADER_API
#endif

extern "C" {
    PSRNETWORKREADER_API void* PSRNetworkReaderSDDP_create(void *study, const char *path, int is_psrcore);
    PSRNETWORKREADER_API void* PSRNetworkReaderNETPLAN_create(void *study, const char *path);

    PSRNETWORKREADER_API void PSRNetworkReaderSDDP_getGerter(void *psrnetwork_reader, int stage, int scenario, int block, int output_gerter_size, double *output_gerter);

    PSRNETWORKREADER_API void PSRNetworkReaderSDDP_getGerhid(void *psrnetwork_reader, int stage, int scenario, int block, int output_gerhid_size, double *output_gerhid);

    PSRNETWORKREADER_API void PSRNetworkReaderSDDP_getGergnd(void *psrnetwork_reader, int stage, int scenario, int block, int output_gergnd_size, double *output_gergnd);

    PSRNETWORKREADER_API void PSRNetworkReaderSDDP_getExcbus(void *psrnetwork_reader, int stage, int scenario, int block, int output_excbus_size, double *output_excbus);

    PSRNETWORKREADER_API void PSRNetworkReaderSDDP_getDCLink(void *psrnetwork_reader, int stage, int scenario, int block, int output_dclink_size, double *output_dclink);

    PSRNETWORKREADER_API void PSRNetworkReaderSDDP_getDembus(void *psrnetwork_reader, int stage, int block, int output_dembus_size, double *output_dembus);

    PSRNETWORKREADER_API void PSRNetworkReaderSDDP_getDefbus(void *psrnetwork_reader, int stage, int scenario, int block, int output_defbus_size, double *output_defbus);

    PSRNETWORKREADER_API void PSRNetworkReaderSDDP_getBusinj(void *psrnetwork_reader, int stage, int scenario, int block, int output_businj_size, double *output_businj);

    PSRNETWORKREADER_API void PSRNetworkReaderSDDP_getPhaseShifter(void *psrnetwork_reader, int stage, int scenario, int block, int output_phase_shifter_size, double *output_phase_shifter);

    PSRNETWORKREADER_API void PSRNetworkReader_getInjections(void *psrnetwork_reader, int stage, int scenario, int block, int injections_size, double *out_injections);

    PSRNETWORKREADER_API void* PSRNetworkReader_load(const char* path, const char* filename, int is_psrcore);
}

#endif // PSRNETWORKREADER_H