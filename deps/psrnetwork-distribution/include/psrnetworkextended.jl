module PSRNetworkExtended
    include("psrnetwork")

    using Libdl

    @static if VERSION < v"0.7"
        const Nothing = Void
    end

    export
        initialize,
        initialize_PSRClasses

        function print_B(psrnetwork::PSRNetwork.Pointer)::Nothing
            @ccall (:PSRNetworkExtended_printB,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8})::Nothing
            return nothing
        end
    
        function compensate_slacks!(psrnetwork::PSRNetwork.Pointer, bus0::Integer, bus1::Integer, connected_components::Vector, P::Vector)::Nothing
            @ccall (:PSRNetworkExtended_compensateSlacks,:PSRNetwork)(psrnetwork.pointer::Ptr{UInt8}, bus0::Int32, bus1::Int32, connected_components::Ptr{UInt8}, length(connected_components)::Int32, P::Ptr{UInt8}, length(P)::Int32)::Nothing
            return nothing
        end
end