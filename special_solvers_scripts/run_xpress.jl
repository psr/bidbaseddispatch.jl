import Pkg
Pkg.add(url="https://bitbucket.org/psr/xpresspsr.jl"; io=devnull)
using XpressPSR
using BidBasedDispatch

@assert length(ARGS) == 2
path_case = ARGS[1]
button = parse(Int32,ARGS[2])

BidBasedDispatch.main(path_case,button)
Pkg.rm("XpressPSR"; io=devnull)