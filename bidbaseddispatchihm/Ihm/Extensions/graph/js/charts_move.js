$("#btn_MoveCharts").click(function () {
    const _studyCfg = callbackObj.requestGetStydyConfiguration();
    const _studyCfgObj = _studyCfg ? JSON.parse(_studyCfg) : { GrafList: [] };

    $(".grid").remove();

    const grid = $("<div/>", { class: "grid" });

    $("#MoveChartsBody").append(grid);

    _studyCfgObj.GrafList.forEach((currentItem, index) => {
        const itemContent = $("<div/>", { class: "item-content" });
        itemContent.append(currentItem.Name);

        const chartItem = $("<div/>", { class: "item" });
        chartItem.append(itemContent);
        chartItem.data("_id", currentItem.avid);
        chartItem.data("_index", index);

        grid.append(chartItem);
    });

    let muuriGrid;
    setTimeout(() => {
        muuriGrid = new Muuri(".grid", {
            dragEnabled: true,
            dragAxis: "y",
            dragAutoScroll: {
                targets: function (item) {
                    return [
                        { element: window, priority: 0 },
                        { element: item.getGrid().getElement().parentNode, priority: 1 },
                    ];
                }
            },
        });
    }, 200);

    $("#MoveCharts").modal("show");

    $("#MoveChartsTitle").text("Move charts");
    $("#btn_MoveChartsStop").text("Done");
    $("#btn_MoveChartsStop").removeClass("btn-outline-warning").addClass("btn-outline-success");
    $("#btn_MoveChartsStop").show();
    $("#btn_MoveChartsStop").off("click");
    $("#btn_MoveChartsStop").on("click", function (e) {
        applyReorderBoxChart(muuriGrid);
        $("#MoveCharts").modal("hide");
        return false;
	});
});

const applyReorderBoxChart = function (muuriGrid) {
    const _listIndexOrder = muuriGrid.getItems().map(function(item, index) { 
        const objOrder = $(item.getElement()).data();
        objOrder.index = index;
        return objOrder;
    });

    //Reorder layout
    $(".boxChart").each(function (_i, div) {
        const _div = $(div);
		const _indexOrder = _listIndexOrder.filter(function(item) { return item._id === _div.data().item.avid; })[0];
        const _index = (_indexOrder || { index: 0 }).index;
		_div.data("index", _index);
		tinysort(".boxChart", {data:'index'});
    });
    tinysort(".boxChart", { sortFunction: function(a, b) {
        var rowA = $(a.elm).data('index');
        var rowB = $(b.elm).data('index');
        return rowA == rowB ? 0 : (rowA > rowB ? 1 : -1);
    }});

    //Reorder list chart cache
    const grafDataListCurrent = graf.getObj("grafDataList") || [];
    const grafDataListUpdate = [];
    _listIndexOrder.forEach(function(item) {
        let chartData = grafDataListCurrent.filter(function(chart) { return chart.avid === item._id; })[0];
        if (chartData) grafDataListUpdate.push(chartData);
    });
    graf.setObj("grafDataList", grafDataListUpdate);              

    //Reorder list chart in file
    callbackObj.requestUpdateOrderGrafList(_listIndexOrder.map(function(item) { return item._id; }));
};




