window.callbackObj = window.callbackObj || {
    storage: {},
    storageGetItem: function (k) {
        return this.storage[k] || null;
    },
    storageSetItem: function (k, v) {
        this.storage[k] = v;
    },
    requestOpenGrafConfig: function (avid) {
        var divBox = $("#" + avid)
            .parent()
            .parent()
            .parent();
        divBox.append($("#boxOptions"));
        $("#boxOptions").removeClass("hide");
    },
    requestRemoveGraf: function () {},
    requestGetStydyConfiguration: function () {},
    requestExecuteListGrafModel: function () {},
    requestUpdateOrderGrafList: function () {},
    requestUpdateGrafConfig: function () {},
    requestCloneGraf: function () {},
    requestRevealInExplorer: function () {},
};

window.graf = window.graf || {};
window.graf.pageSettings = callbackObj.pageSettings || {};
window.fnNotifyReloadListEditor = function () {};

if (graf.pageSettings.darkMode) {
    document.write('<link id="css_template" href="assets/css/bootstrap.dark.min.css" rel="stylesheet" type="text/css" />');
    document.write('<link id="css_app" href="assets/css/app.dark.min.css" rel="stylesheet" type="text/css" />');
    document.write('<link id="css_custom" href="css/custom.dark.css" rel="stylesheet" type="text/css" />');
} else {
    document.write('<link id="css_template" href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />');
    document.write('<link id="css_app" href="assets/css/app.min.css" rel="stylesheet" type="text/css" />');
    document.write('<link id="css_custom" href="css/custom.light.css" rel="stylesheet" type="text/css" />');
}

// storage objects
graf.setObj = function (key, obj) {
    if (callbackObj.storageSetItem) callbackObj.storageSetItem(key, JSON.stringify(obj));
};
graf.getObj = function (key) {
    if (callbackObj.storageGetItem) return JSON.parse(callbackObj.storageGetItem(key));
    return null;
};
// storage objects

graf.requestUpdateGrafConfig = function(avid, config) {
    if (callbackObj.requestUpdateGrafConfig)
        callbackObj.requestUpdateGrafConfig(avid, config.chartType + "|" + (config.showLegend ? "1" : "0"));
};
