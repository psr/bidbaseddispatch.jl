var loadScript = function (uri) {
    var script = document.createElement("script");
    script.src = uri;
    document.head.appendChild(script);
};

var makeListCasesBox = function(options) {
	options = options || {};
	let _studyCfg = callbackObj.requestGetStydyConfiguration();
	let _studyCfgObj = { GrafList: [] };
	if (_studyCfg) _studyCfgObj = JSON.parse(_studyCfg);

	let listGrafItem = [
		{
			id: "listChart",
			parent: "#",
			text: "Charts",
			state: { opened: true },
		},
	];
	_studyCfgObj.GrafList.forEach(function (d) {
		listGrafItem.push({
			id: d.avid,
			parent: "listChart",
			text: d.Name,
			type: "file",
		});
	});

	$("#ListChartBoxBody").html("");
	let _checkChart = $("<div/>", { style: "font-size: 14px;" });
	$("#ListChartBoxBody").append(_checkChart);
	
	$("#ListChartBoxUtil").html("");
	if (options.funcCustomTool){
		options.funcCustomTool($("#ListChartBoxUtil"));
	}

	$("#ListChartBox").modal("show");

	_checkChart.jstree({
		core: { themes: { responsive: !1 }, data: listGrafItem },
		types: {
			default: { icon: "mdi mdi-folder" },
			file: { icon: "mdi mdi-chart-line" },
		},
		plugins: ["types", "checkbox", "wholerow"],
	});

	$("#ListChartBoxTitle").text(options.textTitle);
	//Configure button
	$("#btn_ListChartBoxStop").text(options.textButton);
	$("#btn_ListChartBoxStop").removeClass("btn-outline-warning").addClass("btn-outline-success");
	$("#btn_ListChartBoxStop").show();
	$("#btn_ListChartBoxStop").off("click");
	$("#btn_ListChartBoxStop").on("click", function () {
		var listSelected = _checkChart.jstree().get_checked();
		if (listSelected && listSelected.length > 0) {
			$("#ListChartBox").modal("hide");
			if (options.funcButton)
				options.funcButton(listSelected);
		}
		return false;
	});
};

$("#btn_ChangeDarkLightMode").click(function () {
    if (!graf.pageSettings.darkMode) {
        $("#btn_ChangeDarkLightMode > i").removeClass("fe-sun noti-icon");
        $("#btn_ChangeDarkLightMode > i").addClass("fe-moon noti-icon");
    } else {
        $("#btn_ChangeDarkLightMode > i").removeClass("fe-moon noti-icon");
        $("#btn_ChangeDarkLightMode > i").addClass("fe-sun noti-icon");
    }

    setTimeout(function () {
        var grafDataList = graf.getObj("grafDataList");
        if (grafDataList) setGrafDataList(grafDataList);
    }, 200);

    changeDarkLight();

    return false;
});

$("#btn_ChangeLayoutGrid").click(function () {
    if ($(".boxChart.col-lg-12").length) {
        $(".boxChart").addClass("col-lg-6").removeClass("col-lg-12");
        graf.pageSettings.smallBox = true;
    } else {
        $(".boxChart").addClass("col-lg-12").removeClass("col-lg-6");
        graf.pageSettings.smallBox = false;
    }
    setTimeout(function () {
        window.dispatchEvent(new Event("resize"));
    }, 200);

    return false;
});

$("#btn_AddChart").click(function () {
    callbackObj.requestOpenGrafConfig("");
    return false;
});

$(".debugModeLink").click(function () {
    $("body").addClass("debugMode");
    return false;
});

$("#btn_ExecuteListChart").click(function () { 
	makeListCasesBox({
		textTitle: "Process charts list",
		textButton: "Process",
		funcButton: function(listSelected) {
			callbackObj.requestExecuteListGrafModel(listSelected);
		}
	});
	return false;
});

var getDivBoxChart = function (item, classSizeBox) {
    var boxPlot = `<div class="boxChart ${classSizeBox}">
			<div class="card-box text-center">
				<div class="card-widgets" style="float: left;">
					<a href="javascript: void(0);" class="btnCustomizeGraf"><i class=" mdi mdi-collage"></i></a>
				</div>
				<div class="card-widgets">
					<div class="dropdown" style=" display: inline-block;">
						<a id="dp${item.avid}" href="javascript: void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style=" font-size: 18px; line-height: 1; color: inherit; ">
							<i class="mdi mdi-download"></i>
						</a>
						<ul class="dropdown-menu bullet dropdown-menu-right" aria-labelledby="dp${item.avid}" role="menu">
							<li><a href="javascript:void(0)" class="btnDownload dropdown-item" data-value="png"><i class=" mdi mdi-file-image" aria-hidden="true"></i> Download image (png)</a></li>
							<li><a href="javascript:void(0)" class="btnDownload dropdown-item" data-value="xlsx"><i class=" mdi mdi-file-table" aria-hidden="true"></i> Download data (xlsx)</a></li>
						</ul>
					</div>
					<a href="javascript: void(0);" class="btnNewTemplate"><i class=" mdi mdi-content-copy"></i></a>
					<a href="javascript: void(0);" class="btnConfigGraf"><i class=" mdi mdi-settings"></i></a>
					<a href="javascript: void(0);" class="btnRemoveGraf"><i class=" mdi mdi-close"></i></a>
				</div>
				<h4 class="header-title">${item.titulo}</h4>
				
				<div class="collapse pt-3 show">
					<div class="default-chart" id="${item.avid}"><svg></svg></div>
				</div>
			</div>
		</div>`; //<script>

    return boxPlot;
};

//public
var updateGrafData = function (item, bSkipPlot) {
    //remove helpNewChart
    $(".boxHelpNewChart").remove();

    //update graf into internal list
    var grafDataList = graf.getObj("grafDataList") || [];
    var chartFound = false;
    for (var i = 0; i < grafDataList.length; i++) {
        if (grafDataList[i].avid == item.avid) {
            grafDataList[i] = item;
            chartFound = true;
        }
    }
    if (!chartFound) {
        //New Chart
        grafDataList.unshift(item);
        var classSizeBox = "col-lg-12";
        if (graf.pageSettings.smallBox) classSizeBox = "col-lg-6";
        item.avid = item.avid || Math.random().toString(36).substring(2) + Date.now().toString(36); //Generating unique avid;
        item.titulo = item.titulo || item.tituloLegenda;
        convertConfigChartData(item);

        var boxPlot = getDivBoxChart(item, classSizeBox);
        var divBox = $(boxPlot);
        divBox.data("item", item);
        $("#board").prepend(divBox);
		getNoDataAvabile(item.avid);
        updateLayoutGrid();

        divBox.find(".dropdown-toggle").dropdown();
        divBox.find(".btnDownload").click(fnDownloadChart);
        divBox.find(".btnCustomizeGraf").click(fnCustomizeChart);
        divBox.find(".btnNewTemplate").click(fnNewTemplate);
        divBox.find(".btnConfigGraf").click(fnConfigChart);
        divBox.find(".btnRemoveGraf").click(fnRemoveChart);
    } else {
        var divBox = $("#" + item.avid)
            .parent()
            .parent()
            .parent();
        convertConfigChartData(item);
        divBox.find(".header-title").text(item.titulo);
        if (!bSkipPlot) 
			divBox.data("item", item);
    }
	if (!chartFound || !bSkipPlot)
		graf.setObj("grafDataList", grafDataList);

	if (!bSkipPlot) {
		if (((!item.dados || item.dados.length === 0) && (!item.results || item.results.length === 0)) || item.tipoGrafico === -1) {
			getNoDataAvabile(item.avid, item.tipoGrafico);
			divBox.find(".card-disabled").remove();
			divBox.find(".chartOptions").remove();
			divBox.find(".chartMessage").remove();
		} else {
			setTimeout(function () {
				plotChart(item.avid, item, item.config);
				divBox.find(".card-disabled").remove();
				divBox.find(".chartOptions").remove();
				divBox.find(".chartMessage").remove();
			}, 200);
		}
	}
};

const convertConfigChartData = function (item) {
    item.configuracaoString = item.configuracaoString || "LINE|1";
    item.config = item.config || { chartType: item.configuracaoString.split("|")[0], showLegend: +item.configuracaoString.split("|")[1] };
    item.config.showLegend = (item.config.showLegend === true || item.config.showLegend === 1) ? true : false;
}

var updateChartInList = function (item) {
    //update graf into internal list
    var grafDataList = graf.getObj("grafDataList") || [];
    var chartFound = false;
    for (var i = 0; i < grafDataList.length; i++) {
        if (grafDataList[i].avid == item.avid) {
            grafDataList[i] = item;
            chartFound = true;
        }
    }
    if (chartFound) graf.setObj("grafDataList", grafDataList);
    graf.requestUpdateGrafConfig(item.avid, item.config);
};

var getNoDataAvabile = function (idDiv, graphType) {
    $("#" + idDiv).html("");
    var nodata = $("<span>", {
        css: {
            display: "table-cell",
            "vertical-align": "middle",
            "text-align": "center",
        },
    });
    nodata.text(graphType === -1 ? "No graph to display" : "No Data Available.");
    var divnodata = $("<div>", {
        css: { display: "table", width: "100%", height: "100%" },
    });
    divnodata.append(nodata);
    $("#" + idDiv).append(divnodata);
};

var setMessageDiv = function (idDiv, message, type) {
	//type = 1 warning | 2 error
	var divBoxCard = $("#" + idDiv)
	.parent()
	.parent();
	divBoxCard.find(".chartMessage").remove();
	
    var msgSpan = $("<span>", {
        css: {
			width: "100%",
			display: "block",
			position: "absolute",
			top: "7px",
			color: "maroon",
            "font-weight": "600"
        },
    });
    msgSpan.text(message || "");
    var divbackground = $("<div>", {
        css: { background: "khaki", padding: "15px 0px 15px", opacity: "0.75", "border-radius": "3px"}
    });
    var divmsg = $("<div>", {
        css: { left: "0px", right: "0px", bottom: "10px", margin: "20px", position: "absolute"}
    });
	divmsg.addClass("chartMessage");
    divmsg.append(divbackground);
    divmsg.append(msgSpan);
    divBoxCard.append(divmsg);
};

//public
var setMessageGraf = function (itemGrafMessage) {
	itemGrafMessage = itemGrafMessage || { avid: "123", titulo: "Chart", message: "message" };
	updateGrafData(itemGrafMessage, true);
	setMessageDiv(itemGrafMessage.avid, itemGrafMessage.message);
}

//public
var setGrafDataList = function (grafDataList) {
    $("#board").html("");
    var classSizeBox = "col-lg-12";
    if (graf.pageSettings.smallBox) classSizeBox = "col-lg-6";
    (grafDataList || []).forEach(function (item) {
        item.avid = item.avid || Math.random().toString(36).substring(2) + Date.now().toString(36); //Generating unique avid;
        item.titulo = item.titulo || item.tituloLegenda;
        convertConfigChartData(item);

        var boxPlot = getDivBoxChart(item, classSizeBox);
        var divBox = $(boxPlot);
        divBox.data("item", item);
        $("#board").append(divBox);

        if (item.results) item = getAggregatePlot(item);

        if (!item.dados || item.dados.length === 0 || item.tipoGrafico === -1) {
            getNoDataAvabile(item.avid, item.tipoGrafico);
        } else {
            setTimeout(function () {
                plotChart(item.avid, item, item.config);
            }, 200);
        }
    });
    $(".dropdown-toggle").dropdown();

    $(".btnDownload").click(fnDownloadChart);
    $(".btnCustomizeGraf").click(fnCustomizeChart);
    $(".btnNewTemplate").click(fnNewTemplate);
    $(".btnConfigGraf").click(fnConfigChart);
    $(".btnRemoveGraf").click(fnRemoveChart);

    graf.setObj("grafDataList", grafDataList);
};

var setStudyInfo = function (studyInfo) {
    studyInfo = studyInfo || {};
    var listProperties = Object.keys(studyInfo);
    listProperties.forEach(function (p) {
        var txt = $("#txt_" + p);
        if (txt.length) txt.text(studyInfo[p]);
    });
    graf.setObj("studyInfo", studyInfo);
};

var setListGraf = function (listGraf) {
    (listGraf || []).forEach(function (item) {
        $("#basic-datatable > tbody").append(`<tr><td>${item.Name}</td><td>${item.StartYear}-${item.StartStage}</td></tr>`); //<script>
    });
    graf.setObj("listGraf", listGraf);
};

//Restoring previows settings
$(document).ready(function () {
    var studyInfo = graf.getObj("studyInfo");
    if (studyInfo) setStudyInfo(studyInfo);
    var listGraf = graf.getObj("listGraf");
    if (listGraf) setListGraf(listGraf);
    var grafDataList = graf.getObj("grafDataList");
    if (grafDataList) setGrafDataList(grafDataList);
});

//editor
$("#editorLink").click(function () {
    $("#editor").attr("src", "editor/index.html");
    $("#editor-modal").modal();

    return false;
});

$("#btn_StartJobTest").click(function () {
    if (callbackObj) callbackObj.requestBeginJob();
    return false;
});

$("#btn_JobLogBoxStop").click(function () {
    if (callbackObj) callbackObj.requestEndJob();
    return false;
});

var show_HelpNewChart = function () {
    if (!$(".boxChart").length) {
		const divBox = $("#boxHelpNewChartOne").clone();
		divBox.addClass("boxHelpNewChart");
        divBox.find(".linkHelpNewChart").click(function () {
            callbackObj.requestOpenGrafConfig("");
            return false;
        });
        $("#board").append(divBox);
        divBox.removeClass("hide");
    }
};

$("body").addClass("sidebar-enable enlarged");

//$("#GrafFormBox").modal('show');
