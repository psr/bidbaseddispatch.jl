const log_Show = function (avid) {
    const vetAVID = (avid || "").split("|");

    (vetAVID || []).forEach(function (avidItem) {
        if ($("#" + avidItem).length > 0) {
            const divBox = $("#" + avidItem)
                .parent()
                .parent()
                .parent();
            divBox.append('<div class="card-disabled"><div class="card-portlets-loader"></div></div>');
        }
    });

    $("#JobLogBoxBody").html("");
};

const log_Close = function () {
    //$("#btn_JobLogBoxStop").hide();
    //$("#JobLogBoxTitle").text('Process exited.');
};

const logError_Show = function (avid) {
    const vetAVID = (avid || "").split("|");
    (vetAVID || []).forEach(function (avidItem) {
        if ($("#" + avidItem).length > 0) {
            const divBox = $("#" + avidItem)
                .parent()
                .parent()
                .parent();
            divBox.find(".card-disabled").remove();
            divBox.find(".chartOptions").remove();
        }
    });
    $("#btn_JobLogBoxStop").hide();

    $("#JobLogBoxTitle").text("Process log");
    $("#JobLogBox").modal("show");
};

const log_WriteLog = function (log) {
    const _span = $("<span/>");
    _span.text(log);
    $("#JobLogBoxBody").append(_span);
    $("#JobLogBoxBody").append("<br/>");
};

const log_WriteErr = function (log) {
    const _span = $("<span/>", { style: "color: #f1556c;" });
    _span.text(log);
    $("#JobLogBoxBody").append(_span);
    $("#JobLogBoxBody").append("<br/>");
};

const log_WriteErrorShow = window.log_WriteErrorShow = function (log) {
    $("#JobLogBoxBody").html("");
    const _span = $("<pre/>", {
        style: "color: #f1556c; white-space: pre-line;",
    });
    _span.text(log);
    $("#JobLogBoxBody").append(_span);
    $("#JobLogBoxBody").append("<br/>");

    $("#btn_JobLogBoxStop").hide();
    $("#JobLogBox").modal("show");
};
