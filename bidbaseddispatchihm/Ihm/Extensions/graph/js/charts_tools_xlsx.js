function saveXlsxFromByteArray(filename, byte) {
    var blob = new Blob([byte], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
    var link = document.createElement("a");
    link.href = window.URL.createObjectURL(blob);
    link.download = filename;
    link.click();
}

var getTextLayoutAxis = function (layoutAxis, defaulText) {
    var _axis = layoutAxis || {};
    var _title = _axis["title"] || {};
    return _title["text"] || defaulText;
};

var tryConvertToUTCDate = function (dateArry) {
    if (dateArry && dateArry.length > 1 && dateArry[0] instanceof Date) {
        for (i = 0; i < dateArry.length; i++) {
            dateArry[i] = moment(dateArry[i]).utcOffset(0, true)._d;
        }
    }
};

var getDataTableFromChart = function (objChart) {
    var _layout = objChart.layout || {};
    var _data = objChart.data || [];
    if (_data.length == 0) return;

    var _columns = [];
    var _rows = [];
    var _serieBase = _data[0];

    if (_serieBase["labels"] && _serieBase["values"]) {
        //pie
        _columns = (_serieBase["labels"] || []).map(function (_label) {
            return { name: _label };
        });
        _rows = [
            (_serieBase["values"] || []).map(function (_value) {
                if (!isNaN(_value)) return +_value;
                return _value;
            }),
        ];
        return { columns: _columns, rows: _rows };
    }
    if (_data.length == 1 && _serieBase["x"] && _serieBase["y"] && _serieBase["z"]) {
        //heatmap or surface3D
        var _layoutCurrent = _layout;
        if (_layout["scene"]) _layoutCurrent = _layout["scene"];
        _columns.push({ name: getTextLayoutAxis(_layoutCurrent["xaxis"], "-") + "\\" + getTextLayoutAxis(_layoutCurrent["yaxis"], "-") });
        for (i = 0; i < _serieBase["y"].length; i++) {
            _columns.push({ name: _serieBase["y"][i] });
        }
        var _stagesLabel = _serieBase["x"] || [];
        tryConvertToUTCDate(_stagesLabel);
        for (i = 0; i < _stagesLabel.length; i++) {
            var _row = [_stagesLabel[i]];
            for (j = 0; j < _serieBase["y"].length; j++) {
                var _val = _serieBase["z"][j][i];
                if (!isNaN(_val)) _val = +_val;
                _row.push(_val);
            }
            _rows.push(_row);
        }
        return { columns: _columns, rows: _rows };
    }
    //other chart types
    var _dataAxis = "y";
    var _labelAxis = "x";
    if (_serieBase["orientation"] == "h") {
        // horizontal bar
        _dataAxis = "x";
        _labelAxis = "y";
    }
    _columns.push({ name: getTextLayoutAxis(_layout[_labelAxis + "axis"], "-") });
    for (i = 0; i < _data.length; i++) {
        var _serie = _data[i];
        _columns.push({ name: _serie["name"] });
    }
    var _stagesLabel = _serieBase[_labelAxis] || [];
    tryConvertToUTCDate(_stagesLabel);
    for (i = 0; i < _stagesLabel.length; i++) {
        var _row = [_stagesLabel[i]];
        for (j = 0; j < _data.length; j++) {
            var _val = _data[j][_dataAxis][i];
            if (!isNaN(_val)) _val = +_val;
            _row.push(_val);
        }
        _rows.push(_row);
    }
    return { columns: _columns, rows: _rows };
};

var downloadXLSXFromPlotLy = function (config, funcSave) {
    config = config || {};
    if (config["idDiv"]) {
        var _objChart = document.getElementById(config["idDiv"]);
        var _titleChart = config["title"] || "ChartTable";
        var _filename = config["filename"] || _titleChart.replace(/[/\\?%*:|"<> /&]/g, '_') + ".xlsx";

        var _dataTable = getDataTableFromChart(_objChart);
        if (!_dataTable) return;

        var workbook = new ExcelJS.Workbook();
        var worksheet = workbook.addWorksheet("Table");
        
		//title
        worksheet.mergeCells('A1:C1');
        worksheet.getCell('A1').value = _titleChart;
        worksheet.getCell('A1').alignment = { horizontal:'center'} ;
		
		//data
		if (config.useTable) {
			worksheet.addTable({
				name: "Table1",
				ref: "A3",
				headerRow: true,
				totalsRow: false,
				style: {
					theme: "TableStyleMedium9",
					showRowStripes: true,
				},
				columns: _dataTable.columns,
				rows: _dataTable.rows,
			});
		} else {
			worksheet.addRow([]);
			worksheet.addRow((_dataTable.columns || []).map(function(col) { return col.name; }));
			(_dataTable.rows || []).forEach(function(row) {
				worksheet.addRow(row);
			});
		}
		
		funcSave = funcSave || function (_buffer) { saveXlsxFromByteArray(_filename, _buffer); }
        return workbook.xlsx.writeBuffer().then(funcSave);
    }
};
