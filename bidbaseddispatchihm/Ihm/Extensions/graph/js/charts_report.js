/*
Andamento:
# - Estilo do texto selecionado
# - Problema no preview side-by-side (não fica tela completa)
 - Rever estilo de tags (links, lista etc, no tema dark, observar o exemplo abaixo)
	https://cdn.rawgit.com/xcatliu/simplemde-theme-dark/master/dist/simplemde-theme-dark.min.css
 
 - Criar mecanismo para inserir gráficos
 - Auto criar texto inicial com gráficos inseridos
 
# - Replace dos placeholdes com gráficos plotados (em thead separada)
 - Salve/Reload...

*/

var easyMDE;
$("#chartReportLink").click(function () {
	genereateSampleText();
	$("#report-modal").modal("show");
	if (!easyMDE) //easyMDE.toTextArea(); 
	{
		easyMDE = new EasyMDE({
			element: document.getElementById('report'),
			maxHeight: "75vh",
			sideBySideFullscreen: false,
			spellChecker: false,
			toolbar: toolbar,
			renderingConfig: {
				singleLineBreaks: false,
				codeSyntaxHighlighting: true,
				sanitizerFunction: function(renderedHTML) {
					const outputProcessed = replaceDiv(renderedHTML);
					setTimeout(function() { processAllCRCharts(); sortTable(); }, 500);
					return outputProcessed;
				},
			},
		});
		setTimeout(function() {
		  easyMDE.codemirror.refresh();
		}, 250);
	}
    return false;
});

var pagelinkFunction = function () {    				
	field.find(".editor-toolbar").toggleClass("pagelink-open");
	if (field.find(".pagesearch").length) {
	  field.find(".pagesearch").remove();
	  return;
	}
	else {
	  var input = $('<input type="text" class="pagesearch" placeholder="' + field.find(".editor-toolbar").data("pagelink-placeholder") + '">');
	  field.find(".editor-toolbar").append(input);
	}
	var index = {
		url: function(phrase) {
			return searchUrl + "?phrase=" + phrase;
		},
		getValue: "title",
		template: {
			type: "custom",
			method: function(value, item) {
			  return '<span class="title">' + value + '</span>' + 
			  '<span class="uri"> (' + item.uri + ')</span>';
			}
		},
		list: {
			maxNumberOfElements: 100,
			onChooseEvent: function() {
				var title = input.getSelectedItemData().title;
				var uri = input.getSelectedItemData().uri
				
				var cm = simplemde.codemirror;
				var selection = cm.getSelection();
				if (selection) {
				  var replacement = '(link: ' + uri + ' text: ' + selection + ')';
				}
				else {
				  var replacement = '(link: ' + uri + ' text: ' + title + ')';
				}
				cm.replaceSelection(replacement);
				var cursorPos = cm.getCursor();
				cm.focus();
				
				field.find(".editor-toolbar").removeClass("pagelink-open");
				field.find(".editor-toolbar .easy-autocomplete").remove();
			},
			onHideListEvent: function() {
				var containerList = field.find(".easy-autocomplete-container ul");
				if ($(containerList).children('li').length <= 0) {
				  $(containerList).html('<li class="no-results">' + field.find(".editor-toolbar").data("no-results") + '</li>').show();
				}
			}
		}
	};
	input.easyAutocomplete(index);
	input.focus();
	input.on('keyup',function(evt) {
	  if (evt.keyCode == 27) {
		field.find(".editor-toolbar").removeClass("pagelink-open");
		field.find(".pagesearch").remove();
		simplemde.codemirror.focus();
	  }
	});
};

var toolbar = [
	"bold", "italic", "heading", "horizontal-rule", "|",
	//
    "quote", "unordered-list", "ordered-list", "|",
	"undo", "redo", "|",
	//{
	//	name: "pagelink",
	//	action: pagelinkFunction,
	//	className: "fa fa-file",
	//	title: "{{button.page}}",
	//},
	"preview"
];

var replaceDiv = function(contentHTML) {
	//process divBlocks
	var blockBegin = '[chart:';
	var blockEnd = ']';
	var lines = contentHTML.split('\n') || [];
	for(var i = 0;i < lines.length;i++){
		var line = lines[i] || "";
		//  !![chart:Nome chart]
		if (line.includes(blockBegin))
		{
			var partBlock = line.substr(line.indexOf(blockBegin) + blockBegin.length);
			var divName = partBlock.substr(0, partBlock.indexOf(blockEnd));
			divName = divName.trim();
			divId = "cr" + i + (Math.floor(Math.random() * 26) + i + Date.now());
			lines[i] = "<div id=\"" + divId + "\" class=\"crChartDiv toProcess\" data-chartname=\"" + divName + "\"></div>";
		}
	}
	contentHTML = lines.join('\n');
	return contentHTML;
}

var processAllCRCharts = function() {
	const layoutCustom = {paper_bgcolor: "#4a535f", plot_bgcolor: "#4a535f" };
    $(".crChartDiv.toProcess").each(function (_i, div) {
		const _div = $(div);
		_div.removeClass("toProcess");
		const nameChart = _div.data("chartname");
		const idChart = _div.attr("id");
		const chartData = graf.getObj("grafDataList").filter(function(d) { return d.titulo === nameChart; })[0];
		if (chartData) {
			setTimeout(function() {
				if ($("#" + idChart).length === 1)
					plotChart(idChart, chartData, chartData.config, layoutCustom);
			}, 250);
		} else {
			_div.addClass("text-danger");
			_div.text("Not found: " + nameChart);
		}
	});
}

var genereateSampleText = function() {
	var lorem = new LoremIpsum();
	const listLines = [];
	const studyInfo = graf.getObj("studyInfo");
	const variables = graf.getObj("studyInfo").VariableList || [];
	const grafDataList = graf.getObj("grafDataList") || [];
	
	listLines.push("# " + studyInfo.Name);
	listLines.push("**" + studyInfo.Initial + "** -> **" + studyInfo.Final + "**");
	listLines.push("");
	
	listLines.push(lorem.generate(50));
	listLines.push("");
	
	if (variables.length > 0) {
		//Variable list
		listLines.push("|Variable|Unit|Source|");
		listLines.push("|---|---|---|");

		variables.forEach(function(vData) {
			listLines.push("|" + vData.Name + "|" + vData.Unit + "|" + vData.FileName + "|");
		});
	
		listLines.push("");
		listLines.push("*Table of variables*");	
		listLines.push("");			
	}

	listLines.push(lorem.generate(150));
	listLines.push("");
	
	grafDataList.forEach(function(gData) {
		// ## Generacion - Total
		// !![chart:Generacion - Total]
		// *Chart 2. Generacion - Total.*
		listLines.push("## " + gData.titulo);
		listLines.push("!![chart:" + gData.titulo + "]");
		listLines.push("*Chart: " + gData.titulo + "*");
		listLines.push("");
		listLines.push(lorem.generate(100));
		listLines.push("");
	});
	
	$("#report").text(listLines.join('\n'));
}


var sortTable = function() {
	const getCellValue = (tr, idx) => tr.children[idx].innerText || tr.children[idx].textContent;

	const comparer = (idx, asc) => (a, b) => ((v1, v2) => 
		v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2)
		)(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));
	
	document.querySelectorAll('th').forEach(th => th.addEventListener('click', (() => {
	  const table = th.closest('table');
	  const tbody = table.querySelector('tbody');
	  Array.from(tbody.querySelectorAll('tr'))
		.sort(comparer(Array.from(th.parentNode.children).indexOf(th), this.asc = !this.asc))
		.forEach(tr => tbody.appendChild(tr) );
	})));
	
	﻿$(".editor-preview th").css("cursor", "pointer");
	
}


/*
var field = simplemde.closest(".field");

// insert code in markdown
var myEditor = new SimpleMDE({
    toolbar: [
        {
            name: "redText",
            action: drawRedText,
            className: "fa fa-bold", // Look for a suitable icon
            title: "Red text (Ctrl/Cmd-Alt-R)",
        }
    ]
});
function drawRedText(editor) {

    var cm = editor.codemirror;
    var output = '';
    var selectedText = cm.getSelection();
    var text = selectedText || 'placeholder';

    output = '!!' + text + '!!';
    cm.replaceSelection(output);
}



  
## Welcome to Power View

Power View is a tool for the graphical visualization of transmission-related study results of PSR models. This tool allows the visualization on a map, in a georeferenced and integrated environment, of animated circuit flows, circuit loading, as as bus-related information such as bus generation and load. Is is also possible to show an animated chronological evolution of the variables along the study period.

### Technical specifications

The visualization tool has been implemented using the *Adobe AIR* framework (based on *Adobe Flash*) and the *Google Maps* API.

* *Adobe AIR*: graphical development framework based on vector graphics, which allows the construction of rich content applications using high performance graphical processing. Its main characteristics are:
  * GPU Hardware acceleration for performance optimization
  * Layer-bases interface layout
  * Vector animation support
  * Rasterized graphics animation

* *Google Maps API*: Power View integration with *Google Maps* API allows the use of navigation and control functionalities such as:
  * Terrain map visualization
  * Type of terrain visualization
  * Translation and zoom control

### System requirements

Power View requires internet connection.

   
  
*/