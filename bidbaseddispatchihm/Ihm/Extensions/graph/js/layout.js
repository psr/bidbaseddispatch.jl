const loadStyle = function (uri) {
    const linkS = document.createElement("link");
    linkS.rel = "stylesheet";
    linkS.href = uri;
    document.head.appendChild(linkS);
};

const unloadStyle = function (uri) {
    const linkList = document.getElementsByTagName("link");

    for (i = 0; i < linkList.length; i++) {
        const lk = linkList[i];
        if (lk.attributes["href"].value == uri) lk.parentNode.removeChild(lk);
    }
};

const changeStyle = function (id, uri) {
    document.querySelector(id).setAttribute('href', uri);
};

const changeDarkLight = function () {
    if (!graf.pageSettings.darkMode) {
        changeStyle("#css_template", "assets/css/bootstrap.dark.min.css");
        changeStyle("#css_app", "assets/css/app.dark.min.css");
        changeStyle("#css_custom", "css/custom.dark.css");
        graf.pageSettings.darkMode = true;
    } else {
        changeStyle("#css_template", "assets/css/bootstrap.min.css");
        changeStyle("#css_app", "assets/css/app.min.css");
        changeStyle("#css_custom", "css/custom.light.css");
        graf.pageSettings.darkMode = false;
    }
};

const updateLayoutGrid = function () {
    window.dispatchEvent(new Event("resize"));
    setTimeout(function () {
        window.dispatchEvent(new Event("resize"));
    }, 200);
};
