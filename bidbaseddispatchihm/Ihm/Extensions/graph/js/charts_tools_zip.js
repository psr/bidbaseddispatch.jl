$("#btn_DownloadData").click(function() { 
	makeListCasesBox({
		textTitle: "Download chart data",
		textButton: "Download",
		funcButton: function(listSelected) {
			let _option = +$(".downloadData option:selected").val();
			downloadZip(listSelected, _option);
		},
		funcCustomTool: function(boxUtil) {
			let _select = $("<select>", { style: "width: 200px; float: left;" });
			_select.addClass("form-control downloadData");
			_select.append($('<option selected>').val("0").text("Chart spreadsheets"));
			_select.append($('<option>').val("1").text("Chart images"));
			_select.appendTo(boxUtil);
		}
	});
	return false;
});

var addInZipFromUint8 = function(zipController, filename, fileUint8Array) {
	let objData = new zip.Uint8ArrayReader(fileUint8Array);
	let objWork = zipController.add(filename, objData);
	return objWork;
}

var addInZipBase64 = function(zipController, filename, fileBase64) {
	let objData = new zip.Data64URIReader(fileBase64);
	let objWork = zipController.add(filename, objData);
	return objWork;
}

function saveFileFromBlob(filename, blob) {
    var link = document.createElement("a");
    link.href = window.URL.createObjectURL(blob);
    link.download = filename;
    link.click();
}

var downloadZip = function (listFilter, bImages) {
	let listCharts = $(".boxChart");
	window.listDataCharts = [];
	if (listCharts.length > 0){
		listCharts.each(function(i, obj) { 
			let objChart = $(obj).data("item");
			if (objChart) {
				if (listFilter && listFilter.indexOf(objChart.avid) != -1) {
					let filename = objChart.titulo.replace(/[/\\?%*:|"<> /&]/g, '_'); 
					if (listDataCharts.filter(function(obj) { return obj.filename == filename; })[0])
						filename = filename + "_" + objChart.avid;
					listDataCharts.push({
						idDiv: objChart.avid,
						title: objChart.titulo + " (" + (objChart.unidade || "default") + ")",
						filename: filename
					});
				}
			}
		});
		
		if (listDataCharts.length > 0)
		{
			let blobZip = new zip.BlobWriter("application/zip");
			let writerZip = new zip.ZipWriter(blobZip);
			let listWrkSaver = [];
			let listWrkAddZip = [];
			
			if (bImages) {
				// zip *.png
				listDataCharts.forEach(function(obj) {
					let chartDiv = $("#" + obj.idDiv)[0];
					let exportCfg = { height: 400, width: 850 };
					listWrkSaver.push( Plotly.toImage(chartDiv, exportCfg).then(function(_dataBase64) { 
						listWrkAddZip.push(addInZipBase64(writerZip, obj.filename + ".png", _dataBase64));
					}));
				});
			} else {
				// zip *.xlsx
				listDataCharts.forEach(function(obj) {
					listWrkSaver.push(downloadXLSXFromPlotLy(obj, function(_buffer) {
						listWrkAddZip.push(addInZipFromUint8(writerZip, obj.filename + ".xlsx", _buffer));
					}));
				});
			}

			$.when(...listWrkSaver).then(function () {
				$.when(...listWrkAddZip).then(function () {
					writerZip.close().then(function() {
						saveFileFromBlob("dataset_" + (bImages ? "images" : "spreadsheets") + ".zip", blobZip.getData());
					}).catch(function(error){
						console.error("zip error: ", error);
					});
				}).catch(function(error){
					console.error("worker error: ", error);
				});
			});
			
		}
	}
};
