const fnDownloadChart = function (e) {
    const aEl = $(this);
    const divBox = aEl.parent().parent().parent().parent().parent().parent();
    const menu = aEl.parent().parent();
    const objPlot = divBox.data("item");

    if (objPlot) {
		
		let filename = (objPlot.titulo + "-" + (objPlot.unidade || "")).replace(/[/\\?%*:|"<> /&]/g, '_');
        if (aEl.data("value") == "png") {
            Plotly.downloadImage(objPlot.avid, {
                filename: filename,
                height: 400,
                width: 850,
            });
        }
        if (aEl.data("value") == "xlsx") {
            downloadXLSXFromPlotLy({
                idDiv: objPlot.avid,
                title: objPlot.titulo + "(" + (objPlot.unidade || "default") + ")",
				filename: filename + ".xlsx"
            });
        }
    }
    menu.removeClass("show");
    return false;
};

const fnRemoveChart = function () {
    const divBox = $(this).parent().parent().parent();
    const objPlot = divBox.data("item");
    if (objPlot) {
        bootbox.confirm({
            size: "small",
            message: "Are you sure?",
            centerVertical: true,
            buttons: {
                confirm: {
                    label: "Remove!",
                    className: "btn-danger",
                },
                cancel: {
                    label: "Cancel",
                    className: "btn-light",
                },
            },
            callback: function (resConfirm) {
                if (resConfirm) {
                    //remove from instruc.all
                    callbackObj.requestRemoveGraf(objPlot.avid);

                    const grafDataList = graf.getObj("grafDataList") || [];
                    let iChart = -1;
                    for (let i = 0; i < grafDataList.length; i++) {
                        if (grafDataList[i].avid == objPlot.avid) {
                            iChart = i;
                        }
                    }
                    if (iChart > -1) {
                        grafDataList.splice(iChart, 1);
                    }
                    graf.setObj("grafDataList", grafDataList);
                    divBox.remove();
					show_HelpNewChart();
                    updateLayoutGrid();
                }
            },
        });
    }
    return false;
};

const fnNewTemplate = function (e) {
    const divBox = $(this).parent().parent().parent();
    const objPlot = divBox.data("item");
    if (objPlot) {
        callbackObj.requestCloneGraf(objPlot.avid);
    }
    return false;
};

const fnConfigChart = function (e) {
    const divBox = $(this).parent().parent().parent();
    const objPlot = divBox.data("item");
    if (objPlot) {
        callbackObj.requestOpenGrafConfig(objPlot.avid);
    }
    return false;
};

const fnCustomizeChart = function (e) {
    const divBox = $(this).parent().parent().parent();
    if (divBox.find(".boxOptions").length > 0) {
        $(".boxOptions").remove();
    } else {
        $(".boxOptions").remove();
        const objPlot = divBox.data("item");
        if (objPlot) {
            objPlot.config = objPlot.config || {
                chartType: "LINE",
				showLegend: true,
            };

            const divOptions = $("#boxOptions").clone();
            divOptions.addClass("boxOptions");
            divOptions.removeClass("hide");
            if (!objPlot.etapasHorarias) {
                // Chart is not hourly
                divOptions.find(".chartHourly").remove();
            }
            divBox.append(divOptions);
            
			const chartSelected = divOptions.find("[data-chartType=" + objPlot.config.chartType);
            if (chartSelected.length != 0) {
                chartSelected.addClass("active");
            }
            divBox.find(".cIcon").click(function (e) {
                const classIconType = $(this);
                const chartType = classIconType.data("charttype");
                divBox.attr("data-charttype", chartType);
                objPlot.config.chartType = chartType;

                plotChart(objPlot.avid, objPlot, objPlot.config);
                divBox.find(".boxOptions").remove();

                updateChartInList(objPlot);
                return false;
            });

			//Tag
            objPlot.config.dataTag = objPlot.config.dataTag || ["Costs", "Demand", "Generation", "UserTag1", "UserTag2"];
            const selectTag = divOptions.find(".tagEditor");
            selectTag.select2({
                tags: true,
                data: objPlot.config.dataTag,
            });
            selectTag.val(objPlot.config.dataTag);
            selectTag.trigger("change");

            selectTag.on("change", function (e) {
                objPlot.config.dataTag = selectTag.val();
                updateChartInList(objPlot);
            });
            //Tag
			
            const checkShowLegend = divBox.find(".checkShowLegend");
            checkShowLegend.prop('checked', objPlot.config.showLegend);
            checkShowLegend.change(function(e) {
                Plotly.restyle(objPlot.avid, "showlegend", this.checked);
                objPlot.config.showLegend = this.checked;
                updateChartInList(objPlot);
            });
			
        }
    }
    return false;
};

const fnNotifyDownloadCompleted = function (fileNameHash) {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "3000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false,
    };
    const fileName = fileNameHash.split("|")[0];
    const fileHash = fileNameHash.split("|")[1];
    toastr["success"]("<br/>" + fileName + "<br/><br/><div class=\"text-center\"><button type=\"button\" class=\"btn btn-outline-success\" onclick=\"callbackObj.requestRevealInExplorer('" + fileHash + "');toastr.clear();\">Open containing folder</button></div>", "File was saved");
    return false;
};

