//para o autocomplite

var psrioDic = [];
psrioDic.push({  category: "keyword",          type: "Collections", values: ["Area", "BalancingArea", "BalancingAreaHydro", "BalancingAreaThermal", "Battery", "Bus", "Circuit", "CircuitsSum", "DCLink", "DemandSegment", "Fuel", "FuelConsumption", "FuelContract", "FuelReservoir", "GaugingStation", "GenerationConstraint", "Generic", "Hydro", "Interconnection", "Renewable", "Study", "System", "PowerInjection", "Project", "Thermal"] });
psrioDic.push({  category: "support.function", type: "Entity Aggregation", values: ["SYSTEMS", "THERMAL_PLANTS", "HYDRO_PLANTS", "RENEWABLE_PLANTS", "BATTERIES", "CIRCUITS", "BUSES"] });
psrioDic.push({  category: "support.function", type: "Time Aggregation", values: ["DAILY", "PER_WEEK", "PER_MONTH", "PER_YEAR", "WEEK", "MONTH", "YEAR", "STAGE"] });
psrioDic.push({  category: "support.function", type: "Function Aggregation", values: ["BY_SUM", "BY_AVERAGE", "BY_MAX", "BY_MIN", "BY_CVAR_L", "BY_CVAR_R", "BY_PERCENTILE", "BY_STDDEV", "BY_REPEATING", "BY_FIRST_VALUE", "BY_LAST_VALUE"] });
psrioDic.push({  category: "keyword", type: "Function", values: ["abs", "convert", "lt", "le", "gt", "ge", "eq", "ne", "aggregate_agents", "aggregate_agents", "aggregate_blocks", "aggregate_scenarios", "aggregate_scenarios", "aggregate_stages", "aggregate_stages", "select_agents", "select_agents", "rename_agents", "remove_agents", "select_stages", "select_stages_by_year", "to_block", "to_hour", "max", "min", "ifelse", "concatenate", "save", "save_and_load", "save_and_load"] });


var getFunctionCategory = function(category) {
	var arry = [];
	var listDic = psrioDic.filter(function(d) { return d.category == category; });
	if (listDic && listDic.length > 0) {
		listDic.forEach(function(d) {
			d.values.forEach(function(s) {
				arry.push(s);
			});
		});
	}
	return arry.join('|');
}

var customDic = function(rules, type) {
	if (type == 1) {
		for (var stateName in rules) {
			if (typeof stateName === 'string') {
				var result = getFunctionCategory(stateName);
				if (result) {
					rules[stateName] = rules[stateName] + "|" + result;
				}
			}
		}
	}
	else if (type == 2)
	{
		(rules || []).forEach(function(token) {
		
			if (typeof token.token === 'string') {
				var result = getFunctionCategory(token.token);
				if (result) {
					token.regex = token.regex + "|" + result;
				}
			}
		});
	}
}