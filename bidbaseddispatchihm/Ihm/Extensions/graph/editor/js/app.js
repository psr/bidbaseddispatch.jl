ace.config.set("basePath", "js/");
ace.config.set("workerPath", "js/");
ace.config.set("modePath", "js/");
ace.config.set("themePath", "js/");

//Editor core ----------------
var editor = ace.edit("editor");
editor.setTheme("ace/theme/tomorrow_night_eighties");
editor.session.setMode("ace/mode/lua");
editor.setOptions({
    enableLiveAutocompletion: true,
    enableBasicAutocompletion: true,
    fontSize: 18,
});
editor.focus();
var oldSession = editor.session;
var updateFile = function (content) {
    oldSession.destroy();
    var session = ace.createEditSession(content);
    editor.setSession(session);
    editor.session.setMode("ace/mode/lua");
    editor.setOptions({
        enableLiveAutocompletion: true,
        enableBasicAutocompletion: true,
    });
    oldSession = editor.session;
	undoRedoUpdater();
	contextEditor.showHideEditor(false);
};

const checkSaveFileAndContinue = function(callBackToContinue) {
	const hasChanges = editor.getSession().getUndoManager().hasUndo();
	if (hasChanges) {
		bootbox.confirm({
			message: "Do you want to save changes to '" + currentFile + "' ?", 
			callback: function(result){ 
				if (result) contextEditor.editorControllerFn.saveFile();
				if (callBackToContinue && typeof callBackToContinue === 'function') callBackToContinue();
			}, 
			swapButtonOrder: true,
			buttons: {
				cancel: { label: "Don't Save" },
				confirm: { label: "Save" }
			}
		});
	} else {
		if (callBackToContinue && typeof callBackToContinue === 'function') callBackToContinue();
	}
};


//Autocomplete ----------------
var staticWordCompleter = {
    getCompletions: function (editor, session, pos, prefix, callback) {
        var wordList = [];
        psrioDic.forEach(function (dic) {
            var wordListItem = dic.values.map(function (word) {
                return { caption: word, value: word, meta: dic.type, score: 1000 };
            });
            wordList = wordList.concat(wordListItem);
        });
        callback(null, wordList);
    },
};
var langTools = ace.require("ace/ext/language_tools");
langTools.setCompleters([staticWordCompleter, langTools.textCompleter ]);

//Save ----------------
function downloadRecipe(text, name, type) {
    var a = document.getElementById("aFile");
    var file = new Blob([text], { type: type });
    a.href = URL.createObjectURL(file);
    a.download = name;
    a.click();
}
const flagUseCTRL_S_ToSave = true;

var currentFile = "";
var fileNameVisualUpdater = function () {};
var undoRedoUpdater = function () {};

document.addEventListener(
    "keydown",
    function (e) {
        if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
            e.preventDefault();
            if (flagUseCTRL_S_ToSave) 
                contextEditor.editorControllerFn.saveFile();
            else
                downloadRecipe(editor.session.getValue(), currentFile, "text/plain");
        }
    },
    false
);

var updateNameFile = function (file) {
    currentFile = file;
    if (fileNameVisualUpdater) {
        fileNameVisualUpdater(file);
    }
};

window.callbackObj = window.callbackObj || {};
var editorController = window.callbackObj.editorController || {};

var editorControllerFn = {
    loadFile: function (file) {
        if (editorController.carregarArquivo) {
            updateNameFile(file);
            var contentText = editorController.carregarArquivo(file);
            updateFile(contentText);
        }
    },
    getFiles: function () {
        if (editorController.listarArquivos) {
            return editorController.listarArquivos();
        } else {
            return [];
        }
    },
    saveFile: function () {
        if (editorController.salvarArquivo) {
            if (editorController.salvarArquivo(currentFile, editor.getSession().getValue())) {
                editor.getSession().setUndoManager(new ace.UndoManager());
                undoRedoUpdater();
            }
        }
    },
    getNewFilename: function() {
        if (editorController.obterNomeArquivoNovo) {
            return editorController.obterNomeArquivoNovo();
        }
        return "newRecipe"
    },
    renameFile: function (o, n) {
        if (editorController.renomearArquivo) {
            return editorController.renomearArquivo(o, n);
        }
        return false;
    },
    executeFile: function (parm) {
        if (editorController.executarArquivo) {
            var objReturnString;
            objReturnString = editorController.executarArquivo(currentFile, parm);
            if (objReturnString != "") {
				var objReturn = JSON.parse(objReturnString);

				if (objReturn.retorno && objReturn.retorno != "") {
					if (window.parent && window.parent.log_WriteErrorShow) {
						window.parent.log_WriteErrorShow(objReturn.retorno);
					} else {
						console.error("outer", objReturn.retorno);
					}
				}
				if (objReturn.infoByLines && objReturn.infoByLines.length > 0) {
					editor.getSession().setAnnotations(objReturn.infoByLines);
					editor.gotoLine(objReturn.infoByLines[0].row + 1);
				}
				if (objReturn.typeMessage && objReturn.typeMessage.length > 0) {
					let msgDialog = "";
					objReturn.typeMessage.forEach(function (tpMsg) {
						msgDialog += "<b>[" + tpMsg.type + "]</b> " + tpMsg.message + "<br/>";
					});
					bootbox.dialog({ message : msgDialog });
				}

                return false;
            }
            return true;
        }
        return false;
    },
    help: function () {
		//window.open("http://psr.me/psrio", "_blank");
		if (editorController.abrirAjuda) {
            return editorController.abrirAjuda();
        }
        return false;
    },
};

var treeView = null;
var updateListFiles = function (listFiles) {
    var files = (listFiles || []).map(function (file) {
        return { text: file, type: "file.code" };
    });
    //files[0].state = { 'selected' : true };

    if (treeView) treeView.destroy();

    treeView = $("#treeview").jstree({
        types: {
            folder: { icon: "icon-folder-empty" },
            "folder.open": { icon: "icon-folder-open-empty" },
            "file.code": { icon: "icon-file-code" },
            "file.text": { icon: "icon-doc-text" },
        },
        core: {
            data: [
                {
                    text: "PSRIO-Recipes",
                    state: { opened: true },
                    type: "folder.open",
                    children: files,
                },
            ],
            themes: { responsive: false },
        },
        plugins: ["state", "types", "wholerow"],
    });

    $("#treeview")
        .on("open_node.jstree", function (event, data) {
            data.instance.set_type(data.node, "folder.open");
        })
        .on("close_node.jstree", function (event, data) {
            data.instance.set_type(data.node, "folder");
        })
        .on("changed.jstree", function (e, data) {
            if (data.node && data.node.type && data.node.type === "file.code") {
                checkSaveFileAndContinue(function() {
                    editorControllerFn.loadFile(data.node.text);
                });
            }
        });

    treeView = $("#treeview").data("jstree");
};

var contextEditor = {
    editor: editor,
    toolbarEditor: $("#toolbarEditor"),
    editorControllerFn: editorControllerFn,
};

var controllerEditor = function (contextEditor) {
    contextEditor.showHideEditor = function (s) {
        if (s) {
            $("#fileNameText").hide();
            $("#fileNameEditor").show();
        } else {
            $("#fileNameText").show();
            $("#fileNameEditor").hide();
        }
    };
    if (window.parent) {
        window.parent.fnNotifyReloadListEditor = function() {
			updateList();
		};
    }

    $("#txtFileName").on("click", function () {
        contextEditor.showHideEditor(true);
    });
    $(".controlsEditor").on("click", function () {
        contextEditor.showHideEditor(false);
    });

    var newBtn = contextEditor.toolbarEditor.find(".new");
    var saveBtn = contextEditor.toolbarEditor.find(".save");
    saveBtn[0].disabled = true;
    var undoBtn = contextEditor.toolbarEditor.find(".undo");
    var redoBtn = contextEditor.toolbarEditor.find(".redo");
    var findBtn = contextEditor.toolbarEditor.find(".find");

    var executeBtn = contextEditor.toolbarEditor.find(".execute");
    var zoomInBtn = contextEditor.toolbarEditor.find(".zoomin");
    var zoomOutBtn = contextEditor.toolbarEditor.find(".zoomout");
    var helpBtn = contextEditor.toolbarEditor.find(".help");

    undoBtn.on("click", function () {
        editor.getSession().getUndoManager().undo();
    });

    redoBtn.on("click", function () {
        editor.getSession().getUndoManager().redo();
    });

    findBtn.on("click", function () {
        editor.execCommand("find");
    });

    undoRedoUpdater = function () {
        undoBtn.prop("disabled", !editor.getSession().getUndoManager().canUndo());
        redoBtn.prop("disabled", !editor.getSession().getUndoManager().canRedo());
    };

    editor.on("input", function () {
        undoRedoUpdater();
    });

    const updateList = function () {
        updateListFiles(contextEditor.editorControllerFn.getFiles());
    };

    const newRecipe = function () {
		showEditor();
		updateNameFile(contextEditor.editorControllerFn.getNewFilename());
		updateFile("");
    };

    const showEditor = () => {
        saveBtn[0].disabled = false;
        editor.container.classList.remove("hiddenEditor");
    };

    newBtn.on("click", function () {
        checkSaveFileAndContinue(function() {
			newRecipe();
        });
    });

    saveBtn.on("click", function () {
        contextEditor.editorControllerFn.saveFile();
        updateList();
    });

    executeBtn.on("click", function () {
        contextEditor.editorControllerFn.saveFile();
        var parm = $(this).data("meta") || "";
        contextEditor.editorControllerFn.executeFile(parm);
    });

    zoomInBtn.on("click", function () {
        editor.setFontSize(editor.getFontSize()+1);
    });

    zoomOutBtn.on("click", function () {
        editor.setFontSize(editor.getFontSize()-1);
    });

    helpBtn.on("click", function () {
        contextEditor.editorControllerFn.help();
    });

    //fileName
    fileNameVisualUpdater = function (file) {
        showEditor();
        $("#txtFileName").text(file);
        $("#inputFileName").val(file);
    };
    $(".controlsEditor.ok").on("click", function () {
        var newFile = $("#inputFileName").val();
        var oldFile = currentFile;
        if (contextEditor.editorControllerFn.renameFile(oldFile, newFile)) {
            updateNameFile(newFile);
            updateList();
        } else {
            updateNameFile(currentFile);
        }
    });
    $(".controlsEditor.cancel").on("click", function () {
        updateNameFile(currentFile);
    });

    // init
	newRecipe();
    contextEditor.showHideEditor(false);
    undoBtn.prop("disabled", true);
    redoBtn.prop("disabled", true);
};

controllerEditor(contextEditor);

$(function () {
	const _firstLoad = editorControllerFn.getFiles() || [];
    updateListFiles(_firstLoad);

	if (_firstLoad[0]) //Load first file
		editorControllerFn.loadFile(_firstLoad[0]);
});
