local function volale()
    local hydro = require("collection/hydro");
    return hydro.alert_storage;
end
return volale;