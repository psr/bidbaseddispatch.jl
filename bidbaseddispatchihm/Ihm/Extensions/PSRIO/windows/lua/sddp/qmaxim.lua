local function qmaxim()
    local hydro = require("collection/hydro");
    return hydro.qmax;
end
return qmaxim;