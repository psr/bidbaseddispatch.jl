local function capdcl2()
    local dclink = require("collection/dclink");
    return dclink.capacity_left;
end
return capdcl2;