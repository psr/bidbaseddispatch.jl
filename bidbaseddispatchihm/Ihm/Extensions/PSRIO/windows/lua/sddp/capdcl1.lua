local function capdcl1()
    local dclink = require("collection/dclink");
    return dclink.capacity_right;
end
return capdcl1;