local function qminim()
    local hydro = require("collection/hydro");
    return hydro.qmin;
end
return qminim;