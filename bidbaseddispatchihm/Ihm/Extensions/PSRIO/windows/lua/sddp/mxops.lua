local function mxops()
    local hydro = require("collection/hydro");
    return hydro.vmax_chronological;
end
return mxops;