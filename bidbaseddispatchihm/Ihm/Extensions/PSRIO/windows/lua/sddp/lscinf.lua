local function lscinf()
    local circuitssum = require("collection/circuitssum");
    return circuitssum.lb;
end
return lscinf;