local function cinter2()
    local interconnection = require("collection/interconnection");
    return interconnection.cost_left;
end
return cinter2;