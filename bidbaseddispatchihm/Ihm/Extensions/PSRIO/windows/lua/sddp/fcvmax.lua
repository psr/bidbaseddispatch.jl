local function fcvmax()
    local fuelcontract = require("collection/fuelcontract");
    return fuelcontract.amount;
end
return fcvmax;