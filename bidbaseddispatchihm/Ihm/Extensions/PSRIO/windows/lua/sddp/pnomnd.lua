local function pnomnd()
    local renewable = require("collection/renewable");
    return renewable.capacity;
end
return pnomnd;