local function capint1()
    local interconnection = require("collection/interconnection");
    return interconnection.capacity_right;
end
return capint1;