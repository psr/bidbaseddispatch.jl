local function pnomtr()
    local thermal = require("collection/thermal");
    return thermal.capacity;
end
return pnomtr;