local function pnomhd()
    local hydro = require("collection/hydro");
    return hydro.capacity;
end
return pnomhd;