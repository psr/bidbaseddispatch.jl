# Changelog

## [0.10.0] - 2022-02-07
`9723ff15226b14b660a6e46157972b04a0463353`

### Added
- improved error messages
- linear domain axis chart (stage_type = 0)
- `chart:add_heatmap(exp)` and `chart:save(filename, title)`
- `dashboard:set_icon(string)` based on https://feathericons.com
- `collection:cloudname()`
- `collection:get_directories(regex)` and `collection:get_directories(subpath, regex)`
- `exp:select_agent(index)` and `exp:select_agent(label)`
- `exp:set_stage_type(int)`
- `exp:save(filename, {fast_csv = true})` and `exp:save(filename, {bin = true})`
- `exp:add_prefix(string)`
- `exp:rename_agent(string)`
- `exp:has_agent(label)`
- `exp:stages_to_agents()` and `exp:stages_to_agents(selected)`
- `PSR.sleep(seconds)` `PSR.version()`
- `info(table)`
- all aggregation functions in `exp:to_block(function)`

### Syntax Changes
- `study:load_table(filename)` no longer assume the extension is a .csv
- `exp:rename_agents_with_suffix(string)` to `exp:add_suffix(string)`

### Fixed
- `exp:select_scenarios({int, ...})` when the first stage is different than 1
- `chart:add_area_range(exp, exp) with data considering leap years

### Removed
- `exp:save(filename, {tmp = true})`

## [0.9.0] - 2022-01-03
`943d55a48dfc62df36d805a7ec74ac897e6a12be`

### Added
- guards in the loading method to protect from psrclasses unknown error 
- `report:add_header(string)`
- `generic:create({value1, value2, ...})` and `generic:create({value1, value2, ...}, "unit")`
- `study:load_table(filename)` and `collection:get_files(regex)`
- `PSR.studies()`
- negative epoch 
- relationship thermal/hydro/renewable plant to network areas

### Syntax Changes
- `add_index_translation` to `PSR.add_index_translation`

### Changed
- improvements in security
- improvements in report classes
- improvements in units classes
- improvements in makefile and psrcloud deploy
- argument `--recipes` can be a directory, and the psrio will search for all *.lua files 

### Fixed
- CVAR aggregation when the values were equal
- error in dependencies mode when saving a not selected output
- error loading an output with upper case filename in linux
- memory constraint on loading large hourly scenarios
- error in BY_SUM(), BY_MIN(), and BY_MAX() when there were no values to aggregate
- rollback to lua 5.4.2 (https://github.com/ThePhD/sol2/issues/1266)

## [0.8.0] - 2021-11-18
`822263c671ef630d0f4342616da2ec2dfb3cd80c`

### Added
- LaTeX math support in markdown (`$$ ... $$`)
- `exp:select_agents(std::vector<std::string>)`
- `exp:replace(exp)`
- `int = study:get_parameter(string)`
- `exp:select_largest_agents(k)` and `exp:select_smallest_agents(k)`
- `study:openings()`
- `exp:sort_agents()`
- `Compare(title, epsilon = 1e-4)`
- `100` max errors in Compare 
- `sddpconfig.dat` reading
- `exp:force_collection(collection)`
- parm reference relationship 
- `--return_errors` flag

### Syntax Changes
- `study.stages` to `study:stages()`
- `study.stages_per_year` to `study:stages_per_year()`
- `study.scenarios` to `study:scenarios()`
- `study:parent_path()` to `study:dirname()`
- `collection:load(filename, true)` to `collection:load_or_error(filename)`

### Changed
- Perfomance improvement in `concatenate(exp1, exp2, ...)`
- Perfomance improvement in `exp:select_agents(query)`

### Fixed
- stderr when the case does not exists
- unit in power binary expression (^)
- study.discount_rate unit
- error using remove_zeros and horizon

-----------------------------

## [0.7.0] - 2021-09-24
`447d7855623022fb9f435cd62424cb6f7c2b81d0`

### Added
- new dashboard structure with markdown support
- mutiple cases support
- output comparison with report files
- lua helpers, e.g. require("lua/spairs")
- `exp:rename_agents_with_suffix(string)`
- `exp:select_block(block)`
- `collection:load(filename, true)` to raise error if the file is not found
- `exp:force_unit(unit)`
- `exp = exp:save_cache()`
- `exp:aggregate_scenarios_stages()`
- `exp:remove_zeros()`
- `exp:aggregate_agents(f, group_size)`
- `exp:uncouple_stages()`
- `study:path()` and `study:parent_path()`
- `exp:set_initial_year(year)`
- `do_string(code)` (meta programming)
- `shift_stages(stages)`
- topology enumerate and `exp:aggregate_topology(f, topology)`
- `info(msg)`, `warning(msg)`, `error(msg)`
- forward and backward reading 

### Changed
- `exp:agents()` return a vector of lua objects
- unit conversion in logical binary expressions

### Fixed
- `exp:aggregate_stages()` in monthly-hourly cases
- missing blocks in binary expression
- eps in logical binary operations (lq, le, gt, ge)
- `exp:to_hour()` and `exp:to_block()` when the input has no blocks
- stderr not showing in the log in release mode
- optgen study loading

-----------------------------

## [0.6.0] - 2021-06-04
`d192cc7e1e07eff3ca57a55877ef967cfeae57e6`

### Added
- interactive mode (REPL)
- psrio-base and `require("<base>")`
- `include("<file>")` with relative directory
- runtime input data loading: `collection:load_vector(attribute, unit)` and `collection:load_parameter(attribute, unit)`
- `exp:select_stages(stage)`, `exp:select_stages_by_year(initial_year, final_year)`, `exp:reset_stages()`
- `exp:select_scenarios(scenario)` and `exp:select_scenarios({scenario})` [per stage]
- `exp:select_agents(query)`
- `exp:round(digits)`
- `exp:force_hourly()`
- `exp:save("<file>", {horizon=true})`
- attributes getters: 
	- `exp:loaded()`, `exp:unit()`, `exp:agents()`, `exp:agents_size()`,`exp:agent(int i)`, `exp:scenarios()`
    - `exp:stage_type()`, `exp:initial_year()`, `exp:initial_stage()`, `exp:stages()`, `exp:week(stage)`, `exp:month(stage)`, `exp:year(stage)`
    - `exp:blocks(int)`, `exp:has_blocks()`, `exp:is_hourly()`, `exp:to_list()`, `exp:to_int_list()` 
- `study:is_hourly()` and `study:is_genesys()` methods
- `pu` unit
- `add_index_translation(...)` and `macro_agents()` functions
- single binary support
- relationship from buses to circuits in `exp:aggregate_agents()`

### Changed
- added files in indice.grf even if it is not in indexdat.fmt
- deleted bin/hdr files in CSV mode

### Fixed
- several fixes of methods with first_stage > 1
- error handling when loading an output with the wrong collection
- error in concatenate when expression agents = 0
- input hourly data loading when there is no hour-block mapping file
- `exp:to_block()` and historical scenarios performance

### Removed
- variable_by_scenario metadata (variable_by_scenario = false is now scenarios = 1)
- `--skip` flag, now it's automatic

-----------------------------

## [0.5.0] - 2021-01-27
`6d0aa50ad9421d8d701ff0ba052d23964badb21f`

### Added
- psrio + lua
- dashboard support
- `--horizon <date>` to change to initial date
- `--model none` to load an empty study

### Changed
- procedural to functional syntax

### Removed
- bison scanner and parser
- verbose 1f, 2f, and 3f (use 1, 2, or 3)

-----------------------------

## [0.4.1] - 2021-01-12
`fcd9af76bc69d63bdd3148aa4a2f9d71dde94ece`

### Added
- first and last stage attributes to all variables and operators
- `select_stages(exp, first_stage, last_stage)`
- `rename_agents(exp, [string, string, ...])`
- argument `--logname <string>` to rename the log files (`psrio.log` and `psrio-psrclasses.log` to `<string>.log` and `<string>-psrclasses.log`)

### Changed
- `crop_stages(exp)` is now `select_stages(exp)`

### Fixed
- `indexdat.fmt` concurrent access

-----------------------------

## [0.4.0] - 2020-12-01
`726c5740f53b462fd3308283983ec4d27578f2cb`

### Changed
- agents selection by order or name: `select_agents(exp, [(int or string), (int or string), ...])`
- scenarios selection parameter in aggregation: `aggregate_scenarios(type, exp, [int, int, int, ...])`

### Removed
- argument `--append`

-----------------------------

## [x.x.x] - xxxx-xx-xx

### Added
### Syntax Changes
### Changed
### Fixed
### Removed