# psrio

## Arithmetic Operators
```cpp
// parentheses 
expression = (expression);

// minus  
expression = -expression; 

// addition 
expression = expression + expression; 

// subtraction 
expression = expression - expression; 

// multiplication 
expression = expression * expression;

// right division 
expression = expression / expression;
```

## Logic Operators
```cpp
// equal to
expression = expression == expression;

// not equal to
expression = expression != expression;

// less-than 
expression = expression < expression;

// less-than-or-equals to
expression = expression <= expression;

// greater-than 
expression = expression > expression;

// greater-than-or-equals to 
expression = expression >= expression;
```

## Binary Expressions Rules
```cpp
expression = expression1 + expression2; 
```


    stage_type1 == stage_type2
    initial_stage1 == initial_stage2
    initial_year1 == initial_year2

### Stages and Scenarios

|           |   |   |
|:---------:|:-:|:-:|
| exp1/exp2 | **1** | **n** |
|     **1**     | 1 | n |
|     **n**     | n | n |


### Blocks / Hours

|             |          |           |          |
|:-----------:|:--------:|:---------:|:--------:|
| exp1 / exp2 | **NONE** | **BLOCK** | **HOUR** |
| **NONE**    | NONE     | BLOCK     | HOUR     |
| **BLOCK**   | BLOCK    | BLOCK     | ***ERROR***  |
| **HOUR**    | HOUR     | ***ERROR***   | HOUR     |

### Units

```cpp
[m3/s] * [MW / [m3/s]] = MW
factor = 1

[GWh] / [hour] = [MW] 
factor = 1000
```

## Mathematical Operators
```cpp
// absolute value
expression = abs(expression);

// power 
expression = pow(expression, expression);

// maximum
expression = max(expression, expression);

// minimum
expression = min(expression, expression);
```

## Conversion Operators
```cpp
// unit converter
expression = convert(expression, "unit");

// block to hour mapping
expression = to_hour(expression);

// hour to block mapping
expression = to_block(expression);
```

## Aggregation Operators
```cpp
// aggregation types
BY_SUM
BY_AVERAGE
BY_MAX
BY_MIN
BY_CVAR_L number
BY_CVAR_R number
BY_PERCENTILE number
BY_STDDEV
```

```cpp
// time aggregation types
PER_YEAR
```

```cpp
// entity aggregation types
SYSTEMS		
THERMAL_PLANTS
HYDRO_PLANTS	
RENEWABLE_PLANTS
BATTERIES			
CIRCUITS
```

```cpp
// block aggregation by 'aggregation_type'
expression = aggregate_blocks(aggregation_type, expression);

// scenarios aggregation by 'aggregation_type'
expression = aggregate_scenarios(aggregation_type, expression);

// stages aggregation by 'aggregation_type'
expression = aggregate_stages(aggregation_type, expression);

// stages aggregation by 'aggregation_type' per 'time_aggregation_type'
expression = aggregate_stages(aggregation_type, time_aggregation_type, expression);
```

```cpp

// agents aggregation by 'aggregation_type'
expression = aggregate_agents(aggregation_type, expression);

// agents aggregation by 'aggregation_type' into 'entity_aggregation_type'
expression = aggregate_agents(aggregation_type, entity_aggregation_type, expression);

```

## Others Operators
```cpp
// if-else ternary conditional
expression = ifelse(expression, expression, expression);

// crop the stages to fit within the study horizon
expression = crop_stages(expression);
```

## Generic Agents Operators
```cpp
// select agent at the 'index' column
expression = select_agent(expression, index);

// concatenate expressions
expression = concatenate(expression, expression);
expression = concatenate(expression, expression, expression);
expression = concatenate(expression, expression, expression, expression);
```

## Save
```cpp
expression = save("name", expression);
expression = save("name", expression, \*OPTIONAL ARGUMENTS*\);

// save optional arguments
force = true | false
remove_zeros = true | false

```

## Recipes Examples 

```cpp
// QMAXIM - Maximum turbined outflow
save("qmaxim", hydro_qmax);
```

```cpp
// GHCMGB - Hydro Gen. x Bus MargCost 
hydro_ghcmgb = hydro_gerhid * bus_cmgbus;
save("ghcmgb", hydro_ghcmgb);
```

```cpp
// CINTE1 - Thermal plt. unit cost - seg. 1
thermal_cinte1 = thermal_cesp1 * (thermal_transport_cost + fuel_cost) + thermal_omcost;
save("cinte1", thermal_cinte1);
```

```cpp
// MNTCIR - Circuit flag
circuit_mntcir = ifelse(circuit_status > 0.5, 0, 1);
save("mntcir", circuit_mntcir);
```

```cpp
// ENAFHD
hydro_enafhd = convert(hydro_inflow * hydro_fprodtac, "GWh");
save("enafhd", hydro_enafhd);
```

```cpp
// USECIR - Circuit loading
circuit_usecir = convert(abs(circuit_cirflw) / circuit_capacity, "%");
save("usecir", circuit_usecir);
```

```cpp
// ENAFLU
enaflu = aggregate_agents(BY_SUM, SYSTEMS, convert(hydro_inflow * hydro_fprodtac, "GWh"));
save("enaflu", enaflu);
```

```cpp
// DEMAND - Load per system - inelastic
system_demand = aggregate_agents(BY_SUM, SYSTEMS, bus_demxba);
save("demand", system_demand);
```

```cpp
// POTHID - Available hydro capacity
hydro_pothid = ifelse(hydro_existing > 0.5, 0, min(hydro_qmax * hydro_fprodt, hydro_capacity_maintenance));
save("pothid", hydro_pothid);
```


```cpp
// CMGDEM
cmgbus_x_demxba = aggregate_agents(BY_SUM, SYSTEMS, bus_cmgbus * bus_demxba);
demand = aggregate_agents(BY_SUM, SYSTEMS, bus_demxba);

system_cmgdem = ifelse(demand == 0, 0, cmgbus_x_demxba / demand);

save("cmgdem", system_cmgdem);
```