var heatmapLayer = new ol.layer.Heatmap({
	source: new ol.source.Vector({ }) 
});
map.addLayer(heatmapLayer);

//var feature1 = new ol.Feature({ geometry: new ol.geom.Point(ol.proj.transform([-61.6333, -15.9402], 'EPSG:4326', 'EPSG:3857')), weight: 1  });
//var feature2 = new ol.Feature({ geometry: new ol.geom.Point(ol.proj.transform([-59.4799, -17.0357], 'EPSG:4326', 'EPSG:3857')), weight: 0.1  });
//
//heatmapLayer.getSource().addFeature(feature1);
//heatmapLayer.getSource().addFeature(feature2);

var sourceHeatmap = heatmapLayer.getSource();
var setPositionHeatmap = function() {
	sourceHeatmap.clear();
	var listFeatures = sourceFeatures.getFeatures();
	listFeatures.forEach(function(featureMarker) {
		var feature = new ol.Feature(new ol.geom.Point( featureMarker.getGeometry().getCoordinates() ));
		feature.setProperties(featureMarker.getProperties().properties, 'properties');
		feature.set("weight", 0);
		sourceHeatmap.addFeature(feature);
	});
}


var resultHeatmap = pv.dataSet.filter(function(rs) { return rs.idResult == "cmgbus"; })[0] || {};
resultHeatmap.getScaleHeatmap = d3.scaleLinear().domain([ resultHeatmap.abs_min , resultHeatmap.abs_max ]).range([0, 1]);

if (resultHeatmap) {
	sourceHeatmap.getFeatures().forEach(function(feature) {
		var currentValue = pv.getValueLineSelected(resultHeatmap.idResult, feature.getProperties(), true);
		currentValue = resultHeatmap.getScaleHeatmap( currentValue );
		feature.set("weight", currentValue);
	});
}

$(document).on('changeStage', function(e, stage) {
	if (resultHeatmap) {
		sourceHeatmap.getFeatures().forEach(function(feature) {
			var currentValue = pv.getValueLineSelected(resultHeatmap.idResult, feature.getProperties(), true);
			currentValue = resultHeatmap.getScaleHeatmap( currentValue );
			feature.set("weight", currentValue);
		});
	}
});