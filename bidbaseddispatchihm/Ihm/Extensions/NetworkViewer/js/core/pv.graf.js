String.prototype.padZero = function (len, c) {
    var s = ''; c = c || '0'; len = (len || 2) - this.length;
    while (s.length < len) s += c;
    return s + this;
};
Number.prototype.padZero = function (len, c) {
    return String(this).padZero(len, c);
};


var defautColors = ["#4e79a7", "#a0cbe8", "#f28e2b", "#ffbe7d", "#59A14F","#8cd17d","#b6992d","#f1ce63", "#499894","#86bcb6","#E15759","#ff9d9a", "#79706e","#BAB0AB","#d37295","#fabfd2", "#b07aa1","#d4a6c8","#9d7660","#d7b5a6"];
	
var createWindows = function(idDiv, widthBox, customClass, customBox, title, appendTo) {
	idDiv = idDiv || 'window_graf_1';
	title = title || "New Window";
	appendTo = appendTo || "#main-container";
	widthBox = widthBox || 450;
	var divWindows = $('<div/>', {
		'id': idDiv,
		'class': 'block ' + customClass
	});
	divWindows.css({
		'position': 'relative',
		'left': 'calc(50% - ' + (widthBox/2) + 'px)',
		'float': 'left',
		'top': '20px',
		'width': widthBox + 'px',
		'z-index': '109',
		'display': 'none',
		'box-shadow': 'rgba(0, 0, 0, 0.4) 0px 2px 10px 0px'
	});
	$('<div class="block-header bg-gray-lighter"> <ul class="block-options"><li><button type="button" data-toggle="block-option" data-action="content_toggle"></button> </li></ul> <h3 class="block-title">' + title + '</h3> </div>').appendTo(divWindows);
	
	if (!customBox) {
		var block = $(' <div class="block-content graf">   </div> ').appendTo(divWindows);
		block.attr('id', idDiv + 'plot');
		block.css({'height': '260px'});
		block.css({'padding': '0'});
	} else {
		customBox.appendTo(divWindows);
	}

	var divWindowsBox = $('<div/>', {
		//'class': 'block'
	});
	divWindows.appendTo(divWindowsBox);
	divWindowsBox.appendTo(appendTo);
	
	$('#' + idDiv).draggable({ cancel: '.block-content.graf' });
	$('#' + idDiv).show();
	//$('#' + idDiv).resizable({
	//	//maxWidth: 320,
	//	minWidth: 450,
	//	minHeight: 260,
	//	resize: function( event, ui ) {
	//		//updateInfoBoxHeight(ui.size.height -38);
	//	}
	//});
}

window.pv = window.pv || {};
pv.dataSet = pv.dataSet || [];


// Formater dataSet
var loadCaseDataSet = function(objData) {
	
	objData = objData || {};
	pv.animation.waitToAnimate = true;
	var requestDataSet = objData.dataSet;
	pv.scope.nStage = objData.nStage || 1;
	pv.scope.dataInHours = objData.dataInHours;
	
	if (pv.scope.dataInHours) {
		var stages = [];
		if (objData.formattedDate) {
			stages = objData.formattedDate;
		} else {
			var tStages = pv.caseInfo.blocksDuration[pv.scope.nStage - 1] || 1;
			var fixedStage = pv.caseInfo.stagesStudy[pv.scope.nStage - 1];
			for (var i = 0; i < tStages; i++) {
				stages.push(fixedStage + ' - ' + ((i + 1) + "").padZero(3, '0'));
			}
		}
		_setStages(stages);
	} else {
		_setStages(objData.newStages || pv.caseInfo.stagesStudy);
	}
	
	requestDataSet.forEach(function (result) {
		result.shapeType = 0;
		result.color = "#ff";
		result.styleShape = 0;
		
		result.min = Number.MAX_VALUE;
		result.max = Number.MIN_VALUE;
		result.abs_min = Number.MAX_VALUE;
		result.abs_max = Number.MIN_VALUE;
		result.agents.forEach(function (agent) {
			result.min = Math.min(result.min, Math.min(...agent.values));
			result.max = Math.max(result.max, Math.max(...agent.values));
			agent.abs_values = agent.values.map(function(val) { return Math.abs(val); });
			result.abs_min = Math.min(result.abs_min, Math.min(...agent.abs_values));
			result.abs_max = Math.max(result.abs_max, Math.max(...agent.abs_values));
		});
	});
	pv.dataSet = requestDataSet || [];

	pv.dataSet.forEach(function(ds) {
		ds.type = (pv.caseMetaData.filter(function(md) { return md.fileName == ds.idResult; })[0] || {}).type;
	});

	pv.cirflw =  pv.dataSet.filter(function (result) { 
		if (pv.caseInfo.typeTestResultFlowName == 1) return (result.idResult == pv.caseInfo.flowLink1);
		if (pv.caseInfo.typeTestResultFlowName == 2) return (result.idResult.startsWith(pv.caseInfo.flowLink1));
		return false;
	})[0];
	if (pv.cirflw) { pv.getPositiveValuesFlowData(pv.cirflw); }
	
	pv.dclink =  pv.dataSet.filter(function (result) { 
		if (pv.caseInfo.typeTestResultFlowName == 1) return (result.idResult == pv.caseInfo.flowLink2);
		if (pv.caseInfo.typeTestResultFlowName == 2) return (result.idResult.startsWith(pv.caseInfo.flowLink2));
		return false;
	})[0];
	if (pv.dclink) { pv.getPositiveValuesFlowData(pv.dclink); }

	enableBtnReload();
	
	pv.createResultFeatures();
	pv.processCharts();
	
	pv.currentIndexStage = 0;
	prepareAnimationMain();
	$(document).trigger("changeStage", pv.currentIndexStage + 1);
	pv.animation.waitToAnimate = false;
}

pv.getPositiveValuesFlowData = function(flowData) {
	flowData.min = Number.MAX_VALUE; flowData.max = Number.MIN_VALUE;
	flowData.agents.forEach(function (agent) {
		agent.inverters = [];
		for (r = 0; r < agent.values.length; r++)
		{
			var positive_value = Math.abs(agent.values[r]);
			agent.inverters.push(agent.values[r] != positive_value);
			agent.values[r] = positive_value;
		}
		flowData.min = Math.min(flowData.min, Math.min(...agent.values));
		flowData.max = Math.max(flowData.max, Math.max(...agent.values));
	});
}
	
pv.getTypeResult = function (idResult) {
	var result =  pv.dataSet.filter(function (result) { return (result.idResult == idResult) })[0] || {};
	return result.type || 0;
}
pv.getValueLineSelected = function (idResult, properties, absValue) {
	var result =  pv.dataSet.filter(function (result) { return (result.idResult == idResult) })[0];
	
	if (result === undefined) return 0; //Result not found or not loaded
	
	var currentAgent = result.agents.filter(function (agent) { return (agent.name == properties.name); })[0];
	if (!currentAgent) return 0; //Agent not match in result
	var value = currentAgent.values[pv.currentIndexStage];
	if (absValue) return Math.abs(value);
	return value;
}
pv.getValueSubagentsSelected = function (idResult, properties) {
	var result =  pv.dataSet.filter(function (result) { return (result.idResult == idResult) })[0];
	if (result === undefined) return []; //Result not found or not loaded
	var currentAgent = result.agents.filter(function (agent) { return (agent.name == properties.name); })[0];
	if (!currentAgent) return []; //Agent not match in result
	var subagentsValues = [];
	(currentAgent.subagents || []).forEach(function(ag) {
		subagentsValues.push({ name: ag.name, value: ag.values[pv.currentIndexStage], customColor: ag.customColor });
	});
	return subagentsValues;
}
		
		
//charts
pv.processCharts = function() {
	//get all selected results
	var arrSelected = pv.userSettings.resultsPreferences.filter(function(r) { return r.selected; }).map(function(r) { return r.idResult; });
	var chartResults = pv.dataSet.filter(function(rs) { return arrSelected.indexOf(rs.idResult) > -1 && rs.type == 1; });
	
	//clear previous charts
	$(".grafBoxClass").remove();
	pv.chartList = [];
	chartResults.forEach(function(rs) {
		var info = pv.caseMetaData.filter(function(md) { return md.fileName == rs.idResult; })[0] || {};
		
		var title = info.name;
		var divId = ("grafBox-" + rs.idResult).replace(/\./g, "");
		createWindows(divId, 450, "grafBoxClass", null, title);
		_plotChart(rs, divId, rs.newUnit || info.unit);

	});

}

var _plotChartMarker = function (pAgent, pTypeResult) {
	let typeName = "Unknown";
	if (pTypeResult == 2) typeName = "Bus";
	if (pTypeResult == 3) typeName = "Circuit";
	
	let resultsByType = (pv.dataSet || []).filter(function(ds) { return ds.type == pTypeResult; });
	if ((resultsByType || []).length === 0) {
		$.notify({ message: "No results found for: " + typeName, status: "warning", timeout: 2000});
		return;
	}
	
	let title = typeName + ": " + pAgent;
	let result = { agents: [] };
	resultsByType.forEach(function(ds, i) { 
		let valuesAgentResult = ds.agents.filter(function(d) { return d.name == pAgent; })[0];
		if (!valuesAgentResult) {
			$.notify({ message: "The agent was not found in the results: " + pAgent, status: "warning", timeout: 2000});
			return;
		}
		var info = pv.caseMetaData.filter(function(md) { return md.fileName == ds.idResult; })[0] || {};
		let dataResult = {
			name: info.name,
			values: valuesAgentResult.values,
			unit: ds.newUnit || info.unit
		};
		result.agents.push(dataResult);
	});
	if (result.agents.length === 0) return;
	
	result.unit = result.agents[0].unit;
	
	var divId = ("grafBox-" + pAgent + pTypeResult).replace(/\./g, "").replace(/\s/g, '_');
	createWindows(divId, 500, "grafBoxClass", null, title);
	_plotChart(result, divId, result.unit);
}

var _plotChart = function(result, divId, yLabel) {

	if ((result.agents || []).length > 0)
	{
		var data = [];
		result.agents.forEach(function(agt, i) {
			var trace = { mode: 'lines', type: 'scatter' };
			trace.marker = trace.marker || {};
			trace.marker.color = defautColors[i];
			trace.x = pv.stages;
			trace.y = agt.values;
			trace.name = agt.name;
			data.push(trace);
		});

		var layout = {
			margin: { l: 60, r: 30, b: 50, t: 20, pad: 4 },
			xaxis: { 
				type: 'category',
				automargin: true,
				showticklabels: true,
				nticks: 5,
			},
			yaxis: { title: yLabel }, //'MW'
			shapes: [],
			legend: { x: 0.5, y: 1.2, orientation: 'h'},
			showlegend: true
		};
		var defaultPlotlyConfiguration = { modeBarButtonsToRemove: ['sendDataToCloud', 'hoverClosestCartesian', 'hoverCompareCartesian', 'zoom2d', 'pan2d', 'select2d', 'lasso2d', 'zoomIn2d', 'zoomOut2d', 'autoScale2d', 'resetScale2d', 'toggleSpikelines'], displaylogo: false, showTips: true, displayModeBar: false };

		Plotly.newPlot(divId + "plot", data, layout, defaultPlotlyConfiguration);
		pv.chartList.push({id: divId + "plot", layout: layout});
	}
}

var updateLineIndicator = function () {
	if ((pv.chartList || []).length > 0) {
	  var iPos = pv.currentIndexStage;
	  pv.chartList.forEach(function(objChart) {
		var layout = objChart.layout;
		layout.shapes[0] = 
		  {
			type: 'line',
			x0: pv.stages[iPos],
			y0: 0.05,
			x1: pv.stages[iPos],
			y1: 1,
			yref: 'paper',
			line: {  color: 'grey', width: 1.5, dash: 'dot' }
		  };
		Plotly.relayout(objChart.id, layout);
	  });
	}
}

$(document).on('changeStage', function(e, stage) {
	updateLineIndicator();
});


var enableBtnReload = function() {
	_waitReload = false;
	$("#btnReload>i").removeClass("fa-spin");
	$("#btnReload").removeClass("disabled");
}

var disableBtnReload = function() {
	_waitReload = true;
	$("#btnReload>i").addClass("fa-spin");
	$("#btnReload").addClass("disabled");
}

var updateInputReloadData = function() {
	var checked = $("#aggregatedInput").is(':checked');
	$('#scenarioInput').prop("disabled", checked);
	$('#blockInput').prop("disabled", checked);
	if (!pv.caseInfo.stageInputEditable)
		$('#stageInput').prop("disabled", checked);
}

$("#aggregatedInput").on('change', updateInputReloadData);

var setCaseInfo = function(caseInfo) {
	pv.caseInfo = caseInfo || {};
	pv.caseInfo.stagesStudy = caseInfo.stagesStudy || [];
	
	pv.caseInfo.flowLink1 = caseInfo.flowLink1 || "cirflw";
	pv.caseInfo.flowLink2 = caseInfo.flowLink2 || "dclink";
    pv.caseInfo.typeTestResultFlowName = caseInfo.typeTestResultFlowName || 1; // 1 - equal; 2 - start with
	pv.caseInfo.stageInputEditable = caseInfo.stageInputEditable || false;
	 
	_setStages(pv.caseInfo.stagesStudy);

	if (pv.caseInfo.flagHourly)
	{
		$.each(pv.caseInfo.stagesStudy, function (i, item) {
			$('#stageInput').append(
				$('<option>', {  value: i, text : item  })
			);
		});
		$('#boxBlock').hide();
		$('#boxStage').show();
	} else {
		$('#boxStage').hide();
		$('#boxBlock').show();
	}

	// UI Settings
	$("#scenarioInput")[0].max = pv.caseInfo.series || 1;
	$("#blockInput")[0].max = pv.caseInfo.blocks || 1;
	$('#scenarioInput').val(1);
	$('#blockInput').val(1);
	$('#aggregatedInput').prop('checked', true);
	updateInputReloadData();
}
var restoreLastConfig = function(obj) {
	
	$('#scenarioInput').val(obj["serie"] || 1);
	$('#blockInput').val(obj["block"] || 1);
	$('#aggregatedInput').prop('checked', obj["aggregated"]);
	if (pv.caseInfo.flagHourly)
	{
		$('#stageInput').val((obj["nStage"] || 1) - 1)
	}
	updateInputReloadData();
}


var _waitReload = false;
var _reloadFn = function (e) {
	if (!_waitReload) {
		disableBtnReload();
		var arrSelected = pv.userSettings.resultsPreferences.filter(function(r) { return r.selected; }).map(function(r) { return r.idResult; });
		
		var obj = {};
		obj["results"] = arrSelected || [];
		obj["aggregated"] = $("#aggregatedInput").is(':checked');
		obj["serie"] = $('#scenarioInput').val() || 1;
		obj["block"] = $('#blockInput').val() || 1;
		obj["convert"] = pv.userSettings.options.optConvertGWhToMW;
		
		if (pv.caseInfo.flagHourly)
			obj["nStage"] = +($('#stageInput').val() || 0) + 1;
		
		if (typeof callbackObj !== 'undefined') {
			setTimeout(function() {
				callbackObj.requestReloadData(JSON.stringify(obj));
			}, 100);
		}
	}
	return false;
}
$("#btnReload").on('click', _reloadFn);