

//enums
//dataObj.type
// 0 - register
// 1 - result
// 2 - fixed
//-----
//colorCfg.type
// 0 - Ramp
// 1 - Categorical
// 2 - Fixed


var currentConfigColorPref = "colorPrefLink1";
pv.userSettings = pv.userSettings || {};
pv.userSettings.colorPrefLink1Index = 0;
pv.userSettings.colorPrefLink2Index = 0;
pv.userSettings.objColorPrefLink = {};
var colorLineDisconnected = "#BABFC4";
// 

var setColorPrefLink1 = function(vetColors) {
	pv.userSettings.objColorPrefLink["colorPrefLink1"] = vetColors;
}
var setColorPrefLink2 = function(vetColors) {
	pv.userSettings.objColorPrefLink["colorPrefLink2"] = vetColors;
}

var objColorsPref_teste = [
	{
		key: "Volt",
		dataText: "circuit.volt [Register]",
		dataObj: { type: 1, typeText: 'register', prop: 'flow' },
		colorCfg: {
			list: [ { color: '#293537', value: 800 }, { color: '#9d7660', value: 700 }, { color: '#873435', value: 600 }, { color: '#E15759', value: 500 }, { color: '#b07aa1', value: 400 }, { color: '#4e79a7', value: 300 }, { color: '#59A14F', value: 200 }, { color: '#f28e2b', value: 100 }, { color: '#b6992d', value:  30 }, { color: '#86bcb6', value:  10 }, { color: '#BAB0AB', value:   0 } ],
			type: 1,
			typeText: "Categorical"
		}
	},
	{
		key: "Circuit flow",
		dataText: "result: usecir [Stage]",
		dataObj: { type: 0, typeText: 'result', prop: 'usecir' },
		colorCfg: {
			list: [ { color: '#FF0000', value: 100 }, { color: '#FFF000', value: 0 } ],
			type: 0,
			typeText: "Ramp"
		}
	},
	{
		key: "Fixed",
		dataText: "none [Fixed]",
		dataObj: { type: 2, typeText: 'fixed', prop: null },
		colorCfg: {
			list:  [{ color: '#21918C', value: 0 } ],
			type: 2,
			typeText: "Fixed"
		}
	}
];

var objColorsPrefDefaults = [
	{
		key: "Fixed",
		dataText: "none [Fixed]",
		dataObj: { type: 2, typeLabel: 'fixed', prop: null },
		colorCfg: {
			list:  [{ color: '#4e79a7', value: 0 } ],
			type: 2,
			typeText: "Fixed"
		}
	},
	{
		key: "Fixed",
		dataText: "none [Fixed]",
		dataObj: { type: 2, typeLabel: 'fixed', prop: null },
		colorCfg: {
			list:  [{ color: '#21918C', value: 0 } ],
			type: 2,
			typeText: "Fixed"
		}
	}
];

var updateStyleLink = function() {
	
	var objColorPref1 = pv.userSettings.objColorPrefLink["colorPrefLink1"][pv.userSettings.colorPrefLink1Index];
	var _lineStyleFunction1 = getfnColorPrefLineStyle(objColorPref1);
	layerLines.setStyle(_lineStyleFunction1);

	var objColorPref2 = pv.userSettings.objColorPrefLink["colorPrefLink2"][pv.userSettings.colorPrefLink2Index];
	var _lineStyleFunction2 = getfnColorPrefLineStyle(objColorPref2);
	layerLines2.setStyle(_lineStyleFunction2)

}

var getfnColorConfig = function(colorPref) {

	var vetColorValue = colorPref.colorCfg.list;
	if (colorPref.colorCfg.type == 0) { //Ramp
		var fnColor = d3.scaleLinear() 
			.domain(colorPref.colorCfg.list.map(function(l) { return l.value; }))
			.range(colorPref.colorCfg.list.map(function(l) { return l.color; }))
			.interpolate(d3.interpolateHcl); 
		return fnColor;
	}
	if (colorPref.colorCfg.type == 1) { //Categorical 
		return function(value) {
			var _color = vetColorValue[vetColorValue.length-1].color || '#00a54f';
			for (var iV = vetColorValue.length - 1; iV >= 0; iV--) {
				if (value >= vetColorValue[iV].value)
					_color = vetColorValue[iV].color;
			}
			return _color;
		}
	}
	//if (colorPref.colorCfg.type == 2) { //Fixed - Default
		return function(value) {
			return vetColorValue[vetColorValue.length-1].color || '#00a54f';
		}
	//}
}

var _labelDefaultLine = ""; // ["", "name", "code"]
var showHideLineLabel = function (v) {
	_labelDefaultLine = v
	layerLines.changed();
	layerLines2.changed();
}

pv.showLineLabel = false;
var getLineText = function(feature, resolution) {
	var text = "";
	if (_labelDefaultLine != "") {
		var text = feature.get('properties')[_labelDefaultLine];
		if (resolution > maxResolution && !pv.graph.diagramModeActive) {
			text = "";
		} else {
			text = ("" + text).trunc(12).toUpperCase();
		}
	}
	return text;
};

var getStyleBaseLine = function (_width, _color, _lineDash, feature, resolution) {
	return new ol.style.Style({ 
			stroke: new ol.style.Stroke({ width: _width, color: _color, lineDash: _lineDash ? [5, 5] : null }),
			text: new ol.style.Text({
				font: 'bold 11px "Open Sans", "Arial Unicode MS", "sans-serif"',
				placement: 'line',
				fill: new ol.style.Fill( {color: '#555'} ),
				stroke: new ol.style.Stroke({color: 'rgba(255, 255, 255, 0.9)', width: 1.8}),
				text: getLineText(feature, resolution)
			}) 
		 });
}

var getCurrentStage = function() {
	let _currentStage = pv.currentIndexStage || 0;
	if (pv.scope.dataInHours) {
		_currentStage = pv.scope.nStage - 1;
	}
	return _currentStage;
}


var getfnColorPrefLineStyle = function(colorPref) {

	colorPref = JSON.parse(JSON.stringify(colorPref)); //clone
	var fnColorConfig = getfnColorConfig(colorPref);
	
	if (colorPref.dataObj.type == 1) { //result 
		var resultName = colorPref.dataObj.prop;
		if ((pv.caseInfo || {}).typeTestResultFlowName == 2) {
			resultName = (pv.caseMetaData.filter(function (md) { return md.fileName.startsWith(resultName); })[0] || {}).fileName || resultName;
		}
		return function(feature, resolution) {
			let _currentStage = getCurrentStage();
			let _width = 2.5;
			let properties = feature.getProperties().properties;
			let valueDouble = pv.getValueLineSelected(resultName, properties);
			let _color = fnColorConfig(valueDouble);
			let valConnected = (feature.get('properties')["connected"] || [])[_currentStage] || 0;
			_color = (valConnected != 1) ? _color : colorLineDisconnected;
			let _lineDash = ((feature.get('properties').existingAtStage || 1) - 1) > _currentStage;
			let styleReturn = getStyleBaseLine(_width, _color, _lineDash, feature, resolution);
			return styleReturn;
		}
	}	
	if (colorPref.dataObj.type == 2) { //fixed 
		return function(feature, resolution) {
			let _currentStage = getCurrentStage();
			let _width = 2.5;
			let _color = fnColorConfig(0);
			let valConnected = (feature.get('properties')["connected"] || [])[_currentStage] || 0;
			_color = (valConnected != 1) ? _color : colorLineDisconnected;
			let _lineDash = ((feature.get('properties').existingAtStage || 1) - 1) > _currentStage;
			let styleReturn = getStyleBaseLine(_width, _color, _lineDash, feature, resolution);
			return styleReturn;
		}
	}
	if (colorPref.dataObj.type == 0) { //register
		return function(feature, resolution) {
			let _currentStage = getCurrentStage();
			let _width = 2.5;
			let _color;
			_color = fnColorConfig(feature.get('properties')[colorPref.dataObj.prop]);
			let valConnected = (feature.get('properties')["connected"] || [])[_currentStage] || 0;
			_color = (valConnected != 1) ? _color : colorLineDisconnected;
			let _lineDash = ((feature.get('properties').existingAtStage || 1) - 1) > _currentStage;
			let styleReturn = getStyleBaseLine(_width, _color, _lineDash, feature, resolution);
			return styleReturn;
		}
	}
	return lineStyleFunction; //default
}



var loadDataPref = function () {
	closeColorPref(); //close old editors
	$('.colorSelectorListItem.sp-container').remove(); // remove olds colorpickers
	endAddItemColorPref();
	
	var idSel = +($('#cbColorRamp').val());
	var objColorsPref = pv.userSettings.objColorPrefLink[currentConfigColorPref];
	var objPref = objColorsPref[idSel];
	
	pv.userSettings[currentConfigColorPref + "Index"] = idSel;
	updateStyleLink();
	
	openEditorColorPref = function () {
		objPref = JSON.parse(JSON.stringify(objPref)); //clone
			
		$('#txtColorResult').val(objPref.dataText);
		$("#cbColorStyle").val(objPref.colorCfg.type).trigger('change');

		$('#table-color-list').html('');
		
		for (iI = 0; iI < objPref.colorCfg.list.length; iI++) {
			
			var sVal = objPref.colorCfg.list[iI].value;
			var sCor = objPref.colorCfg.list[iI].color;
					
			var rowItem = $('<tr>');
			
			var cellItem1 = $('<td>', { 'css': { 'width': '25%' }  } );
			var cellItem2 = $('<td>', { 'css': { 'width': '25%' }  } );
			var cellItem3 = $('<td>', { 'css': { 'width': '25%', 'text-align': '-webkit-center' }  } );
			var cellItem4 = $('<td>', { 'css': { 'width': '25%' }  } );
			rowItem.append(cellItem1);
			rowItem.append(cellItem2);
			rowItem.append(cellItem3);
			rowItem.append(cellItem4);

			cellItem1.text(iI);
			cellItem2.text(sVal);
			cellItem3.append($('<input>', { 'class': 'form-control input-sm colorselectorList input-color-picker',
				'type': 'color',
				'value': sCor,
				'data-indexcolor': iI,
				'data-container-class-name': 'colorSelectorListItem',
				'data-type': 'color',
				'data-show-alpha': 'false',
				'data-show-input': 'true',
				'data-preferred-format': 'hex',
				'data-show-initial': 'false',
				'data-show-palette': 'false',
				'data-show-buttons': 'true',
				'data-allow-empty': 'false'
			}));

			var btnRemove = $('<button>', { 
				'class': 'btn btn-sm btn-default btnRemove', 
				'data-indexcolor': iI, 
				'data-itemvalue': sVal, 
				'type': 'button' });
			var iconRemove = $('<span>', { 'class': 'fa fa-minus' });
			btnRemove.append(iconRemove);
			cellItem4.append(btnRemove);
				
			$('#table-color-list').append(rowItem);
		}
		$('.colorselectorList').spectrum({
			chooseText: "Ok",
			cancelText: "Cancel"
		});
		
		$('.btnRemove').click(function(e) {
			e.stopPropagation();
			var el = $(this);
			var row = el.parent().parent();
			row.hide("highlight", {}, 250, function() { 
				//var sIndex = el.data('indexcolor');
				var sIndex = objPref.colorCfg.list.findIndex(function(element, index, array) { return element.value == el.data('itemvalue'); });
				objPref.colorCfg.list.splice(sIndex, 1);

				row.remove(); 
			});
			return false;
		});
		$('.colorselectorList').change(function() {
			var el = $(this);
			var row = el.parent().parent();
			var sIndex = el.data('indexcolor');
			var sCor = el.val();
			objPref.colorCfg.list[sIndex].color = sCor;
			//updateCbPref();
			return false;
		});
		newItemColorPref = function ()  {
			var maxValue = Math.max.apply(Math, objPref.colorCfg.list.map(function(o) { return o.value; })) || 0;
			maxValue = Math.max(maxValue, 0);
			$('.inputValueColorPref').val(maxValue + 10);
			$('.inputColorColorPref').spectrum("set", "#0000FF");
		};
		saveItemColorPref = function ()  {
			objPref.colorCfg.list.push({
				color: $('.inputColorColorPref').val() || "#0000FF",
				value: +$('.inputValueColorPref').val() || 0
			});
			objPref.colorCfg.list = objPref.colorCfg.list.sort(function(a, b) { return (a.value < b.value) ? 1 : -1; });
			openEditorColorPref();
		};
		restoreCurrentColorPref = function ()  {
			var objColorsPref = pv.userSettings.objColorPrefLink[currentConfigColorPref];
			objPref = objColorsPref[idSel];
			updateCbPref();
		};
		saveCurrentColorPref = function() {
			var objColorsPref = pv.userSettings.objColorPrefLink[currentConfigColorPref];
			objColorsPref[idSel] = objPref;
			updateCbPref();
			updateStyleLink();
		};
	}
}

var updateCbPref = function() {
	$("#cbColorRamp").select2({ 
		minimumResultsForSearch: -1,
		templateResult: function(el) { if (!el.id) return el; 
			return fnGetDivCanvasSelect2(el, 0, {'width': '50%', 'text-overflow': 'ellipsis', 'overflow': 'hidden', 'white-space': 'nowrap'} );
		},
		templateSelection: function(el) { if (!el.id) return el; 
			return fnGetDivCanvasSelect2(el, "8px -4px", { 'width': '50%', 'text-overflow': 'ellipsis', 'overflow': 'hidden', 'white-space': 'nowrap', 'line-height': '20px', 'margin-top': '5px'} );
		}
	});
}

var openCloseColorPref = function() {
	$('.colorList').toggleClass('divReadonly');
	$('.btnEdit').toggleClass('activeBtn');
	
	if ($('.btnEdit').hasClass('activeBtn')) {
		openEditorColorPref();
	}
}

var closeColorPref = function() {
	if (!$('.colorList').hasClass('divReadonly')) 
		$('.colorList').addClass('divReadonly');
	if ($('.btnEdit').hasClass('activeBtn')) 
		$('.btnEdit').removeClass('activeBtn');
	endAddItemColorPref();
}

var endAddItemColorPref = function() {
	if ($('.headerColorPref').hasClass('hide')) 
		$('.headerColorPref').removeClass('hide');
	if (!$('.addItemColorPref').hasClass('hide')) 
		$('.addItemColorPref').addClass('hide');
}
var initAddItemColorPref = function() {
	if ($('.addItemColorPref').hasClass('hide')) 
		$('.addItemColorPref').removeClass('hide');
	if (!$('.headerColorPref').hasClass('hide')) 
		$('.headerColorPref').addClass('hide');
}


var styleHasLoaded = false;
var restoreCurrentColorPref = function () { }
var saveCurrentColorPref = function () { }
var openEditorColorPref = function () { }
var newItemColorPref = function() { }
var saveItemColorPref = function() { }

var createColorPrefBox = function () {
	if (!styleHasLoaded) {
		$(".btnEdit").click(function() {
			openCloseColorPref();
			return false;
		});
		$('.btnAddItemColorPref').click(function(e) {
			e.stopPropagation();
			initAddItemColorPref();
			newItemColorPref();
			return false;
		});
		$('.btnCancelAddItemColorPref').click(function(e) {
			e.stopPropagation();
			endAddItemColorPref();
			return false;
		});
		$('.btnSaveAddItemColorPref').click(function(e) {
			e.stopPropagation();
			endAddItemColorPref();
			saveItemColorPref();
			return false;
		});
		$('.btnCancelColorPref').click(function(e) {
			e.stopPropagation();
			restoreCurrentColorPref();
			openCloseColorPref();
			return false;
		});
		$('.inputColorColorPref').spectrum({
			chooseText: "Ok",
			cancelText: "Cancel"
		});
		$('.btnSaveColorPref').click(function(e) {
			e.stopPropagation();
			saveCurrentColorPref();
			openCloseColorPref();
			return false;
		});
		$('#style-line-modal').on('hidden.bs.modal', function () {
			restoreCurrentColorPref();
		});
		
		$("#cbColorStyle").select2({ minimumResultsForSearch: -1 });
		styleHasLoaded = true;
	}
	$('#cbColorRamp').html('');
	var objColorsPref = pv.userSettings.objColorPrefLink[currentConfigColorPref];
	var selectedIndex = pv.userSettings[currentConfigColorPref + 'Index'] || 0;
	
	$.each(objColorsPref, function (i, item) {
		$('#cbColorRamp').append($('<option>', {
			value: i,
			text: item.key,
			selected: selectedIndex == i
		}));
	});
	updateCbPref();
	$("#cbColorRamp").change(loadDataPref);
}

$('#btnStyleLine1').click(function(e) {
	e.stopPropagation();
	currentConfigColorPref = "colorPrefLink1";
	createColorPrefBox();
	loadDataPref();
	$('#style-line-modal').modal({
            backdrop: 'static',
            keyboard: false
        });
	return false;
});

$('#btnStyleLine2').click(function(e) {
	e.stopPropagation();
	currentConfigColorPref = "colorPrefLink2";
	createColorPrefBox();
	loadDataPref();
	$('#style-line-modal').modal('show');
	return false;
});
function fnGetDivCanvasSelect2(el, marginCanvas, cssSpan) {
	var divItem = $('<div/>', { css: { 'display': 'flex'} });
	var spanLabel = $('<span/>', { css: cssSpan });
	spanLabel.text(el.text);
	divItem.append(spanLabel);
	var objColorsPref = pv.userSettings.objColorPrefLink[currentConfigColorPref];
	var objPref = objColorsPref[el.id];
	var arrColors = objPref.colorCfg.list.map(function(obj) { return obj.color; });
	var objColor = {
		fnColor: getColorInterpolate(arrColors),
		count: objPref.colorCfg.type == 1 ? objPref.colorCfg.list.length : 10 
	};
	var canvas = ramp(objColor.fnColor, objColor.count);
	canvas.style.margin = marginCanvas;
	divItem.append($(canvas));
	return divItem; 
}
function getColorInterpolate(arrColors) {
	var arrDomain = [0];
	arrColors = arrColors || [];
	if (arrColors.length > 1) {
	  for (let i = 1; i < arrColors.length; ++i) {
		arrDomain.push(i/(arrColors.length-1));
	  }
	}
	var fnColor = d3.scaleLinear() 
			.domain(arrDomain)
			.range(arrColors.reverse())
			.interpolate(d3.interpolateHcl); //interpolateHsl interpolateHcl interpolateRgb 
	return fnColor;
}
function newCanvas (width, height) {
	var canvas = document.createElement("canvas");
	canvas.height = height || 1;
	canvas.width = width || 1;
	return canvas;
}
function ramp(color, n = 512) {
  const canvas = newCanvas(n, 1);
  const context = canvas.getContext("2d");
  canvas.style.margin = 0;
  canvas.style.width = "50%";
  canvas.style.height = "15px";
  canvas.style.imageRendering = "-moz-crisp-edges";
  canvas.style.imageRendering = "pixelated";
  for (let i = 0; i < n; ++i) {
    context.fillStyle = color(i / (n - 1));
	 context.fillRect(i, 0, 1, 1);
  }
  return canvas;
}


setColorPrefLink1([
	{
		key: "Volt",
		dataText: "circuit.volt [Register]",
		dataObj: { type: 0, typeText: 'register', prop: 'voltage' },
		colorCfg: {
			list: [ { color: '#293537', value: 800 }, { color: '#9d7660', value: 700 }, { color: '#873435', value: 600 }, { color: '#E15759', value: 500 }, { color: '#b07aa1', value: 400 }, { color: '#4e79a7', value: 300 }, { color: '#59A14F', value: 200 }, { color: '#f28e2b', value: 100 }, { color: '#b6992d', value:  30 }, { color: '#86bcb6', value:  10 }, { color: '#BAB0AB', value:   0 } ],
			type: 1,
			typeText: "Categorical"
		}
	},
	{
		key: "Circuit flow",
		dataText: "result: usecir [Stage]",
		dataObj: { type: 1, typeText: 'result', prop: 'usecir' },
		colorCfg: {
			list: [ { color: '#FF0000', value: 100 }, { color: '#FFF000', value: 0 } ],
			type: 0,
			typeText: "Ramp"
		}
	},
	{
		key: "Fixed",
		dataText: "none [Fixed]",
		dataObj: { type: 2, typeText: 'fixed', prop: null },
		colorCfg: {
			list:  [{ color: '#21918C', value: 0 } ],
			type: 2,
			typeText: "Fixed"
		}
	}
]);
setColorPrefLink2([
	{
		key: "Fixed",
		dataText: "none [Fixed]",
		dataObj: { type: 2, typeText: 'fixed', prop: null },
		colorCfg: {
			list:  [{ color: '#59A14F', value: 0 } ],
			type: 2,
			typeText: "Fixed"
		}
	}
]);
updateStyleLink();