/*
*  Copyright (c) 2016 The WebRTC project authors. All Rights Reserved.
*
*  Use of this source code is governed by a BSD-style license
*  that can be found in the LICENSE file in the root of the source
*  tree.
*/

'use strict';

function handleSourceOpen(event) {
  //console.log('MediaSource opened');
  sourceBuffer = mediaSource.addSourceBuffer('video/webm; codecs="vp8"');
  //console.log('Source buffer: ', sourceBuffer);
}

function handleDataAvailable(event) {
  if (event.data && event.data.size > 0) {
    recordedBlobs.push(event.data);
  }
}

function handleStop(event) {
  //console.log('Recorder stopped: ', event);
  var superBuffer = new Blob(recordedBlobs, {type: 'video/webm'});
  video.src = window.URL.createObjectURL(superBuffer);
}

function toggleRecording() {
  if (recordButton.textContent === 'Start Recording') {
    startRecording();
  } else {
    stopRecording();
    recordButton.textContent = 'Start Recording';
    playButton.disabled = false;
    //downloadButton.disabled = false;
  }
}

// The nested try blocks will be simplified when Chrome 47 moves to Stable
function startRecording() {
  var options = {mimeType: 'video/webm'};
  recordedBlobs = [];
  try {
    mediaRecorder = new MediaRecorder(stream, options);
  } catch (e0) {
    //console.log('Unable to create MediaRecorder with options Object: ', e0);
    try {
      options = {mimeType: 'video/webm,codecs=vp9'};
      mediaRecorder = new MediaRecorder(stream, options);
    } catch (e1) {
      //console.log('Unable to create MediaRecorder with options Object: ', e1);
      try {
        options = 'video/vp8'; // Chrome 47
        mediaRecorder = new MediaRecorder(stream, options);
      } catch (e2) {
        alert('MediaRecorder is not supported by this browser.\n\n' +
          'Try Firefox 29 or later, or Chrome 47 or later, ' +
          'with Enable experimental Web Platform features enabled from chrome://flags.');
        console.error('Exception while creating MediaRecorder:', e2);
        return;
      }
    }
  }
  //console.log('Created MediaRecorder', mediaRecorder, 'with options', options);
  //recordButton.textContent = 'Stop Recording';
  //playButton.disabled = true;
  //downloadButton.disabled = true;
  mediaRecorder.onstop = handleStop;
  mediaRecorder.ondataavailable = handleDataAvailable;
  mediaRecorder.start(100); // collect 100ms of data
  //console.log('MediaRecorder started', mediaRecorder);
}

function stopRecording() {
  mediaRecorder.stop();
  //console.log('Recorded Blobs: ', recordedBlobs);
  video.controls = true;
  
  //showPreview();
}

function play() {
  video.play();
}

function download() {
  var blob = new Blob(recordedBlobs, {type: 'video/webm'});
  var url = window.URL.createObjectURL(blob);
  var a = document.createElement('a');
  a.style.display = 'none';
  a.href = url;
  a.download = 'pv3_' + moment().format('YYYYMMDD_HHmm') + '.webm';
  document.body.appendChild(a);
  a.click();
  //console.log("download Capture.");
  setTimeout(function () {
    document.body.removeChild(a);
    window.URL.revokeObjectURL(url);
  }, 100);
}

	
$("#btnRecorderRec").on('click', function (e) {
	$("#infoRec").text("00:59").addClass("rec");
	$("#btnRecorderRec").addClass("hide");
	$("#btnRecorderSave").addClass("hide");
	$("#btnRecorderStop").removeClass("hide");
	
	initializeCapture();
	initDuplicate();
	initRecorderCountDown();
	startRecording();
	
});
var recorderStopFunction = function() {
	clearInterval(duplicateInterval);
	clearInterval(recorderInterval);
	$("#btnRecorderStop").addClass("hide");
	$("#btnRecorderRec").addClass("hide");
	$("#btnRecorderSave").removeClass("hide");
	recorderCountDownInt = 59;
	$("#infoRec").text("SAVE").removeClass("rec");
			
	stopRecording();
}
$("#btnRecorderStop").on('click', function (e) {
	recorderStopFunction();
});
$("#btnRecorderSave").on('click', function (e) {
	
	download();
	
	$("#infoRec").text("REC").removeClass("rec");
	$("#btnRecorderStop").addClass("hide");
	$("#btnRecorderSave").addClass("hide");
	$("#btnRecorderRec").removeClass("hide");
	
	$("#divCapture").remove();
});

var mediaRecorder;
var recordedBlobs;
var sourceBuffer;

var mediaSource = new MediaSource();
mediaSource.addEventListener('sourceopen', handleSourceOpen, false);
var recordButton = document.querySelector('#btnRec');
var playButton = document.querySelector('#btnStop');
var canvas;
var video;
var stream;


var recorderInterval = null;
var recorderCountDownInt = 59;
var initRecorderCountDown = function() {
	clearInterval(recorderInterval);
	recorderInterval = setInterval(function() {
		if (recorderCountDownInt <= 0)
		{
			recorderStopFunction();
		} else {
			recorderCountDownInt--;
			$("#infoRec").text("00:" + String(recorderCountDownInt).padStart(2, '0'));
			//getPreview();
		}
	}, 1000);	
}

var getPreview = function() {
  var _superBuffer = new Blob(recordedBlobs, {type: 'video/webm'});
  video.src = window.URL.createObjectURL(_superBuffer);
  video.currentTime = 60 - recorderCountDownInt;
}

var showPreview = function() {
	var body = $('body')[0];
	var width_div = body.getBoundingClientRect().width * 0.4;
	$("#divCapture").remove();
	createWindows("divCapture", width_div, 'videoCaptureBox', $('<div/>', { id: 'videoCapture', class: 'block-content videoCaptureBox', style: 'padding: 0;' }), "Video Preview");
	$("#divCapture").css({ position: 'fixed' });
	$('#videoCapture').html('<video id="recordedPreview" autoplay="autoplay" playsinline loop width="100%" height="107%" muted="muted"></video>');
	var videoPreview = $('#recordedPreview')[0];
    var _superBuffer = new Blob(recordedBlobs, {type: 'video/webm'});
    videoPreview.src = window.URL.createObjectURL(_superBuffer);
    videoPreview.controls = true;
}


var infoAreasCanvas = [
    //"case: <case_name>",
	//"path: <path_case>"
];
function WriteScaletoCanvas(ctx) {
	let fontSize = 24;
	let font = "bold " + fontSize + "px Courier New";
	if (ctx) {
		ctx.beginPath();
		ctx.textAlign = "left";

		ctx.lineWidth = 3;
		ctx.font = font;
		var infosplitted = [];
		infoAreasCanvas.forEach(function(elem, ind, arr) {
			infosplitted.push(elem);
		});
		infosplitted.push("Current stage: " + pv.stages[pv.currentIndexStage]);

		var leninfosplitted = infosplitted.length;
		ctx.fillStyle = "#000";
		ctx.shadowOffsetX = 2;
		ctx.shadowOffsetY = 2;
		ctx.shadowColor = "rgba(255,255,255,0.8)";
		ctx.shadowBlur = 4;
		infosplitted.forEach(function(elem, ind, arr) {
			ctx.fillText( [elem], 5, (ind + 1) * fontSize + 5 );
		});
	}
}

var ctxCanvas = null;
var duplicateInterval = null;
var canvas1 = null;
var canvas2 = null;
var canvas3 = null;
var initDuplicate = function() {
	clearInterval(duplicateInterval);
	duplicateInterval = setInterval(function() {
		ctxCanvas.drawImage(canvas1, 0, 0);
		ctxCanvas.drawImage(canvas2, 0, 0);
		ctxCanvas.drawImage(canvas3, 0, 0);
		
		// Para exibir info no canto superior esquerdo
		WriteScaletoCanvas(ctxCanvas); 
	}, 150);	
}

var initializeCapture = function() {
	
	//if (setZIndex) setZIndex();
	var _canv = $("<canvas />");
	_canv.attr({ width: $("canvas").width(), height: $("canvas").height() });
	//_canv.width($("canvas").width());
	//_canv.height($("canvas").height());
	canvas = _canv[0];
	ctxCanvas = canvas.getContext('2d');
	canvas1 = $("canvas")[0];
	canvas2 = $("canvas")[1];
	canvas3 = $("canvas")[2];

	video = document.querySelector('video');
	stream = canvas.captureStream();
	
	//console.log("initialized Capture.");
}

