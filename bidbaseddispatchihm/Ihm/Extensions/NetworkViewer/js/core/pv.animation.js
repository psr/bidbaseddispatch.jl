pv.animation = pv.animation || {};
pv.animation.waitToAnimate = false;
pv.animation.pixelLengthSplit = 20;
pv.animation.counterInterval = 5;
pv.animation.pixelLengthSplitRange = d3.scaleLinear().domain([4, 17]).rangeRound([6, 22]);
pv.animation.counterIntervalRange = d3.scaleLinear().domain([4, 17]).rangeRound([4, 12]);

//4
//setAnimationConfig(10, 3);
//17
//setAnimationConfig(20, 10);
//setAnimationConfig(22, 12);

var setAnimationConfig = function(pixelLengthSplit, counterInterval) {
	pv.animation.pixelLengthSplit = pixelLengthSplit;
	pv.animation.counterInterval = Math.max(counterInterval, 2);
	prepareAnimationMain();
}
var prepareAnimationMain = function() {
	prepareAnimation(layerLines);
}


var fnStyleArrow = function(pointStart, pointEnd, scale, color) {

	var _getSVGChevron = function(color)
	{
		return ('data:image/svg+xml;utf8,<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="24pt" height="24pt" xml:space="preserve">' +
		  '<path opacity="0.8" fill="' + color + '" d="M1.609,26.357L1.609,5.643L30.52,16   L1.609,26.357z"></path>' +
		  '</svg>').replace('#', '%23');
	}
	var pRotation = null;

	if (pointStart && pointEnd) {
		var dx = pointEnd[0] - pointStart[0];
		var dy = pointEnd[1] - pointStart[1];
		pRotation = Math.atan2(dy, dx);
	}

	return new ol.style.Style({
		image: new ol.style.Icon(({
		  snapToPixel: false,
		  anchor: [0.5, 0.5],
		  anchorOrigin: 'top-left',
		  offset: [0, 0],
		  scale: scale,
		  opacity: 1,
		  rotateWithView: true,
		  rotation: -pRotation,
		  src: _getSVGChevron(color)
		}))
	});
}

var setAnimationStyle = function(feature, pointA, pointB, scale, color, inverter) {

	var currentStyle = feature.getStyle();
	var pointStart = inverter ? pointB : pointA;
	var pointEnd = inverter ? pointA : pointB;
	scale = scale || 1;
	color = color || '#494a4b';
	
	var newColor = (feature.get("color") || "-") != color;
	if (currentStyle == null || newColor) {
		feature.setStyle(fnStyleArrow(pointStart, pointEnd, scale, color));
		feature.set("color", color);
	} else {
		var image = feature.getStyle().getImage();
		var pRotation = null;
		if (pointStart && pointEnd) {
			var dx = pointEnd[0] - pointStart[0];
			var dy = pointEnd[1] - pointStart[1];
			pRotation = Math.atan2(dy, dx);
		}
		image.setScale(scale);
		if (image.getOpacity() == 0) image.setOpacity(1);
		image.setRotation(-pRotation);
	}
}
var setAnimationInvisible = function (feature) {
	var image = feature.getStyle().getImage();
	image.setOpacity(0);
}

var fid = 0;
var createMarker = function (coord) {
	var feature = new ol.Feature({
		coord: coord,
		geometry: new ol.geom.Point(coord)
	});
	feature.setId(fid);
	var style_marker = [
		new ol.style.Style({
			image: new ol.style.Icon({
				scale: .7, opacity: 1,
				rotateWithView: false, anchor: [0.5, 1],
				anchorXUnits: 'fraction', anchorYUnits: 'fraction',
				src: 'marker.png'
			})
		})
	];
	feature.setStyle(style_marker);
	sourceAnimation.addFeature(feature);
	fid++;
}    
var createLine = function (coord, lineObject) {
	var linestring = new ol.geom.LineString(coord);
	var feature = new ol.Feature({
		geometry: linestring
	});
	var line_style = [
		new ol.style.Style({
			stroke: new ol.style.Stroke({
				color: '#444', width: 2
			})
		}),
	];
	feature.setStyle(line_style);
	feature.set('lineObject', lineObject);
	sourceAnimation.addFeature(feature);
}
var getArrayLineFragments = function (coord, pixelLengthSplit) {
	var linestring = new ol.geom.LineString(coord);
	var coords_interval = [];
	var _getLengthPixelLineString = function(_map, lineString) {
		var coord = linestring.getCoordinates();
		var dist = 0;
		for (i = 0; i < coord.length-1; i++) {
			var firstCoord = _map.getPixelFromCoordinate(coord[i]);
			var lastCoord = _map.getPixelFromCoordinate(coord[i+1]);
			var
				x1 = firstCoord[0],
				y1 = firstCoord[1],
				x2 = lastCoord[0],
				y2 = lastCoord[1]
			dist += Math.sqrt( Math.pow((x1-x2), 2) + Math.pow((y1-y2), 2) );
		}
		return dist;
	}
	var distanceLineString = _getLengthPixelLineString( map, linestring);
	var lengthSplit = Math.round(distanceLineString / pixelLengthSplit); // fatiar a cada 20 px aproximadamente
	
	for (i = 0; i < lengthSplit+1; i++)
		coords_interval.push(linestring.getCoordinateAt((i)/lengthSplit));
	return coords_interval;
}

var getAnimator = function(feature, domain, vValues, vInverters) { 
	return {
		_currentFeatureLine: feature,
		_currentScale: null,
		_domain: domain || [0,100],
		_vValues: vValues,
		_vInverters: vInverters,
		getScale: function (value) { 
			if (this._currentScale == null) this._currentScale = d3.scaleLinear().domain(this._domain).range([0.5,1.8]);
			return this._currentScale(value);
		},
		_currentColor: '#494a4b',
		clearElements: function () { this.coords_interval = []; this.list_a = [];},
		setColor: function (color) { this._currentColor = color },
		getColor: function () {
			if (this._currentFeatureLine && this._currentFeatureLine.get("layer")) {
				var _layer = this._currentFeatureLine.get("layer");
				var _fnStyle = _layer.getStyle();
				if (_fnStyle) {
					var vetColor = _fnStyle(this._currentFeatureLine).getStroke().getColor();
					if (vetColor.length == 4) vetColor.pop();
					this._currentColor = ol.color.asString(vetColor);
				}
			}
			return this._currentColor;
		},
		coords_interval: [],
		hasCoordinatiesList: function () { return this.coords_interval.length > 0 },
		list_a: [],
		getViewExtent: function() { 
			return pv.currentViewExtent;
		},
		_currentExtent: [],
		setExtent: function (extent) { this._currentExtent = extent },
		_currentGeometry: null,
		setGeometry: function (geom) { this._currentGeometry = geom },
		isVisible: function() {
			return ol.extent.intersects(this.getViewExtent(), this._currentExtent);
		},
		animation: function(frame){
			if (!this.isVisible()) { 
				return false; 
			}
			if (!this.hasCoordinatiesList()) {
				var linestring = new ol.geom.LineString(this._currentGeometry.getCoordinates());
				this.coords_interval = getArrayLineFragments(linestring.getCoordinates(), pv.animation.pixelLengthSplit);
			}
			var _coords_interval = this.coords_interval;
			var inverter = this._vInverters[pv.currentIndexStage];
			var value = this._vValues[pv.currentIndexStage];
			if (this.list_a.length == 0) { // Cria as features em suas posições
				for (i = 0; i < (_coords_interval.length / pv.animation.counterInterval >> 0); i++) {
					var a = new ol.Feature({});
					sourceAnimation.addFeature(a);
					a.iPos = Math.min(_coords_interval.length-1,(i * pv.animation.counterInterval));
					a.setGeometry(new ol.geom.Point(_coords_interval[a.iPos]));
					if (a.iPos == 0)
						setAnimationStyle(a, _coords_interval[a.iPos], _coords_interval[a.iPos+1], 1, this.getColor(), inverter);
					else
						setAnimationStyle(a, _coords_interval[a.iPos-1], _coords_interval[a.iPos], 1, this.getColor(), inverter);
					this.list_a.push(a);
					a.on('change', function(e) { 
						e.preventDefault();
						e.stopPropagation();
					});
				}
			}
			var _this = this;
			this.list_a.forEach(function (a) { // Movimenta as features
				if (value >= 1 && ol.extent.containsCoordinate(_this.getViewExtent(), _coords_interval[a.iPos])) {
					a.getGeometry().setCoordinates(_coords_interval[a.iPos]);
					setAnimationStyle(a, _coords_interval[a.iPos-1], _coords_interval[a.iPos], _this.getScale(value), _this.getColor(), inverter);
				} else {
					setAnimationInvisible(a);
				}
				if (inverter) {
					if(a.iPos <= 1)
						a.iPos = _coords_interval.length-1;
					else
						a.iPos--;
				} else {
					if(a.iPos >= _coords_interval.length-1)
						a.iPos = 1;
					else
						a.iPos++;
				}
			});
		}
	};
}

var animatorColection = [];
var lastTimeRender = null;

var skippFrame = false;
var animationRender = function (t) { 
	//var frame = Math.round((1.0 + Math.sin(t*0.001))*15); //Get frame 0-30 
	//if (lastTimeRender) {
	//	skippFrame = (t - lastTimeRender) > 100;
	//	if ((t - lastTimeRender) > 150) prepareAnimation(layerLines);
	//}
	//lastTimeRender = t;
	if (!skippFrame && !pv.animation.waitToAnimate) {
		skippFrame = true;
		animatorColection.forEach(function(animator) {
			animator.animation(t);
		});
		skippFrame = false;
	}
}
var startAnimation = function (velocity) { velocity = velocity || 10; skippFrame = false; browserAnimation.addRenderer(animationRender, velocity); }
var stopAnimation = function () { browserAnimation.removeRenderer(animationRender); }
  


var sourceAnimation = new ol.source.Vector({ });
var layerAnimation = new ol.layer.Vector({
		renderMode: 'image',
        source: sourceAnimation
    });
map.addLayer(layerAnimation);

var prepareAnimation = function (layerLines) {

	pv.animation.waitToAnimate = true;
	
	//clearLayer(layerAnimation);
	sourceAnimation.clear();
	if (animatorColection.length > 0) {
		animatorColection.forEach(function(animator) {
			animator.clearElements();
		});
		animatorColection = [];
	}
	//animatorColection = [];
	
	if (pv.userSettings.options.optAnimationFlow && pv.cirflw ) { //&& pv.resultIsSelected('cirflw')
		layerLines.getSource().getFeatures().forEach(function (f) {
			pv.fnUpdateFlowLine(f, pv.cirflw);
		});
	}

	if (pv.userSettings.options.optAnimationFlow && pv.dclink ) { //&& pv.resultIsSelected('dclink')
		layerLines2.getSource().getFeatures().forEach(function (f) {
			pv.fnUpdateFlowLine(f, pv.dclink);
		});
	}

	
	pv.animation.waitToAnimate = false;
}

pv.resultIsSelected = function(idResult) {
	return (pv.userSettings.resultsPreferences.filter(function(r) { return r.selected && r.idResult == idResult; })[0] || null) != null;
}


pv.fnUpdateFlowLine = function(feature, flowData) {
	if (feature.getGeometry().getType() == 'LineString') {
		var properties = feature.get("properties");
		if (properties) {
			var currentAgent = flowData.agents.filter(function (agent) { return (agent.name == properties.name) })[0];
			if (currentAgent)
			{
				var animator = getAnimator(feature,
					[flowData.min, flowData.max],
					currentAgent.values,
					currentAgent.inverters
				);
				animator.setColor(properties.style.color);
				animator.setExtent(feature.getGeometry().getExtent());
				animator.setGeometry(feature.getGeometry());
				animatorColection.push(animator);
			}
		}
	}
}

pv.currentViewExtent = map.getView().calculateExtent(map.getSize());
var currentZoomLevel = map.getView().getZoom();
function checknewzoom(evt)
{
	pv.currentViewExtent = map.getView().calculateExtent(map.getSize());
	var newZoomLevel = map.getView().getZoom();
	if (newZoomLevel != currentZoomLevel)
	{
		currentZoomLevel = newZoomLevel;
		setAnimationConfig(pv.animation.pixelLengthSplitRange(currentZoomLevel), pv.animation.counterIntervalRange(currentZoomLevel));
		//prepareAnimation(layerLines);
	}
}
map.on('moveend', checknewzoom);