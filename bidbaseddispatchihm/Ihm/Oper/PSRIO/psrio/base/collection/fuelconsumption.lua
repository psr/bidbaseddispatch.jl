local fuelconsumption = FuelConsumption(); 
warning("fuel_consumption = require(\"collection/fuelconsumption\") will be deprecated in the future, use fuel_consumption = FuelConsumption();");
return fuelconsumption;
