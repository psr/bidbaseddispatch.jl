local generic = Generic(); 
warning("generic = require(\"collection/generic\") will be deprecated in the future, use generic = Generic();");
return generic;
