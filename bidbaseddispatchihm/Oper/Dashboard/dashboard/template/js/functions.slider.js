	const sliderTemplate = `<div class="pv-timeslider" style="display: none;">
		<ul class="groupList" style=" background: white;">
			<li class="itemList controls">
				<span class="btnSlider button"> <i class="fa fa-fw fa-play"> </i> <i class="fa fa-fw fa-pause"> </i>  </span>
			</li>
			<li class="itemList time">
				<input class="textDisplay lbTime value"></input>
			</li>
			<li class="itemList controls1">
				<span class="btnSliderLoop button loop"> <i class="fa fa-fw fa-retweet"> </i> </span>
			</li>
			<li class="itemList timeline" style=" border-right: none; ">
				<div class="sliderDiv">
					<input class="sliderControl" style="display: none;"/>
				</div>
			</li>
		</ul>
	</div>`;
	var createDivSlider = function(divId) {
		
		var listDataSets = $('#' + divId).data("datasetTimelineList") || [];
		var divContent = $('#' + divId).data("content");
		var objChart = $('#' + divId).data('objChart');
				
		var divBox = $('#'+divContent).parent();
		
		divBox.find(".pv-timeslider").remove(); //remove previous
		
		if (divBox.data("intervalThread"))
		{
			clearInterval(divBox.data("intervalThread"));
			$(divBox).off('changeStage');
		}

		if ((objChart) && (listDataSets.length > 1)) {
			divBox.append(sliderTemplate);
		
			var listLabels  = listDataSets.map(function(item) { return item.key });
			var listDataSet = listDataSets.map(function(item) { return item.dataSet });
			var pv = {};
			var setStages = function(stages) {
				divBox.find(".lbTime").val(stages[0]);
				divBox.find('.pv-timeslider').fadeIn();

				divBox.find('.btnSlider').on('click', function (e) {
					$(this).toggleClass('stop');
					isRunning = !isRunning;
				});
				divBox.find('.btnSliderLoop').on('click', function (e) {
					$(this).toggleClass('loop');
					loop = !loop;
				});
				var maxValue = 262800 / 30 / 365;
				maxValue = stages.length;
				var customSlider = divBox.find('.sliderControl').bootstrapSlider({
					formatter: function(value) {
						return 'Current value: ' + value;
					},
					tooltip: 'hide',
					max: maxValue,
					min: 1,
					value: 1
				});
				divBox.find(".sliderControl").change(function(slideEvt) {
					slideEvt.value = customSlider.bootstrapSlider('getValue');
					var currentIndexStage = slideEvt.value - 1;
					divBox.find(".lbTime").val(stages[currentIndexStage]);
					$(divBox).trigger("changeStage", currentIndexStage);
				});
				
				var isRunning = false;
				var loop = true;
				var interval = setInterval(function() {
					if (isRunning) {
						var currentValue = customSlider.bootstrapSlider('getValue') + Math.max(1, (maxValue * 0.005));
						if (currentValue > maxValue) { 
							if (loop) currentValue = 1; 
							else divBox.find('.btnSlider').click();
						}
						customSlider.bootstrapSlider('setValue', currentValue, true);
						divBox.find(".sliderControl").change();
					}
				}, 1200);
				divBox.data("intervalThread", interval);
				
				function updateSizeSlider(){
					var boxSlider = divBox.find(".pv-timeslider");
					var widthCalc = boxSlider.width() - 270;
					divBox.find(".sliderDiv .slider").css("width", (widthCalc) + "px");
				}
				updateSizeSlider();
				
				$(window).on("resize", function() {
					updateSizeSlider();
				});
				$(document).on('updateLayout', function(e) {
					updateSizeSlider();
				});
				
			}
			setStages(listLabels);
			var objDtPicker = createDatepicker(listLabels, divBox.find('.textDisplay'), divBox.find('.sliderControl')); 
			
			$(divBox).on('changeStage', function(e, indexStage) {
				objChart.updateChart(listDataSet[indexStage]);
				if (objDtPicker) objDtPicker.updateDatePicker(indexStage);
			});
		}
	}
	
	
	function createDatepicker(listLabels, divBoxInput, customSlider) {
		var format = tryGetFormat(listLabels);
		if (format != "")
		{
			var dateList = listLabels.map(function(sDt) { return moment(sDt, format)._d });
			var dt = divBoxInput.datetimepicker({
				allowInputToggle: true, //not working
				defaultDate: dateList[0],
				enabledDates: dateList,
				minDate: dateList[0],
				maxDate: dateList[dateList.length - 1],
				format: format,
				sideBySide: true,
			});
			dt.on("dp.change", function(d) { 
				if (!dt.data("InternalUpdate")) {
					var iDate = listLabels.indexOf(d.date.format(format)) + 1;
					customSlider.bootstrapSlider('setValue', iDate, true);
					customSlider.change();
					dt.data("DateTimePicker").hide();
				}
			});
			dt.on("dp.show", function(d) {
				setTimeout(function() { 
					dt.data("isOpen", true);
				}, 250);
			});
			dt.on("dp.hide", function(d) {
				dt.data("isOpen", null);
			});
			divBoxInput.on("click", function() {
				if (dt.data("isOpen")) {
					dt.data("isOpen", null);
					dt.data("DateTimePicker").hide();
				}
			});
		}
		if (dt) {
			return { updateDatePicker: function(indexStage) {
					dt.data("InternalUpdate", true);
					dt.data("DateTimePicker").date(dateList[indexStage]);
					dt.data("InternalUpdate", false);
				}
			}
		} else {
			return null;
		}
	
	}