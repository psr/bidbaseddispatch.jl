
function adjustSankeyData(DataListGraph) {
	//Setting default colors
	var color = d3.scale.ordinal().range(customColors);
	DataListGraph.nodes.forEach(function (d, i) {
		if (d.node == undefined)
			d.node = i;
		if (d.color == undefined)
			d.color = color(d.node);
	});

    DataListGraph.links.forEach(function(link) {
      var source = link.source,
        target = link.target;
      if (typeof source === "number") source = link.source = DataListGraph.nodes[link.source];
      if (typeof target === "number") target = link.target = DataListGraph.nodes[link.target];
	});
}

function getChartSankey(LabelListGraph, DataListGraph, chartName) {
	var zoom = {};
	var drag = {};
	var svg = {};

	//d3.selectAll("a[data-zoom]").on("click", clicked);
	function clicked() {
		var valueZoom = this.getAttribute("data-zoom");
		if (valueZoom != 0)
		{
		   svg.call(zoom);
		  // Record the coordinates (in data space) of the center (in screen space).
		  var center0 = zoom.center(), translate0 = zoom.translate(), coordinates0 = coordinates(center0);
		  zoom.scale(zoom.scale() * Math.pow(2, +valueZoom));

		  // Translate back to the center.
		  var center1 = point(coordinates0);
		  zoom.translate([translate0[0] + center0[0] - center1[0], translate0[1] + center0[1] - center1[1]]);

		  svg.transition().duration(750).call(zoom.event);
		} else {
			fitZoom();
		}
	}

	function fitZoom()
	{
		//svg.transition().duration(500).call(zoom.translate([20,10]).scale(1).event);
	}
	
	function coordinates(point) {
	  var scale = zoom.scale(), translate = zoom.translate();
	  return [(point[0] - translate[0]) / scale, (point[1] - translate[1]) / scale];
	}

	function point(coordinates) {
	  var scale = zoom.scale(), translate = zoom.translate();
	  return [coordinates[0] * scale + translate[0], coordinates[1] * scale + translate[1]];
	}
			  
	function showSankey(energy)
	{
		
		var color = d3.scale.ordinal().range(customColors);
		//var color = d3.scale.category10();

		adjustSankeyData(energy);
		
		$('#' + chartName).removeClass().addClass('sankeybox').html('');
		$('.d3-tip').remove(); //clear olds tips
		$('#' + chartName).css('display', '');
		var chartBox = d3.select('#' + chartName).node().getBoundingClientRect();
		$('#' + chartName).css('display', 'none');
		var margin = {top: 10, right: 10, bottom: 10, left: 10},
		width = chartBox.width - chartBox.left - margin.left - margin.right,
		height = chartBox.height - chartBox.top  - margin.top - margin.bottom;
		
		width = chartBox.width - margin.left - margin.right;
		height = chartBox.height - margin.top - margin.bottom;	
		
	  var linkTooltipOffset = 72,
		  nodeTooltipOffset = 130;
		  
	  //Tooltip function:
	  //D3 sankey diagram with view options (Austin Czarnecki’s Block cc6371af0b726e61b9ab)
	  //https://bl.ocks.org/austinczarnecki/cc6371af0b726e61b9ab
	  var tipLinks = d3.tip()
		  .attr('class', 'd3-tip')
		  .offset([-10,0]);

	  var tipNodes = d3.tip()
		  .attr('class', 'd3-tip d3-tip-nodes')
		  .offset([-10, 0]);
	   
	  function formatAmount(val) {
		  //return val.toLocaleString("en-US", {style: 'currency', currency: "USD"}).replace(/\.[0-9]+/, "");
		  return parseFloat(val).toFixed(1) + " TJ";
	  };
		  
		// "➡" 
		tipLinks.html(function(d) {
		  var title, candidate;
		  if (energy.links.indexOf(d.source.name) > -1) {
			candidate = d.source.name;
			title = d.target.name;
		  } else {
			candidate = d.target.name;
			title = d.source.name;
		  }
			var html =  '<div class="table-wrapper">'+
				'<h1>'+title+'</h1>'+
				'<table>'+
					'<tr>'+
						'<td class="col-left">'+candidate+'</td>'+
						'<td align="right">'+formatAmount(d.value)+'</td>'+
					'</tr>'+
				'</table>'+
				'</div>';
		  return html;
		});

		tipNodes.html(function(d) {
		  var nodeName = d.name,
			  linksTo = d.sourceLinks,
			  linksFrom = d.targetLinks,
			  html;

		  html =  '<div class="table-wrapper">'+
				  '<h1>'+nodeName+'</h1>'+
				  '<table>';
		  if (linksFrom.length > 0 & linksTo.length > 0) {
			html+= '<tr><td><h2>Input:</h2></td><td></td></tr>'
		  }
		  for (i = 0; i < linksFrom.length; ++i) {
			html += '<tr>'+
			  '<td class="col-left">'+linksFrom[i].source.name+'</td>'+
			  '<td align="right">'+formatAmount(linksFrom[i].value)+'</td>'+
			'</tr>';
		  }
		  if (linksFrom.length > 0 & linksTo.length > 0) {
			html+= '<tr><td><h2>Output:</h2></td><td></td></tr>'
		  }
		  for (i = 0; i < linksTo.length; ++i) {
			html += '<tr>'+
					  '<td class="col-left">'+linksTo[i].target.name+'</td>'+
					  '<td align="right">'+formatAmount(linksTo[i].value)+'</td>'+
					'</tr>';
		  }
		  html += '</table></div>';
		  return html;
		});
		
		// function zoomed() {
		  // svg.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
		// }

		// function dragstarted(d) {
		  // d3.select(this).classed("dragging", true);
		// }

		// function dragged(d) {
		  // d3.select(this).attr("cx", d.x = d3.event.x).attr("cy", d.y = d3.event.y);
		// }

		// function dragended(d) {
		  // d3.select(this).classed("dragging", false);
		// }	
		
		// drag = d3.behavior.drag()
		// .origin(function(d) { return d; })
		// .on("dragstart", dragstarted)
		// .on("drag", dragged)
		// .on("dragend", dragended);
		
		// zoom = d3.behavior.zoom()
			// .scaleExtent([0, 5])
			// .center([width / 2, height / 2])
			// .on("zoom", zoomed);
		
	var formatNumber = d3.format(",.0f"),
		format = function(d) { return formatNumber(d) + " $"; };
	
	svg = d3.select('#' + chartName).append("svg")
		//.attr("width", width + chartBox.left + margin.left + margin.right)
		//.attr("height", height + chartBox.top + margin.top + margin.bottom)
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		//.call(zoom)
		.call(tipLinks)
		.call(tipNodes)
	  .append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	var sankey = d3sankey()
        .nodeWidth(15) // .nodeWidth(40)
        .nodePadding(10) // .nodePadding(15)
		.size([width, height]);

	var path = sankey.link();
	  sankey
		  .nodes(energy.nodes)
		  .links(energy.links)
		  .layout(32);

	//var fontScale = d3.scaleLinear().range([8, 30]);
	var fontScale = d3.scale.linear().domain(d3.extent(energy.nodes, function(d) { return d.value })).range([13, 20]);

	  var link = svg.append("g").selectAll(".link")
		  .data(energy.links)
		.enter().append("path")
		  .attr("class", "link")
		  .attr("d", path)
		  .style("stroke", function(d){ return d.source.color; })
		  .style("stroke-width", function(d) { return Math.max(1, d.dy); })
		  .sort(function(a, b) { return b.dy - a.dy; })
		  .on('mousemove', function(event) {
			tipLinks
			  .style("top", (d3.event.pageY - linkTooltipOffset) + "px")
			  .style("left", function () {
				var left = (Math.max(d3.event.pageX - linkTooltipOffset, 10)); 
				left = Math.min(left, window.innerWidth - $('.d3-tip').width() - 20)
				return left + "px"; })
			})
		  .on('mouseover', tipLinks.show)
		  .on('mouseout', tipLinks.hide);
		  
	  var node = svg.append("g").selectAll(".node")
		  .data(energy.nodes)
		.enter().append("g")
		  .attr("class", "node")
		  .attr("transform", function(d) {
			  return "translate(" + d.x + "," + d.y + ")";
		  })
		.on('mousemove', function(event) {
			tipNodes
			  .style("top", (d3.event.pageY - $('.d3-tip-nodes').height() - 20) + "px")
			  .style("left", function () {
				var left = (Math.max(d3.event.pageX - nodeTooltipOffset, 10)); 
				left = Math.min(left, window.innerWidth - $('.d3-tip').width() - 20)
				return left + "px"; })
			})
		.on('mouseover', tipNodes.show)
		.on('mouseout', tipNodes.hide)
		.call(d3.behavior.drag()
		  .origin(function(d) { return d; })
		  .on("dragstart", function() { 
			d3.event.sourceEvent.stopPropagation();  //Disable drag sankey on node select
			this.parentNode.appendChild(this); })
		  .on("drag", dragmove));

	  node.append("rect")
		  .attr("height", function(d) { return d.dy; })
		  .attr("width", sankey.nodeWidth())
		  .style("fill", function(d) {
			  if (d.color == undefined)
				return d.color = color(d.name.replace(/ .*/, "")); //get new color if node.color is null
			  return d.color;
		  })
		  .style("stroke", function(d) { return d3.rgb(d.color).darker(2); });

	  //Desativando exibicao do valor no node
	  // node.append("text")
		  // .attr("class","nodeValue")
		  // .text(function(d) { return d.name + "\n" + format(d.value); });

	  // node.selectAll("text.nodeValue")
		  // .attr("x", sankey.nodeWidth() / 2)
		  // .attr("y", function (d) { return (d.dy / 2) })
		  // .text(function (d) { return formatNumber(d.value); })
		  // .attr("dy", 5)
		  // .attr("text-anchor", "middle");

	  node.append("text")
		  .attr("class","nodeLabel")
		  .style("fill", function(d) {
				return d3.rgb(d.color).darker(2.4);
		  })
		  .style("font-size", function(d) {
				return fontScale(d.value) + "px";
		  });
		  
	  node.selectAll("text.nodeLabel")	
		  .attr("x", -6)
		  .attr("y", function(d) { return d.dy / 2; })
		  .attr("dy", ".35em")
		  .attr("text-anchor", "end")
		  .attr("transform", null)
		  .text(function(d) { return d.name; })
		.filter(function(d) { return d.x < width / 2; })
		  .attr("x", 6 + sankey.nodeWidth())
		  .attr("text-anchor", "start");
		function dragmove(d) {
			d3.select(this)
				.attr("transform", "translate(" + d.x + "," +
				   (d.y = Math.max(0, Math.min(height - d.dy, d3.event.y))) + ")");
			sankey.relayout();
			link.attr("d", path);
		};

		
		d3.selectAll(".d3-tip").on("click", function(event) { console.log(this.remove()) } );
		//fitZoom();
	}

	showSankey(DataListGraph);
}

