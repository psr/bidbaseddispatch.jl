if ("".endsWith === undefined) {
	String.prototype.endsWith = function(pattern) {
	  var d = this.length - pattern.length;
	  return d >= 0 && this.lastIndexOf(pattern) === d;
	};
}

				
					var requestUrl = window.location.pathname.toLowerCase().replace('#','');
					function defineMenuExpanded (menuData, l, f, b)
					{
						var i; b = false; 
						for (i = 0; i < menuData.length; i++) { 
							var m = menuData[i]; m.expanded = false; f = false; l = false;
							if ((m.page) && requestUrl.endsWith('/' + m.page.toLowerCase())) l = true;
							if (m.childes && (m.childes.length != 0)) f = defineMenuExpanded(m.childes, l, f, b);
							m.expanded = l || f;
							b = b || m.expanded;
						}
						return b;
					}
					
					function generateMenu(menuData)
					{
						defineMenuExpanded(menuData, false);
						
						var responseHtml = '';
						function adjustDefaults(m)
						{
							if (m.iconLetter)  
								m.icon = 'fa-fw'
							else {
								m.icon = 'fa ' + m.icon + ' fa-fw';
								m.iconLetter = '';
							}
								
							if (m.childes)
								if (m.childes.length != 0) {
									m.page = '#';
									m.hasChildes = true;
								}
								
							if (!m.node) m.page = '#';
						}
						
						function adjustLabel(m, b, h)
						{
							if (b) {
								m.label	= '<span class="hide-menu">' + m.label;
								if (h) m.label += '<i class="fa arrow"></i>';
								m.label	+= '</span>';
							}
						}
						
						var i;
						for (i = 0; i < menuData.length; i++) { 
							var menuFirstLevel = menuData[i];

							// <li class="devider"></li>
							responseHtml += '<li';
							if (i == 0) responseHtml += ' style="padding: 70px 0 0;"';
							if (menuFirstLevel.expanded) responseHtml += ' class="active"';
							responseHtml += '>\n';
							
							adjustDefaults(menuFirstLevel);
							adjustLabel(menuFirstLevel, menuFirstLevel.hasChildes,  menuFirstLevel.hasChildes);

							responseHtml += '<a href="' + menuFirstLevel.page + '" class="waves-effect"><i class="' + menuFirstLevel.icon + '" aria-hidden="true">' + menuFirstLevel.iconLetter + '</i>' + menuFirstLevel.label + '</a>';
							
							if (menuFirstLevel.hasChildes)
							{
								if (menuFirstLevel.expanded)
									responseHtml += '<ul class="nav nav-second-level collapse in" aria-expanded="true" style="">\n';
								else
									responseHtml += '<ul class="nav nav-second-level collapse" aria-expanded="false" style="">\n';
								var j;
								for (j = 0; j < menuFirstLevel.childes.length; j++) { 
									var menuSecondLevel = menuFirstLevel.childes[j];
									adjustDefaults(menuSecondLevel);
									adjustLabel(menuSecondLevel, true, menuSecondLevel.hasChildes);
									
									responseHtml += '<li> <a href="' + menuSecondLevel.page + '" ';
									if (menuSecondLevel.expanded) responseHtml += ' class="active"';
									responseHtml += '><i class="' + menuSecondLevel.icon + '" aria-hidden="true">' + menuSecondLevel.iconLetter + '</i>' + menuSecondLevel.label + '</a>';
									
									if (menuSecondLevel.hasChildes)
									{
										if (menuSecondLevel.expanded)
											responseHtml += '<ul class="nav nav-third-level collapse in" aria-expanded="true" style="">\n';
										else
											responseHtml += '<ul class="nav nav-third-level collapse" aria-expanded="false" style="">\n';
										var l;
										for (l = 0; l < menuSecondLevel.childes.length; l++) { 
											var menuThirdLevel = menuSecondLevel.childes[l];
											adjustDefaults(menuThirdLevel);
											adjustLabel(menuThirdLevel, true, menuThirdLevel.hasChildes);
											
											responseHtml += '<li> <a href="' + menuThirdLevel.page + '"><i class="' + menuThirdLevel.icon + '" aria-hidden="true">' + menuThirdLevel.iconLetter + '</i>' + menuThirdLevel.label + '</a>';
											
											
											responseHtml += '</li>\n';
										}
										responseHtml += '</ul>\n';
									}
									
									responseHtml += '</li>\n';
								}
								responseHtml += '</ul>\n';
							}
							responseHtml += '</li>\n';
						}
						return responseHtml;
					}
					

	function tryGetFormat(LabelListGraph) {
		LabelListGraph = LabelListGraph || [""];
		
		var formats = ["DD/MM/YYYY HH:mm", "YYYY-MM-DD HH:mm", "YYYY-MM-DD", "YYYY/MM", "YYYY-MM", "MM/YYYY", "YYYY/WW", "YYYY-WW", "WW/YYYY", "YYYY"];
		var format = "";
		
		if (LabelListGraph[0].trim().length < 4) return format;
		for (var i = 0; i < formats.length; i++) {
			var bValid = true; var format = formats[i];
			for (var j = 0; j < LabelListGraph.length; j++) {
				bValid = bValid && moment(LabelListGraph[j], format, true).isValid()
			}
			if (bValid) {
				return format;
			}
		}
		return "";
	
	}
	function convertToDate(LabelListGraph) {
		var format = tryGetFormat(LabelListGraph);
		if (format != "")
		{
			var arr = [];
			for (var i = 0; i < LabelListGraph.length; i++)
				arr.push(moment(LabelListGraph[i], format)._d);
			return arr;
		} else {
			return LabelListGraph; // sem formato identificado
		}
	}
					
	//Tableau20
	var colorsTableau20 = ["#4e79a7", "#a0cbe8", "#f28e2b", "#ffbe7d", "#59A14F","#8cd17d","#b6992d","#f1ce63", "#499894","#86bcb6","#E15759","#ff9d9a", "#79706e","#BAB0AB","#d37295","#fabfd2", "#b07aa1","#d4a6c8","#9d7660","#d7b5a6"];
	//Pastel2
	var colorsPastel2 = ["#B3E2CD", "#FDCDAC", "#CBD5E8", "#F4CAE4", "#E6F5C9", "#FFF2AE", "#F1E2CC", "#CCCCCC"];
	//Tableau10
	var colorsTableau10 = ["#4E79A7", "#F28E2C", "#E15759", "#76B7B2", "#59A14F", "#EDC949", "#AF7AA1", "#FF9DA7", "#9C755F", "#BAB0AB"];
	//Custom
	var colorsCustom = ["#66c2a5","#fc8d62","#8da0cb","#e78ac3","#a6d854","#ffd92f","#e5c494"];
	
	                                               
	var customColorsGroup = ["#4e79a7", "#f28e2b", "#59A14F", "#b6992d", "#E15759", "#499894", "#79706e", "#d37295", "#b07aa1", "#9d7660", "#a0cbe8", "#ffbe7d", "#8cd17d", "#f1ce63", "#ff9d9a", "#86bcb6", "#BAB0AB", "#fabfd2", "#d4a6c8", "#d7b5a6" ];
	var customColors = colorsTableau20;