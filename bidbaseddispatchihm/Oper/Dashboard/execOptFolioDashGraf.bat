@ECHO OFF

SETLOCAL EnableExtensions EnableDelayedExpansion
SET CASE_PATH=%~1

IF /I "%CASE_PATH%"=="" GOTO SPECIFIC

:MAIN
 SET SCRIPT_PATH=%~dp0
 SET CONVERT_APP="%SCRIPT_PATH%OptFolioDashGraf.exe"
 ::SET CONVERT_APP="D:\Project\OptValue-EOR_DashGraf\OptValueDashGraf\OptValueDashGraf\bin\Debug\OptValueDashGraf.exe"
 SET DASHBOARD_APP="%SCRIPT_PATH%PSRDashboard.exe"
 ::SET DASHBOARD_APP="D:\Project\PSRDashboard\bin\x64\Debug\PSRDashboard.exe"
 SET DASHBOARD_CFG="%SCRIPT_PATH%dash_config.dat"
  
 ECHO Executing to BIN2CSV...
 "%SCRIPT_PATH%bin2csv" -file "%CASE_PATH%\amort" >NUL
 "%SCRIPT_PATH%bin2csv" -file "%CASE_PATH%\cashflow" >NUL
 "%SCRIPT_PATH%bin2csv" -file "%CASE_PATH%\deprec" >NUL
 "%SCRIPT_PATH%bin2csv" -file "%CASE_PATH%\ebit" >NUL
 "%SCRIPT_PATH%bin2csv" -file "%CASE_PATH%\ebitda" >NUL
 "%SCRIPT_PATH%bin2csv" -file "%CASE_PATH%\ebt" >NUL
 "%SCRIPT_PATH%bin2csv" -file "%CASE_PATH%\equity" >NUL
 "%SCRIPT_PATH%bin2csv" -file "%CASE_PATH%\costs" >NUL
 "%SCRIPT_PATH%bin2csv" -file "%CASE_PATH%\grev" >NUL
 "%SCRIPT_PATH%bin2csv" -file "%CASE_PATH%\inctax" >NUL
 "%SCRIPT_PATH%bin2csv" -file "%CASE_PATH%\interest" >NUL
 "%SCRIPT_PATH%bin2csv" -file "%CASE_PATH%\irrdist" >NUL
 "%SCRIPT_PATH%bin2csv" -file "%CASE_PATH%\netinc" >NUL
 "%SCRIPT_PATH%bin2csv" -file "%CASE_PATH%\netrev" >NUL
 "%SCRIPT_PATH%bin2csv" -file "%CASE_PATH%\npvdist" >NUL
 "%SCRIPT_PATH%bin2csv" -file "%CASE_PATH%\vat" >NUL
 
 ECHO Removing old results...
 DEL /Q "%CASE_PATH%\.dashresults\dsh_cashflow_*.csv" 1>NUL 2>&1
 
 ECHO Converting to DASHBOARD...
 %CONVERT_APP% "%CASE_PATH%" >NUL
 COPY /Y "%SCRIPT_PATH%menu.json" "%CASE_PATH%" 1>NUL 2>&1

 ECHO Executing DASHBOARD...
 %DASHBOARD_APP% "%CASE_PATH%" 1 %DASHBOARD_CFG% -OPENBROWSER 1 > "%CASE_PATH%\dash.log"

GOTO END

:SPECIFIC
 ECHO Directory not valid.
GOTO END

:END
 ECHO.
::PAUSE
