@echo off

SET BASEPATH=%~dp0
SET BBD_FOLDER=%BASEPATH:~0,-1%
SET XPRESS_JL_NO_DEPS_ERROR=1
SET XPRESS_JL_NO_AUTO_INIT=1
SET XPRESS_JL_SKIP_LIB_CHECK=1
SET XPRESS_JL_NO_INFO=1

SET XPRESSDIR=
SET XPAUTH_PATH=

"%BBD_FOLDER%\deps\microsoft-deps\vc_2013_redist_x64.exe"
"%BBD_FOLDER%\deps\microsoft-deps\vc_2015-2019_redist.x64.exe"

julia --project="%BBD_FOLDER%" "%BBD_FOLDER%\installpkg.jl"

pause