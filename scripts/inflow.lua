-- .\psrio.exe -v 3 -r D:\Casos\Base_Brasil_Cosolidado_v0.2.0\psrio.lua D:\Casos\Base_Brasil_Cosolidado_v0.2.0\

hydro = Hydro();
inflow = hydro:load("inflow");
inflow_gauging_station = inflow:aggregate_agents(BY_AVERAGE(), Collection.GAUGING_STATION);
inflow_gauging_station:save("inflow_gauging_station", {csv=true});

compare = Compare("inflow check");
compare:add(inflow:aggregate_agents(BY_MIN(), Collection.GAUGING_STATION));
compare:add(inflow:aggregate_agents(BY_MAX(), Collection.GAUGING_STATION));

report = Report("");
report:push(compare);
report:save("inflow_check");