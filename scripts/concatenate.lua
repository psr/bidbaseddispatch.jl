size = PSR.studies();

study = Study();

labels = study:get_files("(.*\\.csv)");
for i = 1,#labels do
    label = labels[i];
    info("Concatenating file " .. label);

    outputs = {};
    for j = 1,size do 
        output = Generic(j):load(label);
        table.insert(outputs, output);
    end

    concatenate_stages(outputs):save(label);
end


-- local old_path = Generic(1); -- caso1
-- local new_path = Generic(2); -- caso2
-- local dir = Generic(3); -- caso2

-- old = generic:load(Generic(1));
-- new = generic:load(Generic(2));

-- concatenate = concatenate_stages(old, new);

-- concatenate:save("output");