 local hydro = require("collection/hydro");
 local gerhid = hydro:load("HGen");
 local thermal = require("collection/thermal");
 local gerter = thermal:load("TGen");
 
 concatenate(
     gerhid:aggregate_agents(BY_SUM(), "Total Hydro"):aggregate_blocks(BY_SUM()):aggregate_scenarios(BY_AVERAGE()),
     gerter:aggregate_agents(BY_SUM(), "Total Thermal"):aggregate_blocks(BY_SUM()):aggregate_scenarios(BY_AVERAGE())
 ):save("nome");