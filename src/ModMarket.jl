"""
(C) Modulo para determinar as ofertas aceitas e calcular o preço spot

input: - GRAF de ofertas
       - parametros fisicos das usinas / sistemas
output: - GRAF de ofertas aceitas
        - GRAF de preços spot
"""
function eval_offers(prb::BidDispatchProblem, day)

    IO_building_model(prb)

    start = time()

    d = prb.data
    n = prb.n
    options = prb.options
    options.maxupdntime = false
    options.PhysOper = false
    options.network = options.network_offer

    m = build_archetype(prb)

    for hour in 1:n.FullBuffer
        d.hour = hour
        for scen in 1:n.Scenarios
            d.scen = scen
            
            pull_scenarios_data!(prb, Int32(hour), Int32(scen))

            if options.DayAhead
                thermal_unit_COMT(m, prb, hour, n.FullBuffer, scen)
            end

            if options.ramp
                ramp_down!(m, prb, hour, s, max_hour)
                ramp_up!(m, prb, hour, s, max_hour)
            end

            thermal_generation_capacity!(m, prb, hour, scen)
            
            load_balance!(m, prb, day, hour, scen)
        end
        setobjective_exp!(m, prb, hour)
    end

    for scen in 1:n.Scenarios
        hydro_generation_offers!(m, prb, scen)
        thermal_generation_offers!(m, prb, scen)
        renewable_generation_offers!(m, prb, scen)
    end

    setobjective!(m, prb)

    # Solve
    IO_solving_problem(prb)
    m = solve(m, prb)

    # Write to file
    if termination_status( m ) == MOI.OPTIMAL
        print_msg(prb, "Optimal solution found!")
        # write outputs
        IO_writing_results(prb)

        elapsed = trunc(time() - start)
        print_global_results!(prb, m, elapsed)
        # write_fobj(prb)
        if prb.options.solver_id == xpress
            # prb_type = (options.DayAhead ? "DA" : "RT")
            # write_to_file( m , joinpath( options.INPUTPATH , string("market_", prb_type, "_", options.stage, "_", day, ".lp") ) )
            write_to_file( m , joinpath( options.INPUTPATH , "market.lp") )
        end
    end

    # Model outputs
    if n.Hydros > 0
        d.sale_offer_hyd_acc = sum(JuMP.value.(m[:soldVR]), dims=2)[:, 1, :, :]
        d.purchase_offer_hyd_acc = sum(JuMP.value.(m[:boughtVR]), dims=2)[:, 1, :, :]
    end

    if n.Thermals > 0
        d.sale_offer_ter_acc = JuMP.value.(m[:TGenAux]).data
        if options.DayAhead
            d.dailyCOMT = convert_binary_variable_value_to_Int32.(JuMP.value.(m[:COMT]))
        end
    end

    if n.Gnd > 0
        d.sale_offer_gnd_acc = JuMP.value.(m[:RGenAux])
    end
    
    return m
end