function main(path::String, button::Int32 = Int32(0))

    start = time()
    
    if path[end] == '"'
        path = path[1:end-1]
    end
    if path[end] != "\\"
        path *= "\\"
    end

    prb = BidBasedDispatch.load_exec_options(path,button)
    BidBasedDispatch.map_problem(prb)
    if button == 0
        if prb.options.gametype == Computer
            computer_simulation(prb)
        elseif prb.options.gametype == Human
            human_simulation(prb)
        else
            error("GameType must be Human (1) or Computer(2)")
        end
    else
        exportBids!(prb)
    end

    IO_elapsedtime(prb, trunc(time()-start))

    return true
end

function exportBids!(prb)

    options = prb.options

    currentDay = options.day+(options.week-1)* prb.n.Days +(options.year-1) *52*prb.n.Days
    currentWeek = options.week + (options.year-1) *52

    # le arquivos GRAF
    readGrafs(prb, currentWeek, options.day)
    prb.options.DayAhead = true
    if currentDay == 1
        create_initial_budget!(prb)
    else
        load_budget!(prb,currentDay - 1)
        load_vol!(prb,currentDay - 1)
    end
    add_inflow_to_budget!(prb)
    update_power_budget!(prb, currentWeek, options.day)
    create_offers!(prb, true, currentWeek, options.day)
    nothing
end

function computer_simulation(prb)
    BidBasedDispatch.readGrafs(prb, 1, 1)
    BidBasedDispatch.create_initial_budget!(prb)

    for etapa in 1:prb.n.Stages, dia in 1:prb.n.Days

        prb.options.stage = etapa

        BidBasedDispatch.readGrafs(prb, etapa, dia)

        add_inflow_to_budget!(prb)
        update_power_budget!(prb, etapa, dia)

        create_offers!(prb, false, etapa, dia)

        if etapa==1 && dia==1
            init_outputs!(prb)
        end

        # day ahead
        prb.options.DayAhead = true

        if prb.options.SimulOpt
            m = market_and_phys_oper(prb, dia, etapa)
        else
            m = eval_offers(prb, dia)
            m = phys_oper(prb, dia, etapa)
        end

        get_outputs!(m,prb,etapa,dia)

        # real time
        add_RT_distortion!(prb, dia)
        prb.options.DayAhead = false

        if prb.options.SimulOpt
            m = market_and_phys_oper(prb, dia, etapa)
        else
            m = eval_offers(prb, dia)
            m = phys_oper(prb, dia, etapa)
        end

        update_initial_budget!(prb, m)
        
        get_outputs!(m,prb,etapa,dia)
        
        remove_RT_distortion!(prb, dia)
        
    end

    write_outputs(prb)
    return true
end
function human_simulation(prb)
    options = prb.options
    # le arquivos GRAF
    readGrafs(prb, options.week, options.day)
    prb.options.DayAhead = true

    currentDay = options.day+(options.week-1)* prb.n.Days +(options.year-1) *52*prb.n.Days
    currentWeek = options.week + (options.year-1) *52
    if currentDay == 1
        create_initial_budget!(prb)
    else
        load_budget!(prb,currentDay - 1)
        load_vol!(prb,currentDay - 1)
    end
    add_inflow_to_budget!(prb)
    update_power_budget!(prb, currentWeek, options.day)
    create_offers!(prb, false, currentWeek, options.day)
    init_outputs!(prb)


    if prb.options.SimulOpt
        m = market_and_phys_oper(prb, options.day, currentWeek)
    else
        m = eval_offers(prb, options.day)
        m = phys_oper(prb, options.day, currentWeek)
    end

    get_outputs!(m,prb,currentWeek,options.day)
    # real time
    add_RT_distortion!(prb, options.day)
    prb.options.DayAhead = false
    
    if prb.options.SimulOpt
        m = market_and_phys_oper(prb, options.day, currentWeek)
    else
        m = eval_offers(prb, options.day)
        m = phys_oper(prb, options.day, currentWeek)
    end

    update_initial_budget!(prb, m)
    get_outputs!(m,prb,currentWeek,options.day)
    remove_RT_distortion!(prb, options.day)
    write_outputs(prb, currentDay)
    return true
end