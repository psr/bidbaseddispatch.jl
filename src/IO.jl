function IO_header(prb::BidDispatchProblem)
    str = "BidBasedDispatch"
    spaces = 3
    print_msg(prb,"")
    print_msg(prb,"="^(length(str)+2*spaces))
    print_msg(prb," "^spaces * str)
    print_msg(prb,"="^(length(str)+2*spaces))
    print_msg(prb,"")
end

function IO_reading_inputs(prb::BidDispatchProblem)
    print_msg(prb,"Reading inputs ...")
end

IO_mapping(prb::BidDispatchProblem) = print_msg(prb,"Mapping files ...")


function IO_building_model(prb::BidDispatchProblem)
    print_msg(prb,"Building optimization model ...")
end

function IO_solving_problem(prb::BidDispatchProblem)
    print_msg(prb,"Solving optimization problem ...")
end

function IO_writing_results(prb::BidDispatchProblem)
    print_msg(prb,"Writing results ...")
end

function IO_running_path(prb::BidDispatchProblem, path)
    print_msg(prb,"Executing model on $path
    ")
end

function IO_loading_data(prb::BidDispatchProblem)
    print_msg(prb,"Loading data ...")
end
function IO_loading_scenarios(prb::BidDispatchProblem)
    print_msg(prb,"Loading scenarios ...")
end
function IO_mapping_data(prb::BidDispatchProblem)
    print_msg(prb,"Mapping data ...")
end

function IO_executionmode(prb::BidDispatchProblem)
    print_msg(prb,"Data mode : $(prb.options.readingmode)")
    print_msg(prb,"Max hours : $(prb.n.MaxHours)")
    print_msg(prb,"Buffer : $(prb.n.Buffer)")
    print_msg(prb,"Scenarios : $(prb.n.Scenarios)")
    print_msg(prb,"Hydro balance : $(prb.options.hydroBlocks)")
end

function IO_elapsedtime(prb::BidDispatchProblem, elapsed)
    print_msg(prb, "Total time elapsed: $elapsed s")
end

# Log
function print_log(prb::BidDispatchProblem, msg::String, force::Bool = false)
    if prb.options.logprint || force
        write(prb.data.logstream, msg * "\n")
        flush(prb.data.logstream)
    end
end
function print_log(prb::BidDispatchProblem, msg::Any, force::Bool = false)
    if prb.options.logprint || force
        write(prb.data.logstream, "$msg \n")
        flush(prb.data.logstream)
    end
end
function print_msg(prb::BidDispatchProblem, msg::String)
    print_log(prb,msg)
    println(msg )
end
function print_msg(prb::BidDispatchProblem, msg::Any)
    print_log(prb, msg)
    println(msg )
end