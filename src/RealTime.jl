function add_RT_distortion!(prb::BidDispatchProblem, dia::Int64)

    d = prb.data
    n = prb.n

    d.current_demand.valores = d.demand_graf.valores .+ d.demand_distortion.valores 
    d.current_demand.valores = max.(d.current_demand.valores, 0)

    if n.Gnd > 0
        d.gergndpu.valores .+= d.gergndpu_distortion.valores
        d.gergndpu.valores = max.(d.gergndpu.valores, 0.0)
        d.gergndpu.valores = min.(d.gergndpu.valores, 1.0)

        for agn in 1:d.gergndpu.n_agentes
            d.gergnd.valores[:, :, :, agn] = d.gergndpu.valores[:, :, :, agn] .* d.gFatOp[agn] .* d.rPotInst[agn]
        end

        d.sale_offer_gnd = create_renewable_offers(prb, dia)
        pad_offers!(prb)
    end

    return

end

function remove_RT_distortion!(prb::BidDispatchProblem, dia::Int64)

    d = prb.data
    n = prb.n

    d.current_demand.valores = copy(d.demand_graf.valores)

    if n.Gnd > 0
        d.gergndpu.valores .-= d.gergndpu_distortion.valores
        d.gergndpu.valores = max.(d.gergndpu.valores, 0.0)
        d.gergndpu.valores = min.(d.gergndpu.valores, 1.0)

        for agn in 1:d.gergndpu.n_agentes
            d.gergnd.valores[:, :, :, agn] = d.gergndpu.valores[:, :, :, agn] .* d.gFatOp[agn] .* d.rPotInst[agn]
        end

        d.sale_offer_gnd = create_renewable_offers(prb, dia)
        pad_offers!(prb)
    end

    return

end