"""
    Base methods for dispatch problem
"""
function init_jump_types!(prb::BidDispatchProblem, m)
    d = prb.data
    n = prb.n
    options = prb.options
    # initialzie expressions
    d.HTurb = @expression(m, [h=1:n.Hydros, hour=1:n.FullBuffer, scen=1:n.ScenariosExtended], 0)
    d.spillage = @expression(m, [h=1:n.Hydros, hour=1:n.FullBuffer, scen=1:n.ScenariosExtended], 0)
    d.TGen = @expression(m, [t=1:length(d.tExisting), hour=1:n.FullBuffer, scen=1:n.ScenariosExtended], 0)

    # objective function
    d.fobj.hydroCosts = @expression(m, 0)
    d.fobj.hydroCosts = @expression(m, 0)
    d.fobj.FutureCost = @expression(m, 0)
    d.fobj.thermalCosts = @expression(m, 0)
    d.fobj.CommitmentCost = @expression(m, 0)
    d.fobj.RenewableCosts = @expression(m, 0)
    d.fobj.ReserveCosts = @expression(m, 0)
    d.fobj.DeficitCost = @expression(m, 0)
    d.fobj.PenaltySpillage = @expression(m, 0)
    if options.PhysOper
        d.fobj.PenaltyWaveGuide1 = @expression(m, 0)
        d.fobj.PenaltyWaveGuide2 = @expression(m, 0)
    end

    nothing
end
# create variables
function build_archetype(prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options

    m = Model()

    # initialzie expressions
    init_jump_types!(prb, m)

    # add variables
    # -------------

    if options.PhysOper
        var_hydro_vol!(m, prb)
        if options.useWaveguide != none
            var_hydro_volw!(m, prb)
            var_waveguide_aux!(m, prb)
            var_lambda!(m, prb)
        end
        var_dclinks(m, prb)
        var_network!(m, prb)
        var_werrorlin!(m, prb)
    end

    # thermal
    if options.commitment
        var_COMT(m, prb)
        var_startup(m, prb)
        var_shutdown(m, prb)
    end

    if options.ramp
        var_ramp_slack(m, prb)
    end

    # system
    var_deficit!(m, prb)

    var_interc!(m, prb)

    # renewables
    var_gergnd!(m, prb)

    # outflow slack
    var_outflow!(m, prb)

    # thermal generation
    var_thermalgen!(m, prb)

    var_buy_sell_VR!(m, prb)
    var_thermalgen_aux!(m, prb)
    var_gergnd_aux!(m, prb)
    if options.PhysOper
        var_turbining!(m, prb)
        if !options.linear_forced_spillage
            var_spillage_control!(m, prb)
        end
    end

    return m
end

function get_spillage(m,prb::BidDispatchProblem)
    return m[:spillage]
end

function get_commit(m,prb::BidDispatchProblem)

    if (prb.options.PhysOper && !prb.options.SimulOpt) || !prb.options.DayAhead 
        return prb.data.dailyCOMT
    else
        return m[:COMT]
    end
end

function get_commit_value(m,prb::BidDispatchProblem)

    if (prb.options.PhysOper && !prb.options.SimulOpt) || !prb.options.DayAhead 
        return prb.data.dailyCOMT
    else
        return JuMP.value.(m[:COMT])
    end
end

function get_tgen(m, prb::BidDispatchProblem)
    return m[:TGen].data
end

# Thermal unit COMT
function thermal_unit_COMT(m::JuMP.Model, prb::BidDispatchProblem, hour, max_hour, s)
    options = prb.options

    if !options.commitment && (options.ramp || options.minupdntime)
        throw("As opções de mínimo tempo ligado/desligado, restrição de rampa e número máximo de partidas e paradas não podem estar ligadas sem o commit ser ativado.")
    end

    if options.minupdntime
        minimum_up_time2!(m, prb, hour,s)
        minimum_down_time2!(m, prb, hour,s)
    end

    # using tight formulation
    if options.commitment
        tight_startup_shutdown3!(m, prb, hour,s)
    end
    if options.maxt
        max_up!(m, prb, s)
        max_dn!(m, prb, s)
    end

    return nothing
end

function maximum_up_time!(m::JuMP.Model, prb::BidDispatchProblem, hour, max_hour,s)
    COMT   = get_commit(m,prb)
    tau = prb.data.tMinUptime
    d = prb.data

    for (j,t) in enumerate(prb.data.tExisting)
        if d.consider_commit[t] > 0
            MaximumUpTime  =  @constraint(m, sum(COMT[j, hour + k,s] for k in 0:min(tau[t] + 1, max_hour - hour)) <= tau[t])
            set_name(MaximumUpTime, "MaxUT($j, $(hour), $s)")
        end
    end
    nothing
end

function minimum_up_time2!(m::JuMP.Model, prb::BidDispatchProblem, hour,s)
    k   = prb.data.initial_state_time 
    COMT   = get_commit(m,prb)
    STup   = m[:STup]
    tau = Int.(prb.data.tMinUptime)
    d = prb.data

    # operative hours
    for (j,t) in enumerate(prb.data.tExisting)
        if tau[t] > 0 && d.consider_commit[t] > 0
            past = (k[s,j] > 0 && k[s,j] + hour - tau[t] <= 0 ? 1 : 0) 
            mint = max(1, (hour - tau[t] + 1) )
            MinimumUpTime =  @constraint(m, sum(STup[j, k,s] for k in mint:hour ) + past <= COMT[j,hour,s])

            set_name(MinimumUpTime, "MinUT($j, $(hour), $s)")
        end
    end
    nothing
end

function minimum_down_time2!(m::JuMP.Model, prb::BidDispatchProblem, hour,s)
    k   = prb.data.initial_state_time
    STdn   = m[:STdn]
    COMT   = get_commit(m,prb)
    tau = Int.(prb.data.tMinDowntime)
    d = prb.data

    # operative hours
    for (j,t) in enumerate(prb.data.tExisting)
        if tau[t] > 0 && d.consider_commit[t] > 0
            past = (-k[s,j] > 0 && -k[s,j] + hour - tau[t] <= 0 ? 1 : 0)
            mint = max(1, (hour - tau[t] + 1) )
            MinimumDownTime = @constraint(m, sum(STdn[j, k,s] for k in mint:hour) + past <= 1 - COMT[j,hour,s])
            set_name(MinimumDownTime, "MinDT($j, $(hour ), $s)")
        end
    end
    nothing
end

function max_up!(m::JuMP.Model, prb::BidDispatchProblem, s)
    
    n = prb.n
    STup= m[:STup]
    for (j,t) in enumerate(prb.data.tExisting)
        MaxUp = @constraint(m, prb.data.tMaxStartUps[t] >= sum(STup[j,1:n.MaxHours,s]))
        set_name(MaxUp, "MaxUp($j, $s)")
    end 
    nothing

end

function max_dn!(m::JuMP.Model, prb::BidDispatchProblem, s)
    
    n = prb.n
    STdn = m[:STdn]
    for (j,t) in enumerate(prb.data.tExisting)
        MaxDn = @constraint(m, prb.data.tMaxShutDowns[t] >= sum(STdn[j,1:n.MaxHours,s]))
        set_name(MaxDn, "MaxDn($j, $s)")
    end 
    nothing

end


function ramp_up!(m::JuMP.Model, prb::BidDispatchProblem, hour, s, max_hour)
    options = prb.options
    d = prb.data
    TGen = get_tgen(m, prb)
    n = prb.n

    if options.commitment
        STup= m[:STup]
        COMT= get_commit(m,prb)
    end

    for (j,t) in enumerate(prb.data.tExisting)
        if hour <= max_hour && prb.data.tRampUpCold[t] > 0 && d.consider_commit[t] > 0
            if hour == 1 
                RampUp= @constraint(m, TGen[j, hour, s] - prb.data.initial_power[s,t] <= (prb.data.tRampUpCold[j])* STup[j, hour,s] + prb.data.tRampUpWarm[j] * prb.data.initial_state_commit[s,t])
                set_name(RampUp, "RampUp($j, $(hour), $s)")
            else
                RampUp = @constraint(m, TGen[j,hour,s] - TGen[j,hour-1,s] <= (prb.data.tRampUpCold[t])* STup[j, hour,s] + prb.data.tRampUpWarm[t] * COMT[j,hour-1,s])
                set_name(RampUp, "RampUp($j, $(hour), $s)")
            end
        end
    end
    nothing
end

function ramp_down!(m::JuMP.Model, prb::BidDispatchProblem, hour, s, max_hour)
    d = prb.data
    
    STdn = m[:STdn]
    COMT = get_commit(m,prb)
    ramp_slack = m[:ramp_slack]

    TGen = get_tgen(m, prb)

    for (j,t) in enumerate(prb.data.tExisting)
        if hour <= max_hour && prb.data.tRampDown[t] > 0 && d.consider_commit[t] > 0
            if hour == 1
                RampDown = @constraint(m, TGen[j, hour, s] - prb.data.initial_power[s,t] >= -prb.data.tRampDown[j]*COMT[j,hour,s] - prb.data.tGerMin[j]*STdn[j,hour,s] - ramp_slack[j,hour,s])
                set_name(RampDown, "RampDown($j, $(hour), $s)")
            else 
                RampDown = @constraint(m, TGen[j,hour,s] - TGen[j,hour-1,s] >= -prb.data.tRampDown[j]*COMT[j,hour,s] - prb.data.tGerMin[j]*STdn[j,hour,s] - ramp_slack[j,hour,s])
                set_name(RampDown, "RampDown($j, $(hour), $s)")
            end
        end
    end
    nothing
end

function tight_startup_shutdown3!(m, prb::BidDispatchProblem, hour,s)

    n = prb.n
    d = prb.data

    if n.Thermals > 0
        COMT = get_commit(m,prb)
        STup = m[:STup]
        STdn = m[:STdn]

        for (j,t) in enumerate(d.tExisting)
            if d.tExist[t] <= 0 && d.consider_commit[t] > 0 && d.consider_commit[t] > 0
                # count startups
                if hour == 1
                    StartUpCstr =  @constraint(m, STup[j, hour,s] - STdn[j, hour,s] == COMT[j, hour,s] - d.initial_state_commit[s,t])
                    StateIsOff =  @constraint(m, STup[j, hour,s] + STdn[j, hour,s] <= COMT[j, hour,s] + d.initial_state_commit[s,t])
                    StateIsOn =  @constraint(m, STup[j, hour,s] + STdn[j, hour,s] + COMT[j, hour,s] + d.initial_state_commit[s,t] <= 2)
                else
                    StartUpCstr =  @constraint(m, STup[j, hour,s] - STdn[j, hour,s] == COMT[j, hour,s] - COMT[j, hour-1,s])
                    StateIsOff =  @constraint(m, STup[j, hour,s] + STdn[j, hour,s] <= COMT[j, hour,s] + COMT[j, hour-1,s])
                    StateIsOn =  @constraint(m, STup[j, hour,s] + STdn[j, hour,s] + COMT[j, hour,s] + COMT[j, hour-1,s] <= 2)
                end
                set_name(StartUpCstr, "CmtUpDn($j,$(hour),$s)")
                set_name(StateIsOff, "StOff($j,$(hour),$s)")
                set_name(StateIsOn, "StOn($j,$(hour),$s)")
            end
        end
    end

    nothing
end

# System constraints
function load_balance!(m::JuMP.Model, prb::BidDispatchProblem, day, hour, s)
    n = prb.n
    d = prb.data
    options = prb.options

    # load variables into scope
    # -------------------------
    def = m[:def]

    data_hour = use_buffer(prb, hour)
    var_hour = hour

    data_week_hour = (day-1)*n.MaxHours + data_hour
    var_week_hour = (day-1)*n.MaxHours + var_hour

    demand = d.current_demand.valores[1, s, data_week_hour, :]*1e3

    if n.Thermals>0
        g_ter = get_tgen(m, prb)
    end

    if n.Gnd>0
        g_gnd = m[:RGen]
    end

    if n.Hydros>0
        if options.PhysOper
            g_hid = m[:HTurb] .* d.fprodt
        else
            g_hid = m[:soldVR] - m[:boughtVR] 
        end
    end

    loadbal_list = Array{ConstraintRef, 1}(undef, n.SpotPrice)

    if options.network == no::ConsiderNetwork

        loadbal = @constraint(m,
            # hydro
            + sum(g_hid[i, var_hour, s] for i in 1:n.Hydros if options.PhysOper) # if PhysOper == true
            + sum(g_hid[i, var_hour, s, seg] for i in 1:n.VirtualReservoirs, seg in 1:n.OfferSegments if !options.PhysOper) # if PhysOper == false
            
            # thermal
            + sum(g_ter[j,var_hour,s] for (j,t) in enumerate(d.tExisting) if d.tExist[t] <= 0)

            # renewable
            + sum(g_gnd[r, var_hour, s] for r in 1:n.Gnd if d.gExist[r] <= 0)

            # deficit
            + sum( def[1, var_hour, s])

            # demand
            - sum(demand)

            == 0.0
        )
        set_name(loadbal, "Lbal(1,$var_hour,$s)")

        loadbal_list[1] = loadbal

    elseif options.network == inter::ConsiderNetwork
        IntercTo = m[:IntercTo]
        IntercFrom = m[:IntercFrom]
        for b in 1:n.Sys
            loadbal = @constraint(m,
                # hydro
                + sum(g_hid[i, var_hour, s] for i in 1:n.Hydros if d.hyd2sys[i] == b && options.PhysOper) # if PhysOper == true
                + sum(g_hid[i, var_hour, s, seg] for i in 1:n.VirtualReservoirs, seg in 1:n.OfferSegments if d.vr2sys[i] == b && !options.PhysOper) # if PhysOper == false

                # thermal
                + sum(g_ter[j,var_hour,s] for (j,t) in enumerate(d.tExisting) if d.ter2sys[t] == b && d.tExist[t] <= 0)

                # # renewable
                + sum(g_gnd[r, var_hour, s] for r in 1:n.Gnd if d.gnd2sys[r] == b && d.gExist[r] <= 0)

                # network
                # entering node
                + sum((1-d.iLossTo[l]) * IntercTo[l, var_hour, s] for l in 1:n.Intercs if d.intFsys[l] == b)
                - sum(IntercFrom[l, var_hour, s] for l in 1:n.Intercs if d.intFsys[l] == b)

                # leaving node
                + sum((1-d.iLossFrom[i]) * IntercFrom[i, var_hour, s] for i in 1:n.Intercs if d.int2sys[i] == b)
                - sum(IntercTo[i, var_hour, s] for i in 1:n.Intercs if d.int2sys[i] == b)

                # deficit
                + def[b, var_hour, s]

                # demand
                - sum(demand[ d.sys2dem[b] ] )

                == 0.0
            )
            set_name(loadbal, "Lbal($b,$var_hour,$s)")
            loadbal_list[b] = loadbal
        end
    elseif options.network == network::ConsiderNetwork
        
        if options.respresentationnetwork == 0

            demand = bus_load_bid(prb, data_hour, data_week_hour, s)
            if n.DCLinks > 0
                DCTo = m[:DCTo]
                DCF = m[:DCF]
            end
            Flow = m[:Flow]
            Angle = m[:Angle]
            for k in 1:n.Circuits
                ohm = @constraint(m, 100*(Angle[d.cir2bus[k],var_hour,s] - Angle[d.cirFbus[k],var_hour,s])/d.circuit_x[k] == Flow[k,var_hour,s])
                set_name(ohm, "Ohm($k,$var_hour,$s)")
            end
            for b in 1:n.Buses

                dem =  demand[b]#PSRTimeController_getCurrentBlockHour(prb.psrc.timec_ptr)]#d.bus2dem[b] > 0 ? demand[d.bus2dem[b]] : 0
                loadbal = @constraint(m,
                    # hydro
                    + sum(g_hid[i, var_hour, s] for i in 1:n.Hydros if d.hyd2bus[i] == b && options.PhysOper) # if PhysOper == true
                    + sum(g_hid[i, var_hour, s, seg] for i in 1:n.VirtualReservoirs, seg in 1:n.OfferSegments if d.hyd2bus[vr2hyd[i]] == b && !options.PhysOper) # if PhysOper == false

                    # thermal
                    + sum(g_ter[j,var_hour,s] for (j,t) in enumerate(d.tExisting) if d.ter2bus[t] == b && d.tExist[t] <= 0)

                    # # renewable
                    + sum(g_gnd[r, var_hour, s] for r in 1:n.Gnd if d.gnd2bus[r] == b && d.gExist[r] <= 0)

                    + sum( (1-d.dLossTo[l]) * DCTo[l, var_hour, s] for l in 1:n.DCLinks if d.dcl2bus[l] == b)
                    - sum(  DCF[l, var_hour, s] for l in 1:n.DCLinks if d.dcl2bus[l] == b)

                    # leaving node
                    + sum((1-d.dLossFrom[i]) * DCF[i, var_hour, s] for i in 1:n.DCLinks if d.dclFbus[i] == b )
                    - sum(DCTo[i, var_hour, s] for i in 1:n.DCLinks if d.dclFbus[i] == b)
                    
                    + sum(Flow[k, var_hour, s] for k in 1:n.Circuits if d.cir2bus[k] == b)
                    - sum(Flow[k, var_hour, s] for k in 1:n.Circuits if d.cirFbus[k] == b)

                    # deficit
                    + sum(def[b, var_hour, s])

                    # demand
                    - dem

                    == 0.0
                )
                set_name(loadbal, "Lbal($b,$var_hour,$s)")
                loadbal_list[b] = loadbal
            end
        else
            demand = bus_load_bid(prb,data_hour,data_week_hour,s)
            if n.DCLinks > 0
                DCTo = m[:DCTo]
                DCF = m[:DCF]
            end
            inj_bus = m[:inj_bus]
            for b in 1:n.Buses
                dem =  demand[b]#PSRTimeController_getCurrentBlockHour(prb.psrc.timec_ptr)]#d.bus2dem[b] > 0 ? demand[d.bus2dem[b]] : 0
                @constraint(m, inj_bus[b,var_hour,s] == 
                    
                + sum(g_hid[i, var_hour, s] for i in 1:n.Hydros if d.hyd2bus[i] == b && options.PhysOper) # if PhysOper == true
                + sum(g_hid[i, var_hour, s, seg] for i in 1:n.VirtualReservoirs, seg in 1:n.OfferSegments if d.hyd2bus[vr2hyd[i]] == b && !options.PhysOper) # if PhysOper == false

                # thermal
                + sum(g_ter[j,var_hour,s] for (j,t) in enumerate(d.tExisting) if d.ter2bus[t] == b && d.tExist[t] <= 0)

                # # renewable
                + sum(g_gnd[r, var_hour, s] for r in 1:n.Gnd if d.gnd2bus[r] == b && d.gExist[r] <= 0)

                + sum( (1-d.dLossTo[l]) * DCTo[l, var_hour, s] for l in 1:n.DCLinks if d.dcl2bus[l] == b)
                - sum(  DCF[l, var_hour, s] for l in 1:n.DCLinks if d.dcl2bus[l] == b)

                # leaving node
                + sum((1-d.dLossFrom[i]) * DCF[i, var_hour, s] for i in 1:n.DCLinks if d.dclFbus[i] == b )
                - sum(DCTo[i, var_hour, s] for i in 1:n.DCLinks if d.dclFbus[i] == b)

                + sum(def[b, var_hour, s])
                
                - dem)
            end
            
            for k in 1:n.Circuits
                loadcir = @constraint(m, -d.circuit_Rn[k] <= sum(d.Beta[k,b]*inj_bus[b,var_hour,s] for b in 1:n.Buses) <= d.circuit_Rn[k])
                set_name(loadcir, "cbal($k,$var_hour,$s)")
            end
            for i in 1:maximum(d.ilhas)
                loadbal = @constraint(m, sum(inj_bus[d.ilhas.==i,var_hour,s]) == 0 )
                set_name(loadbal, "Lbal($i,$var_hour,$s)")
                loadbal_list[i] = loadbal
            end
        end
    end
    return loadbal_list
end

function w_balance!(m::JuMP.Model, prb::BidDispatchProblem, day , etapa)
    d = prb.data
    n = prb.n
    options = prb.options
    # load variables in to scope
    # --------------------------
    if n.Hydros>0
        HydroVol = m[:HydroVol]
        spillage = m[:spillage] #get_spillage(m, prb)
        forced_spillage = m[:ForcedSpillage]
        turbining = m[:HTurb] #get_hturb(m, prb)

        # m3/s -> hm3
        # 1m3 = 1.0E-6 hm3
        conv = 1.0*10^(-6) * 60 * 60

        for s in collect(Int32, 1:n.Scenarios)
            for hour in collect(Int32, 1:n.FullBuffer)
                var_hour = hour
                data_hour = use_buffer(prb, hour)
                pull_scenarios_data!(prb, data_hour, s)
                inflow = d.inflow
                # hydrocoeff = d.Qmax[h] * 1 * d.conv / d.ρ[h]
                # upstream spillage
                up_spill = @expression(m, [h=1:n.Hydros], length(d.upstream_spill[h]) >= 1 ? sum(spillage[j,var_hour,s] for j in d.upstream_spill[h] ) : 0.0)
                forced_up_spill = @expression(m, [h=1:n.Hydros], length(d.upstream_spill[h]) >= 1 ? sum(forced_spillage[j,var_hour,s] for j in d.upstream_spill[h] ) : 0.0)
                # upstream turbining
                up_turbining = @expression(m, [h=1:n.Hydros], length(d.upstream_turb[h]) >= 1 ? sum(turbining[j,var_hour,s] for j in d.upstream_turb[h] ) : 0.0 )
                for (idxh,h) in enumerate(d.reservoirs)
                    if day == 1 && etapa == 1 && var_hour == 1
                        stored_water = d.volini[h, s]
                    elseif var_hour == 1
                        stored_water = d.volendday[idxh, end, s]
                    else
                        stored_water = HydroVol[idxh, var_hour-1, s]
                    end

                    if d.hExist[h] == 0
                        HydroBalance =  @constraint(m, HydroVol[idxh, var_hour, s] ==
                            + stored_water
                            + conv * inflow[d.hyd2station[h]]
                            - conv * spillage[h,var_hour,s]
                            - conv * turbining[h,var_hour,s]
                            - conv * forced_spillage[h,var_hour,s]
                            # upstream plants
                            + conv * up_turbining[h]
                            + conv * up_spill[h]
                            + conv * forced_up_spill[h]
                        )
                        set_name(HydroBalance, "HBal($h,$s,$var_hour)")
                    else # plant does not turbine and dont have reservoir
                        HydroBalance =  @constraint(m, 0 ==
                            conv * inflow[d.hyd2station[h]]
                            - conv * spillage[h,var_hour,s]
                            - conv * turbining[h,var_hour,s]
                            - conv * forced_spillage[h,var_hour,s]
                            # upstream plants
                            + conv * up_turbining[h]
                            + conv * up_spill[h]
                            + conv * forced_up_spill[h]
                        )
                        set_name(HydroBalance, "HBal($h,$s,$var_hour)")
                    end
                end

                for h in d.runoffriver
                    if d.hExist[h] == 0
                        HydroBalance =  @constraint(m,0 ==
                            + conv * inflow[d.hyd2station[h]]
                            - conv * spillage[h,var_hour,s]
                            - conv * turbining[h,var_hour,s]
                            - conv * forced_spillage[h,var_hour,s]
                            # upstream plants
                            + conv * up_turbining[h]
                            + conv * up_spill[h]
                            + conv * forced_up_spill[h]
                        )
                        set_name(HydroBalance, "HBal($h,$s,$var_hour)")
                    else # plant does not turbine
                        HydroBalance =  @constraint(m,0 ==
                            + conv * inflow[d.hyd2station[h]] 
                            - conv * spillage[h,var_hour,s] 
                            - conv * turbining[h,var_hour,s]
                            - conv * forced_spillage[h,var_hour,s]
                            # upstream plants
                            + conv * up_turbining[h]
                            + conv * up_spill[h]
                            + conv * forced_up_spill[h]
                        )
                        set_name(HydroBalance, "HBal($h,$s,$var_hour)")
                    end
                end
            end
        end
    end
end

function thermal_generation_capacity!(m::JuMP.Model, prb::BidDispatchProblem, hour, s)
    n = prb.n
    d = prb.data
    options = prb.options
    var_hour = hour
    data_hour = use_buffer(prb, hour)
    if n.Thermals > 0
        x = get_commit(m,prb)

        TGen = get_tgen(m, prb)

        for (j, t) in enumerate(d.tExisting)
            if d.consider_commit[t] > 0
                MaximumGenerationThermal = @constraint(m, TGen[j,var_hour,s]  <= d.tPotInst[t] * x[j,var_hour,s])
            else
                MaximumGenerationThermal = @constraint(m, TGen[j,var_hour,s]  <= d.tPotInst[t])
            end
            set_name(MaximumGenerationThermal, "MaxGT($t,$(var_hour),$s)")
            # Minimum capacity
            if options.termingen
                gen_min = max(d.tGerMin[t], 0.0)
                gen_min = gen_min == 0.0 ? 0.05 * d.tGerMax[t] : gen_min
                MinGenTer = @constraint(m, TGen[j,var_hour,s] >= gen_min * x[j,var_hour,s])
                set_name(MinGenTer, "MinGT($t,$(var_hour),$s)")
            end
        end
    end
    nothing
end

function renewable_generation_offers!(m::JuMP.Model, prb::BidDispatchProblem, s)
    n = prb.n
    d = prb.data
    options = prb.options
    
    if n.Gnd > 0
        RGen = m[:RGenAux]
        RSpillPen = m[:RSpillPen]

        for ren in 1:n.Gnd, seg in 1:n.OfferSegments, h in 1:n.FullBuffer
            var_hour = h
            data_hour = use_buffer(prb, h)

            if options.PhysOper
                RenOffer = @constraint(m, RGen[ren,var_hour,s,seg]  <= d.sale_offer_gnd_acc[ren, data_hour, s, seg])
            else
                RenOffer = @constraint(m, RGen[ren,var_hour,s,seg]  <= d.sale_offer_gnd[ren, data_hour, s][seg].quantity)
            end

            set_name(RenOffer, "RenOffer($ren,$var_hour,$s,$seg)")
        end

        for ren in 1:n.Gnd, h in 1:n.FullBuffer
            var_hour = h
            data_hour = use_buffer(prb, h)
            if options.PhysOper
                RenSpillPenOffer = @constraint(m, RSpillPen[ren,var_hour,s]  == sum(d.sale_offer_gnd_acc[ren, data_hour, s, seg] - RGen[ren,var_hour,s,seg] for seg in 1:n.OfferSegments))
            else
                RenSpillPenOffer = @constraint(m, RSpillPen[ren,var_hour,s]  == sum(d.sale_offer_gnd[ren, data_hour, s][seg].quantity - RGen[ren,var_hour,s,seg] for seg in 1:n.OfferSegments))
            end
            set_name(RenSpillPenOffer, "RenSpillPenOffer($ren,$var_hour,$s)")
        end

    end
end

function thermal_generation_offers!(m::JuMP.Model, prb::BidDispatchProblem, s)
    n = prb.n
    d = prb.data
    options = prb.options
    
    if n.Thermals > 0
        TGen = m[:TGenAux].data

        for ter in 1:length(d.tExisting), seg in 1:n.OfferSegments, h in 1:n.FullBuffer
            var_hour = h
            data_hour = use_buffer(prb, h)
            if options.PhysOper
                ThermalOffer = @constraint(m, TGen[ter,var_hour,s,seg]  <= d.sale_offer_ter_acc[ter, data_hour, s, seg])
            else
                ThermalOffer = @constraint(m, TGen[ter,var_hour,s,seg]  <= d.sale_offer_ter[ter, data_hour, s][seg].quantity)
            end

            set_name(ThermalOffer, "ThermalOffer($ter,$var_hour,$s,$seg)")
        end
    end
end

function hydro_generation_offers!(m::JuMP.Model, prb::BidDispatchProblem, s)
    n = prb.n
    d = prb.data
    options = prb.options
    
    if n.Hydros > 0
        soldVR = m[:soldVR]
        boughtVR = m[:boughtVR]
        for vr in 1:n.VirtualReservoirs, seg in 1:n.OfferSegments
            if options.PhysOper
                HydroOfferSale = @constraint(m, sum(soldVR[vr,:,s,seg])  <= d.sale_offer_hyd_acc[vr, s, seg])
            else
                HydroOfferSale = @constraint(m, sum(soldVR[vr,:,s,seg])  <= d.sale_offer_hyd[vr, s][seg].quantity)
            end

            if options.PhysOper
                HydroOfferPurchase = @constraint(m, sum(boughtVR[vr,:,s,seg])  <= d.purchase_offer_hyd_acc[vr, s, seg])
            else
                HydroOfferPurchase = @constraint(m, sum(boughtVR[vr,:,s,seg])  <= d.purchase_offer_hyd[vr, s][seg].quantity)
            end

            set_name(HydroOfferSale, "HydroOfferSale($vr,$s,$seg)")
            set_name(HydroOfferPurchase, "HydroOfferPurchase($vr,$s,$seg)")
        end
    end
end

function hydro_generation_agent!(m::JuMP.Model, prb::BidDispatchProblem, s)
    n = prb.n
    d = prb.data

    hydro_offer_list = Array{ConstraintRef, 1}(undef, n.StorageRegions)

    if n.Hydros > 0
        boughtVR = m[:boughtVR]
        soldVR = m[:soldVR]
        HTurb = m[:HTurb]
        HSpil = m[:spillage]
        WaterIn = m[:WaterIn]
        WaterOut = m[:WaterOut]

        for k in 1:n.StorageRegions
            VR = 1:n.VirtualReservoirs
            H = 1:n.Hydros
            VR_rgn = VR[d.vr2rgn .== k]
            H_rgn = H[d.hyd2rgn .== k]

            H_in_spill = []
            H_in_turb = []
            H_out_spill = []
            H_out_turb = []

            for h in H_rgn
                for h_aux in d.upstream_spill[h]
                    if d.hyd2rgn[h_aux] != k
                        append!(H_in_spill, h_aux)
                    end
                end
                for h_aux in d.upstream_turb[h]
                    if d.hyd2rgn[h_aux] != k
                        append!(H_in_turb, h_aux)
                    end
                end
                if d.downstream_spill[h] != 0 && d.hyd2rgn[d.downstream_spill[h]] != k
                    append!(H_out_spill, h)
                end
                if d.downstream_turb[h] != 0 && d.hyd2rgn[d.downstream_turb[h]] != k
                    append!(H_out_turb, h)
                end
            end

            HydroPhy = @constraint(m, sum(d.fprodt[h] * (HTurb[h, hour, s] + HSpil[h, hour, s])  for h in H_rgn, hour in 1:n.FullBuffer)
            - sum((d.rho_region[h,k] - d.fprodt[h]) * HSpil[h, hour, s]  for h in H_in_spill, hour in 1:n.FullBuffer)
            - sum((d.rho_region[h,k] - d.fprodt[h]) * HTurb[h, hour, s]  for h in H_in_turb, hour in 1:n.FullBuffer)
            + sum((d.rho_region[h,k] - d.fprodt[h]) * HSpil[h, hour, s]  for h in H_out_spill, hour in 1:n.FullBuffer)
            + sum((d.rho_region[h,k] - d.fprodt[h]) * HTurb[h, hour, s]  for h in H_out_turb, hour in 1:n.FullBuffer)
            == sum(soldVR[vr,hour,s,seg] for vr in VR_rgn, hour in 1:n.FullBuffer, seg in 1:n.OfferSegments ) - sum(boughtVR[vr,hour,s,seg] for vr in VR_rgn, hour in 1:n.FullBuffer, seg in 1:n.OfferSegments ))
            
            hydro_offer_list[k] = HydroPhy
            set_name(HydroPhy, "HPhy($k,$s)")

            for hour in 1:n.FullBuffer
                BorderWaterIn = @constraint(m, WaterIn[k, hour, s] ==
                + sum(HSpil[h, hour, s]  for h in H_in_spill)
                + sum(HTurb[h, hour, s]  for h in H_in_turb) )
                set_name(BorderWaterIn, "BorderWaterIn($k,$hour,$s)")
                
                BorderWaterOut = @constraint(m, WaterOut[k, hour, s] ==
                + sum(HSpil[h, hour, s]  for h in H_out_spill)
                + sum(HTurb[h, hour, s]  for h in H_out_turb) )
                set_name(BorderWaterOut, "BorderWaterOut($k,$hour,$s)")
            end
        end
    end
    return hydro_offer_list
end

function hydro_forced_spillage!(m::JuMP.Model, prb::BidDispatchProblem, s, day, etapa)
    n = prb.n
    d = prb.data
    options = prb.options
    
    if n.Hydros > 0
        # m3/s -> hm3
        # 1m3 = 1.0E-6 hm3
        conv = 1.0*10^(-6) * 60 * 60
        bigM = d.upstream_vol[:,s]

        ForcSpil = m[:ForcedSpillage]
        vol_final = m[:HydroVol]

        for (i,h) in enumerate(d.reservoirs)

            if options.linear_forced_spillage
                x = bigM[h] >= d.volmax[h]
            else
                if bigM[h] < d.volmax[h]
                    x = 0
                else
                    x = m[:SpillControl][i,s]
                    x_lim = @constraint(m, x * (d.volmax[h]-d.volmin[h]) <= (vol_final[i,n.FullBuffer,s]-d.volmin[h]))
                    set_name(x_lim, "FSpillX($h,$s)")
                end
            end

            spill_lim = @constraint(m, conv * sum(ForcSpil[h, hour, s] for hour in 1:n.FullBuffer) <= bigM[h] * x)
            set_name(spill_lim, "FSpill($h,$s)")
        end
    end
end

function get_upstream_water!(prb::BidDispatchProblem, h, stored_water, index_water, volmax, offer_limit, bigMlist, use_limit, use_inflow)

    if bigMlist[h] != -1
        return
    end

    n = prb.n
    d = prb.data

    # m3/s -> hm3
    # 1m3 = 1.0E-6 hm3
    conv = 1.0*10^(-6) * 60 * 60

    upstream = unique(vcat(d.upstream_spill[h], d.upstream_turb[h]))

    if use_inflow
        inflow = conv * d.inflow[d.hyd2station[h]] * n.FullBuffer
    else
        inflow = 0.0
    end

    res_index = findfirst(x->x==h, d.reservoirs)

    if res_index === nothing # run-of-river
        vol = 0.0
    else
        vol = stored_water[index_water[res_index]] - d.volmin[h]
    end

    if isempty(upstream)
        bigMlist[h] = vol + inflow
    else
        upstream_water = 0.0
        for plant in upstream
            if bigMlist[plant] == -1
                get_upstream_water!(prb, plant, stored_water, index_water, volmax, offer_limit, bigMlist, use_limit, use_inflow)
            end
            # if forced_spillage is not allowed, we can consider the minimum between water and outflow limit
            if use_limit && (bigMlist[plant] < volmax[plant])
                upstream_water += minimum([bigMlist[plant], offer_limit[plant]])
            else
                upstream_water += bigMlist[plant]
            end
        end
        bigMlist[h] = upstream_water + vol + inflow
    end

    nothing

end


function get_bigM(prb::BidDispatchProblem, s, day, etapa, use_limit::Bool)
    n = prb.n
    d = prb.data

    if day == 1 && etapa == 1
        stored_water = d.volini[:, s]
        index_water = d.reservoirs
    else
        stored_water = d.volendday[:, end, s]
        index_water = range(1, length=length(d.reservoirs))
    end

    offer_limit = zeros(n.Hydros)
    VR = 1:n.VirtualReservoirs
    for h in 1:n.Hydros
        rgn = d.hyd2rgn[h]
        VR_rgn = VR[d.vr2rgn .== rgn]
        offer_limit[h] = sum(d.sale_offer_hyd[vr, s][seg].quantity for vr in VR_rgn, seg in n.OfferSegments)
    end

    bigMlist = -ones(n.Hydros)
    use_inflow = true

    for h in 1:n.Hydros
        get_upstream_water!(prb, h, stored_water, index_water, d.volmax, offer_limit, bigMlist, use_limit, use_inflow)
    end

    bigMlist = (bigMlist.*1.1).+10

    return bigMlist

end
"""
    Set objective function for hour
"""
function setobjective_exp!(m::JuMP.Model, prb::BidDispatchProblem, hour)

    # f = prb.rtflag
    d = prb.data
    n = prb.n
    options = prb.options
    quants = prb.data.quantities
    var_hour = hour
    data_hour = use_buffer(prb, hour)

    # load variables in to scope
    # --------------------------
    if n.Thermals>0
        g_ter = get_tgen(m, prb)
        g_ter_aux = m[:TGenAux].data
        if options.ramp
            ramp_slack = m[:ramp_slack]
        end
        if options.commitment
            ShutDown = m[:STdn]
        end
        if options.commitment
            StartUp = m[:STup]
        end
        if d.has_resPup
            PRTup=m[:PRTup]
        end
        if d.has_resPdn
            PRTdn=m[:PRTdn]
        end
        if d.has_resSup
            SRTup=m[:SRTup]
        end
        if d.has_resSdn
            SRTdn=m[:SRTdn]
        end
        if d.has_resT
            TertiaryReserveThermal=m[:TertiaryReserveThermal]
        end
    end
    def = m[:def]
    if n.Hydros>0
        def_slack = m[:def_slack]
        soldVR = m[:soldVR]
        boughtVR = m[:boughtVR]
        if d.has_resPup
            PrimaryReserveHydroUp = m[:PrimaryReserveHydroUp]
        end
        if d.has_resPdn
            PrimaryReserveHydroDown=m[:PrimaryReserveHydroDown]
        end
        if d.has_resSup
            SRHup=m[:SRHup]
        end
        if d.has_resSdn
            SRHdn=m[:SRHdn]
        end
        if d.has_resT
            TertiaryReserveHydro=m[:TertiaryReserveHydro]
        end
    end
    if n.Gnd > 0
        g_gnd = m[:RGen]
        g_gnd_aux = m[:RGenAux]
        RSpillPen = m[:RSpillPen]
    end

    # regularization
    reg = 1

    # Deficit cost
    for sys in 1:size(def)[1]
        for scen in 1:n.Scenarios
            add_to_expression!(d.fobj.DeficitCost, d.dcost * def[sys, var_hour, scen] / n.Scenarios)
        end
    end

    # penalty cost
    if options.PhysOper && n.Hydros > 0
        spillage =m[:spillage]
        forced_spillage = m[:ForcedSpillage]
        # m3/s -> hm3
        # 1m3 = 1.0E-6 hm3
        conv = 1.0*10^(-6) * 60 * 60
        add_to_expression!(d.fobj.PenaltySpillage, @expression(m, options.spill_pen_weight * sum(spillage[h,var_hour,s] + forced_spillage[h,var_hour,s] for h=1:n.Hydros, s=1:n.Scenarios) * conv / n.Scenarios))
        if (options.penwaveguide == 0 && options.useWaveguide != none)
            HydroVolw = m[:HydroVolw]
            HydroVol = m[:HydroVol]
            WlinE = m[:WlinE]
            for r in 1:length(d.reservoirs)
                for scen in 1:n.Scenarios
                    @constraint(m, WlinE[r,scen] >= HydroVol[r,n.FullBuffer,scen] - HydroVolw[r,scen])
                    @constraint(m, WlinE[r,scen] >= - HydroVol[r,n.FullBuffer,scen] + HydroVolw[r,scen])
                end
            end
            add_to_expression!(d.fobj.PenaltyWaveGuide1, @expression(m, options.waveguide_pen_weight*sum(WlinE) / n.Scenarios))

        elseif options.useWaveguide != none
            HydroVolw = m[:HydroVolw]
            HydroVol = m[:HydroVol]
            WaveguideAux = m[:WaveguideAux]
            for r in 1:length(d.reservoirs)
                for scen in 1:n.Scenarios
                    add_to_expression!(d.fobj.PenaltyWaveGuide,options.waveguide_pen_weight/ n.Scenarios, WaveguideAux[r,scen], WaveguideAux[r,scen])
                end
            end
        end
    end

    # Thermal Cost
    # ------------

    if n.Thermals > 0

        # thermal cost

        ofertas_preco_termo = [d.sale_offer_ter[j, data_hour, s][seg].price for j in 1:n.Thermals, s in 1:n.Scenarios, seg in 1:n.OfferSegments]
        add_to_expression!(d.fobj.thermalCosts, @expression(m, sum( reg  * ofertas_preco_termo[j, s, seg] * g_ter_aux[j, var_hour, s, seg] / n.Scenarios for j in 1:length(d.tExisting), s in 1:n.Scenarios, seg in 1:n.OfferSegments if ofertas_preco_termo[j ,s, seg] < Inf)))
        # startup cost
        # simple startup cost
        if options.commitment #!options.startupcost
            add_to_expression!(d.fobj.CommitmentCost, @expression(m, reg *sum( d.tShutDownCost[j] * ShutDown[j, var_hour,s] for j in 1:length(d.tExisting), s in 1:n.Scenarios )))
            add_to_expression!(d.fobj.CommitmentCost, @expression(m, reg *sum( d.tStartUpCost[j] * StartUp[j, var_hour,s] for j in 1:length(d.tExisting),s in 1:n.Scenarios)))
            if options.ramp
                add_to_expression!(d.fobj.CommitmentCost, @expression(m, reg *sum( n.FullBuffer*d.dcost * ramp_slack[j, var_hour,s] for j in 1:length(d.tExisting),s in 1:n.Scenarios)))
            end
        elseif options.startupcost && any( ( d.startupHot .+ d.startupWarm .+ d.startupCold) .> 0)
            if d.startup_old
                add_to_expression!(d.fobj.CommitmentCost, @expression(m, reg *sum( d.startupCold[j] * StartUp[j,var_hour,s] for j in d.tExisting, s in 1:n.ScenariosExtended if d.tExist[j] <= 0)))
            else
                c   = m[:STupCost]
                add_to_expression!(d.fobj.CommitmentCost, @expression(m, reg * sum( c[j,var_hour,s] for j in d.tExisting, s in 1:n.ScenariosExtended if d.tExist[j] <= 0)))
            end
        end
        # shutdown cost
        #if options.startupcost && any(d.tShutDownCost .> 0)
        #    add_to_expression!(d.fobj.CommitmentCost, reg *sum( d.tShutDownCost[j] * ShutDown[j,hour] for j in 1:length(d.tExisting), s in 1:n.ScenariosExtended ))
        #end
        # Reserve Cost
        # ------------
        # primary up and down must have the same plants and reserves
        for (j,t) in enumerate(d.tExisting)
            #TODO: reserva na fd.fobj
            if d.has_resPup
                add_to_expression!(d.fobj.ReserveCost, @expression(m, quants["PresPup"][d.therm2resPup[t]] * sum(PRTup[j,:,var_hour])))
            end
            if d.has_resPdn
                add_to_expression!(d.fobj.ReserveCost, @expression(m, quants["PresPdn"][d.therm2resPdn[t]] * sum(PRTdn[j, :,var_hour])))
            end
            # secondary
            if d.has_resSup && d.therm2QresSup[t] > 0
                add_to_expression!(d.fobj.ReserveCost, @expression(m, quants["PresSup"][d.therm2QresSup[t]] * sum(SRTup[j, :,var_hour])))
            end
            if d.has_resSdn && d.therm2QresSdn[t] > 0
                add_to_expression!(d.fobj.ReserveCost, @expression(m, quants["PresSdn"][d.therm2QresSdn[t]] * sum(SRTdn[j, :,var_hour])))
            end
            # tertiary
            if d.has_resT
                add_to_expression!(d.fobj.ReserveCost, @expression(m, quants["PresT"][d.therm2resT[t]] * sum(TertiaryReserveThermal[j, :, var_hour])))
            end
        end
    end
    # Hydro Cost
    # ----------
    if n.Hydros > 0
        ofertas_preco_hidro = [d.sale_offer_hyd[vr, s][seg].price for vr in 1:n.VirtualReservoirs, s in 1:n.Scenarios, seg in 1:n.OfferSegments]
        demanda_preco_hidro = [d.purchase_offer_hyd[vr, s][seg].price for vr in 1:n.VirtualReservoirs, s in 1:n.Scenarios, seg in 1:n.OfferSegments]
        add_to_expression!(d.fobj.hydroCosts, @expression(m, sum( reg * ofertas_preco_hidro[i, s, seg] * soldVR[i, var_hour, s, seg]/ n.Scenarios for i in 1:n.VirtualReservoirs, s in 1:n.Scenarios, seg in 1:n.OfferSegments if ofertas_preco_hidro[i, s, seg] < Inf)))
        add_to_expression!(d.fobj.hydroCosts, @expression(m, -sum( reg * demanda_preco_hidro[i, s, seg] * boughtVR[i, var_hour, s, seg]/ n.Scenarios for i in 1:n.VirtualReservoirs, s in 1:n.Scenarios, seg in 1:n.OfferSegments if ofertas_preco_hidro[i, s, seg] < Inf)))

        # outflow slack
        add_to_expression!(d.fobj.hydroCosts, @expression(m, reg * sum(def_slack) / (n.Scenarios)))

        # Reserve Cost
        # ------------
        # primary up and down must have the same plants and reserves
        for j in 1:n.Hydros
            if d.has_resPup
                add_to_expression!(d.fobj.ReserveCost, @expression(m, quants["PresPup"][d.resPup2hyd[j]] * sum(PrimaryReserveHydroUp[j, :, var_hour])))
            end
            if d.has_resPdn
                add_to_expression!(d.fobj.ReserveCost, @expression(m, quants["PresPdn"][d.resPdn2hyd[j]] * sum(PrimaryReserveHydroDown[j, :, var_hour])))
            end
            # secondary
            if d.has_resSup && d.PresSup2hyd[j] > 0
                add_to_expression!(d.fobj.ReserveCost, @expression(m, quants["PresSup"][d.PresSup2hyd[j]] * sum(SRHup[j, :, var_hour])))
            end
            if d.has_resSdn && d.PresSdn2hyd[j] > 0
                add_to_expression!(d.fobj.ReserveCost, @expression(m, quants["PresSdn"][d.PresSdn2hyd[j]] * sum(SRHdn[j, :, var_hour]) ))
            end
            # tertiary
            if d.has_resT
                add_to_expression!(d.fobj.ReserveCost, @expression(m, quants["PresT"][d.resT2hyd[j]] * sum(TertiaryReserveHydro[j, :, var_hour])))
            end
        end
    end

    if n.Gnd > 0
        ofertas_preco_gnd = [d.sale_offer_gnd[i, data_hour, s][seg].price for i in 1:n.Gnd, s in 1:n.Scenarios, seg in 1:n.OfferSegments]
        add_to_expression!(d.fobj.RenewableCosts, @expression(m, sum( reg * ofertas_preco_gnd[i, s, seg] * g_gnd_aux[i, var_hour, s, seg]/ n.Scenarios for i in 1:n.Gnd, s in 1:n.Scenarios, seg in 1:n.OfferSegments if ofertas_preco_gnd[i, s, seg] < Inf)))

        add_to_expression!(d.fobj.RenewableCosts, @expression(m, sum( reg * d.gPenalty[i] * RSpillPen[i,var_hour,s]/ n.Scenarios for i in 1:n.Gnd, s in 1:n.Scenarios)))
    end

    nothing
end

"""
    Add objective function to Model
"""
function setobjective!(m::JuMP.Model, prb::BidDispatchProblem)
    fobj = prb.data.fobj
    options = prb.options
    totalcost = fobj.hydroCosts + fobj.thermalCosts + fobj.CommitmentCost + fobj.RenewableCosts + fobj.ReserveCosts + fobj.DeficitCost + fobj.FutureCost + (options.PhysOper ? (options.penwaveguide == 0 ? fobj.PenaltyWaveGuide1 + fobj.PenaltySpillage : fobj.PenaltyWaveGuide2 + fobj.PenaltySpillage) : 0)
    @objective(m , Min , totalcost)
    nothing
end

function write_fobj(prb::BidDispatchProblem)
    fobj = prb.data.fobj
    filename = "objective_function.csv"

    open(joinpath(prb.options.INPUTPATH, filename), "w") do f
        header = "hydroCosts, FutureCost, thermalCosts, CommitmentCost, RenewableCosts, ReserveCosts, DeficitCost"
        write(f, header*"\n")

        values = "$(sum(JuMP.value.(fobj.hydroCosts))), $(sum(JuMP.value.(fobj.FutureCost))), $(sum(JuMP.value.(fobj.thermalCosts))), $(sum(JuMP.value.(fobj.CommitmentCost))), $(sum(JuMP.value.(fobj.RenewableCosts))), $(sum(JuMP.value.(fobj.ReserveCosts))), $(sum(JuMP.value.(fobj.DeficitCost)))"
        write(f, values*"\n")
    end
end

"""
    Solve problem as MIP
"""
function solve(m::JuMP.Model, prb::BidDispatchProblem)
    if prb.options.solver_id == xpress
        set_optimizer(m, Xpress.Optimizer) 
        set_optimizer_attribute(m, "logfile", prb.options.INPUTPATH*"xpress.log")
    elseif prb.options.solver_id == glpk
        set_optimizer(m, GLPK.Optimizer) 
    elseif prb.options.solver_id == cbc_clp
        set_optimizer(m, Cbc.Optimizer)
    elseif prb.options.solver_id == cplex
        set_cplex_optimizer(m) 
    else
        println("options.solver_id = ", prb.options.solver_id)
        error("Nenhum solver foi escolhido.")
    end
    # solve MILP
    # if prb.options.solver_id == xpress
        write_to_file( m , joinpath(prb.options.INPUTPATH , "milp_problem.lp") )
    # end

    if prb.options.SolverTimeLimit != 0.0
        set_time_limit_sec(m, prb.options.SolverTimeLimit)
    end

    if prb.options.solver_id == xpress
        set_optimizer_attribute(m, "MIPRELSTOP", prb.options.RelativeGap)
    elseif prb.options.solver_id == glpk
        set_optimizer_attribute(m, "mip_gap", prb.options.RelativeGap)
    elseif prb.options.solver_id == cbc_clp
        set_optimizer_attribute(m, "optcr", prb.options.RelativeGap)
    elseif prb.options.solver_id == cplex
        set_optimizer_attribute(m, "CPX_PARAM_EPGAP", prb.options.RelativeGap)
    end
    
    optimize!(m)
    if termination_status( m ) == MOI.OPTIMAL
        @show termination_status( m )
        @show objective_value(m)
        solveLP(prb, m, m)
        # write_fobj(prb)
    elseif termination_status( m ) == MOI.TIME_LIMIT
        if primal_status(m) == MOI.FEASIBLE_POINT
            @show termination_status( m )
            @show objective_value(m)
            solveLP(prb, m, m)
            # write_fobj(prb)
        else
            throw("ERROR: Couldn't solve the MIP problem: $(termination_status( m )) and $(primal_status( m ))")
        end
    else
        throw("ERROR: Couldn't solve the MIP problem: $(termination_status( m ))")
    end
    return m
end

"""
    Fix integer variables and solve problem as LP
"""
function solveLP(prb::BidDispatchProblem, m2::JuMP.Model, m::JuMP.Model)
    d = prb.data
    n = prb.n
    options = prb.options
    # Resolve o PL fixando as variáveis binárias para obter as duais.
    # get values
    STup = zeros(length(d.tExisting),n.FullBuffer,n.Scenarios)
    STdn = zeros(length(d.tExisting),n.FullBuffer,n.Scenarios)
    COMT = zeros(length(d.tExisting),n.FullBuffer,n.Scenarios)
    SpillControl = zeros(length(d.reservoirs), n.Scenarios)
    if options.commitment && n.Thermals > 0
        STup = JuMP.value.(m[:STup])
        STdn = JuMP.value.(m[:STdn])
        COMT = JuMP.value.(m[:COMT]) #get_commit_value(m,prb) #fixcommit
    end
    if options.PhysOper && (n.Hydros > 0) && (!options.linear_forced_spillage)
        for i in 1:length(d.reservoirs), s=1:n.Scenarios
            if d.upstream_vol[d.reservoirs[i],s] >= d.volmax[d.reservoirs[i]]
                SpillControl[i,s] = JuMP.value(m[:SpillControl][i,s])
            end
        end
    end
    # modify problem
    if options.commitment && n.Thermals > 0
        @constraint(m2, m2[:STup] .== STup)
        @constraint(m2, m2[:STdn] .== STdn)
        @constraint(m2, m2[:COMT] .== COMT)
        unset_binary.(m2[:COMT])
    end
    if options.PhysOper && (n.Hydros > 0) && (!options.linear_forced_spillage)
        for i in 1:length(d.reservoirs), s=1:n.Scenarios
            if d.upstream_vol[d.reservoirs[i],s] >= d.volmax[d.reservoirs[i]]
                @constraint(m2, m2[:SpillControl][i, s] .== SpillControl[i, s])
                unset_binary.(m2[:SpillControl][i, s])
            end
        end
    end

    if prb.options.solver_id == xpress
        write_to_file( m , joinpath(prb.options.INPUTPATH , "lp_problem.lp") )
    end

    if prb.options.solver_id == cbc_clp
        set_optimizer(m, Clp.Optimizer) 
    end
    optimize!(m2)
    @show termination_status(m2)
    @show objective_value(m2)
    nothing
end

#TODO parece que falta um ! no nome dessa função
function hydro_storedenergy(m, prb::BidDispatchProblem,s)
    n = prb.n
    d = prb.data

    rho = d.rho

    if n.Hydros > 0
        HydroVol = m[:HydroVol]
        Lambda = m[:Lambda]

        EnerAC = @constraint(m, 
        sum(rho[d.reservoirs[index]]*(Lambda[i,s]*d.waveguide.valores[i,1,1,d.reservoirs[index]] - d.volmin[d.reservoirs[index]]) for i in 1:n.wstage,index in 1:length(d.reservoirs)) ==
        sum((HydroVol[index, n.FullBuffer, s] - d.volmin[d.reservoirs[index]]) * rho[d.reservoirs[index]] for  index in 1:length(d.reservoirs)))
        set_name(EnerAC, "EnerAC($s)")
    end
end

function waveguide_balance!(m, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data

    if n.Hydros > 0
        HydroVolw = m[:HydroVolw]
        Lambda = m[:Lambda]
        for s in 1:n.Scenarios
            for (index,h) in enumerate(d.reservoirs)
                wavebalance = @constraint(m, HydroVolw[index,s] == sum( Lambda[i,s]*d.waveguide.valores[i,1,1,h] for i in 1:n.wstage))
                set_name(wavebalance, "wavebalance($index,$s)")
            end
        end
    end
end

function downstream_ρ(h::Int, prb::BidDispatchProblem; with_future::Bool = false, mean = false)

    d = prb.data
    n = prb.n
    rho = 0.0#hydro_operating(d, h) ? d.ρ[h] : 0.0
    nextplant = h#d.JT[h]
    added = BitSet()
    # if startswith(d.hName[])
    while nextplant != 0
        if nextplant in added
            break
        else
            push!(added, nextplant)
        end
        # if d.ρ[nextplant] < 0.0
        #     break
        # end
        rho += max(0.0, d.fprodt[nextplant])
        # rho += d.FPMed[nextplant]
        # CAR uses ENA cascade
        nextplant = d.downstream_turb[nextplant]
    end

    return rho
end
function downstream_ρ(prb::BidDispatchProblem; with_future::Bool = false, mean = false)

    n = prb.n
    rho = zeros(n.Hydros)
    for i in 1:n.Hydros
        rho[i] = downstream_ρ(i, prb, with_future = with_future, mean = mean)
    end

    return rho
end

function downstream_ρ_region(h::Int,r::Int, prb::BidDispatchProblem; with_future::Bool = false, mean = false)

    d = prb.data
    n = prb.n
    rho_region = 0.0#hydro_operating(d, h) ? d.ρ[h] : 0.0
    nextplant = h#d.JT[h]
    added = BitSet()
    # if startswith(d.hName[])
    while nextplant != 0
        if nextplant in added
            break
        else
            push!(added, nextplant)
        end
        # if d.ρ[nextplant] < 0.0
        #     break
        # end
        if d.hyd2rgn[nextplant] == r
            rho_region += max(0.0, d.fprodt[nextplant])
        end
        # rho += d.FPMed[nextplant]
        # CAR uses ENA cascade
        nextplant = d.downstream_turb[nextplant]
    end

    return rho_region
end
function downstream_ρ_region(prb::BidDispatchProblem; with_future::Bool = false, mean = false)

    d = prb.data
    n = prb.n
    rho_region = zeros(n.Hydros,n.StorageRegions)
    for i in 1:n.Hydros, r in 1:n.StorageRegions
        rho_region[i,r] = downstream_ρ_region(i, r, prb, with_future = with_future, mean = mean)
    end

    return rho_region
end