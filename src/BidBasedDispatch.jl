module BidBasedDispatch

using Cbc
using Clp
using CSV
using DataFrames
using GLPK
using JuMP
import Libdl
using LinearAlgebra
using TOML
using Statistics
# using SpecialFunctions
using Xpress
using Requires
using PSRIO

function __init__()
    @require XpressPSR="3a57e2e4-7268-4354-91dd-d57606666fa4" @eval XpressPSR.initialize()
    @require CPLEX="a076750e-1247-5638-91d2-ce28b192dca0"  @eval set_cplex_optimizer(m) = JuMP.set_optimizer(m, CPLEX.Optimizer)
end


# core code
include("types.jl")
include("mapping.jl")
include("build_vars.jl")
include("build_problem.jl")
include("utils.jl")

# IO/file
include("inputs.jl")
include("write_outputs.jl")
include("IO.jl")

# PSRClasses
include("PSRClassesUtils.jl")
include("psrc_mapping.jl")

# BidBased Modules
include("ModEnergyBudget.jl")
include("ModWizard.jl")
include("ModMarket.jl")
include("ModPhy.jl")
include("ModMarketPhys.jl")
include("ModSettlement.jl")
include("RealTime.jl")

PATH_IHM = joinpath(@__DIR__, "..", "bidbaseddispatchihm", "Oper")
PATH_PSRC = joinpath(@__DIR__, "..", "deps", "psrclassesinterfacejulia")
include(joinpath(PATH_PSRC, "PSRClasses.jl"))

include(joinpath(@__DIR__,"..", "deps", "psrnetwork-distribution", "include", "psrnetwork.jl"))
ret = PSRClasses_init(PATH_PSRC)

using .PSRNetwork

include("main_routines.jl")

end