"""
(D) Modulo op fis

input:
output:
"""
function market_and_phys_oper(prb::BidDispatchProblem, day, etapa)

    IO_building_model(prb)

    start = time()

    d = prb.data
    n = prb.n
    options = prb.options
    options.maxupdntime = false
    options.PhysOper = true
    options.network = options.network_phys

    loadbal_refs = Array{JuMP.ConstraintRef, 3}(undef, n.SpotPrice, n.FullBuffer, n.Scenarios)
    loadvr_refs = Array{JuMP.ConstraintRef, 2}(undef, n.StorageRegions, n.Scenarios)

    if n.Hydros > 0
        d.upstream_vol = zeros(n.Hydros, n.Scenarios)
        for s in 1:n.Scenarios
            d.upstream_vol[:,s] = get_bigM(prb, s, day, etapa, options.linear_forced_spillage)
        end
    end

    m = build_archetype(prb)
    
    w_balance!(m, prb, day, etapa)
    
    d.rho = downstream_ρ(prb; with_future = false, mean = false)
    if options.useWaveguide != none
        waveguide_balance!(m, prb)
    end

    for hour in 1:n.FullBuffer
        d.hour = hour
        for scen in 1:n.Scenarios
            d.scen = scen
            
            pull_scenarios_data!(prb, Int32(hour), Int32(scen))

            if options.DayAhead
                thermal_unit_COMT(m, prb, hour, n.FullBuffer , scen)
            end

            if options.ramp
                ramp_down!(m, prb, hour, scen, n.FullBuffer)
                ramp_up!(m, prb, hour, scen, n.FullBuffer)
            end

            loadbal_refs[:, hour, scen] = load_balance!(m, prb, day, hour, scen)
        end
        setobjective_exp!(m, prb, hour)
    end

    options.PhysOper = false # problema simultaneo é semelhante à operacao fisica, mas com ofertas originais e variaveis de COMT

    for hour in 1:n.FullBuffer
        d.hour = hour
        for scen in 1:n.Scenarios
            d.scen = scen
            thermal_generation_capacity!(m, prb, hour, scen)
        end
    end

    for scen in 1:n.Scenarios
        hydro_generation_offers!(m, prb, scen)
        thermal_generation_offers!(m, prb, scen)
        renewable_generation_offers!(m, prb, scen)
    end

    options.PhysOper = true

    for scen in 1:n.Scenarios
        pull_inflow_data!(prb, Int32(scen))
        hydro_forced_spillage!(m, prb, scen, day, etapa)
        if n.Hydros > 0
            loadvr_refs[:,scen] = hydro_generation_agent!(m, prb, scen)
        end
    end

    setobjective!(m, prb)

    # Solve
    IO_solving_problem(prb)
    m = solve(m, prb)

    # Model outputs
    d.preco_spot_dia = ([dual(x) for x in loadbal_refs] .* n.Scenarios)

    if n.Hydros > 0
        d.pricevr = ([dual(x) for x in loadvr_refs].*n.Scenarios.*(-1))
        d.sale_offer_hyd_acc = sum(JuMP.value.(m[:soldVR]), dims=2)[:, 1, :, :]
        d.purchase_offer_hyd_acc = sum(JuMP.value.(m[:boughtVR]), dims=2)[:, 1, :, :]
        
        if !options.DayAhead
            d.volendday = JuMP.value.(m[:HydroVol])
        end
    end

    if n.Thermals > 0
        d.sale_offer_ter_acc = JuMP.value.(m[:TGenAux]).data
        if options.DayAhead
            d.dailyCOMT = convert_binary_variable_value_to_Int32.(value.(m[:COMT]))
        end

        if options.commitment && !options.DayAhead

            d.initial_power = transpose(JuMP.value.(m[:TGen]).data[:,n.MaxHours,:])
            d.initial_state_commit = permutedims(d.dailyCOMT[:,n.MaxHours,:], [2,1])

            x = d.dailyCOMT
            for s in 1:n.Scenarios, t in 1:length(d.tExisting)
                sign = (x[t,n.MaxHours,s] == 1 ? 1 : -1)
                value = 1
                for hour in 1:n.MaxHours-1
                    if x[t,n.MaxHours-hour,s] == x[t,n.MaxHours,s]
                        value += 1
                    else
                        break
                    end
                end
                d.initial_state_time[s,t] = (value == n.MaxHours && sign*prb.data.initial_state_time[s,t] > 0 ? sign*value + prb.data.initial_state_time[s,t] : sign*value )
            end
        end
    end

    if n.Gnd > 0
        d.sale_offer_gnd_acc = JuMP.value.(m[:RGenAux])
    end

    # Write to file
    if termination_status( m ) == MOI.OPTIMAL
        print_msg(prb, "Optimal solution found!")
        # write outputs
        IO_writing_results(prb)

        elapsed = trunc(time() - start)
        print_global_results!(prb, m, elapsed)
        # write_fobj(prb)

        if prb.options.solver_id == xpress
            # prb_type = (options.DayAhead ? "DA" : "RT")
            # write_to_file( m , joinpath( options.INPUTPATH , string("market_phys_", prb_type, "_", options.stage, "_", day, ".lp") ) )
            write_to_file( m , joinpath( options.INPUTPATH , "market_phys.lp") )
        end
    end
    return m
end
