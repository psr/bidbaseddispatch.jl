function load_interface!(prb::BidDispatchProblem)

    psrc = prb.psrc
    options = prb.options
    n = prb.n
    data = prb.data
    config_ptr= PSRStudy_getConfigurationModel(psrc.study_ptr)

    ponteiro = PSRModel_parm2(config_ptr , "Tipo_Etapa")
    options.stageType = PSRParm_getInteger(ponteiro)
    ponteiro = PSRModel_parm2(config_ptr , "Ano_inicial")
    options.firstYearSDDP = PSRParm_getInteger(ponteiro)
    ponteiro = PSRModel_parm2(config_ptr , "Etapa_inicial")
    options.firstStageSDDP = PSRParm_getInteger(ponteiro)

    ponteiro = PSRModel_parm2(config_ptr , "InitialYearBID")
    options.firstYear = PSRParm_getInteger(ponteiro)
    ponteiro = PSRModel_parm2(config_ptr , "InitialStageBID")
    options.firstWeek = PSRParm_getInteger(ponteiro)
    options.FirstDay = 1 + (options.firstWeek-1)*7

    ponteiro = PSRModel_parm2(config_ptr , "ConsiderNetwork")
    options.network_phys = ConsiderNetwork(PSRParm_getInteger(ponteiro))
    ponteiro = PSRModel_parm2(config_ptr , "useWaveguide")
    options.useWaveguide = WaveguideType(PSRParm_getInteger(ponteiro))
    ponteiro = PSRModel_parm2(config_ptr , "DeficitCostBID")
    data.dcost = PSRParm_getReal(ponteiro)
    ponteiro = PSRModel_parm2(config_ptr , "Weeks")
    n.Stages = PSRParm_getInteger(ponteiro)
    ponteiro = PSRModel_parm2(config_ptr , "Scenarios")
    n.Scenarios = PSRParm_getInteger(ponteiro)
    n.ScenariosExtended = PSRParm_getInteger(ponteiro)
    ponteiro = PSRModel_parm2(config_ptr , "SolverID")
    options.solver_id = SolverId(PSRParm_getInteger(ponteiro))

    ponteiro = PSRModel_parm2(config_ptr , "SolverTimeLimit")
    options.SolverTimeLimit = PSRParm_getReal(ponteiro)

    ponteiro = PSRModel_parm2(config_ptr , "RelativeGap")
    options.RelativeGap = PSRParm_getReal(ponteiro)

    ponteiro = PSRModel_parm2(config_ptr , "Buffer")
    n.Buffer = PSRParm_getInteger(ponteiro)
    n.FullBuffer = n.Buffer + n.MaxHours
    ponteiro = PSRModel_parm2(config_ptr , "SlackRamp")
    options.SlackRamp = PSRParm_getInteger(ponteiro)

    ponteiro = PSRModel_parm2(config_ptr , "GameType")
    options.gametype = GameType(PSRParm_getInteger(ponteiro))
    ponteiro = PSRModel_parm2(config_ptr , "SimulOpt")
    options.SimulOpt = PSRParm_getInteger(ponteiro)
    ponteiro = PSRModel_parm2(config_ptr , "SpillPenWeight")
    options.spill_pen_weight = PSRParm_getReal(ponteiro)
    ponteiro = PSRModel_parm2(config_ptr , "WaveguidePenWeight")
    options.waveguide_pen_weight = PSRParm_getReal(ponteiro)
    ponteiro = PSRModel_parm2(config_ptr , "day")
    options.day = PSRParm_getInteger(ponteiro)
    ponteiro = PSRModel_parm2(config_ptr , "week")
    options.week = PSRParm_getInteger(ponteiro)
    ponteiro = PSRModel_parm2(config_ptr , "year")
    options.year = PSRParm_getInteger(ponteiro)
    ponteiro = PSRModel_parm2(config_ptr , "minupdntime")
    options.minupdntime = PSRParm_getInteger(ponteiro)
    ponteiro = PSRModel_parm2(config_ptr , "ramp")
    options.ramp = PSRParm_getInteger(ponteiro)
    ponteiro = PSRModel_parm2(config_ptr , "maxt")
    options.maxt = PSRParm_getInteger(ponteiro)
    ponteiro = PSRModel_parm2(config_ptr , "inicondgraf")
    options.inicondgraf = PSRParm_getInteger(ponteiro)
    ponteiro = PSRModel_parm2(config_ptr , "UseRTFiles")
    options.useRTFiles = InputFileType(PSRParm_getInteger(ponteiro))
    ponteiro = PSRModel_parm2(config_ptr , "UsePurchaseVRFiles")
    options.UsePurchaseVRFiles = PSRParm_getInteger(ponteiro)    
    ponteiro = PSRModel_parm2(config_ptr , "LinearForcedSpillage")
    options.linear_forced_spillage = PSRParm_getInteger(ponteiro)

    

    options.offset =  evaluateOffset(prb)

end

#PSRCGraf

function nextRegistry!(graf::PSRCGraf)
    nothing
end

function readGraf(PATH_CASE::String, header::Vector{String}, filename::String, single_stage::Int64, stage_type::StageType, Scenarios::Int64, ano_inicial::Int64, etapa_inicial::Int64, etapa::Int64 = 1, dia::Int64 = 1, etapa_arquivo::Int32 = Int32(0))

    path = joinpath(PATH_CASE, filename)
    if isfile(path*".csv") && isfile(path*".bin")
        error("Arquivo $filename tem as versões .bin e .csv na pasta $PATH_CASE. Só pode ter uma versão.")
    elseif !isfile(path*".csv") && !isfile(path*".bin")
        error("Arquivo $filename não tem as versões .bin e .csv na pasta $PATH_CASE.")
    elseif isfile(path*".csv")
        typefile = "csv"
    else
        typefile = "bin"
    end
        
    # abrir o arquivo
    if typefile == "csv"
        ptr_pointer = PSRIOGrafResult_create(0) 
        PSRIOGrafResult_initLoad( ptr_pointer, path*".csv", PSRIO_GRAF_FORMAT_EXTENDED)
    else
        ptr_pointer = PSRIOGrafResultBinary_create(0)
        PSRIOGrafResultBinary_initLoad( ptr_pointer, path*".hdr",path*".bin")
    end

    # pegar informações do arquivo
    n_etapas = PSRIOGrafResultBase_getTotalStages(ptr_pointer)
    n_series = PSRIOGrafResultBase_getTotalSeries(ptr_pointer)
    n_blocos = PSRIOGrafResultBase_getTotalBlocks(ptr_pointer)
    n_agentes = PSRIOGrafResultBase_maxAgent(ptr_pointer)
    etapa_inicial_arquivo = PSRIOGrafResultBase_getInitialStage(ptr_pointer)
    ano_inicial_arquivo = PSRIOGrafResultBase_getInitialYear(ptr_pointer)

    print_inflow_warning = false
    if filename == "inflow"
        file_stage_type = PSRIOGrafResultBase_getStageType(ptr_pointer)
        # Monthly
        if file_stage_type == 2
            # mes_corrente = floor((7 * (etapa + etapa_inicial - 2) + (dia-1))/30) + 1
            mes_corrente = floor((etapa + etapa_inicial - 1)/4)
            etapa = 1 + (mes_corrente - etapa_inicial_arquivo) + 12 * (ano_inicial - ano_inicial_arquivo)
            print_inflow_warning = true
        elseif file_stage_type == 1
            etapa += (etapa_inicial - etapa_inicial_arquivo) + 52 * (ano_inicial - ano_inicial_arquivo)
        end
        if etapa == etapa_arquivo
            return nothing
        end
    else
        if stage_type == daily::StageType
            etapa += (etapa_inicial - etapa_inicial_arquivo) + 365 * (ano_inicial - ano_inicial_arquivo)
        elseif stage_type == weekly::StageType
            etapa += (etapa_inicial - etapa_inicial_arquivo) + 52 * (ano_inicial - ano_inicial_arquivo)
        end
    end

    if etapa <= n_etapas && etapa >= 1
        PSRIOGrafResultBase_seekStage( ptr_pointer, etapa )
    else
        error("Erro ao tentar ler etapa $etapa do arquivo $filename.")
    end

    # header

    names = String[]
    for i in 1:n_agentes
        name = strip(PSRIOGrafResultBase_getAgent(ptr_pointer, i-1), [' ', '\"'])
        push!(names, name)
    end
    agent_index = Array{Int32}(undef, n_agentes)
    for (i,agent) in enumerate(header)
        _agent = strip(agent)
        ret = findfirst(x -> x == _agent, names)
        if ret === nothing
            error("agent $(_agent) not found in file")
        end
        agent_index[ret] = i
    end
    # ler os dados
    # arquivo etapa =1 serie =1 bloco =1
    # loop na etapa, serie, bloco
    if single_stage == 1

        valores = zeros(1, n_series, n_blocos, n_agentes)
        ignore_data = false

        for serie in 1:n_series, bloco in 1:n_blocos 
            PSRIOGrafResultBase_nextRegistry( ptr_pointer,ignore_data )
            #PSRIOGrafResultBase_seekStage2(ptr_pointer, 1, serie, bloco)
            for agente in 1:n_agentes
                valores[1, serie, bloco, agent_index[agente]] = PSRIOGrafResultBase_getData( ptr_pointer, agente-1 )
            end
        end
    else
        valores = zeros(n_etapas, n_series, n_blocos, n_agentes)
        ignore_data = false
        for etapa in 1:n_etapas, serie in 1:n_series, bloco in 1:n_blocos
            PSRIOGrafResultBase_nextRegistry( ptr_pointer, ignore_data )
            #PSRIOGrafResultBase_seekStage2(ptr_pointer, etapa, serie, bloco)
            for agente in 1:n_agentes
                valores[etapa, serie, bloco, agent_index[agente]] = PSRIOGrafResultBase_getData( ptr_pointer, agente-1 )
            end
        end
    end

    # fechar o arquivo
    PSRIOGrafResultBase_closeLoad(ptr_pointer)

    if n_series == 1
        if filename != "waveguide"
            valores = repeat(valores, 1, Scenarios)
        end
    elseif n_series >= Scenarios
        valores = valores[:, 1:Scenarios, :, :]
    else
        error("O arquivo $(filename) possui $(n_series) cenarios, o caso possui $(Scenarios)")
    end

    if print_inflow_warning
        println("Lendo arquivo de afluência com etapa mensal. Se o caso foi criado com o conversor BidModelsConverter, esse aviso pode ser ignorado.")
    end
    
    return PSRCGraf(ptr_pointer, n_etapas, n_series, n_blocos, n_agentes, etapa, 1, 1, valores)
end

function readGrafs(prb::BidDispatchProblem, etapa::Int64,dia::Int64)

    d = prb.data
    n = prb.n
    Scenarios = n.Scenarios
    options = prb.options

    if options.commitment && n.Thermals > 0
        if dia == 1 && etapa == 1
            if options.thermalfile 
                d.initial_state_commit = readGraf(options.INPUTPATH, d.tName[d.tExisting], "initial_state_commit", 0, daily::StageType, Scenarios, options.firstYear, options.FirstDay, 1).valores[1,:,1,:]
                d.initial_state_time = readGraf(options.INPUTPATH, d.tName[d.tExisting], "initial_state_time", 0, daily::StageType, Scenarios, options.firstYear, options.FirstDay, 1).valores[1,:,1,:]
                d.initial_power = readGraf(options.INPUTPATH, d.tName[d.tExisting], "initial_power", 0, daily::StageType, Scenarios, options.firstYear, options.FirstDay, 1).valores[1,:,1,:]
            else
                d.initial_state_commit = zeros(prb.n.Scenarios,length(d.tExisting))
                d.initial_state_time = zeros(prb.n.Scenarios,length(d.tExisting)).-9999
                d.initial_power = zeros(prb.n.Scenarios,length(d.tExisting)) 
            end
        else
            if options.gametype == Human::GameType
                d.initial_state_commit = readGraf(joinpath(options.OUTPUTPATH,string(dia-1+(etapa-1)*n.Days)), d.tName[d.tExisting], "initial_state_commitRT", 0, daily::StageType, Scenarios, options.firstYear, options.FirstDay, 1).valores[1,:,1,:]
                d.initial_state_time = readGraf(joinpath(options.OUTPUTPATH,string(dia-1+(etapa-1)*n.Days)), d.tName[d.tExisting], "initial_state_timeRT", 0, daily::StageType, Scenarios, options.firstYear, options.FirstDay, 1).valores[1,:,1,:]
                d.initial_power = readGraf(joinpath(options.OUTPUTPATH,string(dia-1+(etapa-1)*n.Days)), d.tName[d.tExisting], "initial_powerRT", 0, daily::StageType, Scenarios, options.firstYear, options.FirstDay, 1).valores[1,:,1,:]
            end
        end
    end

    if any( (d.aWizardIDHyd.==1) .& (.! isempty.(d.agn2hyd)) ) # algum agente tem usinas hidro e wizardID = 1
        if etapa != d.gerhid.etapa && n.Hydros > 0
            println("Reading GRAF file gerhid...")
            d.gerhid = readGraf(options.INPUTPATH, d.hName, "gerhid", 1, weekly::StageType, Scenarios, options.firstYear, options.firstWeek, etapa)
        end

        if etapa != d.custo_agua.etapa && n.Hydros > 0
            println("Reading GRAF file oppchg...")
            d.custo_agua = readGraf(options.INPUTPATH, d.hName, "oppchg", 1, weekly::StageType, Scenarios, options.firstYear, options.firstWeek, etapa)
        end
    end

    if etapa != d.gergndpu.etapa && n.Gnd > 0
        println("Reading GRAF file ggndpu...")
        d.gergndpu = readGraf(options.INPUTPATH, d.gName, "ggndpu", 1, weekly::StageType, Scenarios, options.firstYear, options.firstWeek, etapa)
        d.gergnd = PSRCGraf(C_NULL, d.gergndpu.n_etapas, d.gergndpu.n_series, d.gergndpu.n_blocos, d.gergndpu.n_agentes, d.gergndpu.etapa, d.gergndpu.serie, d.gergndpu.bloco, copy(d.gergndpu.valores))
        if n.Gnd > 0
            for agn in 1:d.gergndpu.n_agentes
                d.gergnd.valores[:, :, :, agn] = d.gergndpu.valores[:, :, :, agn] .* d.gFatOp[agn] .* d.rPotInst[agn]
            end
        end
    end

    if options.grafInflow && n.Hydros > 0
        println("Reading GRAF file inflow...")
        ret = nothing
        try
            ret = readGraf(options.INPUTPATH, d.stationName, "inflow", 1, weekly::StageType, Scenarios, options.firstYear, options.firstWeek, etapa, dia, d.inflow_graf.etapa)
        catch
            ret = readGraf(options.INPUTPATH, string.(d.stsCode), "inflow", 1, weekly::StageType, Scenarios, options.firstYear, options.firstWeek, etapa, dia, d.inflow_graf.etapa)
        end
        if !isnothing(ret)
            d.inflow_graf = ret
        end
    end

    if etapa != d.demand_graf.etapa
        println("Reading GRAF file demand...")
        d.demand_graf = readGraf(options.INPUTPATH, d.dName, "demand", 1, weekly::StageType, Scenarios, options.firstYear, options.firstWeek, etapa)
        d.current_demand = readGraf(options.INPUTPATH, d.dName, "demand", 1, weekly::StageType, Scenarios, options.firstYear, options.firstWeek, etapa)
    end

    if options.useRTFiles == required::InputFileType || (options.useRTFiles == optional::InputFileType && isfile(options.INPUTPATH*"demand_rt.csv"))
        if etapa != d.demand_distortion.etapa
            println("Reading GRAF file demand_rt...")
            d.demand_distortion = readGraf(options.INPUTPATH, d.dName, "demand_rt", 1, weekly::StageType, Scenarios, options.firstYear, options.firstWeek, etapa)
        end
    else
        valores = zeros(1, d.demand_graf.n_series, d.demand_graf.n_blocos, d.demand_graf.n_agentes)
        d.demand_distortion = PSRCGraf(C_NULL, d.demand_graf.n_etapas, d.demand_graf.n_series, d.demand_graf.n_blocos, d.demand_graf.n_agentes, d.demand_graf.etapa, d.demand_graf.serie, d.demand_graf.bloco, valores)
    end

    if options.useRTFiles == required::InputFileType || (options.useRTFiles == optional::InputFileType && isfile(options.INPUTPATH*"ggndpu_rt.csv"))
        if etapa != d.gergndpu_distortion.etapa && n.Gnd > 0
            println("Reading GRAF file ggndpu_rt...")
            d.gergndpu_distortion = readGraf(options.INPUTPATH, d.gName, "ggndpu_rt", 1, weekly::StageType, Scenarios, options.firstYear, options.firstWeek, etapa)
        end
    else
        valores = zeros(1, d.gergnd.n_series, d.gergnd.n_blocos, d.gergnd.n_agentes)
        d.gergndpu_distortion = PSRCGraf(C_NULL, d.gergnd.n_etapas, d.gergnd.n_series, d.gergnd.n_blocos, d.gergnd.n_agentes, d.gergnd.etapa, d.gergnd.serie, d.gergnd.bloco, valores)
    end

    if etapa == 1 && etapa != d.volini_graf.etapa && options.inicondgraf && n.Hydros > 0
        println("Reading GRAF file volini...")
        d.volini_graf = readGraf(options.INPUTPATH, d.hName, "volini", 1, daily::StageType, Scenarios, options.firstYear, options.firstWeek)
        d.volini = permutedims(d.volini_graf.valores[1, 1:n.Scenarios, 1, :],[2,1])
    end

    if etapa == 1 && etapa != d.waveguide.etapa && options.useWaveguide != none
        if options.useWaveguide == input
            println("Reading GRAF file waveguide...")
            d.waveguide = readGraf(options.INPUTPATH, d.hName, "waveguide", 0, points::StageType, Scenarios, 1, 1)

            path = joinpath(options.INPUTPATH, "waveguide.csv")
            ptr_pointer = PSRIOGrafResult_create(0)
            PSRIOGrafResult_initLoad(ptr_pointer, path, PSRIO_GRAF_FORMAT_EXTENDED)
            prb.n.wstage = PSRIOGrafResultBase_getTotalStages(ptr_pointer)
        else
            println("Creating default waveguide...")
            ptr_pointer = PSRIOGrafResult_create(0) 
            valores = zeros(2, 1, 1, n.Hydros)
            valores[1,1,1,:] = d.volmax
            valores[2,1,1,:] = d.volmin
            d.waveguide = PSRCGraf(ptr_pointer, 2, 1, 1, n.Hydros, 2, 1, 1, valores)

            prb.n.wstage = 2
        end
    end
    return
end