function load_budget!(prb::BidDispatchProblem,start::Int)
    d = prb.data
    n = prb.n
    Scenarios = n.Scenarios
    options = prb.options

    if n.Hydros > 0

        vr_names = [string("VR ", i) for i in 1:n.VirtualReservoirs]
        aBudgetse = readGraf(joinpath(options.OUTPUTPATH, string(start)), vr_names, "VREnergy_reconciled", 1, daily::StageType, Scenarios, options.firstYear, options.FirstDay, 1)

        aBudgetse = transpose(aBudgetse.valores[1,:,1,:])

        d.aBudgets = Budget.(aBudgetse, aBudgetse) # power budget is overwritten, using energy values as placeholder
    end
end

function load_vol!(prb::BidDispatchProblem,start::Int)
    d = prb.data
    n = prb.n
    options = prb.options
    Scenarios = n.Scenarios
    
    if n.Hydros > 0
        volendday = readGraf(joinpath(options.OUTPUTPATH, string(start)), d.hName, "VolRT", 1, daily::StageType, Scenarios, options.firstYear, options.FirstDay, 1)
        volendday = volendday.valores[1,:,:,:]
        
        d.volendday = permutedims(volendday, [3,2,1])
        d.volendday = d.volendday[d.reservoirs,:,:]
    end

end

function create_initial_budget!(prb::BidDispatchProblem)

    d = prb.data
    n = prb.n

    if n.Hydros == 0
        return
    end
    
    # calcula energia e pot por usina
    d.rho_region = downstream_ρ_region(prb; with_future = false, mean = false)

    # somar por sistema
    ea_rgn = zeros(n.StorageRegions, n.Scenarios)
    pot_rgn = zeros(n.StorageRegions, n.Scenarios)

    for scen = 1:n.Scenarios
        ea = get_EA.(d.volini[:, scen], d.volmin, d.rho_region, d.conv)
        pot = get_power.(d.volini[:, scen], d.volmin, d.turb_max, d.rho_region, d.conv)

        for rgn in 1:n.StorageRegions
            ea_rgn[rgn, scen] = sum(ea[:,rgn])
            pot_rgn[rgn, scen] = sum(pot[:,rgn])
        end
    end

    GF_rgn = zeros(n.StorageRegions)

    for i in 1:n.VirtualReservoirs
        rgn = d.vr2rgn[i]
        GF_rgn[rgn] += d.hGF2[i]
    end

    energy_values = zeros(n.VirtualReservoirs, n.Scenarios)
    power_values = zeros(n.VirtualReservoirs, n.Scenarios)

    for scen = 1:n.Scenarios, vr = 1:n.VirtualReservoirs
    
        GF_agn = d.hGF2[vr]
        rgn = d.vr2rgn[vr]
        
        energy_values[vr, scen] = get_energy_budget.(ea_rgn[rgn, scen], GF_agn, GF_rgn[rgn])
        power_values[vr, scen] = get_power_budget.(pot_rgn[rgn, scen], GF_agn, GF_rgn[rgn])
    
    end
    
    d.aBudgets = Budget.(energy_values, power_values)

    return

end

"""
    Update energy and power budget

    input:
            - current energy budget
            - current power budget
    output:
            - updated Energy Budget GRAF
            - updated Power Budget GRAF
"""
function update_initial_budget!(prb::BidDispatchProblem, m)
    
    d = prb.data
    n = prb.n
    options = prb.options

    if n.Hydros == 0
        return
    end

    if options.gametype == Human::GameType
        d.rho_region = downstream_ρ_region(prb; with_future = false, mean = false)
    end

    temporary_budget = zeros(n.VirtualReservoirs, n.Scenarios)
    soldVR = JuMP.value.(m[:soldVR])
    boughtVR = JuMP.value.(m[:boughtVR])

    for vr in 1:n.VirtualReservoirs, s in 1:n.Scenarios
        temporary_budget[vr, s] = d.aBudgets[vr, s].energy - sum(soldVR[vr, :, s,:]) + sum(boughtVR[vr,:,s,:])
    end

    #output
    d.VREnergy_sale = temporary_budget

    ## Calcula energia armazenada real

    # inicializa vol com os valores de volmin para as fios d'agua
    # volendday só preenche os valores dos reservatórios
    vol = copy(d.volmin)
    vol = repeat(vol, 1, n.Scenarios)
    vol[d.reservoirs, :] = d.volendday[:,end, :]
    
    # calcula energia e pot por usina
    ea = zeros(n.Hydros, n.StorageRegions, n.Scenarios)
    pot = zeros(n.Hydros, n.StorageRegions, n.Scenarios)
    for scen in 1:n.Scenarios
        ea[:,:,scen] = get_EA.(vol[:,scen], d.volmin, d.rho_region, d.conv)
        pot[:,:,scen] = get_power.(vol[:,scen], d.volmin, d.turb_max, d.rho_region, d.conv)
    end
 
    # somar por sistema
    ea_rgn = zeros(n.StorageRegions, n.Scenarios)
    pot_rgn = zeros(n.StorageRegions, n.Scenarios)

    for r in 1:n.StorageRegions
        ea_rgn[r, :] += sum(ea[i,r,:] for i in 1:n.Hydros)
        pot_rgn[r, :] += sum(pot[i,r,:] for i in 1:n.Hydros)
    end

    GF_rgn = zeros(n.StorageRegions)
    temporary_budget_rgn = zeros(n.StorageRegions, n.Scenarios)

    for i in 1:n.VirtualReservoirs
        rgn = d.vr2rgn[i]
        GF_rgn[rgn] += d.hGF[i]
        temporary_budget_rgn[rgn, :] += temporary_budget[i, :]
    end

    # Compatibiliza saldos

    energy_values = zeros(n.VirtualReservoirs, n.Scenarios)
    power_values = zeros(n.VirtualReservoirs, n.Scenarios)

    for s = 1:n.Scenarios, vr = 1:n.VirtualReservoirs
    
        GF_agn = d.hGF[vr]
        temporary_budget_agn = temporary_budget[vr, s]

        rgn = d.vr2rgn[vr]
        
        energy_values[vr, s] = get_updated_energy_budget(ea_rgn[rgn, s], temporary_budget_agn, temporary_budget_rgn[rgn, s])
        power_values[vr, s] = get_power_budget(pot_rgn[rgn, s], GF_agn, GF_rgn[rgn])
    
    end

    d.aBudgets = Budget.(energy_values, power_values)
    
end

function add_inflow_to_budget!(prb::BidDispatchProblem)
    d = prb.data
    n = prb.n

    if n.Hydros == 0
        return
    end

    d.rho_region = downstream_ρ_region(prb; with_future = false, mean = false)

    GF_rgn = zeros(n.StorageRegions)

    for i in 1:n.VirtualReservoirs
        rgn = d.vr2rgn[i]
        GF_rgn[rgn] += d.hGF[i]
    end

    for s = 1:n.Scenarios
        pull_inflow_data!(prb, Int32(s))

        ena = zeros(n.Hydros, n.StorageRegions)
        for h in 1:n.Hydros
            ena[h,:] = get_ENA.(d.inflow[d.hyd2station[h]], d.rho_region[h,:])
        end
        
        ena_rgn = sum(ena, dims=1)

        for vr = 1:n.VirtualReservoirs
    
            GF_agn = d.hGF[vr]
            rgn = d.vr2rgn[vr]
            d.aBudgets[vr, s].energy += ena_rgn[rgn] * (GF_agn/GF_rgn[rgn])
    
        end
    end

    # output
    d.VREnergy_inflow = [x.energy for x in d.aBudgets]
end


function update_power_budget!(prb::BidDispatchProblem, etapa::Int64, dia::Int64)

    d = prb.data
    n = prb.n

    if n.Hydros == 0
        return
    end
    
    # calcula energia e pot por usina
    d.rho_region = downstream_ρ_region(prb; with_future = false, mean = false)

    if dia == 1 & etapa==1
        vol = d.volini
    else
        vol = copy(d.volmin)
        vol = repeat(vol, 1, n.Scenarios)
        vol[d.reservoirs, :] = d.volendday[:, end, :]
    end

    # somar por sistema
    pot_rgn = zeros(n.StorageRegions, n.Scenarios)

    for scen = 1:n.Scenarios
        
        pull_inflow_data!(prb, Int32(scen))

        pot = zeros(n.Hydros, n.StorageRegions)
        for h = 1:n.Hydros
            pot[h, :] = get_power.(vol[h, scen] + d.inflow[d.hyd2station[h]], d.volmin[h], d.turb_max[h], d.rho_region[h,:], d.conv)
        end

        for rgn in 1:n.StorageRegions
            pot_rgn[rgn, scen] = sum(pot[:,rgn])
        end
    end

    GF_rgn = zeros(n.StorageRegions)

    for i in 1:n.VirtualReservoirs
        rgn = d.vr2rgn[i]
        GF_rgn[rgn] += d.hGF[i]
    end

    for scen = 1:n.Scenarios, vr = 1:n.VirtualReservoirs
    
        GF_agn = d.hGF[vr]
        rgn = d.vr2rgn[vr]
        
        d.aBudgets[vr, scen].power = get_power_budget.(pot_rgn[rgn, scen], GF_agn, GF_rgn[rgn])
    
    end

    return

end

"""
    Calculates EA

    input:
            - current storage of the system
            - minimum storage of the system
            - mean production factor
    output:
            - EA by plant/system
"""
function get_EA(Vol::Float64, Vmin::Float64, FP::Float64, conv::Float64)
    # conv = 1e-6*3600 # = 0.0036 [ (Hm3/h) / (m3/s) ] = 1e-6[Hm3/m3] * 3600[s/h]
    ea = max((Vol - Vmin) * FP / conv, 0.0)
    return ea # [Hm3] * [MW/ m3/s] / [Hm3/m3 * s/h] = [MWh]
end


"""
    Calculates ENA

    input:
            - inflow
            - mean production factor
    output:
            - ENA by plant/system
"""
function get_ENA(inflow::Float64, FP::Float64)
    day_len = 24 # [h]
    ena = max(inflow * FP * day_len, 0.0)
    return ena # [m3/s] * [MW/ m3/s] * [h] = [MWh]
end

"""
    Calculates Power

    input:
            - current storage of the system
            - minimum storage of the system
            - max turbination
            - mean production factor
    output:
            - Power by plant/system
"""
function get_power(Vol::Float64, Vmin::Float64, Umax::Float64, FP::Float64, conv::Float64)
    # conv = 1e-6*3600 # = 0.0036 [ (Hm3/h) / (m3/s) ] = 1e-6[Hm3/m3] * 3600[s/h]
    day_len = 24 # [h]
    u = (Vol - Vmin) / (conv * day_len) # [hm3] / [hm3/m3 *s] = [m3/s]
    u = min(u, Umax)
    pot = max(u*FP, 0.0)
    return pot # [m3/s] * [MW/ m3/s] = [MW]
end

"""
    Calculates Energy budget

    input:
            - ENA
            - EA
            - GF
    output:
            - Energy budget by plant
"""
function get_energy_budget(EA::Float64, GF::Float64, GFtotal::Float64)
    return (GF/ GFtotal) * EA
end

"""
    Calculates Power budget

    input:
            - Power
            - GF
    output:
            - Power budget by plant
"""
function get_power_budget(power::Float64, GF::Float64, GFtotal::Float64)
    power = max(power,0.0)
    return (GF / GFtotal) * power
end

"""
    Calculates updated Energy budget

    input:
            - ENA
            - EA
            - GF
            - temporary energy budget
    output:
            - Energy budget by plant
"""
function get_updated_energy_budget(EA::Float64, ratio::Float64, ratio_sum::Float64)
    if ratio_sum == 0
        return EA
    else
        return ((ratio / ratio_sum) * EA)
    end
end
