function is_approximately_zero(num::Real; atol::Float64=1e-3)
    if -atol <= num <= atol
        return true
    elseif (-0.5 < num < -atol) || (0.5 > num > atol)
        @warn("rounding $num to 0.")
        return true
    end
    return false
end

function is_approximately_one(num::Real; atol::Float64=1e-3)
    if 1-atol <= num <= 1+atol
        return true
    elseif (0.5 < num < 1-atol) || (1.5 > num > 1+atol)
        @warn("rounding $num to 1.")
        return true
    end
    return false
end

function convert_binary_variable_value_to_Int32(num::Real; atol::Float64 = 1e-3)
    if is_approximately_one(num; atol=atol)
        return one(Int32)
    elseif is_approximately_zero(num; atol=atol)
        return zero(Int32)
    end
    error("Could not convert binary variable value $num to 0 or 1.")    
    return nothing
end

function use_buffer(prb::BidDispatchProblem, hour::Integer)
    if hour > prb.n.MaxHours
        return hour - prb.n.MaxHours
    else
        return hour
    end
end