function init_outputs!(prb)
    d = prb.data
    n = prb.n
    options = prb.options

    # sizes
    num_problems = length(instances(ProblemType))
    problem_duration = options.gametype==Computer::GameType ? n.Stages*n.Days : 1
    num_sale_offer_hyd = sum(d.sale_offer_hyd_num_segments)
    num_purchase_offer_hyd = sum(d.purchase_offer_hyd_num_segments)
    num_sale_offer_ter = sum(d.sale_offer_ter_num_segments)
    num_sale_offer_gnd = sum(d.sale_offer_gnd_num_segments)
    num_ter_existing = length(d.tExisting)

    # names
    reservoir_names = [d.hName[i] for i in d.reservoirs]
    vr_names = [string("VR ", i) for i in d.vrCode]
    region_names = [string("Region ", i) for i in d.rId]
    agent_names = [string("Agent ", i) for i in d.aId]
    interc_names = [string("Interc ", i) for i in 1:n.Intercs]
    hyd_sale_offer_names = vcat([[string("VR ", i, " - ", seg) for seg in 1:d.sale_offer_hyd_num_segments[i]] for i in 1:n.VirtualReservoirs]...)
    hyd_purchase_offer_names = vcat([[string("VR ", i, " - ", seg) for seg in 1:d.purchase_offer_hyd_num_segments[i]] for i in 1:n.VirtualReservoirs]...)
    ter_offer_names = vcat([[string("Ter ", i, " - ", seg) for seg in 1:d.sale_offer_ter_num_segments[i]] for i in 1:n.Thermals]...)
    gnd_offer_names = vcat([[string("Ren ", i, " - ", seg) for seg in 1:d.sale_offer_gnd_num_segments[i]] for i in 1:n.Gnd]...)

    # dimension order: (nCols, nBlocks, nSeries, nStages, nProblems)

    if n.Hydros > 0
        # Hourly generation
        d.HGen_output = BidDispatchOutput(n.Hydros, n.MaxHours, n.Scenarios, problem_duration, num_problems,
                                        zeros(n.Hydros,n.MaxHours,n.Scenarios,problem_duration,num_problems),
                                        d.hName, "HGen", "MWh")

        # Hydro operation
        d.Vol_output = BidDispatchOutput(n.Hydros, n.MaxHours, n.Scenarios, problem_duration, num_problems,
                                        zeros(n.Hydros,n.MaxHours,n.Scenarios,problem_duration,num_problems),
                                        d.hName, "Vol", "hm3")
        d.Turb_output = BidDispatchOutput(n.Hydros, n.MaxHours, n.Scenarios, problem_duration, num_problems,
                                        zeros(n.Hydros,n.MaxHours,n.Scenarios,problem_duration,num_problems),
                                        d.hName, "Turb", "m3/s")
        d.Vert_output = BidDispatchOutput(n.Hydros, n.MaxHours, n.Scenarios, problem_duration, num_problems,
                                        zeros(n.Hydros,n.MaxHours,n.Scenarios,problem_duration,num_problems),
                                        d.hName, "Vert", "m3/s")
        d.VertForc_output = BidDispatchOutput(n.Hydros, n.MaxHours, n.Scenarios, problem_duration, num_problems,
                                        zeros(n.Hydros,n.MaxHours,n.Scenarios,problem_duration,num_problems),
                                        d.hName, "VertForc", "m3/s")
        d.WaterIn_output = BidDispatchOutput(n.StorageRegions, n.MaxHours, n.Scenarios, problem_duration, num_problems,
                                        zeros(n.StorageRegions,n.MaxHours,n.Scenarios,problem_duration,num_problems),
                                        region_names, "WaterIn", "m3/s")
        d.WaterOut_output = BidDispatchOutput(n.StorageRegions, n.MaxHours, n.Scenarios, problem_duration, num_problems,
                                        zeros(n.StorageRegions,n.MaxHours,n.Scenarios,problem_duration,num_problems),
                                        region_names, "WaterOut", "m3/s")

        # Prices
        d.pricevr_output = BidDispatchOutput(n.StorageRegions, 1, n.Scenarios, problem_duration, num_problems,
                                        zeros(n.StorageRegions,1,n.Scenarios,problem_duration,num_problems),
                                        region_names, "VRPrice", "\$/MWh")

        # VR budgets
        d.VREnergy_inflow_output = BidDispatchOutput(n.VirtualReservoirs, 1, n.Scenarios, problem_duration, 1,
                                        zeros(n.VirtualReservoirs,1,n.Scenarios,problem_duration,1),
                                        vr_names, "VREnergy_initial", "MWh")
        d.VREnergy_sale_output = BidDispatchOutput(n.VirtualReservoirs, 1, n.Scenarios, problem_duration, 1,
                                        zeros(n.VirtualReservoirs,1,n.Scenarios,problem_duration,1),
                                        vr_names, "VREnergy_finalRT", "MWh")
        d.aBudgetsEnergy_output = BidDispatchOutput(n.VirtualReservoirs, 1, n.Scenarios, problem_duration, 1,
                                        zeros(n.VirtualReservoirs,1,n.Scenarios,problem_duration,1),
                                        vr_names, "VREnergy_reconciled", "MWh")
        d.StoredEnergy_output = BidDispatchOutput(n.StorageRegions, 1, n.Scenarios, problem_duration, 1,
                                        zeros(n.StorageRegions,1,n.Scenarios,problem_duration,1),
                                        region_names, "StoredEnergy", "MWh")

        # Offer prices
        d.sale_offer_hyd_price_output = BidDispatchOutput(num_sale_offer_hyd, 1, n.Scenarios, problem_duration, 1,
                                        zeros(num_sale_offer_hyd,1,n.Scenarios,problem_duration,1),
                                        hyd_sale_offer_names, "HydSaleOfferPrice", "\$/MWh")
                                        
        # Offer quantities
        d.sale_offer_hyd_qtd_output = BidDispatchOutput(num_sale_offer_hyd, 1, n.Scenarios, problem_duration, 1,
                                        zeros(num_sale_offer_hyd,1,n.Scenarios,problem_duration,1),
                                        hyd_sale_offer_names, "HydSaleOfferQtd", "MWh")

        # Accepted offers
        d.sale_offer_hyd_acc_output = BidDispatchOutput(num_sale_offer_hyd, 1, n.Scenarios, problem_duration, num_problems,
                                        zeros(num_sale_offer_hyd,1,n.Scenarios,problem_duration,num_problems),
                                        hyd_sale_offer_names, "HydSaleOfferAcc", "MWh")

        # Offer prices
        d.purchase_offer_hyd_price_output = BidDispatchOutput(num_purchase_offer_hyd, 1, n.Scenarios, problem_duration, 1,
                                        zeros(num_purchase_offer_hyd,1,n.Scenarios,problem_duration,1),
                                        hyd_purchase_offer_names, "HydPurchaseOfferPrice", "\$/MWh")    
        
        # Offer quantities
        d.purchase_offer_hyd_qtd_output = BidDispatchOutput(num_purchase_offer_hyd, 1, n.Scenarios, problem_duration, 1,
                                        zeros(num_purchase_offer_hyd,1,n.Scenarios,problem_duration,1),
                                        hyd_purchase_offer_names, "HydPurchaseOfferQtd", "MWh")

        # Accepted offers
        d.purchase_offer_hyd_acc_output = BidDispatchOutput(num_purchase_offer_hyd, 1, n.Scenarios, problem_duration, num_problems,
                                        zeros(num_purchase_offer_hyd,1,n.Scenarios,problem_duration,num_problems),
                                        hyd_purchase_offer_names, "HydPurchaseOfferAcc", "MWh")
    end

    if n.Thermals > 0
        # Hourly generation
        d.TGen_output = BidDispatchOutput(num_ter_existing, n.MaxHours, n.Scenarios, problem_duration, num_problems,
                                        zeros(num_ter_existing,n.MaxHours,n.Scenarios,problem_duration,num_problems),
                                        d.tName, "TGen", "MWh")

        # Thermo operation
        d.initial_state_commit_output = BidDispatchOutput(num_ter_existing, 1, n.Scenarios, problem_duration, num_problems,
                                            zeros(num_ter_existing,1,n.Scenarios,problem_duration,num_problems),
                                            d.tName, "initial_state_commit", "")
        d.initial_power_output = BidDispatchOutput(num_ter_existing, 1, n.Scenarios, problem_duration, num_problems,
                                            zeros(num_ter_existing,1,n.Scenarios,problem_duration,num_problems),
                                            d.tName, "initial_power", "MW")
        d.initial_state_time_output = BidDispatchOutput(num_ter_existing, 1, n.Scenarios, problem_duration, num_problems,
                                            zeros(num_ter_existing,1,n.Scenarios,problem_duration,num_problems),
                                            d.tName, "initial_state_time", "h")

        # Offer prices
        d.sale_offer_ter_price_output = BidDispatchOutput(num_sale_offer_ter, n.MaxHours, n.Scenarios, problem_duration, 1,
                                            zeros(num_sale_offer_ter,n.MaxHours,n.Scenarios,problem_duration,1),
                                            ter_offer_names, "TerOfferPrice", "\$/MWh")
                   
        # Offer quantities
        d.sale_offer_ter_qtd_output = BidDispatchOutput(num_sale_offer_ter, n.MaxHours, n.Scenarios, problem_duration, 1,
                                            zeros(num_sale_offer_ter,n.MaxHours,n.Scenarios,problem_duration,1),
                                            ter_offer_names, "TerOfferQtd", "MWh")

        # Accepted offers                          
        d.sale_offer_ter_acc_output = BidDispatchOutput(num_sale_offer_ter, n.MaxHours, n.Scenarios, problem_duration, num_problems,
                                            zeros(num_sale_offer_ter,n.MaxHours,n.Scenarios,problem_duration,num_problems),
                                            ter_offer_names, "TerOfferAcc", "MWh")
    end

    if n.Gnd > 0
        # Hourly generation
        d.RGen_output = BidDispatchOutput(n.Gnd, n.MaxHours, n.Scenarios, problem_duration, num_problems,
                                        zeros(n.Gnd,n.MaxHours,n.Scenarios,problem_duration,num_problems),
                                        d.gName, "RGen", "MWh")

        # Offer prices                          
        d.sale_offer_gnd_price_output = BidDispatchOutput(num_sale_offer_gnd, n.MaxHours, n.Scenarios, problem_duration, 1,
                                        zeros(num_sale_offer_gnd,n.MaxHours,n.Scenarios,problem_duration,1),
                                        gnd_offer_names, "GndOfferPrice", "\$/MWh")
                                                              
        # Offer quantities
        d.sale_offer_gnd_qtd_output = BidDispatchOutput(num_sale_offer_gnd, n.MaxHours, n.Scenarios, problem_duration, 1,
                                        zeros(num_sale_offer_gnd,n.MaxHours,n.Scenarios,problem_duration,1),
                                        gnd_offer_names, "GndOfferQtd", "MWh")

        # Accepted offers                    
        d.sale_offer_gnd_acc_output = BidDispatchOutput(num_sale_offer_gnd, n.MaxHours, n.Scenarios, problem_duration, num_problems,
                                        zeros(num_sale_offer_gnd,n.MaxHours,n.Scenarios,problem_duration,num_problems),
                                        gnd_offer_names, "GndOfferAcc", "MWh")
    end

    # Prices
    d.preco_spot_dia_output = BidDispatchOutput(n.SpotPrice, n.MaxHours, n.Scenarios, problem_duration, num_problems,
                                                zeros(n.SpotPrice,n.MaxHours,n.Scenarios,problem_duration,num_problems),
                                                d.preco_spot_names, "SpotPrice", "\$/MWh")

    d.demand_output = BidDispatchOutput(n.Demand, n.MaxHours, n.Scenarios, problem_duration, num_problems,
                                                zeros(n.Demand,n.MaxHours,n.Scenarios,problem_duration,num_problems),
                                                d.dName, "Demand", "GWh") 

    if options.network_phys == inter::ConsiderNetwork
                                            
        d.IntercTo_output = BidDispatchOutput(n.Intercs, n.MaxHours, n.Scenarios, problem_duration, num_problems,
                                                zeros(n.Intercs,n.MaxHours,n.Scenarios,problem_duration,num_problems),
                                                interc_names, "IntercTo", "MWh")
        d.IntercFrom_output = BidDispatchOutput(n.Intercs, n.MaxHours, n.Scenarios, problem_duration, num_problems,
                                                zeros(n.Intercs,n.MaxHours,n.Scenarios,problem_duration,num_problems),
                                                interc_names, "IntercFrom", "MWh")
    end

    return
  
end

function load_complete_vol(m, prb::BidDispatchProblem)

    n = prb.n
    d = prb.data

    complete_vol =  zeros(n.Hydros, n.MaxHours, n.Scenarios)

    for i in 1:n.Hydros
        r = findfirst(x -> x == i, d.reservoirs)
        if r !== nothing
            complete_vol[i,:,:] = JuMP.value.(m[:HydroVol][r,:,:]) 
        else
            complete_vol[i,:,:] .= d.volmax[i]
        end
    end

    return complete_vol
end

function get_outputs!(m,prb,etapa,dia)
    d = prb.data
    n = prb.n
    options = prb.options

    current_problem = Int32(options.DayAhead ? DA::ProblemType : RT::ProblemType)
    current_day = options.gametype==Computer::GameType ? n.Days*(etapa-1) + dia : 1

    if n.Hydros > 0
        # Hourly generation
        d.HGen_output.data[:,:,:,current_day,current_problem] = JuMP.value.(m[:HTurb])[:,1:n.MaxHours,:] .* d.fprodt
        
        # Hydro operation
        complete_vol = load_complete_vol(m, prb)
        d.Vol_output.data[:,:,:,current_day,current_problem] = complete_vol[:,1:n.MaxHours,:]
        d.Turb_output.data[:,:,:,current_day,current_problem] = JuMP.value.(m[:HTurb])[:,1:n.MaxHours,:]
        d.Vert_output.data[:,:,:,current_day,current_problem] = JuMP.value.(m[:spillage])[:,1:n.MaxHours,:]
        d.VertForc_output.data[:,:,:,current_day,current_problem] = JuMP.value.(m[:ForcedSpillage])[:,1:n.MaxHours,:]
        d.WaterIn_output.data[:,:,:,current_day,current_problem] = JuMP.value.(m[:WaterIn])[:,1:n.MaxHours,:]
        d.WaterOut_output.data[:,:,:,current_day,current_problem] = JuMP.value.(m[:WaterOut])[:,1:n.MaxHours,:]
        
        # Prices 
        d.pricevr_output.data[:,1,:,current_day,current_problem] = d.pricevr
        
        # VR budgets
        if !options.DayAhead
            d.aBudgetsEnergy_output.data[:,1,:,current_day,1] = [x.energy for x in d.aBudgets]
            d.VREnergy_inflow_output.data[:,1,:,current_day,1] = d.VREnergy_inflow
            d.VREnergy_sale_output.data[:,1,:,current_day,1] = d.VREnergy_sale
            d.StoredEnergy_output.data[:,1,:,current_day,1] = get_stored_energy(prb)
        end
    end

    if n.Thermals > 0
        # Hourly generation
        d.TGen_output.data[:,:,:,current_day,current_problem] = JuMP.value.(m[:TGen]).data[:,1:n.MaxHours,:]

        # Thermo operation
        if options.commitment
            d.initial_state_commit_output.data[:,:,:,current_day,current_problem] = get_commit_value(m,prb)[:,n.MaxHours,:]
            d.initial_power_output.data[:,:,:,current_day,current_problem] = JuMP.value.(m[:TGen]).data[:,n.MaxHours,:]
            d.initial_state_time_output.data[:,1,:,current_day,current_problem] = permutedims(d.initial_state_time,[2,1])
        else
            d.initial_state_commit_output.data[:,:,:,current_day,current_problem] .= 0
            d.initial_power_output.data[:,:,:,current_day,current_problem] .= 0
            d.initial_state_time_output.data[:,1,:,current_day,current_problem] .= 0
        end
    end

    if n.Gnd > 0
        # Hourly generation
        d.RGen_output.data[:,:,:,current_day,current_problem] = JuMP.value.(m[:RGen])[:,1:n.MaxHours,:]
    end


    # Prices 
    d.preco_spot_dia_output.data[:,:,:,current_day,current_problem] = d.preco_spot_dia[:,1:n.MaxHours,:]

    # Demand
    d.demand_output.data[:,:,:,current_day,current_problem] = copy(permutedims(d.current_demand.valores[1,:,n.MaxHours*(dia-1)+1:n.MaxHours*(dia),:],[3,2,1]))

    # Hydro offers
    offer_index = 0
    for vr in 1:n.VirtualReservoirs, seg in 1:d.sale_offer_hyd_num_segments[vr]
        offer_index+=1
        if !options.DayAhead
            for (s, offer) in enumerate(d.sale_offer_hyd[vr,:])
                d.sale_offer_hyd_price_output.data[offer_index, 1, s, current_day, 1] = offer[seg].price
                d.sale_offer_hyd_qtd_output.data[offer_index, 1, s, current_day, 1] = offer[seg].quantity
            end
        end
        d.sale_offer_hyd_acc_output.data[offer_index, 1, :, current_day, current_problem] = d.sale_offer_hyd_acc[vr,:,seg]
    end

    # Hydro offers
    offer_index = 0
    for vr in 1:n.VirtualReservoirs, seg in 1:d.purchase_offer_hyd_num_segments[vr]
        offer_index+=1
        if !options.DayAhead
            for (s, offer) in enumerate(d.purchase_offer_hyd[vr,:])
                d.purchase_offer_hyd_price_output.data[offer_index, 1, s, current_day, 1] = offer[seg].price
                d.purchase_offer_hyd_qtd_output.data[offer_index, 1, s, current_day, 1] = offer[seg].quantity
            end
        end
        d.purchase_offer_hyd_acc_output.data[offer_index, 1, :, current_day, current_problem] = d.purchase_offer_hyd_acc[vr,:,seg]
    end

    # Thermal offers
    offer_index = 0
    for (j, ter) in enumerate(d.tExisting), seg in 1:d.sale_offer_ter_num_segments[ter]
        offer_index+=1
        if !options.DayAhead
            for hour in 1:n.MaxHours, s in 1:n.Scenarios
                d.sale_offer_ter_price_output.data[offer_index, hour, s, current_day, 1] = d.sale_offer_ter[j, hour, s][seg].price
                d.sale_offer_ter_qtd_output.data[offer_index, hour, s, current_day, 1] = d.sale_offer_ter[j, hour, s][seg].quantity
            end
        end
        d.sale_offer_ter_acc_output.data[offer_index, :, :, current_day, current_problem] = d.sale_offer_ter_acc[j,1:n.MaxHours,:,seg]
    end

    # Renewable offers
    offer_index = 0
    for gnd in 1:n.Gnd, seg in 1:d.sale_offer_gnd_num_segments[gnd]
        offer_index+=1
        if options.DayAhead
            for hour in 1:n.MaxHours, s in 1:n.Scenarios
                d.sale_offer_gnd_price_output.data[offer_index, hour, s, current_day, 1] = d.sale_offer_gnd[gnd, hour, s][seg].price
                d.sale_offer_gnd_qtd_output.data[offer_index, hour, s, current_day, 1] = d.sale_offer_gnd[gnd, hour, s][seg].quantity
            end
        end
        d.sale_offer_gnd_acc_output.data[offer_index, :, :, current_day, current_problem] = d.sale_offer_gnd_acc[gnd,1:n.MaxHours,:,seg]
    end

    if options.network_phys == inter::ConsiderNetwork
        d.IntercTo_output.data[:,:,:,current_day,current_problem] = JuMP.value.(m[:IntercTo])
        d.IntercFrom_output.data[:,:,:,current_day,current_problem] = JuMP.value.(m[:IntercFrom])
    end

    nothing

end

function write_outputs(prb, day::Int=1)
    d = prb.data
    n = prb.n
    options = prb.options
    if !isdir(joinpath(options.INPUTPATH, "outputs"))
        mkdir(joinpath(options.INPUTPATH, "outputs"))
    end

    if options.gametype == Human::GameType
        output_path = joinpath(options.OUTPUTPATH, string(day, "\\"))
        mkdir(output_path)
    elseif options.gametype == Computer::GameType
        output_path = options.OUTPUTPATH
    end

    output_list = [
        d.preco_spot_dia_output
        d.demand_output
    ]

    if n.Hydros > 0
        append!(output_list, [
            d.HGen_output,
            d.Vol_output,
            d.Turb_output,
            d.Vert_output,
            d.VertForc_output,
            d.WaterIn_output,
            d.WaterOut_output,
            d.pricevr_output,
            d.aBudgetsEnergy_output,
            d.sale_offer_hyd_price_output,
            d.sale_offer_hyd_qtd_output,
            d.sale_offer_hyd_acc_output,
            d.purchase_offer_hyd_price_output,
            d.purchase_offer_hyd_qtd_output,
            d.purchase_offer_hyd_acc_output,
            d.VREnergy_inflow_output,
            d.VREnergy_sale_output,
            d.StoredEnergy_output
        ])
    end

    if n.Thermals > 0
        append!(output_list, [
            d.TGen_output,
            d.initial_state_commit_output,
            d.initial_power_output,
            d.initial_state_time_output,
            d.sale_offer_ter_price_output,
            d.sale_offer_ter_qtd_output,
            d.sale_offer_ter_acc_output
        ])
    end

    if n.Gnd > 0
        append!(output_list, [
            d.RGen_output,
            d.sale_offer_gnd_price_output,
            d.sale_offer_gnd_qtd_output,
            d.sale_offer_gnd_acc_output
        ])
    end

    if options.network_phys == inter::ConsiderNetwork
        append!(output_list, [
            d.IntercTo_output, 
            d.IntercFrom_output
        ])
    end

    for output in output_list
        if output.nProblems == 1
            if prb.options.outBin
                binPrint(output.data[:,:,:,:,1], output.names, output.nCols, output.nStages, output.nSeries, output.nBlocks, NAME = output.filename, OUTPATH = output_path, unit = output.unit, initialStage = options.FirstDay, initialYear = options.firstYear)
            else
                csvPrint(output.data[:,:,:,:,1], output.names, output.nCols, output.nStages, output.nSeries, output.nBlocks, NAME = output.filename, OUTPATH = output_path, unit = output.unit, initialStage = options.FirstDay, initialYear = options.firstYear)
            end
        else
            for iProblem in 1:output.nProblems
                filename = string(output.filename, ProblemType(iProblem))
                if prb.options.outBin
                    binPrint(output.data[:,:,:,:,iProblem], output.names, output.nCols, output.nStages, output.nSeries, output.nBlocks, NAME = filename, OUTPATH = output_path, unit = output.unit, initialStage = options.FirstDay, initialYear = options.firstYear)
                else
                    csvPrint(output.data[:,:,:,:,iProblem], output.names, output.nCols, output.nStages, output.nSeries, output.nBlocks, NAME = filename, OUTPATH = output_path, unit = output.unit, initialStage = options.FirstDay, initialYear = options.firstYear)
                end
            end
        end
    end

    if options.gametype == Human::GameType && day == 2
        psrio = PSRIO.create()

        old_path = joinpath(options.OUTPUTPATH, string(day-1, "\\"))
        new_concatenate_path = joinpath(options.OUTPUTPATH, "all_1_"*string(day, "\\"))
        p = PSRIO.run(psrio, [old_path,output_path], verbose=0,csv = true, output_path = new_concatenate_path, recipes=[joinpath("scripts","concatenate.lua")], model="none")
    elseif options.gametype == Human::GameType && day > 2
        psrio = PSRIO.create()

        old_concatenate_path = joinpath(options.OUTPUTPATH, "all_1_"*string(day-1, "\\"))
        new_concatenate_path = joinpath(options.OUTPUTPATH, "all_1_"*string(day, "\\"))
        p = PSRIO.run(psrio, [old_concatenate_path,output_path], verbose=0,csv = true, output_path = new_concatenate_path, recipes=[joinpath("scripts","concatenate.lua")], model="none")
        rm(old_concatenate_path, recursive = true)
    end

    if options.gametype == Human::GameType
        config_ptr= PSRStudy_getConfigurationModel(prb.psrc.study_ptr)
        ponteiroDay = PSRModel_parm2(config_ptr , "day")
        ponteiroWeek = PSRModel_parm2(config_ptr , "week")
        ponteiroYear = PSRModel_parm2(config_ptr , "year")
        oldWeek = prb.options.week
        oldYear = prb.options.year

        newDay, newWeek, newYear = update_human_date(day, oldWeek, oldYear)

        PSRParm_setData2(ponteiroDay, newDay)
        PSRParm_setData2(ponteiroWeek, newWeek)
        PSRParm_setData2(ponteiroYear, newYear)

        PSRCsave(prb)
    end

    return

end

function update_human_date(oldDay, oldWeek, oldYear)
    newDay = mod1(oldDay+1,7)
    newWeek = mod1((newDay == 1 ? oldWeek + 1 : oldWeek),52)
    newYear = ((newWeek == 1 && newDay == 1 ) ? oldYear + 1 : oldYear)

    return newDay, newWeek, newYear
end

# generic csv WRITER
# ------------------
function csvPrint(data::Array{Float64,4}, names::Vector{String}, nCols::Int32,nStages::Int32,nSeries::Int32,nBlocks::Int32;
    NAME = "file", OUTPATH = pwd(),
    varBlock = 1, varSerie = 1, unit = "",
    initialStage = 1, initialYear = 1,
    stageType = PSR_STAGETYPE_DAILY)


    # Cria objeto para manipulacao de saida GRAF
    # ------------------------------------
    iograf = PSRIOGrafResult_create(0)
    # Configura parametros da saida
    # -----------------------------
    PSRIOGrafResultBase_setVariableByBlock(iograf, varBlock)
    PSRIOGrafResultBase_setVariableBySerie(iograf, varSerie)

    PSRIOGrafResultBase_setUnit(iograf, unit)
    PSRIOGrafResultBase_setStageType(iograf, stageType)
    PSRIOGrafResultBase_setInitialStage(iograf, initialStage)
    PSRIOGrafResultBase_setInitialYear(iograf, initialYear)

    PSRIOGrafResultBase_setTotalBlocks(iograf, nBlocks)
    PSRIOGrafResultBase_setTotalSeries(iograf, nSeries)
    # loop in hydros
    # ----------------------------------------
    for j in 1:nCols
        PSRIOGrafResultBase_addAgent(iograf, names[j])
    end


    # Inicia gravacao do resultado
    # ----------------------------
    PSRIOGrafResult_initSave(iograf, string(OUTPATH,NAME*".csv"), PSRIO_GRAF_FORMAT_DEFAULT)

    #Loop de gravacao
    #----------------
    for t = 1:nStages
        for s = 1:nSeries
            for b = 1:nBlocks

                # Seta valor dos agentes para o registro
                # --------------------------------------
                for j in 1:nCols
                    PSRIOGrafResultBase_setData(iograf, j-1, data[j,b,s,t])
                end

                # Grava registro
                # --------------
                PSRIOGrafResult_writeRegistry(iograf)
            end
        end
    end


    # Finaliza gravacao
    # -----------------
    PSRIOGrafResult_closeSave(iograf)
    PSRIOGrafResult_deleteInstance(iograf)
    iograf = C_NULL
end

# generic bin WRITER
# ------------------
function binPrint(data::Array{Float64,4},names::Vector{String}, nCols::Int32,nStages::Int32,nSeries::Int32,nBlocks::Int32;
    NAME::String = "file", OUTPATH = pwd(),
    varBlock::Integer = 1, varSerie::Integer = 1, unit::String = "",
    initialStage::Integer = 1, initialYear::Integer = 1,
    nameLength::Integer = 24, stageType = PSR_STAGETYPE_DAILY)
    # psrc must be initialized

    # -----------------------------------
    # Parte 2 - Configuracao do resultado0
    # -----------------------------------

    # Cria objeto para manipulacao de saida GRAF
    # ------------------------------------
    iograf = PSRIOGrafResultBinary_create(0)

    # Configura parametros da saida
    # -----------------------------
    PSRIOGrafResultBase_setNameLength(iograf, nameLength)

    PSRIOGrafResultBase_setVariableByBlock(iograf, varBlock)
    PSRIOGrafResultBase_setVariableBySerie(iograf, varSerie)

    PSRIOGrafResultBase_setUnit(iograf, unit)
    PSRIOGrafResultBase_setStageType(iograf, stageType)
    PSRIOGrafResultBase_setInitialStage(iograf, initialStage)
    PSRIOGrafResultBase_setInitialYear(iograf, initialYear)

    PSRIOGrafResultBase_setTotalBlocks(iograf, nBlocks)
    PSRIOGrafResultBase_setTotalSeries(iograf, nSeries)
    PSRIOGrafResultBase_setTotalStages(iograf, nStages)

    # Define os agentes (3 agentes, X, Y e Z)
    # ----------------------------------------
    for i in 1:nCols
        PSRIOGrafResultBase_addAgent(iograf, names[i] )
    end

    # Inicia gravacao do resultado
    # ----------------------------
    PSRIOGrafResultBinary_initSave(iograf, OUTPATH*NAME*".hdr", OUTPATH*NAME*".bin")

    # ---------------------------------------------
    # Parte 3 - Gravacao dos registros do resultado
    # ---------------------------------------------

    # Loop de gravacao
    # ----------------
    for estagio in 1:nStages, serie in 1:nSeries, bloco in 1:nBlocks
        # Seta valor dos agentes para o registro
        # --------------------------------------
        for i in 1:nCols
            PSRIOGrafResultBase_setData(iograf, i-1, data[i, bloco, serie, estagio])
        end

        # Grava registro
        # --------------
        PSRIOGrafResultBinary_writeRegistry(iograf)
    end

    # Finaliza gravacao
    # -----------------
    PSRIOGrafResultBinary_closeSave(iograf)
    PSRIOGrafResultBinary_deleteInstance(iograf)
    iograf = C_NULL
end

"""
    Writes total time and total cost
"""
function print_global_results!(prb, m, elapsed)

     println("time elapsed: $elapsed s")
    #  open(joinpath(prb.options.INPUTPATH, "global_results.csv"), "w") do f
    #      write(f, "total cost: $(objective_value(m)) \n")
    #      write(f, "time elapsed: $elapsed s \n")
    #  end
end

function get_stored_energy(prb::BidDispatchProblem)
    d = prb.data
    n = prb.n

    stored_energy = zeros(n.StorageRegions,1,n.Scenarios)

    for s in 1:n.Scenarios
        stored_water = d.volendday[:, end, s]
        index_water = range(1, length=length(d.reservoirs))

        offer_limit = zeros(n.Hydros)
        upstream_water = -ones(n.Hydros)
        use_limit = false
        use_inflow = false

        for h in 1:n.Hydros
            get_upstream_water!(prb, h, stored_water, index_water, d.volmax, offer_limit, upstream_water, use_limit, use_inflow)
        end

        for h in 1:n.Hydros
            rgn = d.hyd2rgn[h]
            stored_energy[rgn,1,s] += upstream_water[h]*d.fprodt[h]
        end

    end

    return stored_energy

end