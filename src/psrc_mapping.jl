#
# Loading PSRClasses
# ------------------
function load_problem(prb::BidDispatchProblem)
    println("Options...")
    options_load!(prb)

    println("Constants...")
    constants_load!(prb)

    println("Inflow...")
    inflow_load!(prb)

    IO_mapping_data(prb)
    data_load!(prb)


    IO_loading_scenarios(prb)
    scenarios_load!(prb)

    GC.gc()
end

function options_load!(prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    psrc = prb.psrc
    options = prb.options
    # io = prb.io

    psrc.config_ptr = PSRStudy_getConfigurationModel(psrc.study_ptr)

    psrc.iexec   = PSRModel_model2(psrc.config_ptr, "ExecutionParameters")    # Config to Hourly simulation
    psrc.ihourly = PSRModel_model2(psrc.config_ptr, "HourlyData")             # Config to Hourly simulation
    ichron       = PSRModel_model2(psrc.config_ptr, "ChronologicalData")      # Config to Hourly simulation

    # d.tx_discount = psr_get(Float64, psrc.config_ptr, "TaxaDesconto")
    d.tx_discount = PSRParm_getReal(PSRModel_parm2(psrc.config_ptr, "TaxaDesconto") )
    stages_in_yer = PSRStudy_getStagesPerYear(psrc.study_ptr)
    # get corrected discount rate
    # Discount Rate
    # if (DESR <> 0.0) then
    #     DESR:= NUM1 / pow(NUM1 + DESR,- NSTG / NANO)
    # else
    #     DESR:= 1.0
    # end-if

    d.vpl_coef = 1.0
    # if d.tx_discount != 0.0
    #     # discount rate is applied for stages in between current stage and last stage of the year

    #     # assumption : 1 semana de 1 mes
    #     # rendimento = 3 semanas até o fim do mes
    #     stages_to_profit = 3 / 52
    #     d.vpl_coef = 1/(1 + d.tx_discount)^stages_to_profit
    # else
    #     d.vpl_coef = 1.0
    # end
    
    n.OriginalScenarios = PSRStudy_getNumberSimulations(psrc.study_ptr)
    
    # spillage penalty
    # d.scost = psr_get_real(psrc.config_ptr, "SpillagePenalty")
    # if d.scost > 0
    #     d.scost = 0.0
    #     errormsg(:filescreen, "SpillagePenalty > 0 not allowed", io.versionwarns)
    # end

    # Deficit Cost
    # d.dcost = psr_get_real_vec(psrc.config_ptr, "DeficitCost")
    # d.dsegs = psr_get_real_vec(psrc.config_ptr, "DeficitSegment")

    # convert units
    d.conv = 1e-6*3600 # = 0.0036 [ (Hm3/h) / (m3/s) ] = 1e-6[Hm3/m3] * 3600[s/h]

    nothing
end

function constants_load!(prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options
    psrc = prb.psrc

    # time controller
    psrc.timec_ptr = PSRTimeController_create(0)
    # psrc.timec_ptr = PSRScenarioTimeController_create(0)

    PSRTimeController_addElement(psrc.timec_ptr , psrc.study_ptr , true )
    PSRTimeController_configureFrom(psrc.timec_ptr , psrc.study_ptr )

    # Obtem lista de usinas do estudo temporarias
    # ---------------------------------
    psrc.sys_lst = PSRStudy_getCollectionSystems(psrc.study_ptr)
    psrc.ter_lst = PSRStudy_getCollectionPlants(psrc.study_ptr, PSR_PLANTTYPE_THERMAL)
    PSRCollectionElement_query(psrc.ter_lst, "REMOVEDATA WHERE Existing>=2")
    psrc.hyd_lst = PSRStudy_getCollectionPlants(psrc.study_ptr, PSR_PLANTTYPE_HYDRO)
    PSRCollectionElement_query(psrc.hyd_lst, "REMOVEDATA WHERE Existing>=2")
    psrc.gnd_lst = PSRStudy_getCollectionPlants(psrc.study_ptr, PSR_PLANTTYPE_GND)
    psrc.GndStations_lst = PSRStudy_getCollectionByString(psrc.study_ptr, "PSRGndGaugingStation")
    PSRCollectionElement_query(psrc.gnd_lst, "REMOVEDATA WHERE Existing>=2")
    psrc.sts_lst = PSRStudy_getCollectionGaugingStations(psrc.study_ptr)
    psrc.dem_lst = PSRStudy_getCollectionDemands(psrc.study_ptr)
    psrc.dsg_lst = PSRStudy_getCollectionDemandSegments(psrc.study_ptr)
    psrc.fls_lst = PSRStudy_getCollectionFuels(psrc.study_ptr)
    psrc.int_lst = PSRStudy_getCollectionInterconnections(psrc.study_ptr)
    PSRCollectionElement_query(psrc.int_lst, "REMOVEDATA WHERE Existing>=2")
    psrc.fcs_lst = PSRStudy_getCollectionFuelConsumptions(psrc.study_ptr)

    psrc.agn_lst = PSRStudy_getCollectionByString(psrc.study_ptr, "BidBasedDispatch_Agente")
    psrc.vr_lst = PSRStudy_getCollectionByString(psrc.study_ptr, "BidBasedDispatch_VirtualReservoir")
    psrc.rgn_lst = PSRStudy_getCollectionByString(psrc.study_ptr, "BidBasedDispatch_RegiaoArmazenamento")

    # auxiliary for network
    # ---------------------
    psrc.bus_lst = PSRStudy_getCollectionBuses(psrc.study_ptr)
    psrc.gen_lst = PSRStudy_getCollectionShunts(psrc.study_ptr, PSR_DEVICETYPE_GENERATOR)
    psrc.lod_lst = PSRStudy_getCollectionShunts(psrc.study_ptr, PSR_DEVICETYPE_LOAD)

    # Obtem a lista de soma de restricoes de interconexoes (para poder utilizar os metodos especificos de colecoes)
    # --------------------------------------------------------------------------------------------------
    isc_lst = PSRStudy_listConstraintSumInterconnections(psrc.study_ptr)
    psrc.isc_lst = PSRConstraintSumList_asCollectionElements(isc_lst )

    psrc.dcl_lst = PSRStudy_getCollectionSeries2(psrc.study_ptr, PSR_DEVICETYPE_LINKDC)
    PSRCollectionElement_query(psrc.dcl_lst, "REMOVEDATA WHERE Existing>=2")
    psrc.cir_lst = PSRStudy_getCollectionSeries(psrc.study_ptr)
    psrc.are_lst = PSRStudy_getCollectionAreas(psrc.study_ptr)
    PSRQueryStatement_updateCollection(PSRQueryStatement_create(0), psrc.are_lst, "REMOVEELEMENT WHERE code == 0")

    csc_lst_temp = PSRStudy_listConstraintSumCircuits(psrc.study_ptr)
    psrc.csc_lst = PSRConstraintSumList_asCollectionElements(csc_lst_temp)

    # must be sorted to match psrnetwork
    PSRCollectionElement_sortOn(psrc.cir_lst, "code")
    PSRCollectionElement_sortOn(psrc.bus_lst, "code")

    # Obtem a lista de reserva como uma colecao (para poder utilizar os metodos especificos de colecoes)
    # --------------------------------------------------------------------------------------------------
    psrc.grc_lst = PSRStudy_getCollectionReserveGenerationConstraints(psrc.study_ptr)
    psrc.ggc_lst = PSRStudy_getCollectionGenerationConstraints(psrc.study_ptr)

    # Obtem total de sistemas e usinas associadas
    # -------------------------------------------
    n.Hydros   = PSRCollectionElement_maxElements(psrc.hyd_lst)
    n.Thermals = PSRCollectionElement_maxElements(psrc.ter_lst)
    n.Gnd      = PSRCollectionElement_maxElements(psrc.gnd_lst)
    n.GndStations = PSRCollectionElement_maxElements(psrc.GndStations_lst)

    n.Agents   = PSRCollectionElement_maxElements(psrc.agn_lst)
    n.VirtualReservoirs = PSRCollectionElement_maxElements(psrc.vr_lst)
    n.StorageRegions = PSRCollectionElement_maxElements(psrc.rgn_lst)

    n.Stations = PSRCollectionElement_maxElements(psrc.sts_lst)
    n.Fuels    = PSRCollectionElement_maxElements(psrc.fls_lst)
    n.DemSeg   = PSRCollectionElement_maxElements(psrc.dsg_lst)
    n.Demand   = PSRCollectionElement_maxElements(psrc.dem_lst)
    n.Intercs  = PSRCollectionElement_maxElements(psrc.int_lst)
    n.FuelCons = PSRCollectionElement_maxElements(psrc.fcs_lst)
    n.Buses    = PSRCollectionElement_maxElements(psrc.bus_lst)
    n.Sys    = PSRCollectionElement_maxElements(psrc.sys_lst)
    n.Blocks    = PSRStudy_getNumberBlocks(psrc.study_ptr)

    

    n.DCLinks    = PSRCollectionElement_maxElements(psrc.dcl_lst)
    n.Circuits   = PSRCollectionElement_maxElements(psrc.cir_lst)
    n.Areas      = PSRCollectionElement_maxElements(psrc.are_lst)
    n.CircSumCtr = PSRCollectionElement_maxElements(psrc.csc_lst)

    # n.Generators = PSRCollectionElement_maxElements(psrc.gen_lst)
    n.Loads      = PSRCollectionElement_maxElements(psrc.lod_lst)
    # n.Injections = PSRCollectionElement_maxElements(psrc.inj_lst)

    n.GenResCstr    = PSRCollectionElement_maxElements(psrc.grc_lst)
    n.GenGzdCstr    = PSRCollectionElement_maxElements(psrc.ggc_lst)
    n.IntercSumCstr = PSRCollectionElement_maxElements(psrc.isc_lst)

end

"some data is loaded into data and some data is loaded into options"
function scenarios_load!(prb::BidDispatchProblem)

    n = prb.n
    d = prb.data
    psrc = prb.psrc
    options = prb.options

    d.inflow = zeros(Float64, n.Stations)
    d.inflowStage = zeros(Float64, n.Stations)
    if n.Gnd > 0 && psrc.iosddp != C_NULL
        psrc.gnd_ptr = PSRIOSDDP_getGndHourlyScenarios(psrc.iosddp)
    end
    if !options.grafInflow && n.Hydros > 0
        PSRIOSDDPHydroForwardBackward_mapTo(psrc.inflow, d.inflowStage)
    end

    # FCF
    load_fcf!(prb)
end

function load_fcf!(prb::BidDispatchProblem)
    options = prb.options
    fcf_name = "costmexx.psr"
    PATH_FCF = joinpath(options.INPUTPATH, fcf_name)

    #     Cria handle para funcao de custo futuro
    #     -----------------------------------------
    prb.psrc.fcf = C_NULL
    if isfile(PATH_FCF)
        prb.psrc.fcf = PSRIOSDDPFutureCost_create(0)
        #     Carrega funcao de custo futuro
        #     ------------------------------
        ret = PSRIOSDDPFutureCost_load( prb.psrc.fcf, PATH_FCF)
        if ret != PSR_IO_OK
            throw("Error reading fcf")
        end
        PSRIOSDDPFutureCost_gotoStage(prb.psrc.fcf, 1 + options.offset)
    end
end

function data_load!(prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    psrc = prb.psrc
    options = prb.options

    PSRTimeController_resetDate(psrc.timec_ptr)
    gotoStageBID(psrc.timec_ptr, Int32(1), options.offset)

    if n.Agents > 0
        println("Agent...")
        agent_map!(prb)
        println("Agent...")
    end

    if n.VirtualReservoirs > 0
        println("Virtual Reservoir...")
        virtual_reservoir_map!(prb)
    end

    if n.StorageRegions > 0
        println("Storage Region...")
        storage_region_map!(prb)
    end

    if n.Hydros > 0
        println("Hydro...")
        hydro_map!(prb)
    end

    # thermal mappings
    if n.Thermals > 0
        println("Thermal...")
        thermal_map!(prb)
        println("Fuel...")
        fuel_map!(prb)
    end

    # renewable mappings
    if n.Gnd > 0
        println("Renewable...")
        renewable_map!(prb)
    end

    # network mappings
    if n.Demand > 0
        println("Load...")
        demands_map!(prb)
    end

    if n.Sys > 0
        println("System...")
        systems_map!(prb)
    end

    if n.Intercs > 0
        println("Interconnection...")
        interconnections_map!(prb)
    end

    if prb.n.Buses > 0
         println("Network...")
         circuit_map!(prb)
         #sensib_map!(prb)
         network_map!(prb)
    end

    if n.DCLinks > 0
        #dclink_map!(prb)
    end
    if n.Circuits > 0
        circuit_map!(prb)
    end

    # data validation
    validate_agent_data(prb)
    validate_vr_data(prb)
    
    # constraint mappings
    # generationreserve_map!(prb)
    # generationgeneralized_map!(prb)
    # electrical_area_map!(prb)

    # finalize mappers (nor more association to time controller should be done)
    PSRTimeController_createDimensionMappers(psrc.timec_ptr)

    # to get some registry value such as Existing
    gotoStageBID(psrc.timec_ptr, 1, options.offset)
    PSRTimeController_setDimension(psrc.timec_ptr, "block", 1)
    PSRTimeController_setDimension(psrc.timec_ptr, "segment", 1)

    duraci_t = 0.0
    stage=1 #TODO generalizar
    for b::Int32 in 1:n.Blocks
        duraci_b = PSRStudy_getStageDuration(psrc.study_ptr, stage, b )
        duraci_t += duraci_b
        if duraci_b < 0
            error("duration of stage $(stage) block $(b) is negative")
        end
    end
    d.duraci_t = duraci_t

    if options.network_phys == no::ConsiderNetwork
        n.SpotPrice = 1
        d.preco_spot_names = ["Sys"]
    elseif options.network_phys == inter::ConsiderNetwork
        n.SpotPrice = n.Sys
        d.preco_spot_names = [string("Sys ", i) for i in 1:n.Sys]
    elseif options.network_phys == network::ConsiderNetwork
        if options.respresentationnetwork == 0
            n.SpotPrice = n.Buses
            d.preco_spot_names = [string("Bus ", i) for i in 1:n.Buses]
        else
            n.SpotPrice = maximum(d.ilhas)
            d.preco_spot_names = [string("BusGroup ", i) for i in 1:maximum(d.ilhas)]
        end
    end
    nothing
end

function agent_map!(prb::BidDispatchProblem)
    d = prb.data
    n = prb.n
    psrc = prb.psrc
    options = prb.options

    # time controller
    timec_agent_ptr = PSRTimeController_create(0)
    PSRTimeController_configureFrom(timec_agent_ptr, psrc.study_ptr)
    PSRTimeController_addCollection(timec_agent_ptr, psrc.agn_lst, false)

    # local pointer to map
    agn_map_stc_local = PSRMapData_create(0)
    PSRMapData_addElements(agn_map_stc_local, psrc.agn_lst)

    # dynamic pointer
    psrc.agn_map_dyn = PSRMapData_create(0)
    PSRMapData_addElements(psrc.agn_map_dyn, psrc.agn_lst)

    # mappings
    d.aRiskFactorHyd = psr_map_parm(agn_map_stc_local, "RiskFactorHyd", psrc.agn_lst, Float64)
    d.aRiskFactorTer = psr_map_parm(agn_map_stc_local, "RiskFactorTer", psrc.agn_lst, Float64)
    d.aRiskFactorGnd = psr_map_parm(agn_map_stc_local, "RiskFactorGnd", psrc.agn_lst, Float64)
    d.aRiskFactorDem = psr_map_parm(agn_map_stc_local, "RiskFactorDem", psrc.agn_lst, Float64)
    d.aWizardIDHyd = psr_map_parm(agn_map_stc_local, "WizardIDHyd", psrc.agn_lst, Int32)
    d.aWizardIDTer = psr_map_parm(agn_map_stc_local, "WizardIDTer", psrc.agn_lst, Int32)
    d.aWizardIDGnd = psr_map_parm(agn_map_stc_local, "WizardIDGnd", psrc.agn_lst, Int32)
    d.aWizardIDDem = psr_map_parm(agn_map_stc_local, "WizardIDDem", psrc.agn_lst, Int32)
    d.aId = psr_map_parm(agn_map_stc_local, "AgentID", psrc.agn_lst, Int32)

    PSRMapData_pullToMemory(psrc.agn_map_dyn)
    PSRMapData_pullToMemory(agn_map_stc_local)

    # references
    d.agn2hyd = [[] for i in 1:n.Agents]
    d.agn2ter = [[] for i in 1:n.Agents]
    d.agn2gnd = [[] for i in 1:n.Agents]
    d.agn2dem = [[] for i in 1:n.Agents]

    d.hyd2agn = []
    d.ter2agn = []
    d.gnd2agn = []
    d.dem2agn = zeros(n.Demand).-1

    hyd_ids = []
    ter_ids = []
    gnd_ids = []
    dem_ids = []
    
    for i in 1:n.Agents
        ptr_agn = PSRCollectionElement_element(psrc.agn_lst, i-1)
        model_agn = PSRElement_model(ptr_agn)

        code_agn = PSRParm_getInteger(PSRModel_parm2(model_agn, "AgentID"))

        vec_hyd = PSRModel_vector2(model_agn, "HydroPlants")
        vec_ter = PSRModel_vector2(model_agn, "ThermalPlants")
        vec_gnd = PSRModel_vector2(model_agn, "GndPlants")
        vec_dem = PSRModel_vector2(model_agn, "DemandBid")

        n_hyd = PSRVector_size(vec_hyd)
        n_ter = PSRVector_size(vec_ter)
        n_gnd = PSRVector_size(vec_gnd)
        n_dem = PSRVector_size(vec_dem)

        for j in 1:n_hyd
            elem_plant = PSRVectorReference_getReference(vec_hyd, j-1)
            code = PSRPlant_code(elem_plant)
            push!(d.agn2hyd[i], code)
            push!(hyd_ids, code)
            push!(d.hyd2agn, code_agn)
        end

        for j in 1:n_ter
            elem_plant = PSRVectorReference_getReference(vec_ter, j-1)
            code = PSRPlant_code(elem_plant)
            push!(d.agn2ter[i], code)
            push!(ter_ids, code)
            push!(d.ter2agn, code_agn)
        end

        for j in 1:n_gnd
            elem_plant = PSRVectorReference_getReference(vec_gnd, j-1)
            code = PSRPlant_code(elem_plant)
            push!(d.agn2gnd[i], code)
            push!(gnd_ids, code)
            push!(d.gnd2agn, code_agn)
        end
        
        for j in 1:n_dem
            dem = PSRVectorReference_getReference(vec_dem, j-1)
            code = PSRDemand_code(dem)
            push!(d.agn2dem[i], code)
            push!(dem_ids, code)
            d.dem2agn[code] = code_agn
        end
    end

    d.hyd2agn = d.hyd2agn[sortperm(hyd_ids)]
    d.ter2agn = d.ter2agn[sortperm(ter_ids)]
    d.gnd2agn = d.gnd2agn[sortperm(gnd_ids)]

    if !allunique(hyd_ids)
        throw("Uma usina hidrelétrica está associada a múltiplos agentes.")
    end

    if !allunique(ter_ids)
        throw("Uma usina termelétrica está associada a múltiplos agentes.")
    end

    if !allunique(gnd_ids)
        throw("Uma usina renovável está associada a múltiplos agentes.")
    end

    if !allunique(dem_ids)
        throw("Uma demanda está associada a múltiplos agentes.")
    end

end

function storage_region_map!(prb::BidDispatchProblem)
    d = prb.data
    n = prb.n
    psrc = prb.psrc
    options = prb.options

    # time controller
    timec_region_ptr = PSRTimeController_create(0)
    PSRTimeController_configureFrom(timec_region_ptr, psrc.study_ptr)
    PSRTimeController_addCollection(timec_region_ptr, psrc.rgn_lst, false)

    # local pointer to map
    rgn_map_stc_local = PSRMapData_create(0)
    PSRMapData_addElements(rgn_map_stc_local, psrc.rgn_lst)

    # dynamic pointer
    psrc.rgn_map_dyn = PSRMapData_create(0)
    PSRMapData_addElements(psrc.rgn_map_dyn, psrc.rgn_lst)

    # mappings
    d.rId = psr_map_parm(rgn_map_stc_local, "RegionID", psrc.rgn_lst, Int32)

    PSRMapData_pullToMemory(psrc.rgn_map_dyn)
    PSRMapData_pullToMemory(rgn_map_stc_local)

    # references
    d.rgn2hyd = [[] for i in 1:n.StorageRegions]

    d.hyd2rgn = []

    hyd_ids = []

    for i in 1:n.StorageRegions
        ptr_rgn = PSRCollectionElement_element(psrc.rgn_lst, i-1)
        model_rgn = PSRElement_model(ptr_rgn)
        
        parm_sys = PSRModel_parm2(PSRElement_model(ptr_rgn), "Sistema")
        id_sys = PSRSystem_code(PSRParmReference_getReference(parm_sys))
        push!(d.rgn2sys, id_sys)

        code_rgn = PSRParm_getInteger(PSRModel_parm2(model_rgn, "RegionID"))
        vec_hyd = PSRModel_vector2(model_rgn, "HydroPlants")
        n_hyd = PSRVector_size(vec_hyd)

        for j in 1:n_hyd
            elem_plant = PSRVectorReference_getReference(vec_hyd, j-1)
            code = PSRPlant_code(elem_plant)
            push!(d.rgn2hyd[i], code)
            push!(hyd_ids, code)
            push!(d.hyd2rgn, code_rgn)
        end
        
    end

    d.hyd2rgn = d.hyd2rgn[sortperm(hyd_ids)]

    if !allunique(hyd_ids)
        throw("Uma usina hidrelétrica está associada a múltiplas regiões de armazenamento.")
    end

end

function validate_agent_data(prb::BidDispatchProblem)
    d = prb.data
    n = prb.n

    if length(d.hyd2agn) < n.Hydros
        throw("Uma usina hidrelétrica não está associada a nenhum agente.")
    end

    if length(d.ter2agn) < n.Thermals
        throw("Uma usina termelétrica não está associada a nenhum agente.")
    end

    if length(d.gnd2agn) < n.Gnd
        throw("Uma usina renovável não está associada a nenhum agente.")
    end

end

function virtual_reservoir_map!(prb::BidDispatchProblem)
    d = prb.data
    n = prb.n
    psrc = prb.psrc
    options = prb.options

    # time controller
    timec_virtual_reservoir_ptr = PSRTimeController_create(0)
    PSRTimeController_configureFrom(timec_virtual_reservoir_ptr, psrc.study_ptr)
    PSRTimeController_addCollection(timec_virtual_reservoir_ptr, psrc.vr_lst, false)

    # local pointer to map
    vr_map_stc_local = PSRMapData_create(0)
    PSRMapData_addElements(vr_map_stc_local, psrc.vr_lst)

    # dynamic pointer
    psrc.vr_map_dyn = PSRMapData_create(0)
    PSRMapData_addElements(psrc.vr_map_dyn, psrc.vr_lst)

    # mappings
    d.vrCode = []
    for i in 1:n.VirtualReservoirs
        ptr_vr = PSRCollectionElement_element(psrc.vr_lst, i-1)
        model_vr = PSRElement_model(ptr_vr)

        code_vr = PSRParm_getInteger(PSRModel_parm2(model_vr, "ReservoirID"))
        push!(d.vrCode, code_vr)
    end
    d.hGF = psr_map_vector(psrc.vr_map_dyn, "CotasMRE", psrc.vr_lst, Float64)
    d.hGF2 = psr_map_vector(psrc.vr_map_dyn, "CotasMRE2", psrc.vr_lst, Float64)

    PSRMapData_pullToMemory(psrc.vr_map_dyn)
    PSRMapData_pullToMemory(vr_map_stc_local)

    # references
    for i in 1:n.VirtualReservoirs
        ptr_vr = PSRCollectionElement_element(psrc.vr_lst, i-1)

        parm_agn = PSRModel_parm2(PSRElement_model(ptr_vr), "Agente")
        model_agn = PSRElement_model(PSRParmReference_getReference(parm_agn))
        id_agn = PSRParm_getInteger(PSRModel_parm2(model_agn, "AgentID"))

        parm_rgn = PSRModel_parm2(PSRElement_model(ptr_vr), "RegiaoArmazenamento")
        model_rgn = PSRElement_model(PSRParmReference_getReference(parm_rgn))
        id_rgn = PSRParm_getInteger(PSRModel_parm2(model_rgn, "RegionID"))

        parm_sys = PSRModel_parm2(model_rgn, "Sistema")
        id_sys = PSRSystem_code(PSRParmReference_getReference(parm_sys))
        
        push!(d.vr2agn, id_agn)
        push!(d.vr2rgn, id_rgn)
        push!(d.vr2sys, id_sys)
    end

end

function validate_vr_data(prb::BidDispatchProblem)
    d = prb.data
    n = prb.n
    
    dict_ids = Dict{Int32, Vector{Int32}}()

    for (agent, rgn) in zip(d.vr2agn, d.vr2rgn)

        aux = get(dict_ids, agent, Vector{Int32}())
        append!(aux, rgn)
        dict_ids[agent] = aux

    end
    
    for rgn_list in values(dict_ids)
        if !allunique(rgn_list)
            throw("Cada par agente/região de armazenamento deve estar associado a um único reservatório virtual.")
        end
    end

end

function hydro_map!(prb::BidDispatchProblem)
    d = prb.data
    n = prb.n
    psrc = prb.psrc
    options = prb.options

    # time controller
    timec_hydro_ptr = PSRTimeController_create(0)
    PSRTimeController_configureFrom(timec_hydro_ptr, psrc.study_ptr)
    PSRTimeController_addCollection(timec_hydro_ptr, psrc.hyd_lst, false)

    # local pointer to map
    hyd_map_stc_local = PSRMapData_create(0)
    PSRMapData_addElements(hyd_map_stc_local, psrc.hyd_lst)

    # local pointer to map (gauging stations)
    sts_map_stc_local = PSRMapData_create(0)
    PSRMapData_addElements(sts_map_stc_local, psrc.sts_lst)

    # dynamic pointer
    psrc.hyd_map_dyn = PSRMapData_create(0)
    PSRMapData_addElements(psrc.hyd_map_dyn, psrc.hyd_lst)

    # mappings
    hName = allocstring(n.Hydros, 12)
    PSRMapData_mapParm(hyd_map_stc_local, "name", hName, 12)
    d.hName = splitstring(hName,12)

    d.hCode = psr_map_parm(hyd_map_stc_local, "code", psrc.hyd_lst, Int32)
    d.stsCode = psr_map_parm(sts_map_stc_local, "code", psrc.sts_lst, Int32)

    d.hIsCommit = zeros(n.Hydros)

    d.hExist = psr_map_vector(psrc.hyd_map_dyn, "Existing", psrc.hyd_lst, Int32)

    d.downstream_turb = psr_relation(psrc.hyd_lst, psrc.hyd_lst, PSR_RELATIONSHIP_TURBINED_TO )
    d.downstream_spill = psr_relation(psrc.hyd_lst, psrc.hyd_lst, PSR_RELATIONSHIP_SPILLED_TO )
    d.upstream_turb = psr_complex_relation(psrc.hyd_lst, psrc.hyd_lst, PSR_RELATIONSHIP_TURBINED_TO)
    d.upstream_spill = psr_complex_relation(psrc.hyd_lst, psrc.hyd_lst, PSR_RELATIONSHIP_SPILLED_TO)

    d.hyd2station = psr_relation(psrc.hyd_lst, psrc.sts_lst, PSR_RELATIONSHIP_1TO1 )

    d.fprodt = psr_map_vector(psrc.hyd_map_dyn, "FPMed", psrc.hyd_lst, Float64)

    d.defmax =  psr_map_vector(psrc.hyd_map_dyn, "Qmax", psrc.hyd_lst, Float64)
    d.defmin = psr_map_vector(psrc.hyd_map_dyn, "Qmin", psrc.hyd_lst, Float64)

    
    volini = psr_map_parm(hyd_map_stc_local, "Vinic", psrc.hyd_lst, Float64)
    VinicType = psr_map_parm(hyd_map_stc_local, "VinicType", psrc.hyd_lst, Int32)

    d.volmin = psr_map_vector(psrc.hyd_map_dyn, "Vmin", psrc.hyd_lst, Float64)
    d.volmax = psr_map_vector(psrc.hyd_map_dyn, "Vmax", psrc.hyd_lst, Float64)

    d.hPotInst = psr_map_vector(psrc.hyd_map_dyn, "PotInst", psrc.hyd_lst, Float64)

    # d.hGF = zeros(n.Hydros, 4) # Nsys = 4
    # d.hSaldoInicial = psr_map_parm(hyd_map_stc_local, "SaldoInicialEnergia", psrc.hyd_lst, Float64)

    # gotoStageBID(timec_hydro_ptr, 1, options.offset)
    # for sys::Int32 in 1:4 # Nsys = 4
    #     PSRTimeController_setDimension(timec_hydro_ptr, "regiao", sys)
    #     PSRMapData_pullToMemory(psrc.hyd_map_dyn)

    #     d.hGF[:, sys] = psr_map_vector(psrc.hyd_map_dyn, "GarantiaFisica", psrc.hyd_lst, Float64)
    # end

    table_SxH = psrc_table(Float64, psrc.hyd_lst, "SxH_Head", "SxH_Storage")

    d.conv = 1e-6*3600

    d.hyd2sys = psr_relation(psrc.hyd_lst, psrc.sys_lst, PSR_RELATIONSHIP_1TO1 )
    d.hyd2gen = psr_relation(psrc.hyd_lst, psrc.gen_lst, PSR_RELATIONSHIP_1TO1 )
    d.gen2bus = psr_relation(psrc.gen_lst, psrc.bus_lst, PSR_RELATIONSHIP_1TO1 )

    d.hyd2bus = zeros(Int32, length(d.hyd2gen))
    for i in eachindex(d.hyd2gen)
        gen = d.hyd2gen[i]
        if gen > 0
            d.hyd2bus[i] = d.gen2bus[gen]
        else
            # is a reservoir
            d.hyd2bus[i] = 1#d.gen2bus[gen]
        end
    end

    PSRTimeController_createDimensionMappers(timec_hydro_ptr)
    gotoStageHourBID(timec_hydro_ptr, Int32(1), options.offset, Int32(1))

    hName = allocstring(n.Hydros, 12)
    PSRMapData_mapParm(hyd_map_stc_local, "name", hName, 12)

    stationName = allocstring(n.Stations, 12)
    PSRMapData_mapParm(sts_map_stc_local, "name", stationName, 12)

    PSRMapData_pullToMemory(psrc.hyd_map_dyn)
    PSRMapData_pullToMemory(hyd_map_stc_local)
    PSRMapData_pullToMemory(sts_map_stc_local)

    d.hName = splitstring(hName,12)
    d.stationName = splitstring(stationName,12)
    d.turb_max = zeros(Float64, n.Hydros)

    d.reservoirs = []
    d.runoffriver = []
    d.volini = zeros(n.Hydros, n.Scenarios)
    for i in 1:n.Hydros
        d.turb_max[i] = prb.data.fprodt[i] > 0 ? min(d.hPotInst[i]/prb.data.fprodt[i], d.defmax[i]) : d.defmax[i]
        if d.volmin[i] != d.volmax[i]
            push!(d.reservoirs, i)
        else
            push!(d.runoffriver, i)
        end

        # set volini
        volini2storage!(prb, VinicType, volini, table_SxH[i]["y"], table_SxH[i]["x"], i)
    end

    d.volini = repeat(d.volini[:,1], 1, n.Scenarios)

    nothing
end

# Set volini into storage
function volini2storage!(prb::BidDispatchProblem, VinicType::Vector{Int32}, volini::Vector{Float64}, SxH_Storage, SxH_Head, i)
    d = prb.data
    n = prb.n
    # 0 is storage p.u.
    if VinicType[i] == 1 # 1 is elevation
        VinicType[i] = 0

        # get storage from height
        d.volini[i,1] = lininterp(SxH_Storage, SxH_Head, volini[i])

    else
        if VinicType[i] == 0 # 0 is pu
            d.volini[i,1] = volini[i] * (d.volmax[i] - d.volmin[i]) + d.volmin[i]
        elseif VinicType[i] == 2 # 2 is volume itself
            d.volini[i,1] = volini[1]
        end
        if d.volini[i,1] < d.volmin[i]
            d.volini[i,1] = d.volmin[i]
        end
        if d.volini[i,1] > d.volmax[i]
            d.volini[i,1] = d.volmax[i]
        end
    end

    nothing
end

function lininterp(vec_y, vec_x, x)
    #SxH_Storage : y
    #SxH_Head : x

    # initialize
    i_old = 0.0
    for (idx,i) in enumerate(vec_x)
        # check if is inside interval
        if x < i && x > i_old
            if idx > 1
                # linearization: (vec_y[idx] - vec_y[idx-1])/ (i - i_old) * x
                return (vec_y[idx] - vec_y[idx-1])/ (i - i_old) * (x - i_old) + vec_y[idx-1]
            else
                return (vec_y[idx] - 0.0)/ (i - 0.0) * x
            end
        else
            # update
            i_old = i
        end
    end
    return 0.0
end

# Table methods
function psrc_table(::Type{T},
    collection::PSRClassesPtr,
    field_x::String,
    field_y::String) where T
    # @show field_x, field_y
    x = psrc_table_column(T, collection, field_x)
    y = psrc_table_column(T, collection, field_y)
    tables = Dict()

    for i in eachindex(x)
        tables[i] = Dict("x"=>x[i], "y"=>y[i] )
    end
    return tables
end
function psrc_table_column(::Type{T},
    collection::PSRClassesPtr, field::String) where T
    n = PSRCollectionElement_maxElements(collection)::Int32
    ret = [Float64[] for i in 1:n]
    for element in 1:n
        ret[element] = psrc_table_column(T, collection, element, field)
    end
    return ret
end
function psrc_table_column(::Type{Vector{Float64}}, collection::PSRClassesPtr, index::Integer, field::String)
    n = PSRCollectionElement_maxElements(collection)
    # if n <= 0
    #     error("bad number of arguments")
    # end
    element = PSRCollectionElement_element(collection, index-1)
    model = PSRElement_model(element)

    # first_parm = PSRModel_parm3(model, field, 0)
    # if PSRParm_getDataType(first_parm) != PSR_PARM_REAL
    #     error("wrong data type")
    # end
    # @show index
    max_dim = psrc_get_max_dim(Vector{Float64}, collection, index, field)
    ret = zeros(Float64, max_dim)
    for dim in 1:max_dim
        parm = PSRModel_vector3(model, field, dim)
        ret[dim] = PSRVector_getCurrentReal(parm)::Float64
    end
    return ret
end
function psrc_get_max_dim(::Type{Vector{Float64}}, collection::PSRClassesPtr, index::Integer, name::String)
    n = PSRCollectionElement_maxElements(collection)
    # if index > n
    #     error()
    # end
    if n <= 0
        return 0
    end
    ptr = PSRCollectionElement_element(collection, index-1)
    imodel = PSRElement_model(ptr)
    # @show first_parm = PSRModel_parm3(imodel, name, 0)
    first_parm = PSRModel_vector2(imodel, name*"(1)")
    if first_parm == C_NULL
        return 0
    end
    info = PSRVector_getDimensionInformation(first_parm)
    # if PSRParmDimensionInformation_getNumberDimensions(info) != 1
    #     error("wrong number of dimenstions")
    # end
    max_dim = PSRVectorDimensionInformation_getDimensionSize(info, 0)
    return max_dim
end
function psrc_table_column(::Type{Float64}, collection::PSRClassesPtr, index::Integer, field::String)
    n = PSRCollectionElement_maxElements(collection)
    # if n <= 0
    #     error("bad number of arguments")
    # end
    element = PSRCollectionElement_element(collection, index-1)
    model = PSRElement_model(element)

    # first_parm = PSRModel_parm3(model, field, 0)
    # if PSRParm_getDataType(first_parm) != PSR_PARM_REAL
    #     error("wrong data type")
    # end
    # @show index
    max_dim = psrc_get_max_dim(Float64, collection, index, field)
    ret = zeros(Float64, max_dim)
    for dim in 1:max_dim
        parm = PSRModel_parm3(model, field, dim)
        ret[dim] = PSRParm_getReal(parm)::Float64
    end
    return ret
end
function psrc_get_max_dim(::Type{Float64}, collection::PSRClassesPtr, index::Integer, name::String)
    n = PSRCollectionElement_maxElements(collection)
    # if index > n
    #     error()
    # end
    if n <= 0
        return 0
    end
    ptr = PSRCollectionElement_element(collection, index-1)
    imodel = PSRElement_model(ptr)
    # @show first_parm = PSRModel_parm3(imodel, name, 0)
    first_parm = PSRModel_parm2(imodel, name*"(1)")
    if first_parm == C_NULL
        return 0
    end
    info = PSRParm_getDimensionInformation(first_parm)
    # if PSRVectorDimensionInformation_getNumberDimensions(info) != 1
    #     error("wrong number of dimenstions")
    # end
    max_dim = PSRParmDimensionInformation_getDimensionSize(info, 0)
    return max_dim
end

function thermal_map!(prb::BidDispatchProblem)
    d = prb.data
    n = prb.n
    psrc = prb.psrc
    options = prb.options

    # local pointer to map
    ter_map_stc = PSRMapData_create(0)
    PSRMapData_addElements(ter_map_stc, psrc.ter_lst)

    # dynamic pointer
    psrc.ter_map_dyn = PSRMapData_create(0)
    PSRMapData_addElements(psrc.ter_map_dyn, psrc.ter_lst)

    # mappings
    tName = allocstring(n.Thermals, 12)
    PSRMapData_mapParm(ter_map_stc, "name", tName, 12)
    d.tName = splitstring(tName, 12)

    d.tType = psr_map_parm(ter_map_stc, "thermaltype", psrc.ter_lst, Int32)
    d.tCode = psr_map_parm(ter_map_stc, "code", psrc.ter_lst, Int32)


    # Ramps
    d.tRampUpCold = psr_map_parm(ter_map_stc, "RampUp", psrc.ter_lst, Float64)
    d.tRampUpWarm = d.tRampUpCold

    d.tRampDown = psr_map_parm(ter_map_stc, "RampDown", psrc.ter_lst, Float64)

    # Uptime
    d.tShutDownCost =  psr_map_parm(ter_map_stc, "ShutDownCost", psrc.ter_lst, Float64)
    d.tStartUpCost =  psr_map_vector(ter_map_stc, "StartUp", psrc.ter_lst, Float64)

    #d.tShutDownCost = zeros(n.Thermals).+2 TODO
    d.tMinUptime   = psr_map_parm(ter_map_stc, "MinUptime", psrc.ter_lst, Float64)
    d.tMinDowntime = psr_map_parm(ter_map_stc, "MinDowntime", psrc.ter_lst, Float64)
    d.tGerMin = psr_map_vector(psrc.ter_map_dyn, "GerMin", psrc.ter_lst, Float64)
    d.consider_commit = psr_map_parm(ter_map_stc, "ComT", psrc.ter_lst, Int32)

    # premissas chile
    #d.tMinUptime = 2 .* ones(Int, size(prb.data.tRampDown))
    #d.tMinDowntime = 3 .* ones(Int, size(prb.data.tRampDown))
    #d.consider_commit = ones(Int32, size(d.tRampDown))

    # Startup
    d.tMaxStartUps = psr_map_parm(ter_map_stc, "MaxStartUps", psrc.ter_lst, Int32)
    d.tMaxShutDowns = psr_map_parm(ter_map_stc, "MaxShutDowns", psrc.ter_lst, Int32)
    d.startupCold = zeros(Float64, n.Thermals)
    d.timeIsCold = zeros(Int, n.Thermals)

    d.tExist = psr_map_vector(psrc.ter_map_dyn, "Existing", psrc.ter_lst, Int32)

    # generation limits

    d.tGerMax = psr_map_vector(psrc.ter_map_dyn, "GerMax", psrc.ter_lst, Float64)
    d.tPotInst =  psr_map_vector(psrc.ter_map_dyn, "PotInst", psrc.ter_lst, Float64)

    # commitment

    #d.initial_state_commit = zeros(n.Thermals)

    # warm/cold definition
    d.startupWarm = zeros(Float64, n.Thermals)
    d.timeIsWarm = zeros(Float64, n.Thermals)
    d.startupHot = zeros(Float64, n.Thermals)

    # costs
    d.startup_old = true

    # d.tCVU = psr_map_vector(psrc.ter_map_dyn, "O&MCost", psrc.ter_lst, Float64)
    #TODO: custo de CVU considera combustivel

    # -----------------------------------------------------------
    # relations
    # -----------------------------------------------------------

    #thermals to fuels
    d.ter2fuel = psr_relation(psrc.ter_lst, psrc.fls_lst, PSR_RELATIONSHIP_1TO1 )

    # thermal to system
    d.ter2sys = psr_relation(psrc.ter_lst, psrc.sys_lst, PSR_RELATIONSHIP_1TO1 )

    d.ter2gen = psr_relation(psrc.ter_lst, psrc.gen_lst, PSR_RELATIONSHIP_1TO1 )
    gen2bus = psr_relation(psrc.gen_lst, psrc.bus_lst, PSR_RELATIONSHIP_1TO1 )

    d.ter2bus = zeros(Int32, length(d.ter2gen))
    for i in eachindex(d.ter2gen)
        gen = d.ter2gen[i]
        if gen > 0
            d.ter2bus[i] = d.gen2bus[gen]
        else
            # errormsg("thermal of index $i, code $(d.tCode[i]), name $(d.tName[i]) is not associated to any generator")
        end
    end

    # strings
    tName = allocstring(n.Thermals, 12)
    PSRMapData_mapParm(ter_map_stc, "name", tName, 12)

    PSRMapData_pullToMemory(psrc.ter_map_dyn)
    PSRMapData_pullToMemory(ter_map_stc)

    # fix names
    d.tName = splitstring(tName,12)

    # get index of existing plants
    d.tExisting = Int32[]
    for i in 1:n.Thermals
        if d.tExist[i] <= 0
            push!(d.tExisting, i)
        end
    end
end

function fuel_map!(prb::BidDispatchProblem)
    d = prb.data
    n = prb.n
    psrc = prb.psrc
    options = prb.options

    # local pointer to map
    fls_map_stc = PSRMapData_create(0)
    PSRMapData_addElements(fls_map_stc , psrc.fls_lst)

    # dynamic pointer
    psrc.fls_map_dyn = PSRMapData_create(0)
    PSRMapData_addElements(psrc.fls_map_dyn, psrc.fls_lst)

    fcs_map_dyn_blk_seg = PSRMapData_create(0)
    PSRMapData_addElements(fcs_map_dyn_blk_seg, psrc.fcs_lst)

    fcs_map_dyn_seg = PSRMapData_create(0)
    PSRMapData_addElements(fcs_map_dyn_seg, psrc.fcs_lst)

    fcs_map_dyn = PSRMapData_create(0)
    PSRMapData_addElements(fcs_map_dyn, psrc.fcs_lst)

    # mappings
    fName = allocstring(n.Fuels, 12)
    PSRMapData_mapParm(fls_map_stc, "name", fName, 12)
    d.fName = splitstring(fName, 12)

    fcsCEspTemp = psr_map_vector(fcs_map_dyn_blk_seg,  "CEsp", psrc.fcs_lst, Float64, "segment", "block"  )

    fcG = psr_map_vector(fcs_map_dyn_seg,  "G", psrc.fcs_lst, Float64, "segment")

    fCVaria = psr_map_vector(fcs_map_dyn, "O&MCost", psrc.fcs_lst, Float64)
    d.tCVU = fCVaria

    fCTransp = psr_map_vector(fcs_map_dyn, "CTransp", psrc.fcs_lst, Float64)

    d.fCost = psr_map_vector(psrc.fls_map_dyn, "Custo", psrc.fls_lst, Float64)
    d.fuelconsumptionrate = psr_map_vector(psrc.fls_map_dyn, "ConsumptionMax", psrc.fls_lst, Float64)
    fuelavailability = psr_map_vector(psrc.fls_map_dyn, "Availability", psrc.fls_lst, Float64)

    # fuel consumption to fuel
    d.fcs2fls = psr_relation(psrc.fcs_lst, psrc.fls_lst, PSR_RELATIONSHIP_1TO1 )
    d.fcs2ter = psr_relation(psrc.fcs_lst, psrc.ter_lst, PSR_RELATIONSHIP_1TO1 )

    # Initial Pull
    PSRMapData_pullToMemory(fls_map_stc)
    PSRMapData_pullToMemory(fcs_map_dyn)
    PSRMapData_pullToMemory(fcs_map_dyn_seg)
    PSRMapData_pullToMemory(fcs_map_dyn_blk_seg)
    PSRMapData_pullToMemory(psrc.fls_map_dyn)

    exist_fuelconsumptionrate = loadExistElement(psrc.fls_lst, "ConsumptionMax")
    exist_fuelavailability = loadExistElement(psrc.fls_lst, "Availability")


    # thermal cost
    # ------------
    # Extra time controller for demand segments
    segment_timecontroller = PSRTimeController_create(0)
    PSRTimeController_configureFrom(segment_timecontroller, psrc.study_ptr)
    PSRTimeController_addCollection(segment_timecontroller, psrc.fcs_lst, false)


    d.fcsCost = zeros(Float64, 3, length(d.fcs2fls))
    d.fcsSegment = zeros(Float64, 3, length(d.fcs2fls))

    for seg::Int32 in 1:3

        PSRTimeController_setDimension(segment_timecontroller, "segment", seg)
        PSRMapData_pullToMemory(psrc.fls_map_dyn)
        PSRMapData_pullToMemory(fcs_map_dyn_seg)

        for i in 1:n.FuelCons

            # REM pega apenas 1 bloco

            fcost = d.fCost[ d.fcs2fls[i] ]

            # if ifcost  = 0, put mean value
            if fcost == 0
                fcost = sum(d.fCost[:]) / n.Fuels
            end

            fcs = fcsCEspTemp[i]
            # if ifcost  = 0, put mean value
            if fcs == 0
                fcs = sum(fcsCEspTemp[:]) / n.FuelCons
            end

            d.fcsCost[seg, i] = fcs * ( fcost ) + fCTransp[i]+ fCVaria[i]

            d.fcsSegment[seg,i] = fcG[i]/100.0
        end
    end

    for j in 1:n.FuelCons
        if sum(d.fcsSegment[:,j]) == 0.0
            d.fcsSegment[1,j] = 1.0
        end
    end

    d.fcs_nsegments = 3 .* ones(Int32, n.FuelCons)
    for seg in 3:-1:2
        for i in 1:n.FuelCons
            if d.fcs_nsegments[i] == seg && d.fcsSegment[seg,i] == 0.0
                d.fcs_nsegments[i] -= 1
            end
        end
    end
end

function renewable_map!(prb::BidDispatchProblem)
    d = prb.data
    n = prb.n
    psrc = prb.psrc
    options = prb.options

    # local pointer to map
    gnd_map_stc = PSRMapData_create(0)
    PSRMapData_addElements(gnd_map_stc, psrc.gnd_lst)

    gndStation_map_stc = PSRMapData_create(0)
    PSRMapData_addElements(gndStation_map_stc, psrc.GndStations_lst)

    # dynamic pointer
    psrc.gnd_map_dyn = PSRMapData_create(0)
    PSRMapData_addElements(psrc.gnd_map_dyn, psrc.gnd_lst)
    psrc.gnd_map_dyn_hor = PSRMapData_create(0)
    PSRMapData_addElements(psrc.gnd_map_dyn_hor, psrc.gnd_lst)

    # name
    gName = allocstring(n.Gnd, 12)
    PSRMapData_mapParm(gnd_map_stc, "name", gName, 12)

    d.gExist = psr_map_vector(psrc.gnd_map_dyn, "Existing", psrc.gnd_lst, Int32)
    d.gCode = psr_map_parm(gnd_map_stc, "code", psrc.gnd_lst, Int32)

    d.rPotInst = psr_map_vector(psrc.gnd_map_dyn, "PotInst", psrc.gnd_lst, Float64)
    d.gFatOp = psr_map_vector(psrc.gnd_map_dyn, "FatOpe", psrc.gnd_lst, Float64)
    d.gOMCost = psr_map_vector(psrc.gnd_map_dyn, "O&MCost", psrc.gnd_lst, Float64)
    d.gPenalty = psr_map_vector(psrc.gnd_map_dyn, "CurtailmentCost", psrc.gnd_lst, Float64)

     # //--- Cenarios de geracao horaria ---
    # VETOR REAL        HourGeneration
    d.gGerHourly = psr_map_vector(psrc.gnd_map_dyn_hor, "HourGeneration", psrc.gnd_lst, Float64)

    # d.rStation = strip(splited[4])

    d.gnd2gen = psr_relation(psrc.gnd_lst, psrc.gen_lst, PSR_RELATIONSHIP_1TO1 )
    gen2bus = psr_relation(psrc.gen_lst, psrc.bus_lst, PSR_RELATIONSHIP_1TO1 )

    gnd2gndstation = psr_relation(psrc.gnd_lst, psrc.GndStations_lst, PSR_RELATIONSHIP_NTO1 )

    d.gnd2bus = zeros(Int32, length(d.gnd2gen))
    for i in eachindex(d.gnd2gen)
        gen = d.gnd2gen[i]
        if gen > 0
            d.gnd2bus[i] = d.gen2bus[gen]
        else
            # errormsg("renewable of index $i, code $(d.gCode[i]), name $(d.gName[i]) is not associated to any generator")
        end
    end

    # mappings
    d.gnd2sys = psr_relation(psrc.gnd_lst, psrc.sys_lst, PSR_RELATIONSHIP_1TO1 )

    # Initial Pull
    PSRMapData_pullToMemory(gnd_map_stc)
    PSRMapData_pullToMemory(gndStation_map_stc)
    PSRMapData_pullToMemory(psrc.gnd_map_dyn)

    d.gName = splitstring(gName,12)


    d.forecast = zeros(Float64, n.Gnd, n.MaxHours)
    d.rengen =  zeros(Float64, n.Gnd, n.MaxHours, n.ScenariosExtended)

    d.ren2stat = zeros(Int32, n.Gnd)
end

function inflow_load!(prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    psrc = prb.psrc
    options = prb.options
    Scenarios = n.Scenarios

    if n.Hydros > 0

        # check files
        PATH_CASE = options.INPUTPATH
        PATH_FORW = joinpath(PATH_CASE, "forw.psr")
        PATH_CSV = joinpath(PATH_CASE, "inflow.csv")
        PATH_BIN = joinpath(PATH_CASE, "inflow.bin")
        file_sum = isfile(PATH_FORW) + isfile(PATH_CSV) + isfile(PATH_BIN)

        if file_sum > 1
            error("Existe mais de um arquivo de afluência na pasta $PATH_CASE. Só pode haver um dos seguintes arquivos: forw.psr, inflow.csv, inflow.bin.")
        elseif file_sum < 1
            error("Nenhum arquivo de afluência foi encontrado na pasta $PATH_CASE. É necessário um dos seguintes arquivos: forw.psr, inflow.csv, inflow.bin.")
        end

        # inflow data
        if isfile(PATH_FORW)
            iinflow = PSRIOSDDPHydroForwardBackward_create(0)
            ret = PSRIOSDDPHydroForwardBackward_load(iinflow, psrc.study_ptr , PATH_FORW, PATH_FORW)

            # hydro
            ihydro = PSRIOSDDPHydroParameters_create(0)
            PSRIOSDDPHydroParameters_generateIndexedHydroParameters2(ihydro, psrc.study_ptr)

            # store pointer
            psrc.inflow = iinflow
        else
            options.grafInflow = true
        end
    end

end

function demands_map!(prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    psrc = prb.psrc
    options = prb.options
    options = prb.options

    # ----------------------------------------------
    # system data
    # ----------------------------------------------
    dem_map_stc = PSRMapData_create(0)
    PSRMapData_addElements(dem_map_stc , psrc.dem_lst)

    # strings
    # -------
    dName = allocstring(n.Demand, 12)
    PSRMapData_mapParm(dem_map_stc, "name", dName, 12)

    # allocations
    d.dsg2sys = zeros(Int32, n.DemSeg)
    d.demandHour = zeros(Float64, n.Demand)
    d.demand = zeros(Float64, n.Demand, n.MaxHours)
    d.netdemand = zeros(Float64, n.Demand, n.MaxHours, n.ScenariosExtended)
    d.demandBlock = zeros(n.DemSeg)
    d.demandsegment = zeros(n.DemSeg, n.Blocks)

    # relations

    #demand segment to demand
    d.dCode = psr_map_parm(dem_map_stc, "code", psrc.dem_lst, Int32)
    d.dsg2dem = psr_relation(psrc.dsg_lst, psrc.dem_lst, PSR_RELATIONSHIP_1TO1 )
    d.dem2sys = psr_relation(psrc.dem_lst, psrc.sys_lst, PSR_RELATIONSHIP_1TO1 )
    d.lod2dem = psr_relation(psrc.lod_lst, psrc.dem_lst, PSR_RELATIONSHIP_1TO1 )
    d.lod2bus = psr_relation(psrc.lod_lst, psrc.bus_lst, PSR_RELATIONSHIP_1TO1 )

    #load
    d.loadweight = zeros(n.Loads, n.Blocks)
    psrc.lod_map_dyn_blk = PSRMapData_create(0)
    PSRMapData_addElements(psrc.lod_map_dyn_blk, psrc.lod_lst)

    d.loadweighttemp = psr_map_vector(psrc.lod_map_dyn_blk, "P", psrc.lod_lst, Float64, "block")

    for b in 1:n.Blocks
        PSRTimeController_setDimension(psrc.timec_ptr, "block", b)
        gotoStageBID(psrc.timec_ptr, Int32(1), options.offset)
        PSRMapData_pullToMemory(psrc.lod_map_dyn_blk)
        normalize_loadweight_block!(prb, b)
    end
    
    psrc.dsg_map_dyn_blk = PSRMapData_create(0)
    PSRMapData_addElements(psrc.dsg_map_dyn_blk, psrc.dsg_lst)
    d.demandBlockTemp = psr_map_vector(psrc.dsg_map_dyn_blk, "Demanda", psrc.dsg_lst, Float64, "block")

    getBlockDemandData(prb)

    # Extra time controller for demand segments
    psrc.demand = PSRTimeController_create(0)
    PSRTimeController_configureFrom(psrc.demand, psrc.study_ptr)
    PSRTimeController_addCollection(psrc.demand, psrc.dsg_lst, false)

    # Map SEGMENTS
    psrc.dsg_map_dyn_hor = PSRMapData_create(0)
    PSRMapData_addElements(psrc.dsg_map_dyn_hor, psrc.dsg_lst)

    d.demandHour = psr_map_vector(psrc.dsg_map_dyn_hor, "HourDemand", psrc.dsg_lst, Float64)

    PSRTimeController_createDimensionMappers(psrc.demand)

    # gotoStageHourBID(psrc.demand, options.stage, options.offset, 1)
    # PSRMapData_pullToMemory(psrc.dsg_map_dyn_hor)

    d.sys2dem = [Int32[] for i in 1:n.Sys]
    for i in 1:n.Sys
        for j in 1:n.Demand
            if d.dem2sys[j] == i
                push!(d.sys2dem[i], j)
            end
        end
    end

    # ------------------------------------------------------------------
    # PULL DATA
    # ------------------------------------------------------------------
    PSRMapData_pullToMemory(dem_map_stc)

    # fix names
    # ---------
    d.dName = splitstring(dName,12)

    nothing
end

function systems_map!(prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    psrc = prb.psrc
    options = prb.options

    # ----------------------------------------------
    # system data
    # ----------------------------------------------
    sys_map_stc = PSRMapData_create(0)
    PSRMapData_addElements(sys_map_stc , psrc.sys_lst)

    # strings
    # -------
    sName = allocstring(n.Sys, 12)
    PSRMapData_mapParm(sys_map_stc, "name", sName, 12)

    # ------------------------------------------------------------------
    # PULL DATA
    # ------------------------------------------------------------------
    PSRMapData_pullToMemory(sys_map_stc)

    # fix names
    # ---------
    d.bName = splitstring(sName,12)

end

function interconnections_map!(prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    psrc = prb.psrc
    options = prb.options

    # allocations
    iCapacityTo = zeros(Float64, n.Intercs)
    iCapacityFrom = zeros(Float64, n.Intercs)
    d.iCapacityTo = zeros(Float64, n.Intercs)
    d.iCapacityFrom = zeros(Float64, n.Intercs)
    d.iLossFrom = zeros(Float64, n.Intercs)
    d.iLossFrom = zeros(Float64, n.Intercs)
    d.intExist = zeros(Int32, n.Intercs)

    # intercs to and from systems
    d.intFsys = psr_relation(psrc.int_lst, psrc.sys_lst, PSR_RELATIONSHIP_FROM )
    d.int2sys   = psr_relation(psrc.int_lst, psrc.sys_lst, PSR_RELATIONSHIP_TO )

    # Map

    # strings
    # -------
    int_map_stc = PSRMapData_create(0)
    PSRMapData_addElements(int_map_stc, psrc.int_lst)
    int_map_dyn_blk = PSRMapData_create(0)
    PSRMapData_addElements(int_map_dyn_blk, psrc.int_lst)

    # intName = allocstring(n.Sys, 12)
    # PSRMapData_mapParm(int_map_stc, "name", intName, 12)

    # regular
    d.intExist = psr_map_vector(int_map_stc, "Existing", psrc.int_lst, Int32)

    # VETOR   REAL      Capacity->     DIM(block) INDEX Data
    iCapacityFrom = psr_map_vector(int_map_dyn_blk, "Capacity->", psrc.int_lst, Float64, "block")
    # VETOR   REAL      Capacity<-     DIM(block) INDEX Data
    iCapacityTo = psr_map_vector(int_map_dyn_blk, "Capacity<-", psrc.int_lst, Float64, "block")

    # VETOR   REAL      LossFactor->              INDEX Data
    d.iLossFrom = psr_map_vector(int_map_stc, "LossFactor->", psrc.int_lst, Float64)
    # # VETOR   REAL      LossFactor<-              INDEX Data
    d.iLossTo = psr_map_vector(int_map_stc, "LossFactor<-", psrc.int_lst, Float64)

    # Initial Pull
    PSRMapData_pullToMemory(int_map_stc)
    PSRMapData_pullToMemory(int_map_dyn_blk)

    gotoStageBID(psrc.timec_ptr, 1, options.offset)
    tota_hours= 0.0
    for block::Int32 in 1:n.Blocks
        PSRTimeController_setDimension(psrc.timec_ptr, "block", block)
        gotoStageBID(psrc.timec_ptr, Int32(1), options.offset)
        PSRMapData_pullToMemory(int_map_dyn_blk)

        block_duration = PSRStudy_getStageDuration(psrc.study_ptr, Int32(1), block )
        tota_hours += block_duration

        for i in 1:n.Intercs
            d.iCapacityTo[i]  += iCapacityTo[i] * block_duration
            d.iCapacityFrom[i] += iCapacityFrom[i] * block_duration
        end
    end

    d.iCapacityTo  = d.iCapacityTo ./ tota_hours
    d.iCapacityFrom = d.iCapacityFrom ./ tota_hours

    # d.intName = splitstring(intName,12)
    nothing
end

function circuit_map!(prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    psrc = prb.psrc
    options = prb.options

    d.bus2sys = psr_relation(psrc.bus_lst, psrc.sys_lst, PSR_RELATIONSHIP_1TO1 )
    d.bus2dem = psr_relation(psrc.bus_lst, psrc.dem_lst, PSR_RELATIONSHIP_1TO1 )

    # circuit
    #d.cName, d.cAVId = get_name_and_avid(psrc.cir_lst)
    d.cirFbus = psr_relation(psrc.cir_lst, psrc.bus_lst, PSR_RELATIONSHIP_FROM )
    d.cir2bus = psr_relation(psrc.cir_lst, psrc.bus_lst, PSR_RELATIONSHIP_TO )
    # maps interregional circuits
    d.interregional_circuits = zeros(Bool, n.Circuits)
    for circ in 1:n.Circuits
        if d.bus2sys[d.cirFbus[circ]] != d.bus2sys[d.cir2bus[circ]]
            d.interregional_circuits[circ] = true
        end
    end
    # Circuits (and trafos)
    local_cir_map_stc = PSRMapData_create(0)
    PSRMapData_addElements(local_cir_map_stc, psrc.cir_lst)
    cir_map_dyn = PSRMapData_create(0)
    PSRMapData_addElements(cir_map_dyn, psrc.cir_lst)
    # VETOR REAL    R INDEX Data
    #d.circuit_r = psr_map_vector(cir_map_dyn, "R", psrc.cir_lst, Float64)
    # VETOR REAL    X INDEX Data
    d.circuit_x = psr_map_vector(cir_map_dyn, "X", psrc.cir_lst, Float64)
    # VETOR REAL    MVAr INDEX Data
    # VETOR REAL    Tmn INDEX Data
    # VETOR REAL    Tmx INDEX Data
    # VETOR REAL    Rn INDEX Data
    d.circuit_Rn = psr_map_vector(cir_map_dyn, "Rn", psrc.cir_lst, Float64)
    # VETOR REAL    Re INDEX Data
    #d.circuit_limitemergency = psr_map_vector(cir_map_dyn, "Re", psrc.cir_lst, Float64)
    # VETOR INTEGER    I INDEX Data
    #d.circuit_I = psr_map_vector(cir_map_dyn, "I", psrc.cir_lst, Int32)
    #d.circuit_IRegistry = psr_map_vector(local_cir_map_stc, "I", psrc.cir_lst, Int32) #no varying Vector
    # VETOR REAL    Prob INDEX Data
    # VETOR INTEGER    Iflh INDEX Data
    # VETOR INTEGER    Status INDEX Data
    #d.circuit_status = psr_map_vector(cir_map_dyn, "Status", psrc.cir_lst, Int32)
    #d.circuit_internationalcost_from_temp = psr_map_vector(cir_map_dyn, "InternationalCost->", psrc.cir_lst, Float64, "block")
    #d.circuit_internationalcost_to_temp = psr_map_vector(cir_map_dyn, "InternationalCost<-", psrc.cir_lst, Float64, "block")
    # //--- Marcado para monitoramento
    # PARM INTEGER    FlagMonitored
    #d.circuit_monitored = psr_map_parm(local_cir_map_stc, "FlagMonitored", psrc.cir_lst, Int32)
    # //--- Marcado para contingencia
    # PARM INTEGER    FlagContingency
    #d.circuit_contigency = psr_map_parm(local_cir_map_stc, "FlagContingency", psrc.cir_lst, Int32)
    # PARM INTEGER    FlagConstraint
    # PARM INTEGER    FlagTariff
    # PARM INTEGER    FlagDC
    #d.circuit_phasemin = psr_map_vector(cir_map_dyn, "Pmn", psrc.cir_lst, Float64)
    #d.circuit_phasemax = psr_map_vector(cir_map_dyn, "Pmx", psrc.cir_lst, Float64)
    #d.cirCode = psr_map_parm(local_cir_map_stc, "code", psrc.cir_lst, Int32)
    
    # Initial Pull
    PSRMapData_pullToMemory(cir_map_dyn)
    #read_optgenbigm(prb)
    nothing
end
#=
function sensib_map!(prb::BidDispatchProblem)

    n = prb.n
    d = prb.data
    psrc = prb.psrc
    options = prb.options
    io = prb.io

    # allocations
    d.sensib_demand = zeros(n.Sys)

    sensib_demand_factor = zeros(n.DemandSensibs)

    local_dms_map_stc = PSRMapData_create(0)
    PSRMapData_addElements(local_dms_map_stc, psrc.dms_lst)

    sensib_demand_system = psr_map_parm(local_dms_map_stc, "SystemCode", psrc.dms_lst, Int32)
    sensib_demand_factor = psr_map_parm(local_dms_map_stc, "Factor", psrc.dms_lst, Float64)

    # Initial Pull
    PSRMapData_pullToMemory(local_dms_map_stc)
    PSRMapData_deleteInstance(local_dms_map_stc)
    local_dms_map_stc = C_NULL

    for i in 1:n.DemandSensibs
        if sensib_demand_system[i] == 0
            for j in 1:n.Sys
                d.sensib_demand[j] = sensib_demand_factor[i]
            end
            break
        end
    end
    for i in 1:n.DemandSensibs
        if sensib_demand_system[i] != 0
            for j in 1:n.Sys
                if d.sCode[j] == sensib_demand_system[i]
                    d.sensib_demand[j] = sensib_demand_factor[i]
                    break
                end
            end
        end
    end

    for i in 1:n.Sys
        if d.sensib_demand[i] <= 0.0
            d.sensib_demand[i] = 1.0
        end
    end

    return nothing
end
=#

function network_map!(prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    psrc = prb.psrc
    options = prb.options
    ######################


# PSRNETWORK INITIALIZE
     psrnetwork_windows_path = joinpath(Sys.BINDIR, "PSRNetwork.dll")
     psrnetwork_linux_path = joinpath(Sys.BINDIR, "PSRNetwork.so")
     if isfile(psrnetwork_windows_path) || isfile(psrnetwork_linux_path)
         Sys.iswindows() ? Libdl.dlopen(psrnetwork_windows_path) : Libdl.dlopen(psrnetwork_linux_path)
         PSRNetwork.initialize_PSRClasses(0, Sys.BINDIR, Sys.BINDIR)
     else
         Libdl.dlopen(joinpath(@__DIR__, "..", "deps", "psrnetwork-distribution", "windows", "PSRNetwork"))
         masks_and_models = joinpath(@__DIR__, "..", "deps", "psrnetwork-distribution", "windows")   
         PSRNetwork.initialize_PSRClasses(0, masks_and_models, masks_and_models)
     end
 

    study = PSRNetwork.load_study_SDDP(options.INPUTPATH)
    psrnetwork = PSRNetwork.default(study, 0, 0, 0, 0)
    PSRNetwork.set_max_violations(psrnetwork, 0, 0, 0)

    #psrnetwork = PSRNetwork.Default(study, 0, 1, 0, 0)

    PSRNetwork.set_violations_settings(psrnetwork, PSRNetwork.VIOLATIONS_NONE)
    PSRNetwork.set_contingency_violations_settings(psrnetwork, PSRNetwork.VIOLATIONS_NONE, true)

    PSRNetwork.update_stage(psrnetwork, 1)

    circuits_size = PSRNetwork.get_circuits_size(psrnetwork)
    circuits_codes = PSRNetwork.get_circuits_codes(psrnetwork)
    bus_size = PSRNetwork.get_buses_size(psrnetwork)
     #######################
    

    beta = zeros(bus_size)
    d.Beta = zeros(circuits_size,bus_size)
    for circuit_index in 1:circuits_size
        PSRNetwork.get_beta!(psrnetwork, circuit_index, beta)
        d.Beta[circuit_index,:] = beta
    end

    d.ilhas = PSRNetwork.get_connected_components(psrnetwork)

    # allocations
    dCapacityTo = zeros(n.Buses, n.Blocks)
    dCapacityFrom = zeros(n.Buses, n.Blocks)

    # dclink to and from systems
    d.dclFbus = psr_relation(psrc.dcl_lst, psrc.bus_lst, PSR_RELATIONSHIP_FROM )
    d.dcl2bus   = psr_relation(psrc.dcl_lst, psrc.bus_lst, PSR_RELATIONSHIP_TO )

    # Map

    # strings
    # -------
    dcl_map_stc = PSRMapData_create(0)
    PSRMapData_addElements(dcl_map_stc, psrc.dcl_lst)
    dcl_map_dyn = PSRMapData_create(0)
    PSRMapData_addElements(dcl_map_dyn, psrc.dcl_lst)
    dcl_map_dyn_blk = PSRMapData_create(0)
    PSRMapData_addElements(dcl_map_dyn_blk, psrc.dcl_lst)

    # regular
    d.dExist = psr_map_vector(dcl_map_dyn, "Existing", psrc.dcl_lst, Int32)

    # VETOR   REAL      Capacity->     DIM(block) INDEX Data
    d.dCapacityFrom = psr_map_vector(dcl_map_dyn_blk, "Capacity->", psrc.dcl_lst, Float64, "block")
    # VETOR   REAL      Capacity<-     DIM(block) INDEX Data
    d.dCapacityTo = psr_map_vector(dcl_map_dyn_blk, "Capacity<-", psrc.dcl_lst, Float64, "block")

    # VETOR   REAL      LossFactor->              INDEX Data
    d.dLossFrom = psr_map_vector(dcl_map_dyn, "LossFactor->", psrc.dcl_lst, Float64)
    # VETOR   REAL      LossFactor<-              INDEX Data
    d.dLossTo = psr_map_vector(dcl_map_dyn, "LossFactor<-", psrc.dcl_lst, Float64)


    # Initial Pull
    PSRMapData_pullToMemory(dcl_map_stc)
    PSRMapData_pullToMemory(dcl_map_dyn)
    PSRMapData_pullToMemory(dcl_map_dyn_blk)

    # for i in 1:n.Buses
    #     d.dCapacityTo[i]   = sum(dCapacityTo[b] for b in 1:n.Blocks) / n.Blocks
    #     d.dCapacityFrom[i] = sum(dCapacityFrom[b] for b in 1:n.Blocks) / n.Blocks
    # end
    nothing
end

function pull_scenarios_data!(prb::BidDispatchProblem, hour::Int32, scen::Int32)
    d = prb.data
    n = prb.n
    psrc = prb.psrc
    options = prb.options
    stageDuration = floor(Int, d.duraci_t)

    # if scen == 1
    #     pull_demand_data!(prb, hour)
    # end

    if hour == 1
        pull_inflow_data!(prb, scen)
    end

    # pull_renewable_data!(prb, hour, scen)

    # currentBlock = PSRTimeController_getCurrentBlockHour(psrc.timec_ptr)
    # if currentBlock == 0
    #     println("ERROR: problem in Hour-Block mapping: got block = 0 at t$(d.t), s$(d.s), h$hour")
    # end
end

function pull_inflow_data!(prb::BidDispatchProblem, scen::Int32)
    d = prb.data
    n = prb.n
    psrc = prb.psrc
    options = prb.options
    if n.Hydros > 0
        if options.grafInflow
            d.inflow = d.inflow_graf.valores[1,scen,1,:]
        else
            PSRIOSDDPHydroForwardBackward_setForward(psrc.inflow, options.stage, scen)
            # d.inflowStage = [m3/s]
            d.inflow = d.inflowStage
        end
    end
    nothing
end

function pull_renewable_data!(prb::BidDispatchProblem, hour::Int32, scen::Int32)
    d = prb.data
    n = prb.n
    psrc = prb.psrc
    options = prb.options

    if n.Gnd == 0
        return nothing
    end

    PSRIOElementHourlyScenarios_gotoStageHour(psrc.gnd_ptr, options.stage + options.offset, hour, scen)
    # Refaz o pull para a memoria dos atributos
    PSRMapData_pullToMemory(psrc.gnd_map_dyn_hor)

    nothing
end

function pull_demand_data!(prb::BidDispatchProblem, hour::Int32)
    d = prb.data
    n = prb.n
    psrc = prb.psrc
    options = prb.options

    # Seta o estagio
    # general pointers ( demand)
    # gotoStageHourBID(psrc.demand, options.stage, options.offset, hour)
    # PSRMapData_pullToMemory(psrc.dsg_map_dyn_hor)

    # # save demand
    # d.demand[:, hour] = d.demandHour
    d.demandHour = d.demand[hour, :]
    nothing
end

function set_initial_condition!(prb::BidDispatchProblem, inpts)
    # loop to set initial scenario
    for i in 1:prb.n.MaxHours, j in 1:(prb.n.Scenarios-1)
        set_scenario!(inpts, prb.data.quantities)
    end
    prb.n.Scenarios=1
end

function PSRStudy_getCollectionByString(istudy, entity::String)
    # "PSRGasNode"
    # "PSRGasPipeline"
    # PSRCollectionString *ptrClassNameFilters = new PSRCollectionString();
    istring = PSRCollectionString_create(0)

    # ptrClassNameFilters->addString("PSRMaintenanceData");
    PSRCollectionString_addString(istring, entity)

    # PSRCollectionElement *ptrColElement = getCollectionElements(NULL, ptrClassNameFilters);
    lst = PSRStudy_getCollectionElements(istudy, C_NULL, istring)
	# delete ptrClassNameFilters;

    # ptrColElement->removeRedundant();
    PSRCollectionElement_removeRedundant( lst )

	return lst;
end

function normalize_loadweight_block!(prb::BidDispatchProblem, b::Int)

    n = prb.n
    d = prb.data

    total_lod = zeros(n.Demand)
    for i in eachindex(d.loadweighttemp)
        total_lod[d.lod2dem[i]] += d.loadweighttemp[i]
    end

    for i in eachindex(d.loadweighttemp)
        if total_lod[d.lod2dem[i]] > 0
            d.loadweight[i, b] = d.loadweighttemp[i]/total_lod[d.lod2dem[i]]
        else
            d.loadweight[i, b] = 0.0
        end
    end

    nothing
end

function getBlockDemandData(prb::BidDispatchProblem)
    d = prb.data
    n = prb.n
    p = prb.psrc

    fill!(d.demandsegment,0.0)

    # TODO @stage

    for block in 1:n.Blocks
        # Seta o bloco atual pelo time controller
        PSRTimeController_setDimension(p.timec_ptr, "block", block)
        # Refaz o pull para a memoria dos atributos
        PSRMapData_pullToMemory(p.dsg_map_dyn_blk)
        fixdemand!(prb, d.demandBlockTemp, d.demandBlock)
        # Demanda por bloco
        for seg in 1:n.DemSeg
            # Demand Segment per block
            d.demandsegment[seg, block] += d.demandBlock[seg]
        end
    end

    nothing
end

function fixdemand!(prb::BidDispatchProblem, demand_temp::Vector{Float64}, demand_out::Vector{Float64})

    n = prb.n
    d = prb.data

    first_seg = true

    for i in 1:n.Demand
        first_seg = true
        for j in 1:n.DemSeg
            if d.dsg2dem[j] == i
                if first_seg
                    first_seg = false
                    demand_out[j] = demand_temp[j]
                else
                    demand_out[j] = demand_temp[j] - demand_temp[j-1]
                end
            end
        end
    end

    return nothing
end

function bus_load(prb::BidDispatchProblem)# only_inelastic::Bool=false)
    d = prb.data
    n = prb.n

    out = zeros(n.Buses, n.Blocks)
        for b in 1:n.Blocks
            for lod in 1:n.Loads
                w = d.loadweight[lod, b]
                dem = d.lod2dem[lod]
                bus = d.lod2bus[lod]
                #if dem > 0 # is there some demand
                for dsg in 1:n.DemSeg
                    if dem == d.dsg2dem[dsg]
                            out[bus, b] += w*d.demandsegment[dsg, b]*1000 # getvalue(m, def[defseg,b])
                    end # dem from dsg
                end # for dsg
                #end # if dem>0
            end
        end
    return out
end

function bus_loadhour(prb::BidDispatchProblem)# only_inelastic::Bool=false)
    d = prb.data
    n = prb.n

    out = zeros(n.Buses, n.MaxHours)
        for hour in 1:n.MaxHours
            block = PSRTimeController_getBlockFromStageHour( prb.psrc.timec_ptr,1, hour )
            for lod in 1:n.Loads
                w = d.loadweight[lod, block]
                dem = d.lod2dem[lod]
                bus = d.lod2bus[lod]
                #if dem > 0 # is there some demand
                #for dem in 1:n.Demand
                out[bus, hour] += w*d.demand[hour, dem] # getvalue(m, def[defseg,b])
                #end
                #end # if dem>0
            end
        end
    return out
end

function bus_load_bid(prb::BidDispatchProblem,hour::Int32,week_hour::Int32,s::Int32)
    d = prb.data
    n = prb.n

    out = zeros(n.Buses)
        block = PSRTimeController_getBlockFromStageHour( prb.psrc.timec_ptr,1, hour )
        for lod in 1:n.Loads
            w = d.loadweight[lod, block]
            dem = d.lod2dem[lod]
            bus = d.lod2bus[lod]
            dem_index = findfirst(x->x==dem, d.dCode)
            out[bus] += w*d.current_demand.valores[1, s, week_hour, dem_index]*1e3
        end
    return out
end