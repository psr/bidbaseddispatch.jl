"""
    (B) Modulo para o Wizard que cria ofertas de preço e quantidade a partir de uma execução do SDDP.

    input: - saídas do SDDP (CMO e gerhid,gerter,gergnd)
           - arquivo DAT informando qual é o tipo de Oferta
           - arquivo de contabilizacao de energia
    output: - arquivos de oferta de preço e quantidade em formato GRAF
"""

"""
    Create offer for every agent
"""
function create_offers!(prb::BidDispatchProblem, write_to_file::Bool, etapa, dia)

    d = prb.data
    options = prb.options

    # create offers from wizard for all agents

    d.sale_offer_hyd = create_hydro_sale_offers(prb, dia) # (vr, dia/semana)
    d.purchase_offer_hyd = create_hydro_purchase_offers(prb, dia) # (vr, dia/semana)
    d.sale_offer_ter = create_thermal_offers(prb, dia) # (termo, hora/semana)
    d.sale_offer_gnd = create_renewable_offers(prb, dia)
    update_demand_offers!(prb)

    # overwrite offers with information from files (for agents with offer files)

    read_offers!(prb, options.INPUTPATH, etapa)

    if any(d.aWizardIDHyd.==2)
        read_ref_offers!(prb, options.INPUTPATH, etapa)
        d.sale_offer_hyd = create_hydro_offers_with_ref(prb)
    end

    # write reference offer files

    if write_to_file
        println("Começo da gravação das ofertas")
        for agent_id in d.aId
            if any(d.hyd2agn .== agent_id)
                write_offers!(prb, agent_id, "hyd")
            end
            if any(d.ter2agn .== agent_id)
                write_offers!(prb, agent_id, "ter")
            end
            if any(d.gnd2agn .== agent_id)
                write_offers!(prb, agent_id, "gnd")
            end
        end
    end

    pad_offers!(prb)
end

"""
    Create offer for every Hydro agent
"""
function create_hydro_sale_offers(prb::BidDispatchProblem, dia)

    d = prb.data
    n = prb.n
    options = prb.options

    vec_offers = Array{Vector{Offer}, 2}(undef, n.VirtualReservoirs, n.Scenarios)
    d.sale_offer_hyd_num_segments = Array{Int32, 1}(undef, n.VirtualReservoirs)

    for vr = 1:n.VirtualReservoirs

        d.sale_offer_hyd_num_segments[vr] = options.WizardSegments

        agent_id = d.vr2agn[vr]
        wizard_id = d.aWizardIDHyd[agent_id]

        if wizard_id == 1
            rgn = d.vr2rgn[vr]
            horas = (dia-1)*n.MaxHours + 1 : (dia)*n.MaxHours
            theta = d.aRiskFactorHyd[agent_id] # escalar


            GF = d.hGF[vr] # GF de "vr"
            GF_total = sum(d.hGF[d.vr2rgn.==rgn]) # GF total da regiao "rgn" 
            energy_share = GF/GF_total # parcela da energia da regiao "rgn" atribuida a "vr"

            index_usinas = (d.hyd2rgn.==rgn) # usinas da regiao "rgn"
            gerhid_usinas = d.gerhid.valores[1, :, horas, index_usinas]
            gerhid_total = sum(gerhid_usinas, dims=3)[:, :, 1] # (cenarios, horas), gerhid da regiao "rgn"
            gerhid = energy_share * gerhid_total # (cenarios, horas), parcela de gerhid_total atribuida a "vr"

            custo_agua = d.custo_agua.valores[1, :, horas, index_usinas] # cenarios, horas, usinas
            custo_agua_media = mean(custo_agua, dims=3)[:, :, 1] # (cenarios, horas), media simples
            custo_agua_media_ponderada = sum(custo_agua.*gerhid_usinas, dims=3)[:, :, 1]./gerhid_total # (cenarios, horas), media ponderada
            index_nan = isnan.(custo_agua_media_ponderada)
            custo_agua_media_ponderada[index_nan] = custo_agua_media[index_nan] # (cenarios, horas), preco da agua na regiao "rgn"
        end

        for serie in 1:n.Scenarios
            if wizard_id == 1
                offer_max = d.aBudgets[vr, serie].energy # escalar
                oferta = create_energy_offer!(custo_agua_media_ponderada, gerhid, theta, offer_max, serie, wizard_id, options.WizardSegments)
            else
                oferta = [Offer(0.0, 0.0, 1)] # oferta placeholder, vai ser sobrescrita
            end
            vec_offers[vr, serie] = oferta
        end
    end

    return vec_offers
end

function create_hydro_purchase_offers(prb::BidDispatchProblem, dia)

    d = prb.data
    n = prb.n
    options = prb.options

    vec_offers = Array{Vector{Offer}, 2}(undef, n.VirtualReservoirs, n.Scenarios)
    d.purchase_offer_hyd_num_segments = Array{Int32, 1}(undef, n.VirtualReservoirs)

    for vr = 1:n.VirtualReservoirs

        d.purchase_offer_hyd_num_segments[vr] = options.WizardSegments

        for serie in 1:n.Scenarios
            oferta = [Offer(0.0, 0.0, 1)] # oferta placeholder, vai ser sobrescrita
            vec_offers[vr, serie] = oferta
        end
    end

    return vec_offers
end

"""
    Create offer for every Thermal agent
"""
function create_thermal_offers(prb::BidDispatchProblem, dia)

    d = prb.data
    n = prb.n
    options = prb.options

    vec_offers = Array{Vector{Offer}, 3}(undef, n.Thermals, n.MaxHours, n.Scenarios)
    d.sale_offer_ter_num_segments = Array{Int32, 1}(undef, n.Thermals)

    for i = 1:n.Thermals

        d.sale_offer_ter_num_segments[i] = 1

        sys = d.ter2sys[i]

        agent_id = d.ter2agn[i]

        offer_max = d.tGerMax[i]
        theta = d.aRiskFactorTer[agent_id]

        custo_variavel = d.tCVU[i]

        for hora = (dia-1)*n.MaxHours + 1 : (dia)*n.MaxHours

            hora_dia = hora - (dia-1)*n.MaxHours

            for serie in 1:n.Scenarios
                oferta = create_power_offer(custo_variavel, theta, offer_max, serie, d.aWizardIDTer[agent_id])
                vec_offers[i, hora_dia, serie] = oferta
            end
        end
    end
    
    return vec_offers
end

"""
    Create offer for every Gnd agent
"""
function create_renewable_offers(prb::BidDispatchProblem, dia)

    d = prb.data
    n = prb.n
    options = prb.options

    vec_offers = Array{Vector{Offer}, 3}(undef, n.Gnd, n.MaxHours, n.Scenarios)
    d.sale_offer_gnd_num_segments = Array{Int32, 1}(undef, n.Gnd)

    for i = 1:n.Gnd
        d.sale_offer_gnd_num_segments[i] = 1

        agent_id = d.gnd2agn[i]
        theta = d.aRiskFactorGnd[agent_id]
        preco = d.gOMCost[i] * (1+theta)

        for hora = (dia-1)*n.MaxHours + 1 : (dia)*n.MaxHours
            hora_dia = hora - (dia-1)*n.MaxHours
            gergnd = d.gergnd.valores[1, :, hora, i]
            for serie in 1:n.Scenarios
                oferta = [Offer(gergnd[serie], preco, 1)]
                vec_offers[i, hora_dia, serie] = oferta
            end
        end
    end
    return vec_offers
end

function update_demand_offers!(prb::BidDispatchProblem)

    d = prb.data
    n = prb.n
    for i = 1:n.Demand
        agent_id = d.dem2agn[i]
        if agent_id != -1 && d.aWizardIDDem[agent_id] == 1
            theta = d.aRiskFactorDem[agent_id]
            d.current_demand.valores[1, :, :, i] = (1+theta)*d.demand_graf.valores[1, :, :, i]
        end
    end
    return
end

"""
    Create offer for single agent from CMO and Gen
"""
function create_power_offer(custo::Float64, theta::Float64, offer_max::Float64, serie::Int64, markup_type::Int32)

    segment_id = 1

    if markup_type==0
        price_offer = 0.0
    elseif markup_type==1
        price_offer = custo .* (1+theta)
    else
        throw("Invalid thermal/gnd wizard ID $markup_type .")
    end


    # elseif markup_type==2
    #     ger*=1e3 # GWh -> MWh
    #     receita_esperada = mean(ger .* preco_spot)
    #     premio = theta * receita_esperada

    #     if mean(ger) > 0.0
    #         price_offer = mean(custo) + premio/mean(ger)
    #     else
    #         price_offer = Inf
    #     end

    qtd_offer = offer_max

    return [Offer(qtd_offer, price_offer, segment_id)]
end

"""
    Create offer for single agent from CMO and Gen
"""
function create_energy_offer!(custo::Array{Float64, 2}, ger::Array{Float64, 2}, theta::Float64, offer_max::Float64, serie::Int64, markup_type::Int32, segments::Int32)

    ger*=1e3 # GWh -> MWh
    
    # currently, segments is hard-coded = 1
    if segments > 1

        custo_cenario = mean(custo, dims = 2)[:,1]
        ger_cenario = sum(ger, dims=2)[:,1]
        ger_max = minimum([ger_cenario[serie], offer_max])
        index_max = ger_cenario.<=ger_max
        if sum(index_max) > 0
            custo_cenario = custo_cenario[index_max]
            ger_cenario = ger_cenario[index_max]
        else
            ger_cenario = [offer_max]
            custo_cenario = [custo_cenario[serie]]
        end

        sort_index = sortperm(custo_cenario)

        ger_sort = ger_cenario[sort_index]
        ger_diff = cat(ger_sort[1], diff(ger_sort), dims=1)

        for (index, value) in enumerate(ger_diff)
            if value < 0
                ger_diff[index] = 0
                if index < length(ger_diff)
                    ger_diff[index+1] += value
                end
            end
        end

        custo_sort = custo_cenario[sort_index]

        num_segments = minimum([length(custo_sort), segments])

        indexes = sortperm(ger_diff, rev=true)[1:num_segments]

        return Offer.(ger_diff[indexes], custo_sort[indexes].*(1+theta), 1:num_segments)

    end

    segment_id = 1

    if markup_type==0
        price_offer = 0.0
    elseif markup_type==1
        price_offer = mean(custo) .* (1+theta)
    elseif markup_type==2
        price_offer = 0.0
    else
        throw("Invalid hydro wizard ID $markup_type .")
    end


    # elseif markup_type==2
    #     receita_esperada = mean(ger .* preco_spot, dims = 1) # media nos cenarios, resultado indexado por hora
    #     premio = theta .* receita_esperada # resultado indexado por hora
    
    #     if mean(ger) > 0.0
    #         price_offer = vec(mean(custo) .+ premio./mean(ger, dims=1)) # media nos cenarios, resultado indexado por hora
    #     else
    #         price_offer = Inf
    #     end

    #     price_offer = mean(price_offer) # preco horario medio

    qtd_offer = mean(ger, dims=1)[1,:] # resultado indexado por hora
    qtd_offer = sum(qtd_offer) # energia total do dia
    qtd_offer = minimum([qtd_offer, offer_max]) # energia total do dia

    return [Offer(qtd_offer, price_offer, segment_id)]
end

"""
    Write offers into file
"""
function write_offers!(prb::BidDispatchProblem, agent_id::Int32, plant_type::String)

    d = prb.data
    n = prb.n

    if plant_type == "hyd"

        plant_name =  "VR "
        vec_plants = (1:n.VirtualReservoirs)[d.vr2agn.==agent_id]
        num_segments = d.sale_offer_hyd_num_segments
        offers = d.sale_offer_hyd

    elseif plant_type == "ter"

        plant_name =  "Ter "
        vec_plants = d.agn2ter[agent_id]
        num_segments = d.sale_offer_ter_num_segments
        offers = d.sale_offer_ter

    elseif plant_type == "gnd"

        plant_name =  "Ren "
        vec_plants = d.agn2gnd[agent_id]
        num_segments = d.sale_offer_gnd_num_segments
        offers = d.sale_offer_gnd

    end

    PATH = prb.options.INPUTPATH

    # Configura Gerenciador de Log
    # ----------------------------
    ilog = PSRManagerLog_getInstance(0)

    # Inicializa idioma portugues para log
    # ------------------------------------
    PSRManagerLog_initPortuguese(ilog)
    
    # -----------------------------------
    # Parte 2 - Configuracao do resultado
    # -----------------------------------

    # Cria objetos para manipulacao de saida GRAF
    # ------------------------------------
    if prb.options.outBin
        iografoffer_price = PSRIOGrafResultBinary_create(0)
        iografoffer_qtd = PSRIOGrafResultBinary_create(0)
    else
        iografoffer_price = PSRIOGrafResult_create(0)
        iografoffer_qtd = PSRIOGrafResult_create(0)
    end

    PSRIOGrafResultBase_setUnit(iografoffer_price, "\$/MWh")
    PSRIOGrafResultBase_setUnit(iografoffer_qtd, "MWh")

    PSRIOGrafResultBase_setStageType(iografoffer_price, PSR_STAGETYPE_DAILY)
    PSRIOGrafResultBase_setStageType(iografoffer_qtd, PSR_STAGETYPE_DAILY)

    PSRIOGrafResultBase_setInitialStage(iografoffer_price, 1)
    PSRIOGrafResultBase_setInitialStage(iografoffer_qtd, 1)

    PSRIOGrafResultBase_setVariableByBlock(iografoffer_price, 1)
    PSRIOGrafResultBase_setVariableBySerie(iografoffer_price, 1)
    PSRIOGrafResultBase_setVariableByBlock(iografoffer_qtd, 1)
    PSRIOGrafResultBase_setVariableBySerie(iografoffer_qtd, 1)

    if plant_type == "hyd"
        PSRIOGrafResultBase_setTotalBlocks(iografoffer_price, 1)
        PSRIOGrafResultBase_setTotalBlocks(iografoffer_qtd, 1)
    else
        PSRIOGrafResultBase_setTotalBlocks(iografoffer_price, n.MaxHours)
        PSRIOGrafResultBase_setTotalBlocks(iografoffer_qtd, n.MaxHours)
    end

    PSRIOGrafResultBase_setTotalSeries(iografoffer_price, n.Scenarios)
    PSRIOGrafResultBase_setTotalSeries(iografoffer_qtd, n.Scenarios)

    PSRIOGrafResultBase_setSequencialModel(iografoffer_price, true)
    PSRIOGrafResultBase_setSequencialModel(iografoffer_qtd, true)

    PSRIOGrafResultBase_setInitialYear(iografoffer_price, 1)
    PSRIOGrafResultBase_setInitialYear(iografoffer_qtd, 1)

    for plant in vec_plants
        for seg in num_segments[plant]
            PSRIOGrafResultBase_addAgent(iografoffer_price, string(plant_name, plant, " - ", seg))
            PSRIOGrafResultBase_addAgent(iografoffer_qtd, string(plant_name, plant, " - ", seg))
        end
    end

    filename_price = string("agn_", agent_id, "_offer_", plant_type, "_price_ref")
    filename_qtd = string("agn_", agent_id, "_offer_", plant_type, "_qtd_ref")

    if prb.options.outBin
        PSRIOGrafResultBinary_initSave(iografoffer_price, joinpath(PATH, string(filename_price, ".hdr")), joinpath(PATH, string(filename_price, ".bin")))
        PSRIOGrafResultBinary_initSave(iografoffer_qtd, joinpath(PATH, string(filename_qtd, ".hdr")), joinpath(PATH, string(filename_qtd, ".bin")))
    else
        PSRIOGrafResult_initSave(iografoffer_price, joinpath(PATH, string(filename_price, ".csv")), PSRIO_GRAF_FORMAT_DEFAULT)
        PSRIOGrafResult_initSave(iografoffer_qtd, joinpath(PATH, string(filename_qtd, ".csv")), PSRIO_GRAF_FORMAT_DEFAULT)
    end

    for s = 1:n.Scenarios

        if plant_type == "hyd"
        
            for (index, plant) in enumerate(vec_plants)
                for seg in num_segments[plant]
                    PSRIOGrafResultBase_setData(iografoffer_price, index-1, offers[plant,s][seg].price)
                    PSRIOGrafResultBase_setData(iografoffer_qtd, index-1, offers[plant,s][seg].quantity)
                end
            end
        
            PSRIOGrafResultBase_writeRegistry(iografoffer_price)
            PSRIOGrafResultBase_writeRegistry(iografoffer_qtd)
        
        else

            for h = 1:n.MaxHours
                for (index, plant) in enumerate(vec_plants)
                    for seg in num_segments[plant]
                        PSRIOGrafResultBase_setData(iografoffer_price, index-1, offers[plant,h,s][seg].price)
                        PSRIOGrafResultBase_setData(iografoffer_qtd, index-1, offers[plant,h,s][seg].quantity)
                    end
                end
    
                PSRIOGrafResultBase_writeRegistry(iografoffer_price)
                PSRIOGrafResultBase_writeRegistry(iografoffer_qtd)
            end

        end
    end
    

    if prb.options.outBin
        PSRIOGrafResultBinary_closeSave(iografoffer_price)
        PSRIOGrafResultBinary_closeSave(iografoffer_qtd)
    else
        PSRIOGrafResult_closeSave(iografoffer_price)
        PSRIOGrafResult_closeSave(iografoffer_qtd)
    end

end

"""
    Read an offer file
"""
function read_offer_file!(prb::BidDispatchProblem, PATH_OFFERS::String, filename::String, etapa::Int64, is_ref_file::Bool=false)
    d = prb.data
    n = prb.n
    options = prb.options

    path = joinpath(PATH_OFFERS, filename)
    if !isfile(path)
        error("Arquivo $filename não encontrado.")
    end

    ptr_pointer = PSRIOGrafResult_create(0)
    PSRIOGrafResult_initLoad( ptr_pointer, path, PSRIO_GRAF_FORMAT_EXTENDED )

    n_etapas = PSRIOGrafResultBase_getTotalStages( ptr_pointer )
    n_series = PSRIOGrafResultBase_getTotalSeries( ptr_pointer )
    n_blocos = PSRIOGrafResultBase_getTotalBlocks( ptr_pointer )
    n_agentes = PSRIOGrafResultBase_maxAgent( ptr_pointer )

    etapa_inicial_arquivo = PSRIOGrafResultBase_getInitialStage(ptr_pointer)
    ano_inicial_arquivo = PSRIOGrafResultBase_getInitialYear(ptr_pointer)

    etapa += (options.firstWeek - etapa_inicial_arquivo) + 365 * (options.firstYear - ano_inicial_arquivo)

    if etapa <= n_etapas && etapa >= 1
        PSRIOGrafResultBase_seekStage( ptr_pointer, etapa )
    else
        error("Erro ao tentar ler etapa $etapa do arquivo $filename.")
    end

    agentes = Array{String, 1}(undef, n_agentes)

    for i in 1:n_agentes
        agentes[i] = PSRIOGrafResultBase_getAgent( ptr_pointer, i-1 )
    end

    valores = zeros(n_agentes, n_blocos, n_series)
    ignore_data = false

    for serie in 1:n_series
        for bloco in 1:n_blocos
            
            PSRIOGrafResultBase_nextRegistry( ptr_pointer,ignore_data )

            for agente in 1:n_agentes

                valores[agente, bloco, serie] = PSRIOGrafResultBase_getData( ptr_pointer, agente-1 )

            end

        end
    end

    PSRIOGrafResultBase_closeLoad( ptr_pointer )
    
    if !is_ref_file
        if n_series == 1
            valores = repeat(valores, 1, 1, n.Scenarios)
        elseif n_series >= n.Scenarios
            valores = valores[:, :, 1:n.Scenarios]
        else
            error("O arquivo $(filename) possui $(n_series) cenarios, o caso possui $(n.Scenarios)")
        end
    end

    return agentes, valores, n_series, n_blocos

end

"""
    Map hydro offers
"""
function map_hydro_sale_offer!(prb::BidDispatchProblem, PATH_OFFERS::String, etapa::Int64, file::String)

    d = prb.data
    n = prb.n

    # parse file name
    file_elements = split(file, "_")
    agent_id = parse(Int32, file_elements[2])

    qtd_file = file
    price_file = replace(file, "qtd"=>"price")

    # read files
    agentes_qtd, offer_qtd, n_series_qtd, n_blocos_qtd = read_offer_file!(prb, PATH_OFFERS, qtd_file, etapa)
    agentes_price, offer_price, n_series_price, n_blocos_price = read_offer_file!(prb, PATH_OFFERS, price_file, etapa)

    # validate files
    if n_series_qtd != n_series_price
        @warn "Arquivos $qtd_file e $price_file possuem números de cenários diferentes."
        error()
    end
    if n_blocos_qtd != n_blocos_price
        @warn "Arquivos $qtd_file e $price_file possuem números de blocos diferentes."
        error()
    end
    if length(agentes_qtd) != length(agentes_price)
        @warn "Arquivos $qtd_file e $price_file possuem números de ofertas diferentes."
        error()
    end
    for agente in agentes_qtd
        if !(agente in agentes_price)
            @warn "Arquivos $qtd_file e $price_file possuem cabeçalhos diferentes."
            error()
        end
    end

    # if qtd and price headers are not in the same order
    if agentes_qtd != agentes_price
        agent_indexes = []
        for agente in agentes_qtd
            agent_index = findfirst(x -> x == agente, agentes_price)
            append!(agent_indexes, agent_index)
        end
        offer_price = offer_price[agent_indexes]
    end

    # parse header
    vr_ids, segment_ids = parse_offer_header(agentes_qtd)
    unique_vr_ids = unique(vr_ids)
    num_segments = Dict([(i,count(x->x==i,vr_ids)) for i in unique_vr_ids])

    # allocate offer vectors
    for vr in unique_vr_ids
        index = findfirst(x->x==vr, d.vrCode)
        if isnothing(index)
            @warn "Reservatório virtual $vr nos arquivos $qtd_file e $price_file não existe."
            error()
        end
        if d.vr2agn[index] != agent_id
            @warn "Reservatório virtual $vr nos arquivos $qtd_file e $price_file não pertence ao agente $agent_id."
            error()
        end
        for scen = 1:n_series_qtd
            d.sale_offer_hyd[index, scen] = Vector{Offer}(undef, num_segments[vr])
        end
    end

    # reshape offers
    header_length = length(vr_ids)
    offer_qtd = [offer_qtd[i, :, :] for i in 1:header_length]
    offer_price = [offer_price[i, :, :] for i in 1:header_length]

    # map offers
    for (vr_id, segment_id, qtd, price) in zip(vr_ids, segment_ids, offer_qtd, offer_price)
        vr_index = index = findfirst(x->x==vr_id, d.vrCode)
        for scen = 1:n_series_qtd
            d.sale_offer_hyd[vr_index, scen][segment_id] = Offer(qtd[1, scen], price[1, scen], segment_id)
        end
    end

    return nothing

end

"""
    Map thermal offers
"""
function map_thermal_offer!(prb::BidDispatchProblem, PATH_OFFERS::String, etapa::Int64, file::String)

    d = prb.data
    n = prb.n

    # parse file name
    file_elements = split(file, "_")
    agent_id = parse(Int32, file_elements[2])

    qtd_file = file
    price_file = replace(file, "qtd"=>"price")

    # read files
    agentes_qtd, offer_qtd, n_series_qtd, n_blocos_qtd = read_offer_file!(prb, PATH_OFFERS, qtd_file, etapa)
    agentes_price, offer_price, n_series_price, n_blocos_price = read_offer_file!(prb, PATH_OFFERS, price_file, etapa)

    # validate files
    if n_series_qtd != n_series_price
        @warn "Arquivos $qtd_file e $price_file possuem números de cenários diferentes."
        error()
    end
    if n_blocos_qtd != n_blocos_price
        @warn "Arquivos $qtd_file e $price_file possuem números de blocos diferentes."
        error()
    end
    if length(agentes_qtd) != length(agentes_price)
        @warn "Arquivos $qtd_file e $price_file possuem números de ofertas diferentes."
        error()
    end
    for agente in agentes_qtd
        if !(agente in agentes_price)
            @warn "Arquivos $qtd_file e $price_file possuem cabeçalhos diferentes."
            error()
        end
    end

    # if qtd and price headers are not in the same order
    if agentes_qtd != agentes_price
        agent_indexes = []
        for agente in agentes_qtd
            agent_index = findfirst(x -> x == agente, agentes_price)
            append!(agent_indexes, agent_index)
        end
        offer_price = offer_price[agent_indexes]
    end

    # parse header
    ter_ids, segment_ids = parse_offer_header(agentes_qtd)
    unique_ter_ids = unique(ter_ids)
    num_segments = Dict([(i,count(x->x==i,ter_ids)) for i in unique_ter_ids])

    # allocate offer vectors
    for ter in unique_ter_ids
        index = findfirst(x->x==ter, d.tCode)
        if isnothing(index)
            @warn "Usina $ter nos arquivos $qtd_file e $price_file não existe."
            error()
        end
        if d.ter2agn[index] != agent_id
            @warn "Usina $ter nos arquivos $qtd_file e $price_file não pertence ao agente $agent_id."
            error()
        end
        for scen = 1:n_series_qtd, bloco = 1:n_blocos_qtd
            d.sale_offer_ter[index, bloco, scen] = Vector{Offer}(undef, num_segments[ter])
        end
    end

    # reshape offers
    header_length = length(ter_ids)
    offer_qtd = [offer_qtd[i, :, :] for i in 1:header_length]
    offer_price = [offer_price[i, :, :] for i in 1:header_length]

    # map offers
    for (ter_id, segment_id, qtd, price) in zip(ter_ids, segment_ids, offer_qtd, offer_price)
        ter_index = index = findfirst(x->x==ter_id, d.tCode)
        for scen = 1:n_series_qtd, bloco = 1:n_blocos_qtd
            d.sale_offer_ter[ter_index, bloco, scen][segment_id] = Offer(qtd[bloco, scen], price[bloco, scen], segment_id)
        end
    end

    return nothing

end

"""
    Map renewable offers
"""
function map_gnd_offer!(prb::BidDispatchProblem, PATH_OFFERS::String, etapa::Int64, file::String)

    d = prb.data
    n = prb.n

    # parse file name
    file_elements = split(file, "_")
    agent_id = parse(Int32, file_elements[2])

    qtd_file = file
    price_file = replace(file, "qtd"=>"price")

    # read files
    agentes_qtd, offer_qtd, n_series_qtd, n_blocos_qtd = read_offer_file!(prb, PATH_OFFERS, qtd_file, etapa)
    agentes_price, offer_price, n_series_price, n_blocos_price = read_offer_file!(prb, PATH_OFFERS, price_file, etapa)

    # validate files
    if n_series_qtd != n_series_price
        @warn "Arquivos $qtd_file e $price_file possuem números de cenários diferentes."
        error()
    end
    if n_blocos_qtd != n_blocos_price
        @warn "Arquivos $qtd_file e $price_file possuem números de blocos diferentes."
        error()
    end
    if length(agentes_qtd) != length(agentes_price)
        @warn "Arquivos $qtd_file e $price_file possuem números de ofertas diferentes."
        error()
    end
    for agente in agentes_qtd
        if !(agente in agentes_price)
            @warn "Arquivos $qtd_file e $price_file possuem cabeçalhos diferentes."
            error()
        end
    end

    # if qtd and price headers are not in the same order
    if agentes_qtd != agentes_price
        agent_indexes = []
        for agente in agentes_qtd
            agent_index = findfirst(x -> x == agente, agentes_price)
            append!(agent_indexes, agent_index)
        end
        offer_price = offer_price[agent_indexes]
    end

    # parse header
    gnd_ids, segment_ids = parse_offer_header(agentes_qtd)
    unique_gnd_ids = unique(gnd_ids)
    num_segments = Dict([(i,count(x->x==i,gnd_ids)) for i in unique_gnd_ids])

    # allocate offer vectors
    for gnd in unique_gnd_ids
        index = findfirst(x->x==gnd, d.gCode)
        if isnothing(index)
            @warn "Usina $gnd nos arquivos $qtd_file e $price_file não existe."
            error()
        end
        if d.gnd2agn[index] != agent_id
            @warn "Usina $gnd nos arquivos $qtd_file e $price_file não pertence ao agente $agent_id."
            error()
        end
        for scen = 1:n_series_qtd, bloco = 1:n_blocos_qtd
            d.sale_offer_gnd[index, bloco, scen] = Vector{Offer}(undef, num_segments[gnd])
        end
    end

    # reshape offers
    header_length = length(gnd_ids)
    offer_qtd = [offer_qtd[i, :, :] for i in 1:header_length]
    offer_price = [offer_price[i, :, :] for i in 1:header_length]

    # map offers
    for (gnd_id, segment_id, qtd, price) in zip(gnd_ids, segment_ids, offer_qtd, offer_price)
        gnd_index = index = findfirst(x->x==gnd_id, d.gCode)
        for scen = 1:n_series_qtd, bloco = 1:n_blocos_qtd
            d.sale_offer_gnd[gnd_index, bloco, scen][segment_id] = Offer(qtd[bloco, scen], price[bloco, scen], segment_id)
        end
    end

    return nothing

end

function map_hydro_purchase_offer!(prb::BidDispatchProblem, PATH_OFFERS::String, etapa::Int64, file::String)

    d = prb.data
    n = prb.n

    # parse file name
    file_elements = split(file, "_")
    agent_id = parse(Int32, file_elements[2])

    qtd_file = file
    price_file = replace(file, "qtd"=>"price")

    # read files
    agentes_qtd, offer_qtd, n_series_qtd, n_blocos_qtd = read_offer_file!(prb, PATH_OFFERS, qtd_file, etapa)
    agentes_price, offer_price, n_series_price, n_blocos_price = read_offer_file!(prb, PATH_OFFERS, price_file, etapa)

    # validate files
    if n_series_qtd != n_series_price
        @warn "Arquivos $qtd_file e $price_file possuem números de cenários diferentes."
        error()
    end
    if n_blocos_qtd != n_blocos_price
        @warn "Arquivos $qtd_file e $price_file possuem números de blocos diferentes."
        error()
    end
    if length(agentes_qtd) != length(agentes_price)
        @warn "Arquivos $qtd_file e $price_file possuem números de ofertas diferentes."
        error()
    end
    for agente in agentes_qtd
        if !(agente in agentes_price)
            @warn "Arquivos $qtd_file e $price_file possuem cabeçalhos diferentes."
            error()
        end
    end

    # if qtd and price headers are not in the same order
    if agentes_qtd != agentes_price
        agent_indexes = []
        for agente in agentes_qtd
            agent_index = findfirst(x -> x == agente, agentes_price)
            append!(agent_indexes, agent_index)
        end
        offer_price = offer_price[agent_indexes]
    end

    # parse header
    vr_ids, segment_ids = parse_offer_header(agentes_qtd)
    unique_vr_ids = unique(vr_ids)
    num_segments = Dict([(i,count(x->x==i,vr_ids)) for i in unique_vr_ids])

    # allocate offer vectors
    for vr in unique_vr_ids
        index = findfirst(x->x==vr, d.vrCode)
        if isnothing(index)
            @warn "Reservatório virtual $vr nos arquivos $qtd_file e $price_file não existe."
            error()
        end
        if d.vr2agn[index] != agent_id
            @warn "Reservatório virtual $vr nos arquivos $qtd_file e $price_file não pertence ao agente $agent_id."
            error()
        end
        for scen = 1:n_series_qtd
            d.purchase_offer_hyd[index, scen] = Vector{Offer}(undef, num_segments[vr])
        end
    end

    # reshape offers
    header_length = length(vr_ids)
    offer_qtd = [offer_qtd[i, :, :] for i in 1:header_length]
    offer_price = [offer_price[i, :, :] for i in 1:header_length]

    # map offers
    for (vr_id, segment_id, qtd, price) in zip(vr_ids, segment_ids, offer_qtd, offer_price)
        vr_index = index = findfirst(x->x==vr_id, d.vrCode)
        for scen = 1:n_series_qtd
            d.purchase_offer_hyd[vr_index, scen][segment_id] = Offer(qtd[1, scen], price[1, scen], segment_id)
        end
    end

    return nothing

end

"""
    Map demand offers
"""
function map_dem_offer!(prb::BidDispatchProblem, PATH_OFFERS::String, etapa::Int64, file::String)

    d = prb.data
    n = prb.n

    # parse file name
    file_elements = split(file, "_")
    agent_id = parse(Int32, file_elements[2])

    # read files
    agentes, offers, n_series, n_blocos = read_offer_file!(prb, PATH_OFFERS, file, etapa)

    # parse header
    dem_ids = parse_offer_header_without_segments(agentes)

    # allocate offer vectors
    for dem in dem_ids
        index = findfirst(x->x==dem, d.dCode)
        if isnothing(index)
            error("Demanda $dem no arquivo $file não existe.")
        end
        if d.dem2agn[index] != agent_id
            error("Demanda $dem no arquivo $file não pertence ao agente $agent_id.")
        end
    end

    # map offers
    for dem_id in dem_ids
        dem_index = findfirst(x->x==dem_id, d.dCode)
        dem_index_graf = findfirst(x->x==dem_id, dem_ids)
        for scen = 1:n_series, bloco = 1:n_blocos
            d.current_demand.valores[1, scen, bloco, dem_index] = offers[dem_index_graf, bloco, scen]
        end
    end

    return nothing

end

"""
    Read sale offers from files
"""
function read_offers!(prb::BidDispatchProblem, PATH_OFFERS::String, etapa::Int64)

    d = prb.data
    n = prb.n

    # list wizard ids
    agent_ids_hyd = d.aId[d.aWizardIDHyd.==0]
    agent_ids_ter = d.aId[d.aWizardIDTer.==0]
    agent_ids_gnd = d.aId[d.aWizardIDGnd.==0]
    agent_ids_dem = d.aId[d.aWizardIDDem.==0]

    # list offer files
    hyd_sale_offer_files = ["agn_$(agent_id)_sale_offer_hyd_qtd.csv" for agent_id in agent_ids_hyd]
    hyd_purchase_offer_files = ["agn_$(agent_id)_purchase_offer_hyd_qtd.csv" for agent_id in agent_ids_hyd]
    ter_offer_files = ["agn_$(agent_id)_sale_offer_ter_qtd.csv" for agent_id in agent_ids_ter]
    gnd_offer_files = ["agn_$(agent_id)_sale_offer_gnd_qtd.csv" for agent_id in agent_ids_gnd]
    dem_offer_files = ["agn_$(agent_id)_demand_offer.csv" for agent_id in agent_ids_dem]

    # iterate hydro sale files
    for file in hyd_sale_offer_files
        try
            map_hydro_sale_offer!(prb, PATH_OFFERS, etapa, file)
        catch
            price_file = replace(file, "qtd"=>"price")
            println("Problema na leitura dos arquivos $file e $price_file. Arquivo default será utilizado.")
            map_hydro_sale_offer!(prb, PATH_OFFERS, etapa, replace(file, ".csv"=>"_default.csv"))
        end
    end

    # iterate hydro purchase files
    if prb.options.UsePurchaseVRFiles
        for file in hyd_purchase_offer_files
            try
                map_hydro_purchase_offer!(prb, PATH_OFFERS, etapa, file)
            catch
                price_file = replace(file, "qtd"=>"price")
                println("Problema na leitura dos arquivos $file e $price_file. Valor default (zero) será utilizado.")
            end
        end
    end

    # iterate thermal files
    for file in ter_offer_files
        try
            map_thermal_offer!(prb, PATH_OFFERS, etapa, file)
        catch
            price_file = replace(file, "qtd"=>"price")
            println("Problema na leitura dos arquivos $file e $price_file. Arquivo default será utilizado.")
            map_thermal_offer!(prb, PATH_OFFERS, etapa, replace(file, ".csv"=>"_default.csv"))
        end
    end

    # iterate renewable files
    for file in gnd_offer_files
        try
            map_gnd_offer!(prb, PATH_OFFERS, etapa, file)
        catch
            price_file = replace(file, "qtd"=>"price")
            println("Problema na leitura dos arquivos $file e $price_file. Arquivo default será utilizado.")
            map_gnd_offer!(prb, PATH_OFFERS, etapa, replace(file, ".csv"=>"_default.csv"))
        end
    end

    # iterate demand files
    for file in dem_offer_files
        # try
            map_dem_offer!(prb, PATH_OFFERS, etapa, file)
        # catch
        #     println("Problema na leitura do arquivo $file. Arquivo default será utilizado.")
        # end
    end

    d.sale_offer_hyd_num_segments = maximum(length.(d.sale_offer_hyd), dims = 2)[:,1]
    d.purchase_offer_hyd_num_segments = maximum(length.(d.purchase_offer_hyd), dims = 2)[:,1]
    d.sale_offer_ter_num_segments = maximum(length.(d.sale_offer_ter), dims = (2,3))[:,1,1]
    d.sale_offer_gnd_num_segments = maximum(length.(d.sale_offer_gnd), dims = (2,3))[:,1,1]

end


"""
    Pad offers with extra segments
"""
function pad_offers!(prb::BidDispatchProblem)

    d = prb.data
    n = prb.n

    n.OfferSegments = maximum(cat(d.sale_offer_hyd_num_segments, d.sale_offer_ter_num_segments, d.sale_offer_gnd_num_segments,d.purchase_offer_hyd_num_segments, dims=1))

    vec_offers = [d.sale_offer_ter, d.sale_offer_hyd, d.sale_offer_gnd,d.purchase_offer_hyd]

    for offer in vec_offers
        for (vec_id, offer_vec) in enumerate(offer)

            next_missing_segment = length(offer_vec)+1

            for segment_id in next_missing_segment:n.OfferSegments

                push!(offer[vec_id], Offer(0.0, 0.0, segment_id))
            end

        end
    end

end

function read_ref_offers!(prb::BidDispatchProblem, PATH_OFFERS::String, etapa::Int64)

    d = prb.data
    n = prb.n

    agentes_qtd, offer_qtd = read_offer_file!(prb, PATH_OFFERS, "ref_offer_qtd.csv", etapa, true)
    agentes_price, offer_price = read_offer_file!(prb, PATH_OFFERS, "ref_offer_price.csv", etapa, true)

    sys_ids_qtd, segment_ids_qtd = parse_offer_header(agentes_qtd)
    sys_ids_price, segment_ids_price = parse_offer_header(agentes_price)

    if sys_ids_qtd != sys_ids_price || segment_ids_qtd != segment_ids_price
        error("Arquivos de referência de preço e quantidade de ofertas não estão compatíveis.")
    end

    d.sale_offer_hyd_ref = Array{Vector{Offer}, 1}(undef, n.Sys)
    
    for sys_id in unique(sys_ids_qtd)

        indexes = (sys_ids_qtd .== sys_id)

        d.sale_offer_hyd_ref[sys_id] = Offer.(offer_qtd[indexes], offer_price[indexes], segment_ids_price[indexes])

    end

    nothing

end

function parse_offer_header(names::Vector{String})

    sys_ids = Int32[]
    segment_ids = Int32[]

    for name in names
        arr = split(name, " - ")
        segment_id = parse(Int64, arr[2])
        arr2 = split(arr[1], " ")
        sys_id = parse(Int64, arr2[2])

        append!(sys_ids, sys_id)
        append!(segment_ids, segment_id)
    end

    return sys_ids, segment_ids

end

function parse_offer_header_without_segments(names::Vector{String})

    sys_ids = Int32[]

    for name in names
        arr = split(name, " ")
        sys_id = parse(Int64, arr[2])

        append!(sys_ids, sys_id)
    end

    return sys_ids

end

function create_hydro_offers_with_ref(prb::BidDispatchProblem)

    d = prb.data
    n = prb.n
    options = prb.options

    vec_offers = Array{Vector{Offer}, 2}(undef, n.VirtualReservoirs, n.Scenarios)

    for vr = 1:n.VirtualReservoirs
        
        rgn = d.vr2rgn[vr]
        agent_id = d.vr2agn[vr]
        sys = d.rgn2sys[rgn]

        if d.aWizardIDHyd[agent_id] != 2
            vec_offers[vr, :] = d.sale_offer_hyd[vr, :]
            continue
        end

        offer_ref = d.sale_offer_hyd_ref[sys]

        d.sale_offer_hyd_num_segments[vr] = length(offer_ref)
        theta = d.aRiskFactorHyd[agent_id] # escalar

        GF = d.hGF[vr] # GF de "vr"
        GF_total = sum(d.hGF[d.vr2rgn.==rgn]) # GF total da regiao "rgn"
        energy_share = GF/GF_total # parcela da energia da regiao "rgn" atribuida a "vr"

        offer_price = [x.price for x in offer_ref] * (1+theta)
        offer_qtd_ref = [x.quantity for x in offer_ref] * energy_share

        for serie in 1:n.Scenarios

            vec_offers[vr, serie] = Vector{Offer}(undef, length(offer_qtd_ref))

            offer_max = d.aBudgets[vr, serie].energy # escalar

            for (segment_id, qtd) in enumerate(offer_qtd_ref)
                if offer_max > qtd
                    offer = Offer(qtd, offer_price[segment_id], segment_id)
                    vec_offers[vr, serie][segment_id] = offer
                    offer_max -= qtd
                else
                    offer = Offer(offer_max, offer_price[segment_id], segment_id)
                    vec_offers[vr, serie][segment_id] = offer
                    offer_max = 0
                end
            end
            
        end
    end

    return vec_offers

end