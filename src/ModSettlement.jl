function update_profit!(prb, m, etapa, dia)
    d = prb.data
    d.profit_hyd = get_hydro_profit(prb, m)
    d.profit_ter = get_thermal_profit(prb, m, etapa)
    d.profit_gnd = get_renewable_profit(prb, etapa, dia)
    # profit_contract = get_contract_profit(prb, dia)

    return d.profit_hyd, d.profit_ter, d.profit_gnd #, profit_contract
end

function get_hydro_profit(prb, m)

    d = prb.data
    n = prb.n

    lucros_hid = zeros(n.Agents, n.Scenarios)

    if n.Hydros > 0
        soldVR = JuMP.value.(m[:soldVR])
        boughtVR = JuMP.value.(m[:boughtVR])
        preco_spot = mean(d.preco_spot_dia,dims=1)[1,:,:]

        for vr in 1:n.VirtualReservoirs, s in 1:n.Scenarios
        
            custo_vr = d.custo_agua_vr[vr, s, :]
            gerhid = sum((soldVR - boughtVR)[:,:,:,seg] for seg in 1:n.OfferSegments)
            gerhid_vr = gerhid[vr, :, s]

            lucro_horario = (preco_spot[:, s] .- custo_vr) .* gerhid_vr
            lucro_vr = sum(lucro_horario)

            agn = d.vr2agn[vr]
            lucros_hid[agn, s] += lucro_vr
        end
    end

    return lucros_hid

end

function get_thermal_profit(prb, m, etapa)

    d = prb.data
    n = prb.n

    lucros_ter = zeros(n.Agents, n.Scenarios)

    if n.Thermals > 0
        gerter = JuMP.value.(m[:TGen]).data
        preco_spot = mean(d.preco_spot_dia,dims=1)[1,:,:]

        for j in 1:n.Thermals, s in 1:n.Scenarios

            sys = d.ter2sys[j]
        
            custo_usina = d.tCVU[j]
            gerter_usina = gerter[j, :, s]

            COMT_usina = d.dailyCOMT[j, :, s]
            custo_COMT_usina = d.tShutDownCost[j]

            lucro_horario = (preco_spot[:, s] .- custo_usina) .* gerter_usina - COMT_usina.*custo_COMT_usina
            lucro_usina = sum(lucro_horario)

            agn = d.ter2agn[j]
            lucros_ter[agn, s] += lucro_usina
        end
    end

    return lucros_ter

end

function get_renewable_profit(prb, etapa, dia)
    d = prb.data
    n = prb.n

    lucros_gnd = zeros(n.Agents, n.Scenarios)

    if n.Gnd > 0
        horas = (dia-1)*n.MaxHours + 1 : (dia)*n.MaxHours
        gergnd = d.gergnd.valores[1, :, horas, :]
        preco_spot = mean(d.preco_spot_dia,dims=1)[1,:,:]

        for j in 1:n.Gnd, s in 1:n.Scenarios
            sys = d.gnd2sys[j]
        
            gergnd_usina = gergnd[s, :, j]

            lucro_horario = preco_spot[:, s] .* gergnd_usina
            lucro_usina = sum(lucro_horario)

            agn = d.gnd2agn[j]
            lucros_gnd[agn, s] += lucro_usina
        end
    end

    return lucros_gnd

end

function get_contract_profit(prb, dia)
    d = prb.data
    n = prb.n

    lucros_contratos = zeros(n.Agents)
    contratos = d.aContracts[:, dia]
    preco_spot = mean(d.preco_spot_dia,dims=1)[1,:,:]

    for agn in 1:n.Agents, contrato in contratos[agn], s in 1:n.Scenarios

        preco_contrato = contrato.price
        qtd_contrato = contrato.quantity

        lucro_horario = (preco_contrato .- preco_spot[:, s]) .* qtd_contrato
        lucro_contrato = sum(lucro_horario)

        lucros_contratos[agn] += lucro_contrato
    end

    return lucros_contratos

end