"""
    Methodos for creation of standard variables
"""

function var_werrorlin!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    if n.Hydros>0
        @variable(m, WlinE[r=1:length(d.reservoirs), s=1:n.ScenariosExtended])
        for r=1:length(d.reservoirs), s=1:n.Scenarios
            set_name(WlinE[r, s], "WlinE($r,$s)")
        end
    end
end

function var_network!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    if n.Circuits>0
        @variable(m, -d.circuit_Rn[k] <= Flow[k=1:n.Circuits, h=1:n.FullBuffer, s=1:n.Scenarios] <= d.circuit_Rn[k])
        for k=1:n.Circuits, h=1:n.FullBuffer, s=1:n.Scenarios
            set_name(Flow[k, h, s], "Flow($k,$h,$s)")
        end
        @variable(m, Angle[b=1:n.Buses, h=1:n.FullBuffer, s=1:n.Scenarios])
        for b=1:n.Buses, h=1:n.FullBuffer, s=1:n.Scenarios
            set_name(Angle[b, h, s], "Angle($b,$h,$s)")
        end
        @variable(m, inj_bus[b=1:n.Buses, h=1:n.FullBuffer, s=1:n.Scenarios])
        for b=1:n.Buses, h=1:n.FullBuffer, s=1:n.Scenarios
            set_name(inj_bus[b, h, s], "GerBus($b,$h,$s)")
        end
    end
end

# system deficit slack variable
function var_deficit!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options

    buses = 1
    if options.network == inter::ConsiderNetwork
        buses = n.Sys
    elseif  options.network == network::ConsiderNetwork
        buses = n.Buses
    end

    @variable(m, def[1:buses, 1:prb.n.FullBuffer, 1:(n.Scenarios)]>=0)
    for bus=1:buses, hour=1:prb.n.FullBuffer, scen=1:(n.Scenarios)
        set_name(def[bus, hour, scen], "defct($bus,$hour,$scen)")
    end
    nothing
end
function var_interc!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options
    if n.Intercs > 0 && options.network == inter::ConsiderNetwork
        @variable(m, IntercTo[1:n.Intercs, 1:prb.n.FullBuffer, 1:n.Scenarios])
        @variable(m, IntercFrom[1:n.Intercs, 1:prb.n.FullBuffer, 1:n.Scenarios])
        for h in 1:prb.n.FullBuffer, i in 1:n.Intercs, s in 1:n.Scenarios
            set_name(IntercTo[i, h, s], "IntercTo($i,$h,$s)")
            set_name(IntercFrom[i, h, s], "IntercFrom($i,$h,$s)")
            set_lower_bound(IntercTo[i, h, s], 0.0)
            set_upper_bound(IntercTo[i, h, s], d.iCapacityTo[i])
            set_lower_bound(IntercFrom[i, h, s], 0.0)
            set_upper_bound(IntercFrom[i, h, s], d.iCapacityFrom[i])
        end
    end
    nothing
end
function var_dclinks(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options
    
    if n.DCLinks > 0 && options.network == network::ConsiderNetwork
        @variable(m, DCTo[1:n.DCLinks, 1:prb.n.FullBuffer, 1:n.Scenarios])
        @variable(m, DCF[1:n.DCLinks, 1:prb.n.FullBuffer, 1:n.Scenarios])
        for s in 1:n.Scenarios, h in 1:prb.n.FullBuffer, i in 1:n.DCLinks
            set_lower_bound(DCTo[i, h, s], 0.0)
            set_upper_bound(DCTo[i, h, s], d.dCapacityTo[i])
            set_lower_bound(DCF[i, h, s], 0.0)
            set_upper_bound(DCF[i, h, s], d.dCapacityFrom[i])
        end
    end
    nothing
end
function var_thetas(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options
    if n.Buses > 0 && options.network == network::ConsiderNetwork
        @variable(m, -Inf <= theta[1:n.Buses, 1:prb.n.FullBuffer, 1:n.Scenarios] <= Inf)
    end
    nothing
end
function var_COMT(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data

    if n.Thermals>0
        @variable(m, COMT[1:length(d.tExisting), 1:prb.n.FullBuffer, 1:n.Scenarios], Bin)
        for t=1:length(d.tExisting), h=1:prb.n.FullBuffer, s=1:n.Scenarios
            set_name(COMT[t, h,s], "COMT($t,$h,$s)")
        end
    end
    nothing
end

function var_ramp_slack(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options

    if n.Thermals>0
        UB = (options.SlackRamp ? sum(d.tPotInst) : 0.0)
        @variable(m, 0.0 <= ramp_slack[1:length(d.tExisting), 1:prb.n.FullBuffer, 1:n.Scenarios] <= UB)
        for t=1:length(d.tExisting), h=1:prb.n.FullBuffer, s=1:n.Scenarios
            set_name(ramp_slack[t, h,s], "ramp_slack($t,$h,$s)")
        end
    end
    nothing
end
function var_startup(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    if n.Thermals>0
        @variable(m, 0 <= STup[1:length(d.tExisting),1:prb.n.FullBuffer,1:n.Scenarios] <= 1)
        for t=1:length(d.tExisting), h=1:prb.n.FullBuffer, s = 1:n.Scenarios
            set_name(STup[t, h,s], "STup($t,$h,$s)")
        end
    end
    nothing
end
function var_shutdown(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    if n.Thermals>0
        @variable(m, 0 <= STdn[1:length(d.tExisting), 1:prb.n.FullBuffer,1:n.Scenarios] <= 1)
        for t=1:length(d.tExisting), h=1:prb.n.FullBuffer,s = 1:n.Scenarios
            set_name(STdn[t, h,s], "STdn($t,$h,$s)")
        end
    end
    nothing
end


function var_thermalgen!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data

    if n.Thermals>0
        # Thermal genration (in MWh = MW*h)
        # ---------------------------------
        @variable(m,  0.0 <= TGen[t in d.tExisting, hour in Array(1:n.FullBuffer), s in Array(1:n.Scenarios)] <= d.tPotInst[t])
        for t in (d.tExisting), h=1:prb.n.FullBuffer, s=1:n.Scenarios
            set_name(TGen[t, h, s], "TGen($t,$h,$s)")
        end
    end
    nothing
end

function var_thermalgen_aux!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data

    if n.Thermals>0
        # Thermal genration (in MWh = MW*h)
        # ---------------------------------
        @variable(m,  0.0 <= TGenAux[t in d.tExisting, hour in Array(1:n.FullBuffer), s in Array(1:n.Scenarios), seg in Array(1:n.OfferSegments)] )

        for t in d.tExisting, h=1:prb.n.FullBuffer, s=1:n.Scenarios, seg=1:n.OfferSegments
            set_name(TGenAux[t, h, s, seg], "TGenAux($t,$h,$s,$seg)")
        end

        @constraint(m, sum(m[:TGenAux][:, :, :, seg].data for seg in 1:n.OfferSegments) .==  m[:TGen].data)
    end
    nothing
end

function var_fuelconsumptions!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options
    MAX_EFF_SEGS = maximum(d.fcs_nsegments)

    if n.FuelCons>0
        # Thermal genration (in MWh = MW*h)
        # ---------------------------------

        @variable(m,  FC[l=1:MAX_EFF_SEGS, j=1:n.FuelCons, hour=1:(prb.n.FullBuffer), s=1:n.Scenarios] >= 0.0 )
        for l=1:MAX_EFF_SEGS, j=1:n.FuelCons, h=1:(prb.n.FullBuffer), s=1:n.Scenarios
            set_name(FC[l, j, h, s], "FC($l,$j,$h,$s)")
        end
        for j in 1:n.FuelCons
            t = d.fcs2ter[j]
            if d.tExist[t] == 0
                for l in 1:MAX_EFF_SEGS
                    if l in d.fcs_nsegments[j]
                        set_upper_bound.(FC[l,j,:,:], fcssegment_ub(l, j, prb))
                    else
                        set_upper_bound.(FC[l,j,:,:], 0.0)
                    end
                end
            else
                for l in 1:d.fcs_nsegments[j]
                    set_upper_bound.(FC[l,j,:,:], 0.0)
                end
            end
        end

        # build Tgen expression with fuel consumption
        d.TGen = @expression(m, [t=1:length(d.tExisting), hour=1:n.FullBuffer, scen=1:n.ScenariosExtended], sum(FC[ l ,j , hour, scen] for j in 1:n.FuelCons, l in 1:d.fcs_nsegments[j] if d.tExisting[t] == d.fcs2ter[j]) )
    end
    nothing
end
function fcssegment_ub(l::Integer, j::Integer, prb::BidDispatchProblem)
    d = prb.data
    return max(0.0, d.tGerMax[d.fcs2ter[j]]*d.fcsSegment[l,j])
end
function fcs_ub(j::Integer, prb::BidDispatchProblem)
    d = prb.data
    return max(0.0, d.tGerMax[d.fcs2ter[j]])
end
function thermal_ub(j::Integer, prb::BidDispatchProblem)
    d = prb.data
    return max(0.0, d.tGerMax[j])
end

function var_gergnd!(m::JuMP.Model, prb::BidDispatchProblem)
    # GND with max gen representation (possible to spill) (in 1000*GWh)
    n = prb.n
    d = prb.data

    if n.Gnd>0
        @variable(m, 0.0 <= RGen[r=1:n.Gnd, hour=1:prb.n.FullBuffer, s=1:n.Scenarios] <= d.rPotInst[r])
        @variable(m, 0.0 <= RSpillPen[r=1:n.Gnd, hour=1:prb.n.FullBuffer, s=1:n.Scenarios] <= 100*d.rPotInst[r])

        for r=1:n.Gnd, hour=1:prb.n.FullBuffer, s=1:n.Scenarios
            set_name(RGen[r, hour, s], "RGen($r,$hour,$s)")
            set_name(RSpillPen[r, hour, s], "RSpillPen($r,$hour,$s)")
        end
    end
    nothing
end

function var_gergnd_aux!(m::JuMP.Model, prb::BidDispatchProblem)
    # GND with max gen representation (possible to spill) (in 1000*GWh)
    n = prb.n
    d = prb.data
    options = prb.options

    if n.Gnd>0
        @variable(m, 0.0 <= RGenAux[r=1:n.Gnd, hour=1:prb.n.FullBuffer, s=1:n.Scenarios, seg=1:n.OfferSegments])

        for r=1:n.Gnd, hour=1:prb.n.FullBuffer, s=1:n.Scenarios, seg=1:n.OfferSegments
            set_name(RGenAux[r, hour, s, seg], "RGenAux($r,$hour,$s,$seg)")
        end

        @constraint(m, sum(m[:RGenAux][:, :, :, seg] for seg in 1:n.OfferSegments) .==  m[:RGen])
    end
    nothing
end

# Turbined water (in hm3)
function var_buy_sell_VR!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options

    if n.Hydros>0

        @variable(m, 0.0 <= boughtVR[vr = 1:n.VirtualReservoirs, h = 1:n.FullBuffer, s = 1:n.Scenarios, seg = 1:n.OfferSegments] <= d.purchase_offer_hyd[vr, s][seg].quantity )
        @variable(m, 0.0 <= soldVR[vr = 1:n.VirtualReservoirs, h = 1:n.FullBuffer, s = 1:n.Scenarios, seg = 1:n.OfferSegments])
        if !options.PhysOper
            @constraint(m, [vr = 1:n.VirtualReservoirs, s = 1:n.Scenarios, h = 1:n.FullBuffer], sum(m[:soldVR][vr, h, s, :]) <=  d.aBudgets[vr, s].power)
        end
        for vr = 1:n.VirtualReservoirs, h = 1:n.FullBuffer, s = 1:n.Scenarios, seg = 1:n.OfferSegments
            set_name(boughtVR[vr,h, s, seg], "boughtVR($vr,$h,$s,$seg)")
            set_name(soldVR[vr,h, s, seg], "soldVR($vr,$h,$s,$seg)")
        end
    end
end

# Turbined water (in hm3)
function var_turbining!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data

    if n.Hydros>0
        # Turbined water (in hm3)

        turb_max = d.defmax
        for h in 1:n.Hydros
            if prb.data.fprodt[h] > 0
                turb_max[h] =  min(d.hPotInst[h]/prb.data.fprodt[h], d.defmax[h])
            end
        end

        @variable(m, 0.0 <= HTurb[h=1:n.Hydros, hour=1:n.FullBuffer, 1:n.Scenarios] <= turb_max[h] )
        @variable(m, 0.0 <= spillage[h=1:n.Hydros, hour=1:n.FullBuffer, 1:n.Scenarios])
        @variable(m, 0.0 <= ForcedSpillage[h=1:n.Hydros, hour=1:n.FullBuffer, 1:n.Scenarios])
        for h=1:n.Hydros, hour=1:(n.FullBuffer), s=1:n.Scenarios
            set_name(HTurb[h, hour, s], "HTurb($h,$hour,$s)")
            set_name(spillage[h, hour, s], "Hspil($h,$hour,$s)")
            set_name(ForcedSpillage[h, hour, s], "HForcSpil($h,$hour,$s)")
        end

        @variable(m, 0.0 <= WaterIn[h=1:n.StorageRegions, hour=1:n.FullBuffer, 1:n.Scenarios])
        @variable(m, 0.0 <= WaterOut[h=1:n.StorageRegions, hour=1:n.FullBuffer, 1:n.Scenarios])
        for rgn=1:n.StorageRegions, hour=1:(n.FullBuffer), s=1:n.Scenarios
            set_name(WaterIn[rgn, hour, s], "WaterIn($rgn,$hour,$s)")
            set_name(WaterOut[rgn, hour, s], "WaterOut($rgn,$hour,$s)")
        end
    end
end

function var_spillage_control!(m::JuMP.Model, prb::BidDispatchProblem)
    
    n = prb.n
    d = prb.data

    if n.Hydros>0
        @variable(m, SpillControl[i in 1:length(d.reservoirs), s=1:n.Scenarios; d.upstream_vol[d.reservoirs[i],s] >= d.volmax[d.reservoirs[i]]] , Bin)
        for (i,h) in enumerate(d.reservoirs), s=1:n.Scenarios
            if d.upstream_vol[h,s] >= d.volmax[h]
                set_name(SpillControl[i, s], "SpillControl($i,$s)")
            end
        end
    end

    nothing
end

function var_hydro_vol!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data

    if n.Hydros>0
        # Turbined water (in hm3)
        @variable(m, d.volmin[d.reservoirs[i]] <= HydroVol[i=1:length(d.reservoirs), hour=1:n.FullBuffer, s=1:n.Scenarios] <= d.volmax[d.reservoirs[i]] )
        for i=1:length(d.reservoirs), hour=1:n.FullBuffer, s=1:n.Scenarios
            set_name(HydroVol[i, hour, s], "HydroVol($i,$hour,$s)")
        end
    end
end

function var_hydro_volw!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data

    if n.Hydros>0
        # Turbined water (in hm3)
        @variable(m, d.volmin[d.reservoirs[i]] <= HydroVolw[i=1:length(d.reservoirs), s=1:n.Scenarios] <= d.volmax[d.reservoirs[i]] )
        for i=1:length(d.reservoirs), s=1:n.Scenarios
            set_name(HydroVolw[i, s], "HydroVolw($i,$s)")
        end
    end
end

function var_waveguide_aux!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data

    if n.Hydros>0

        HydroVolw = m[:HydroVolw]
        HydroVol = m[:HydroVol]
        
        # Turbined water (in hm3)
        @variable(m, WaveguideAux[i=1:length(d.reservoirs), s=1:n.Scenarios])
        for i=1:length(d.reservoirs), s=1:n.Scenarios
            set_name(WaveguideAux[i, s], "WaveguideAux($i,$s)")
        end
        @constraint(m, m[:WaveguideAux] .==  HydroVolw - HydroVol[:,n.FullBuffer,:])
    end
end

function var_lambda!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n

    if n.Hydros>0
        @variable(m, 0 <= Lambda[i=1:n.wstage,s=1:n.Scenarios])
        for i=1:n.wstage, s=1:n.Scenarios
            set_name(Lambda[i,s], "Lambda($i,$s)")
        end
        for s=1:n.Scenarios
            convex = @constraint(m, sum(Lambda[i,s] for i in 1:n.wstage) == 1)
            set_name(convex, "convex($s)")
        end
    end
end

function var_outflow!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n

    if n.Hydros>0
        # Turbined water (in hm3)
        @variable(m, def_slack[1:n.Hydros, 1:n.Scenarios] >= 0.0)
        for h=1:n.Hydros, s=1:n.Scenarios
            set_name(def_slack[h, s], "outslk($h,$s)")
        end
    end
end
