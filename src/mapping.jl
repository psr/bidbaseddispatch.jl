#
# Mapping
# -------
function map_problem(prb::BidDispatchProblem)
    psrclasses_mapping!(prb)
    nothing
end

function load_exec_options(PATH_CASE::String, button::Int32)
    prb = BidDispatchProblem()
    prb.data.logstream = open(joinpath(PATH_CASE, "dispatch.log"), "w")
    prb.options.INPUTPATH = PATH_CASE
    prb.options.button = button
    prb.options.OUTPUTPATH = joinpath(PATH_CASE, "outputs\\")
    prb.options.PSRCPATH = PATH_PSRC
    prb.options.IHMPATH = PATH_IHM
    IO_header(prb)
    IO_running_path(prb, PATH_CASE)

    return prb
end
#
# PSRClasses Mapping
#
# -------------------

function psrclasses_mapping!(prb::BidDispatchProblem)
    initialize!(prb)
    load_interface!(prb)
    IO_loading_data(prb)
    load_problem(prb)
end

function initialize!(prb::BidDispatchProblem)
    options = prb.options
    d = prb.data
    psrc = prb.psrc

    psrc.study_ptr, psrc.iosddp = PSRCinit(options)
    nothing
end
