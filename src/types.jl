#
# Helper macro
# ------------

"""
@kwdef mutable structdef
This is a helper macro that automatically defines a keyword-based constructor for the type
declared in the expression `typedef`, which must be a `struct` or `mutable struct`
expression. The default argument is supplied by declaring fields of the form `field::T =
default`. If no default is provided then the default is provided by the `kwdef_val(T)`
function.
```julia
@kwdef struct Foo
a::Cint            # implied default Cint(0)
b::Cint = 1        # specified default
z::Cstring         # implied default Cstring(C_NULL)
y::Bar             # implied default Bar()
end
```
"""
macro kwdef(expr)
    @static if VERSION >= v"0.7"
        expr = macroexpand(__module__, expr) # to expand @static
    else
        expr = macroexpand(expr) # to expand @static
    end
    T = expr.args[2]
    params_ex = Expr(:parameters)
    call_ex = Expr(:call, T)
    _kwdef!(expr.args[3], params_ex, call_ex)
    quote
        Base.@__doc__($(esc(expr)))
        $(esc(Expr(:call,T,params_ex))) = $(esc(call_ex))
    end
end

# @kwdef helper function
# mutates arguments inplace
function _kwdef!(blk, params_ex, call_ex)
    for i in eachindex(blk.args)
        ei = blk.args[i]
        isa(ei, Expr) || continue
        if ei.head == :(=)
            # var::Typ = defexpr
            dec = ei.args[1]  # var::Typ
            var = dec.args[1] # var
            def = ei.args[2]  # defexpr
            push!(params_ex.args, Expr(:kw, var, def))
            push!(call_ex.args, var)
            blk.args[i] = dec
        elseif ei.head == :(::)
            dec = ei # var::Typ
            var = dec.args[1] # var
            def = :(kwdef_val($(ei.args[2])))
            push!(params_ex.args, Expr(:kw, var, def))
            push!(call_ex.args, dec.args[1])
        elseif ei.head == :block
            # can arise with use of @static inside type decl
            _kwdef!(ei, params_ex, call_ex)
        end
    end
    blk
end

"""
    kwdef_val(T)
The default value for a type for use with the `@kwdef` macro. Returns:
 - null pointer for pointer types (`Ptr{T}`, `Cstring`, `Cwstring`)
 - zero for integer types
 - no-argument constructor calls (e.g. `T()`) for all other types
"""
function kwdef_val end

kwdef_val(::Type{Ptr{T}}) where {T} = Ptr{T}(C_NULL)
kwdef_val(::Type{Cstring}) = Cstring(C_NULL)
kwdef_val(::Type{Cwstring}) = Cwstring(C_NULL)

kwdef_val(::Type{T}) where {T<:Real} = zero(T)

kwdef_val(::Type{T}) where {T} = T()
kwdef_val(::Type{IOStream}) = IOStream("tmp")
kwdef_val(::Type{T}) where {T<:Array{Array{Int32,1},1}} = [Int32[]]
kwdef_val(::Type{Array{Array{Int32,1},1}}) = [Int32[]]


kwdef_val(::Type{T}) where {T<:String} = ""
kwdef_val(::Type{T}) where {T<:Symbol} = :NULL

@static if VERSION >= v"0.7"
    kwdef_val(::Type{Array{T,N}}) where {T,N} = Array{T}(undef, zeros(Int,N)...)
else
    kwdef_val{T,N}(::Type{Array{T,N}})::Array{T,N} = Array{T}(tuple(zeros(Int,N)...))
end

# ModEnergyBudget

abstract type AbstractBudget end

mutable struct Budget<:AbstractBudget
    energy::Float64
    power::Float64
end

# ModWizard

abstract type AbstractOffer end

mutable struct Offer <: AbstractOffer
    quantity::Float64
    price::Float64
    segment_id::Int32
end

# ModSettlement

abstract type AbstractContract end

mutable struct Contract <: AbstractContract
    quantity::Array{Float64, 1}
    price::Array{Float64, 1}

    system_id::Int32
    id::Int32
end

# Types
const PSRClassesPtr =  Ptr{UInt8}

@kwdef mutable struct ObjectiveFunction
    hydroCosts::JuMP.GenericAffExpr{Float64,JuMP.VariableRef}
    FutureCost::JuMP.GenericAffExpr{Float64,JuMP.VariableRef}
    thermalCosts::JuMP.GenericAffExpr{Float64,JuMP.VariableRef}
    CommitmentCost::JuMP.GenericAffExpr{Float64,JuMP.VariableRef}
    RenewableCosts::JuMP.GenericAffExpr{Float64,JuMP.VariableRef}
    ReserveCosts::JuMP.GenericAffExpr{Float64,JuMP.VariableRef}
    DeficitCost::JuMP.GenericAffExpr{Float64,JuMP.VariableRef}
    PenaltySpillage::JuMP.GenericAffExpr{Float64,JuMP.VariableRef}
    PenaltyWaveGuide1::JuMP.GenericAffExpr{Float64,JuMP.VariableRef}
    PenaltyWaveGuide2::JuMP.GenericQuadExpr{Float64,JuMP.VariableRef}
end

@kwdef mutable struct PSRCGraf
    Ptr::Ptr{UInt8}
    n_etapas::Int32
    n_series::Int32
    n_blocos::Int32
    n_agentes::Int32
    etapa::Int32
    serie::Int32
    bloco::Int32
    valores::Array{Float64, 4}
end

@kwdef mutable struct BidDispatchOutput
    nCols::Int32
    nBlocks::Int32
    nSeries::Int32
    nStages::Int32
    nProblems::Int32
    data::Array{Float64, 5}
    names::Vector{String}
    filename::String
    unit::String
end

@kwdef mutable struct BidDispatchSizes

    Buffer     ::Int = 0
    FullBuffer ::Int = 24
    MaxHours   ::Int = 24 # horas/dia
    Days       ::Int = 7  # dias/semana
    Stages     ::Int
    # basic
    Hydros     ::Int
    Thermals   ::Int
    Gnd        ::Int
    GndStations::Int
    Stations   ::Int
    Scenarios  ::Int # arquivo de configuracao
    ScenariosExtended  ::Int # Scenarios + 1 (affine)
    OriginalScenarios  ::Int # numero de cenarios de vazao
    Blocks     ::Int
    Agents     ::Int
    VirtualReservoirs  ::Int
    StorageRegions  ::Int
    Loads      ::Int

    #
    Demand     ::Int
    DemSeg     ::Int
    duraci_t   ::Int

    # Thermals
    Fuels::Int
    FuelCons::Int

    # network
    Sys      ::Int
    Buses      ::Int
    DCLinks    ::Int
    Circuits    ::Int
    Areas      ::Int
    CircSumCtr ::Int
    Intercs ::Int

    # constraints
    IntercSumCstr ::Int
    GenResCstr ::Int
    GenGzdCstr ::Int
    wstage ::Int
    SpotPrice ::Int

    OfferSegments ::Int

end

@kwdef mutable struct BidDispatchData
    initial_state_commit::Array{Int,2}
    initial_state_time  ::Array{Int,2}
    initial_power       ::Array{Float64,2}
    cstrs::Dict{Any,Any}
    quantities::Dict{Any,Any}
    inpts::Dict{Any,Any}
    forecast::Array{Float64,2}
    logstream::IOStream
    mapping::Dict{Any,Any}
    hour::Int32
    scen::Int32
    tau::Array{Float64,2}
    HTurb::Array{GenericAffExpr{Float64,VariableRef},3}
    spillage::Array{GenericAffExpr{Float64,VariableRef},3}
    TGen::Array{GenericAffExpr{Float64,VariableRef},3}

    tx_discount::Float64
    vpl_coef::Float64

    # local static
    dcost::Float64 #load shedding cost
    duraci_t::Int

    # ---------------
    # Fobj
    # ---------------
    fobj::ObjectiveFunction

    preco_spot_dia::Array{Float64, 3}
    pricevr::Array{Float64, 2}
    distorcao_RT::Float64 = 0.2
    # --------------------
    # GRAF
    #---------------------
    gerhid::PSRCGraf
    gergnd::PSRCGraf
    custo_agua::PSRCGraf
    waveguide::PSRCGraf
    inflow_graf::PSRCGraf
    gergndpu::PSRCGraf
    demand_graf::PSRCGraf
    current_demand::PSRCGraf
    gergndpu_distortion::PSRCGraf
    demand_distortion::PSRCGraf
    volini_graf::PSRCGraf

    # --------------------
    # AGENT
    #---------------------
    aId::Vector{Int32}
    input_file::Vector{Bool}
    aRiskFactorHyd::Vector{Float64}
    aRiskFactorTer::Vector{Float64}
    aRiskFactorGnd::Vector{Float64}
    aRiskFactorDem::Vector{Float64}
    aWizardIDHyd::Vector{Int32}        # 0 = ler arquivo, 1 = markup simples
    aWizardIDTer::Vector{Int32}
    aWizardIDGnd::Vector{Int32}
    aWizardIDDem::Vector{Int32}
    aBudgets::Array{Budget, 2}
    aContracts::Array{Contract, 2}
    agn2hyd::Array{Array{Int32,1},1}
    agn2ter::Array{Array{Int32,1},1}
    agn2gnd::Array{Array{Int32,1},1}
    agn2dem::Array{Array{Int32,1},1}

    sale_offer_hyd_ref::Array{Vector{Offer}, 1}

    sale_offer_hyd::Array{Vector{Offer}, 2}
    sale_offer_ter::Array{Vector{Offer}, 3}
    sale_offer_gnd::Array{Vector{Offer}, 3}
    purchase_offer_hyd::Array{Vector{Offer}, 2}

    sale_offer_hyd_num_segments::Vector{Int32}
    sale_offer_ter_num_segments::Vector{Int32}
    sale_offer_gnd_num_segments::Vector{Int32}
    purchase_offer_hyd_num_segments::Vector{Int32}

    sale_offer_hyd_acc::Array{Float64, 3} #dimensao "extra" devido aos segmentos
    purchase_offer_hyd_acc::Array{Float64, 3} #dimensao "extra" devido aos segmentos
    sale_offer_ter_acc::Array{Float64, 4}
    sale_offer_gnd_acc::Array{Float64, 4}

    profit_hyd::Array{Float64, 2}
    profit_ter::Array{Float64, 2}
    profit_gnd::Array{Float64, 2}

    # --------------------
    # STORAGE REGION
    #---------------------
    rId::Vector{Int32}
    rgn2hyd::Array{Array{Int32,1},1}
    rgn2sys::Vector{Int32}

    # --------------------
    # HYDRO
    #---------------------
    hyd2sys::Vector{Int32}
    hyd2gen::Vector{Int32}
    hyd2bus::Vector{Int32}
    hyd2agn::Vector{Int32}
    hyd2rgn::Vector{Int32}

    # mapped static
    hName::Vector{String}
    stationName::Vector{String}
    hCode::Vector{Int32}
    stsCode::Vector{Int32}
    hIsCommit::Vector{Int32}
    hExist::Vector{Int32}
    downstream_turb::Vector{Int32}
    downstream_spill::Vector{Int32}
    upstream_turb::Array{Array{Int32,1},1}
    upstream_spill::Array{Array{Int32,1},1}
    fprodt::Vector{Float64}
    hyd2station::Vector{Int32}
    volini::Array{Float64, 2}
    volmax::Vector{Float64}
    defmax::Vector{Float64}
    defmin::Vector{Float64}
    turb_max::Vector{Float64}
    hPotInst::Vector{Float64}
    conv::Float64
    inflow::Vector{Float64}
    inflowStage::Vector{Float64}
    inflow_forecast::Vector{Float64}
    reservoirs::Vector{Int}
    runoffriver::Vector{Int}
    volmin::Vector{Float64}
    volendday::Array{Float64,3}
    rho::Array{Float64,1}
    rho_region::Array{Float64,2}

    upstream_vol::Array{Float64, 2}

    # virtual reservoir
    vrCode::Vector{Int32}
    hGF::Vector{Float64}
    hGF2::Vector{Float64}
    vr2agn::Vector{Int32}
    vr2rgn::Vector{Int32}
    vr2sys::Vector{Int32}
    VREnergy_inflow::Array{Float64, 2}
    VREnergy_sale::Array{Float64, 2}

    # --------------------
    # THERMAL
    # --------------------
    # mapped static
    tName::Vector{String}
    tType::Vector{Int32}
    tCode::Vector{Int32}
    tExistRegistry::Vector{Int32}

    # relations
    ter2sys::Vector{Int32}
    ter2gen::Vector{Int32}
    ter2bus::Vector{Int32}
    ter2fuel::Vector{Int32}
    ter2agn::Vector{Int32}
    cirFbus::Vector{Int32}
    cir2bus::Vector{Int32}
    bus2sys::Vector{Int32}
    dem2agn::Vector{Int32}

    tGerMax::Vector{Float64}
    tGerMax_orig::Vector{Float64}
    tPot::Vector{Float64}# MW
    tGerMin::Vector{Float64}# MW
    tExist::Vector{Int32}
    tExisting::Vector{Int32}
    consider_commit::Vector{Int32}
    startupCold::Vector{Float64}
    timeIsCold::Vector{Float64}
    startupWarm::Vector{Float64}
    timeIsWarm::Vector{Float64}
    startupHot::Vector{Float64}
    timeIsHot::Vector{Float64}
    startup_old::Bool

    # COMT
    tShutDownCost::Vector{Float64}
    tStartUpCost::Vector{Float64}
    tPotInst::Vector{Float64}
    tCVU::Vector{Float64}
    tRampUpCold::Vector{Float64}
    tRampUpWarm::Vector{Float64}
    tRampDown::Vector{Float64}
    tMinUptime::Vector{Float64}
    tMinDowntime::Vector{Float64}
    tMaxStartUps::Vector{Int32}
    tMaxShutDowns::Vector{Int32}
    dailyCOMT::Array{Int32, 3}

    # fuel
    fName::Vector{String}
    fCost::Vector{Float64}
    fuelconsumptionrate::Vector{Float64}
    fcs2fls::Vector{Int}
    fcs2ter::Vector{Int}
    fcsCost::Array{Float64,2}
    fcsSegment::Array{Float64,2}
    fcs_nsegments::Array{Int32,1}

    # --------------------
    # GND
    #---------------------
    gName::Vector{String}
    gCode::Vector{Int32}
    gStationsName::Vector{String}
    gExist::Vector{Int32}
    gFatOp::Vector{Float64}
    gOMCost::Vector{Float64}
    gPenalty::Vector{Float64}
    rPotInst::Vector{Float64}
    rBusName::Vector{String}
    rStation::Vector{String}
    Stations::Vector{String}
    ren2stat::Vector{Int}
    gnd2gen::Vector{Int32}
    gnd2bus::Vector{Int32}
    gnd2sys::Vector{Int32}
    gnd2agn::Vector{Int32}

    gGerHourly::Vector{Float64}
    rengen::Array{Float64,3}

    # ---------------
    # Buses
    # ---------------

    # mapped static
    bName::Vector{String}

    # ---------------
    # LOADS
    # ---------------

    # relations
    dem2bus::Vector{Int32}
    dem2sys::Vector{Int32}
    sys2dem::Array{Array{Int32,1},1}
    bus2dem::Vector{Int32}
    dsg2sys::Vector{Int32}
    dsg2dem::Vector{Int32}
    lod2bus::Vector{Int32}
    lod2dem::Vector{Int32}
    demandHour::Vector{Float64}
    demand::Array{Float64,2}
    netdemand::Array{Float64,3}

    #network

    loadweight::Array{Float64,2}
    loadweighttemp::Array{Float64,1}
    interregional_circuits::Array{Bool,1}
    circuit_x::Array{Float64,1}
    circuit_Rn::Array{Float64,1}
    demandBlockTemp::Array{Float64,1}
    demandBlock::Array{Float64,1}
    demandsegment::Array{Float64,2}
    ilhas::Array{Float64,1}
    Beta::Array{Float64,2}
    sensib_demand::Array{Float64,1}
    


    # Interconnection
    intName::Vector{String}
    intExist::Vector{Int32}
    iCapacityFrom::Vector{Float64}
    iCapacityTo::Vector{Float64}
    iLossFrom::Vector{Float64}
    iLossTo::Vector{Float64}
    intFsys::Vector{Int32}# ->
    int2sys::Vector{Int32}# <-

    #
    # NETWORK
    #
    # ----------------

    # mapped static
    dName::Vector{String}
    dAVId::Vector{String}
    dCode::Vector{Int32}

    # relations
    dclFbus::Vector{Int32}# ->
    dcl2bus::Vector{Int32}# <-

    # mapped dynamic
    dExist::Vector{Int32}
    dCapacityFrom::Vector{Float64}
    dCapacityTo::Vector{Float64}
    dLossFrom::Vector{Float64}
    dLossTo::Vector{Float64}

    dReactanceFrom::Vector{Float64}
    dReactanceTo::Vector{Float64}
    dNameBusFrom::Vector{String}
    dNameBusTo::Vector{String}
    hBusName::Vector{String}
    tBusName::Vector{String}

    therm2Bus::Vector{Int}
    hyd2Bus::Vector{Int}
    gen2bus::Vector{Int}
    ren2Bus::Vector{Int}

    # ------------------
    # Electrical Area
    # ------------------
    aName::Vector{String}
    aAVId::Vector{String}
    aCode::Vector{Int32}

    bus2area::Vector{Int32}
    area2bus::Vector{Int32}
    areaCstr2area::Vector{Int32}
    genForCstr2hydro::Vector{Int32}
    genForCstr2thermal::Vector{Int32}
    genMinCstr2hydro::Vector{Int32}
    genMinCstr2thermal::Vector{Int32}
    aCstr::Array{Tuple{Int,Int,Float64},1}
    aExpCstr::Array{Tuple{Int,Int,Float64},1}

    # Reserve
    therm2resPup::Vector{Int32}
    resPup2therm::Vector{Int32}
    therm2resPdn::Vector{Int32}
    resPdn2therm::Vector{Int32}
    therm2QresSup::Vector{Int32}
    PresSup2therm::Vector{Int32}
    resSup2therm::Vector{Int32}
    therm2QresSdn::Vector{Int32}
    PresSdn2therm::Vector{Int32}
    resSdn2therm::Vector{Int32}
    therm2resT::Vector{Int32}
    resT2therm::Vector{Int32}
    hyd2resPup::Vector{Int32}
    resPup2hyd::Vector{Int32}
    hyd2resPdn::Vector{Int32}
    resPdn2hyd::Vector{Int32}
    hyd2resSup::Vector{Int32}
    hyd2QresSup::Vector{Int32}
    PresSup2hyd::Vector{Int32}
    hyd2resSdn::Vector{Int32}
    hyd2QresSdn::Vector{Int32}
    PresSdn2hyd::Vector{Int32}
    hyd2resT::Vector{Int32}
    resT2hyd::Vector{Int32}
    ren2resPup::Vector{Int32}
    resPup2ren::Vector{Int32}
    ren2resPdn::Vector{Int32}
    resPdn2ren::Vector{Int32}
    ren2resSup::Vector{Int32}
    resSup2ren::Vector{Int32}
    ren2resSdn::Vector{Int32}

    resSdn2ren::Vector{Int32}
    ren2resT::Vector{Int32}
    resT2ren::Vector{Int32}
    nresp::Int
    nress::Int
    nrest::Int
    has_resPup::Bool
    has_resPdn::Bool
    has_resSup::Bool
    has_resSdn::Bool
    has_resT::Bool
    tup2ter::Vector{Int32}
    tup2hyd::Vector{Int32}
    ter2tup::Vector{Int32}
    hyd2tup::Vector{Int32}

    # ------------------
    # OutPut
    # ------------------
    HGen_output::BidDispatchOutput
    TGen_output::BidDispatchOutput
    RGen_output::BidDispatchOutput
    Vol_output::BidDispatchOutput
    Turb_output::BidDispatchOutput
    Vert_output::BidDispatchOutput
    VertForc_output::BidDispatchOutput
    WaterIn_output::BidDispatchOutput
    WaterOut_output::BidDispatchOutput
    initial_state_commit_output::BidDispatchOutput
    initial_power_output::BidDispatchOutput
    initial_state_time_output::BidDispatchOutput
    pricevr_output::BidDispatchOutput
    preco_spot_dia_output::BidDispatchOutput
    aBudgetsEnergy_output::BidDispatchOutput
    sale_offer_hyd_price_output::BidDispatchOutput
    purchase_offer_hyd_price_output::BidDispatchOutput
    sale_offer_hyd_qtd_output::BidDispatchOutput
    purchase_offer_hyd_qtd_output::BidDispatchOutput
    sale_offer_ter_price_output::BidDispatchOutput
    sale_offer_ter_qtd_output::BidDispatchOutput
    sale_offer_gnd_price_output::BidDispatchOutput
    sale_offer_gnd_qtd_output::BidDispatchOutput
    sale_offer_hyd_acc_output::BidDispatchOutput
    purchase_offer_hyd_acc_output::BidDispatchOutput
    sale_offer_ter_acc_output::BidDispatchOutput
    sale_offer_gnd_acc_output::BidDispatchOutput
    VREnergy_inflow_output::BidDispatchOutput
    VREnergy_sale_output::BidDispatchOutput
    StoredEnergy_output::BidDispatchOutput
    demand_output::BidDispatchOutput

    IntercTo_output::BidDispatchOutput
    IntercFrom_output::BidDispatchOutput

    preco_spot_names::Vector{String}

end

@enum SolverId xpress=1 glpk=2 cbc_clp=3 cplex=4
@enum ConsiderNetwork no=0 inter=1 network=2
@enum ProblemType DA=1 RT=2
@enum GameType Human=1 Computer=2
@enum WaveguideType none=0 default_waveguide=1 input=2
@enum InputFileType no_file=0 optional=1 required=2
@enum StageType daily=0 weekly=1 points=2

@kwdef mutable struct BidDispatchOptions
    # paths
    # -----------
    INPUTPATH::String # ends with "\\"
    PSRCPATH::String
    OUTPUTPATH::String
    IHMPATH::String
    LUAPATH::String = "src\\bid.lua"
    PAUSEPATH::String = "Teste"
    button::Int32
    #pause mode
    day::Int
    week::Int
    year::Int

    network::ConsiderNetwork=network::ConsiderNetwork
    network_offer::ConsiderNetwork=no::ConsiderNetwork
    network_phys::ConsiderNetwork=no::ConsiderNetwork
    useWaveguide::WaveguideType=none::WaveguideType
    useRTFiles::InputFileType=optional::InputFileType
    UsePurchaseVRFiles::Bool=true
    networklosses::Int32 = 0
    solver_id::SolverId=xpress::SolverId
    SolverTimeLimit::Float64 = 0.0
    RelativeGap::Float64 = 0.0
    SlackRamp::Bool = false
    waveguide_pen_weight::Float64
    spill_pen_weight::Float64
    linear_forced_spillage::Bool

    gametype::GameType=Human::GameType
    PhysOper::Bool=false
    penwaveguide::Int32=0
    SimulOpt::Bool=false # simultaneous optimization = single optimization problem for offer evaluation and physical operation
    DayAhead::Bool=true
    grafInflow::Bool=false
    WizardSegments::Int32=1

    # commitment
    commitment::Bool = true
    ramp::Bool = true
    maxt::Bool = false
    minupdntime::Bool = true
    maxupdntime::Bool = true

    logprint::Bool = true
    hydroBlocks::Int32 = 1
    inicondgraf::Bool = false
    stage::Int32 = 1
    outBin::Bool = false
    termingen::Bool = false
    thermalfile::Bool = false
    
    #network
    respresentationnetwork::Int32 = 0

    initialcondgraf::Bool = true
    stageType::Int32 = 0
    offset::Int = 0

    firstYear ::Int
    firstWeek ::Int
    FirstDay ::Int

    firstYearSDDP :: Int
    firstStageSDDP :: Int
end

@kwdef mutable struct BidDispatchPointers

    # basic
    study_ptr   ::PSRClassesPtr
    iosddp   ::PSRClassesPtr

    # timecontroller
    timec_ptr ::PSRClassesPtr
    timec_shortterm ::PSRClassesPtr

    # configuration
    config_ptr ::PSRClassesPtr
    iexec ::PSRClassesPtr
    ihourly ::PSRClassesPtr

    #Scenarios
    gnd_ptr::PSRClassesPtr

    # inflow
    inflow::PSRClassesPtr

    # demand
    demand::PSRClassesPtr

    # Fcf
    fcf::PSRClassesPtr

    # lists of elements
    agn_lst::PSRClassesPtr
    vr_lst ::PSRClassesPtr
    rgn_lst ::PSRClassesPtr
    sys_lst::PSRClassesPtr
    ter_lst::PSRClassesPtr
    hyd_lst::PSRClassesPtr
    gnd_lst::PSRClassesPtr
    GndStations_lst::PSRClassesPtr
    sts_lst::PSRClassesPtr
    dem_lst::PSRClassesPtr
    dsg_lst::PSRClassesPtr
    fls_lst::PSRClassesPtr
    fcs_lst::PSRClassesPtr

    # list of network
    dcl_lst::PSRClassesPtr
    cir_lst::PSRClassesPtr
    int_lst::PSRClassesPtr
    bus_lst::PSRClassesPtr
    gen_lst::PSRClassesPtr
    lod_lst::PSRClassesPtr
    

    # list of constraints
    isc_lst::PSRClassesPtr
    csc_lst::PSRClassesPtr
    are_lst::PSRClassesPtr
    grc_lst::PSRClassesPtr
    ggc_lst::PSRClassesPtr

    # mappings
    agn_map_dyn::PSRClassesPtr
    vr_map_dyn ::PSRClassesPtr
    rgn_map_dyn ::PSRClassesPtr
    hyd_map_dyn::PSRClassesPtr
    ter_map_dyn::PSRClassesPtr
    fls_map_dyn::PSRClassesPtr
    gnd_map_dyn::PSRClassesPtr
    gnd_map_dyn_hor::PSRClassesPtr
    int_map_dyn::PSRClassesPtr
    isc_map_dyn::PSRClassesPtr
    int_map_dyn_blk::PSRClassesPtr
    dsg_map_dyn_hor::PSRClassesPtr
    lod_map_dyn_blk::PSRClassesPtr
    dsg_map_dyn_blk::PSRClassesPtr

    # shortterm mapping
    shortterm ::PSRClassesPtr
end

@kwdef mutable struct BidDispatchProblem
    data::BidDispatchData
    n::BidDispatchSizes
    options::BidDispatchOptions
    psrc::BidDispatchPointers
end